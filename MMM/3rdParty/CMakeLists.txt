file(GLOB BOOST_EXTENSION RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/boost/extension/ *.hpp)
#set(BOOST_EXTENSION
#    boost/extension/common.hpp)
add_library(${BOOST_EXTENSION_LIB} SHARED ${BOOST_EXTENSION})
target_link_libraries(${CMAKE_PROJECT_NAME} PUBLIC ${BOOST_EXTENSION_LIB})
