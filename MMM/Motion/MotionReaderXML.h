/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MOTIONREADERXML_H_
#define __MMM_MOTIONREADERXML_H_

#include "MMM/MMMCore.h"

#include <functional>
#include <string>
#include <set>
#include <regex>
#include <thread>
#include <filesystem>
#include <atomic>
#include "XMLTools.h"

#include "MotionRecording.h"

namespace MMM
{

/*! \brief A generic xml reader for motions in the mmm dataformat */
class MMM_IMPORT_EXPORT MotionReaderXML
{

public:
    /*! @param additionalLibPaths Paths to sensor plug-in folders. No need to include the standard folder.
        @param ignoreStandardLibPaths Just use the given additional library Paths.
        @param mmmOnly Only load motions with mmm reference model (mmm.xml) */
    MotionReaderXML(bool convertLegacyMotions = true, bool mmmOnly = false, const std::vector<std::filesystem::path> &additionalLibPaths = std::vector<std::filesystem::path>(), bool loadUnknownSensors = true, bool ignoreStandardLibPaths = false);

    /*! @param sensorFactories The sensor factory plug-ins.
     *  @param mmmOnly Only load motions with mmm reference model (mmm.xml) */
    MotionReaderXML(const std::map<std::string, std::shared_ptr<SensorFactory> > &sensorFactories, bool convertLegacyMotions = true, bool mmmOnly = false, bool loadUnknownSensors = true);

    /*! Load a specific motion from an mmm dataformat xml document.
        @param xml An xml string or a path to an xml document.
        @param motionName The name of the motion to load from the xml document.
        @param xmlIsPath Xml parameter is a path or string.
        @throws XMLFormatException When the mmm data format is not applicable.*/
    MotionPtr loadMotion(const std::string &xml, const std::string &motionName, bool xmlIsPath = true);

    /*! Load all motions from an mmm dataformat xml document.
        @param xml An xml string or a path to an xml document.
        @param xmlIsPath Xml parameter is a path or string.
        @throws XMLFormatException When the mmm data format is not applicable.*/
    MotionRecordingPtr loadMotionRecording(const std::string &xml, bool xmlIsPath = true);

    std::vector<MotionRecordingPtr> loadAllMotionRecordingsFromDirectory(const std::filesystem::path &directoryPath, bool recursive = false, unsigned int threads = 1, const std::string &regex = "", int maxNumber = -1, unsigned int startIndex = 0, bool printLog = false);

    std::vector<MotionRecordingPtr> loadAllMotionRecordingsFromPaths(const std::vector<std::filesystem::path> &paths, unsigned int threads = 1, int maxNumber = -1, bool printLog = false);

    static std::filesystem::path getStandardLibPath();

    void setHandleMissingModelFile(const std::function<void(std::filesystem::path&)> &handleMissingModelFile);

    std::vector<std::filesystem::path> getMotionPathsFromDirectory(const std::filesystem::path &directoryPath, bool recursive = true, const std::string &regex = std::string(), unsigned int startIndex = 0, bool printLog = false);

    static const std::string VERSION;

protected:
    MotionPtr loadMotion(simox::xml::RapidXMLWrapperNodePtr motionNode, const std::string &version, const std::filesystem::path filePath);
    MotionPtr loadMotion(simox::xml::RapidXMLWrapperNodePtr motionNode, const std::filesystem::path &motionFilePath);

    std::map<std::string, std::shared_ptr<SensorFactory> > sensorFactories;

    void error(const std::string &msg);

    static const std::string ERROR_MESSAGE;

private:
    std::vector<simox::xml::RapidXMLWrapperNodePtr> getMotionNodes(simox::xml::RapidXMLWrapperRootNodePtr root, std::string &version);

    void addXMLFile(const std::filesystem::path &filePath, std::vector<std::filesystem::path> &paths, unsigned int &index, const std::regex &regex = std::regex(), unsigned int startIndex = 0, bool printLog = false);

    std::string getName(simox::xml::RapidXMLWrapperNodePtr motionNode);

    bool mmmOnly;
    LegacyMotionConverterPtr converter;
    ModelReaderXMLPtr mr;
    bool loadUnknownSensors;
};

typedef std::shared_ptr<MotionReaderXML> MotionReaderXMLPtr;
}
#endif // __MMM_MOTIONREADERXML_H_

