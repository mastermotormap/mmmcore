/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MOTIONTYPE_H_
#define __MMM_MOTIONTYPE_H_

#include "MMM/Enum.h"

namespace MMM
{
    BETTER_ENUM(MotionType, short, MMM = 1, HUMAN = 2, ROBOT = 3, EXOSKELETON = 10, ENVIRONMENT = 100, OBJECT = 101, UNKNOWN = 111)

    MotionType getMotionTypeFromModel(const std::string &modelName);

}

#endif // __MMM_MOTIONTYPE_H_
