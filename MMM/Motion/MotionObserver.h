/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MOTIONOBSERVER_H_
#define __MMM_MOTIONOBSERVER_H_

#include "MMM/MMMCore.h"

namespace MMM
{

class MotionObserver;
typedef std::shared_ptr<MotionObserver> MotionObserverPtr;

class MMM_IMPORT_EXPORT MotionObserver
{
    friend class Motion;

protected:
    virtual bool isNameChangeAllowed(const std::string &name) = 0;

    virtual void notifyNameChange(MotionPtr motion, const std::string &oldName) = 0;

    virtual void notifyMeasurementShift(MotionPtr motion, float delta) = 0;

    virtual void notifyIntervalChange(MotionPtr motion) = 0;
};

}

#endif // __MMM_UNIQUENAME_H_
