#include "LegacyMotion.h"
#include <SimoxUtility/xml/rapidxml/rapidxml.hpp>
#include <SimoxUtility/algorithm/string.h>
#include <filesystem>
#include "MotionEntries.h"
#include "MotionFrame.h"
#include "MMM/Model/ModelProcessor.h"
#include "MMM/Model/ProcessedModelWrapper.h"
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/rolling_mean.hpp>

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/LU>

namespace MMM
{

LegacyMotion::LegacyMotion (const std::string& name)
:name(name)
{
}

LegacyMotion::LegacyMotion (const LegacyMotion& m):
    motionFrames(m.motionFrames),
    jointNames(m.jointNames),
    name(m.name),
    model(m.model),
    motionFilePath(m.motionFilePath),
    motionFileName(m.motionFileName)
{
}

LegacyMotionPtr LegacyMotion::copy(){
    LegacyMotionPtr m(new LegacyMotion("copy"));
    m->motionFrames.clear();
    for(auto & motionFrame : this->motionFrames){
        MotionFramePtr mfp = motionFrame->copy();
        m->motionFrames.push_back(mfp);
    }

    m->jointNames = this->jointNames;
    m->name = this->name;
    m->model = this->model->clone();

    return m;
}

bool LegacyMotion::addMotionFrame(MotionFramePtr md)
{
	if (!md)
		return false;
	if (jointNames.size()>0 && md->joint.rows()>0)
        if (static_cast<std::size_t>(md->joint.rows()) != jointNames.size())
		{
            MMM_ERROR << "Error: md.joint.rows()=" << md->joint.rows() << ", but jointNames.size()=" << jointNames.size() << std::endl;
			return false;
		}
	motionFrames.push_back(md);
    return true;
}

bool LegacyMotion::removeMotionFrame(size_t frame)
{
    if(frame >= motionFrames.size())
        return false;
    motionFrames.erase(motionFrames.begin()+frame);
    return true;
}

bool LegacyMotion::setJointOrder(const std::vector<std::string> &jointNames)
{
	if (jointNames.size()==0)
		return false;
	this->jointNames = jointNames;
	return true;
}

void LegacyMotion::setComment(const std::string &comment)
{
	CommentEntryPtr c(new CommentEntry());
	c->comments.push_back(comment);
	addEntry("comments",c);
}


void LegacyMotion::addComment(const std::string &comment)
{
    if (!hasEntry("comments"))
        setComment(comment);
    else
    {
        MotionEntryPtr c1 = getEntry("comments");
        CommentEntryPtr c = std::dynamic_pointer_cast<CommentEntry>(c1);
        if (c)
            c->comments.push_back(comment);
    }
}

void LegacyMotion::setModel(ProcessedModelWrapperPtr model)
{
    if (!model->getModelProcessor() && modelProcessor) model->setModelProcessor(modelProcessor);
	this->model = model;
}

ModelPtr LegacyMotion::getModel(bool processedModel)
{
    if (!model) return nullptr;
    return model->getModel(processedModel);
}

const std::filesystem::path& LegacyMotion::getMotionFilePath()
{
    return motionFilePath;
}

const std::filesystem::path& LegacyMotion::getMotionFileName()
{
    return motionFileName;
}

void LegacyMotion::setMotionFilePath(const std::filesystem::path &filepath)
{
    this->motionFilePath = filepath;
}

void LegacyMotion::setMotionFileName(const std::filesystem::path &filename)
{
    this->motionFileName = filename;
}

const std::filesystem::path LegacyMotion::getModelFilePath()
{
    return this->model->getOriginalModelFilePath();
}

ModelProcessorPtr LegacyMotion::getModelProcessor()
{
    if (model) return this->model->getModelProcessor();
    else return this->modelProcessor;
}

void LegacyMotion::setModelProcessor(ModelProcessorPtr modelProcessor) {
    this->modelProcessor = modelProcessor;
    if (model) model->setModelProcessor(modelProcessor);
}

ProcessedModelWrapperPtr LegacyMotion::getProcessedModelWrapper()
{
    return this->model;
}


std::string LegacyMotion::toXML()
{
	std::string tab1 = "\t";
	std::string tab2 = "\t\t";
	std::string tab3 = "\t\t\t";
	std::stringstream res;
    //res << "<? xml version='1.0' ?>" << std::endl;

    res << tab1 << "<Motion name='" << name << "'>" << std::endl;
	std::map<std::string, MotionEntryPtr>::iterator i = motionEntries.begin();
	while (i != motionEntries.end())
	{
		res << i->second->toXML();
		i++;
	}
    if (model && model->getModel(false) && !model->getOriginalModelFileName().empty())
	{
        std::string modelFile = model->getOriginalModelFileName();
        if (std::filesystem::exists(modelFile)) modelFile = std::filesystem::canonical(modelFile);
		if (!motionFilePath.empty())
		{
			// make relative path
            modelFile = std::filesystem::proximate(modelFile, motionFilePath);
		}

        res << tab2 << "<Model>" << std::endl;
        res << tab3 << "<File>" << modelFile << "</File>" << std::endl;
        res << tab2 << "</Model>" << std::endl;
	}
    if (model && model->getModelProcessor())
	{
        res << model->getModelProcessor()->toXML(true, 2);
	}

	if (jointNames.size()>0)
	{
        res << tab2 << "<JointOrder>" << std::endl;
		for (const auto & jointName : jointNames)
		{
            res << tab3 << "<Joint name='" << jointName << "'/>" << std::endl;
		}
        res << tab2 << "</JointOrder>" << std::endl;
	}
    res << tab2 << "<MotionFrames>" << std::endl;
	for (auto & motionFrame : motionFrames)
	{
		res << motionFrame->toXML(); 
	}
    res << tab2 << "</MotionFrames>" << std::endl;
    res << tab1 << "</Motion>" << std::endl;
	return res.str();
}

void  LegacyMotion::print()
{
	std::string s = toXML();
    std::cout << s;
}

void LegacyMotion::print(const std::string &filename){
    std::ofstream ofs(filename.c_str());
    std::string s = toXML();
    ofs << "<?xml version='1.0'?>\n";
    ofs << "<MMM>\n";
    ofs << s;
    ofs << "</MMM>\n";
    ofs.close();
}

void LegacyMotion::outputData(const std::string &filename)
{
    std::ofstream ofs(filename.c_str());

    for(size_t i = 0; i < getMotionFrames().size(); i++){
        MotionFramePtr frame = getMotionFrame(i);
        Eigen::Vector3f pos = frame->getRootPos();

        for(int j= 0; j < pos.rows(); j++){
            ofs << pos[j];
            ofs << '\t';
        }

        ofs << '\n';

    }
    ofs.close();
}


std::vector<MotionFramePtr> LegacyMotion::getMotionFrames()
{
	return motionFrames;
}

MotionFramePtr LegacyMotion::getMotionFrame(size_t frame)
{
	if (frame>=motionFrames.size())
	{
        MMM_ERROR << "OutOfBounds error in getMotionFrame:" << frame << std::endl;
		return MotionFramePtr();
	}
	return motionFrames[frame];
}

std::vector<std::string> LegacyMotion::getJointNames()
{
	return jointNames;
}

unsigned int LegacyMotion::getNumFrames()
{
	return (unsigned int)motionFrames.size();
}

std::string LegacyMotion::getComment()
{
	if (!hasEntry("comments"))
		return std::string();
	MotionEntryPtr e = getEntry("comments");
    CommentEntryPtr r = std::dynamic_pointer_cast<CommentEntry>(e);
	if (!r)
		return std::string();
	return r->getCommentString();
}

void LegacyMotion::setName(const std::string& name) {
    this->name = name;
}

std::string LegacyMotion::getName() {
    return name;
}


bool LegacyMotion::hasJoint(const std::string& name)
{
	std::string lc = name;
    simox::alg::to_lower(lc);
	for(const auto & jointName : jointNames)
	{
		if(jointName==name)
		{
			return true;
		}
	}
	return false;
}

void LegacyMotion::calculateVelocities(int method)
{
    std::vector<MotionFramePtr> frames = getMotionFrames();
    Eigen::MatrixXf jointValues = getJointValuesAsMatrix();
    Eigen::MatrixXf jointVelocities = calculateDifferentialQuotient(jointValues,method);

    for (size_t i=0; i < frames.size(); i++)
    {
        frames[i]->joint_vel = jointVelocities.row(i);
    }

    return;
}

void LegacyMotion::calculateAccelerations(int method)
{
    std::vector<MotionFramePtr> frames = getMotionFrames();
    Eigen::MatrixXf jointVelocities = getJointVelocitiesAsMatrix();
    Eigen::MatrixXf jointAcclerations = calculateDifferentialQuotient(jointVelocities,method);

    for (size_t i=0; i < frames.size(); i++)
    {
        frames[i]->joint_acc = jointAcclerations.row(i);
    }

    return;
}

void LegacyMotion::smoothJointValues(jointEnum type, int windowSize)
{
    if (type == LegacyMotion::eValues)
    {
        Eigen::MatrixXf input = getJointValuesAsMatrix();

        for (int i=0; i < input.cols(); i++)
        {
            Eigen::VectorXf cv = input.col(i);
            Eigen::VectorXf smoothed_cv = applyRollingMeanSmoothing(cv, windowSize);

            // now, write back smoothed values
            for(size_t j=0; j < motionFrames.size(); j++)
            {
                motionFrames[j]->joint(i) = smoothed_cv(j);
            }

        }

    }
    else if (type == LegacyMotion::eVelocities)
    {
        Eigen::MatrixXf input = getJointVelocitiesAsMatrix();

        for (int i=0; i < input.cols(); i++)
        {
            Eigen::VectorXf cv = input.col(i);
            Eigen::VectorXf smoothed_cv = applyRollingMeanSmoothing(cv, windowSize);

            // now, write back smoothed values
            for(size_t j=0; j < motionFrames.size(); j++)
            {
                motionFrames[j]->joint_vel(i) = smoothed_cv(j);
            }

        }
    }
    else if (type == LegacyMotion::eAccelerations)
    {
        Eigen::MatrixXf input = getJointAccelerationsAsMatrix();

        for (int i=0; i < input.cols(); i++)
        {
            Eigen::VectorXf cv = input.col(i);
            Eigen::VectorXf smoothed_cv = applyRollingMeanSmoothing(cv, windowSize);

            // now, write back smoothed values
            for(size_t j=0; j < motionFrames.size(); j++)
            {
                motionFrames[j]->joint_acc(i) = smoothed_cv(j);
            }

        }
    }
}



Eigen::MatrixXf LegacyMotion::getJointValuesAsMatrix()
{
    std::vector<MotionFramePtr> frames = getMotionFrames();
    int nFrames = frames.size();
    int nDof = frames[0]->joint.rows();

    Eigen::MatrixXf jointValues = Eigen::MatrixXf::Zero(nFrames, nDof);

    for (int i=0; i < nFrames; i++)
    {
        jointValues.row(i) = frames[i]->joint;
    }

    return jointValues;
}


Eigen::MatrixXf LegacyMotion::getJointVelocitiesAsMatrix()
{
    std::vector<MotionFramePtr> frames = getMotionFrames();
    int nFrames = frames.size();
    int nDof = frames[0]->joint.rows();

    Eigen::MatrixXf jointVelocities = Eigen::MatrixXf::Zero(nFrames,nDof);

    for (int i=0; i < nFrames; i++)
    {
        jointVelocities.row(i) = frames[i]->joint_vel;
    }

    return jointVelocities;
}
Eigen::MatrixXf LegacyMotion::getJointAccelerationsAsMatrix()
{
    std::vector<MotionFramePtr> frames = getMotionFrames();
    int nFrames = frames.size();
    int nDof = frames[0]->joint.rows();

    Eigen::MatrixXf jointAccelerations = Eigen::MatrixXf::Zero(nFrames,nDof);

    for (int i=0; i < nFrames; i++)
    {
        jointAccelerations.row(i) = frames[i]->joint_acc;
    }

    return jointAccelerations;
}






Eigen::MatrixXf LegacyMotion::calculateDifferentialQuotient(const Eigen::MatrixXf &inputMatrix, int method)
{
    Eigen::MatrixXf outputMatrix = Eigen::MatrixXf(inputMatrix.rows(),inputMatrix.cols());

    if (method == 0) // centralized
    {
        Eigen::VectorXf x1 = Eigen::VectorXf(inputMatrix.cols());
        Eigen::VectorXf x2 = Eigen::VectorXf(inputMatrix.cols());


        //iterate over all frames
        for (int i=0; i< inputMatrix.rows(); i++)
        {
            if (i==0)
                x1 = inputMatrix.row(0);
            else
                x1 = inputMatrix.row(i-1);

            if(i==inputMatrix.rows()-1)
                x2 = inputMatrix.row(inputMatrix.rows()-1);
            else
                x2 = inputMatrix.row(i+1);

            outputMatrix.row(i) = (x2-x1)/float(2.0*0.01);
        }

    }

    return outputMatrix;
}

Eigen::VectorXf LegacyMotion::applyRollingMeanSmoothing(Eigen::VectorXf &input, int windowSize)
{
    Eigen::VectorXf output = Eigen::VectorXf::Zero(input.rows());
    boost::accumulators::accumulator_set<float, boost::accumulators::stats<boost::accumulators::tag::rolling_mean> > acc(boost::accumulators::tag::rolling_window::window_size = windowSize);

    for(int i=0; i < input.rows();i++)
    {
        acc(input(i));
        output(i) = boost::accumulators::rolling_mean(acc);
    }

    return output;
}



LegacyMotionPtr LegacyMotion::getSegmentMotion(size_t frame1, size_t frame2){
    if(frame1 > frame2){
        size_t tframe = frame1;
        frame1 = frame2;
        frame2 = tframe;
    }

    LegacyMotionPtr res(new LegacyMotion("na"));
    res = copy();
    std::string cname = getName() + "_segmented";
    res->setName(cname);

    if(frame2 > motionFrames.size()){
        return res;
    }

    std::vector<MotionFramePtr>::iterator begin = motionFrames.begin() + frame1;
    std::vector<MotionFramePtr>::iterator end = motionFrames.begin() + frame2;

    std::vector<MotionFramePtr> newMotionFrames(begin,end);

    res->setMotionFrames(newMotionFrames);

    res->setModel(model);
    return res;
}


LegacyMotionList LegacyMotion::getSegmentMotions(std::vector<int> segmentPoints){
    LegacyMotionList res;

    std::vector<int>::iterator it = segmentPoints.begin();
    segmentPoints.insert(it, 0);
    segmentPoints.push_back(getNumFrames());
    it = segmentPoints.begin();

    for(size_t i = 0; i < segmentPoints.size()-1; i++){
        std::ostringstream ss;
        ss << i;
        std::string segMotionName = getName() + "_" + "Segment" + ss.str();
        LegacyMotionPtr segMotion = getSegmentMotion(segmentPoints[i], segmentPoints[i+1]);
        segMotion->setName(segMotionName);
        segMotion->setModel(model);
        res.push_back(segMotion);
    }

    return res;
}

void LegacyMotion::joinMotion(LegacyMotionPtr next)
{
    MotionFramePtr lastFrame = getMotionFrame(getMotionFrames().size()-1);
    float timestep = lastFrame->timestep;

    Eigen::Matrix4f basePose = lastFrame->getRootPose();

    LegacyMotionPtr nextCopy = next->copy();
    nextCopy->setStartPose(basePose);

    for(std::size_t i = 0; i < nextCopy->getMotionFrames().size(); i++)
    {
        MotionFramePtr frame = nextCopy->getMotionFrame(i)->copy();
        frame->timestep += timestep;
        addMotionFrame(frame);
    }


}

void LegacyMotion::setStartPosition(const Eigen::Vector3f &startPosition)
{
    MotionFramePtr startFrame = getMotionFrame(0);
    Eigen::Vector3f basePos = startFrame->getRootPos();
    Eigen::Vector3f diffPos = basePos - startPosition;

    for(size_t i = 0; i < getMotionFrames().size(); i++){
        MotionFramePtr curframe = getMotionFrame(i);
        basePos = curframe->getRootPos();
        curframe->setRootPos(basePos - diffPos);
    }
}

void LegacyMotion::setStartPose(const Eigen::Matrix4f & startPose)
{
    MotionFramePtr startFrame = getMotionFrame(0);
    Eigen::Matrix4f basePose = startFrame->getRootPose();
    Eigen::Matrix4f diffPose = startPose * basePose.inverse();

    for(size_t i = 0; i < getMotionFrames().size(); i++) {
        MotionFramePtr curframe = getMotionFrame(i);
        basePose = diffPose * curframe->getRootPose();

        curframe->setRootPose(basePose);
    }

}

std::vector<double> LegacyMotion::getTimestamps()
{
    std::vector<double> res;
    for(size_t i = 0; i < getMotionFrames().size(); i++){
        MotionFramePtr curframe = getMotionFrame(i);
        res.push_back(curframe->timestep);
    }

    return res;
}

void LegacyMotion::setStartTime(double start, double step)
{
    double curTime = start;
    for(size_t i = 0; i < getNumFrames(); i++)
    {
        MotionFramePtr curframe = getMotionFrame(i);
        curframe->timestep = curTime;
        curTime += step;
    }
}

}//namespace


