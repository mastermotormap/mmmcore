#include "AbstractMotion.h"
#include <SimoxUtility/xml/rapidxml/rapidxml.hpp>
#include <SimoxUtility/algorithm/string.h>
#include <fstream>

namespace MMM
{

AbstractMotion::AbstractMotion()
= default;


bool AbstractMotion::addEntry( const std::string &name, MotionEntryPtr entry )
{
	std::string lc = name;
    simox::alg::to_lower(lc);
	motionEntries[lc] = entry;
	return true;
}

MotionEntryPtr AbstractMotion::getEntry( const std::string &name )
{
	std::string lc = name;
    simox::alg::to_lower(lc);
	if (!hasEntry(lc))
	{
        MMM_WARNING << "Could not find entry with name:" << name << std::endl;
		return MotionEntryPtr();
	}
	return motionEntries[lc];
}

bool AbstractMotion::hasEntry( const std::string &name ) const
{
	std::string lc = name;
    simox::alg::to_lower(lc);
	if (motionEntries.find(lc) != motionEntries.end())
	{
		return true;
	}
	return false;
}

bool AbstractMotion::removeEntry( const std::string &name )
{
	std::string lc = name;
    simox::alg::to_lower(lc);
	if (hasEntry(lc))
		motionEntries.erase(lc);
	return true;
}

bool AbstractMotion::saveXML(const std::string &filename)
{
    return saveXML(filename, this->toXML());
}

bool AbstractMotion::saveXML(const std::string &filename, const std::string &content)
{
    std::ofstream outFile(filename.c_str());
    if (!outFile.is_open())
        return false;

    outFile << "<?xml version='1.0'?>" << std::endl;
    outFile << "<MMM>" << std::endl;
    outFile << content << std::endl;
    outFile << "</MMM>" << std::endl;
    outFile.close();
    return true;
}

}
