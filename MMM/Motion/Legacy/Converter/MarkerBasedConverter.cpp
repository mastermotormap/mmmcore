#include "MarkerBasedConverter.h"

#include <VirtualRobot/Robot.h>
#include <SimoxUtility/xml/rapidxml/rapidxml.hpp>
#include <SimoxUtility/algorithm/string.h>
#include <fstream>

namespace MMM
{

MarkerBasedConverter::MarkerBasedConverter(const std::string &name)
	: Converter(name)
{

}

bool MarkerBasedConverter::setup(AbstractMotionPtr inputMotion, ModelPtr outputModel)
{
	return setup(ModelPtr(), inputMotion, outputModel);
}

bool MarkerBasedConverter::setup(ModelPtr inputModel, AbstractMotionPtr inputMotion, ModelPtr outputModel)
{
	if (!inputMotion || !outputModel)
		return false;
    inputMarkerMotion = std::dynamic_pointer_cast<MarkerMotion>(inputMotion);
	if (!inputMarkerMotion)
	{
        MMM_ERROR << "Need a MarkerMotion as input..." << std::endl;
		return false;
	}
	// retrieve joint order
    auto rn = outputModel->getRobotNodes();
	std::vector<std::string> joints;
	int nrDOF = 0;
	for (auto & i : rn)
	{
        if (i->isRotationalJoint())
		{
			nrDOF++;
            joints.push_back(i->getName());
        }
        if (i->isTranslationalJoint())
        {
            std::cout << "MarkerBasedConverter.cpp: Prismatic Link [" << i->getName() << " ] " << std::endl;
            // HACK - test if this works...
            /*
            nrDOF++;
            joints.push_back(rn[i]->name);
            */
        }
    }
	if (nrDOF == 0)
	{
        MMM_ERROR << "No joint definitions in outputModel..." << std::endl;
		return false;
	}
	setupJointOrder(joints);

	return Converter::setup(inputModel, inputMotion, outputModel);
}

std::map<std::string, std::string> MarkerBasedConverter::getMarkerMapping() const
{
	return markerMapping;
}

float MarkerBasedConverter::getOptionalFloatByAttributeName(rapidxml::xml_node<char>* xmlNode, const std::string& attributeName, float standardValue)
{
    if (!xmlNode)
    {
        MMM_ERROR << "getFloatByAttributeName got NULL data" << std::endl;
        return standardValue;
    }
    rapidxml::xml_attribute<> *attr = xmlNode->first_attribute(attributeName.c_str(), 0, false);
    if (!attr)
        return standardValue;
    try {
        return simox::alg::to_<float>(attr->value());
    }
    catch (simox::error::SimoxError &e) {
        MMM_ERROR << e.what() << std::endl;
        return 0.0f;
    }
}

bool MarkerBasedConverter::getOptionalBoolByAttributeName(rapidxml::xml_node<char>* xmlNode, const std::string& attributeName, bool standardValue)
{
    if (!xmlNode)
    {
        MMM_ERROR << "getOptionalBoolByAttributeName got NULL data" << std::endl;
        return standardValue;
    }
    rapidxml::xml_attribute<> *attr = xmlNode->first_attribute(attributeName.c_str(), 0, false);
    if (!attr)
        return standardValue;

    try {
        return simox::alg::to_<bool>(attr->value());
    }
    catch (simox::error::SimoxError &e) {
        MMM_ERROR << e.what() << std::endl;
        return 0.0f;
    }
}

}
