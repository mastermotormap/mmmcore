/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Nikolaus Vahrenkamp
* @copyright  2013 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_AbstractMotion_H_
#define __MMM_AbstractMotion_H_

#include "MMM/MMMCore.h"

#include <Eigen/Core>
#include <string> 
#include <vector>
#include <map>

#include "MotionEntries.h"
#include "MotionFrame.h"

namespace MMM
{

class AbstractMotion;

typedef std::shared_ptr<AbstractMotion> AbstractMotionPtr;
typedef std::vector<AbstractMotionPtr> AbstractMotionList;

/*!
	\brief An interface for all motion classes.
*/
class MMM_IMPORT_EXPORT AbstractMotion
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	AbstractMotion();

	/*!
		Returns the number of entries.
	*/
	virtual unsigned int getNumFrames() = 0;

	/*!
		Creates an XML string for this object.
	*/
	virtual std::string toXML() = 0;

	
	/*! 
        Adds a custom motion entry.  
        Silently overwrites existing entries with same name (lowercase).
        \param name The name is used to identify the custom data. Internally it is converted to lowercase.
        \param entry The data to store.
        \return True on success.
	*/
	virtual bool addEntry(const std::string &name, MotionEntryPtr entry);

	//! Remove custom entry 
	virtual bool removeEntry(const std::string &name);

	//! Check if entry with name (lowercase) is present.
	virtual bool hasEntry(const std::string &name) const;

	//! Retrieve custom motion data entries. Name is internally converted to lowercase.
	virtual MotionEntryPtr getEntry(const std::string &name);


    //! Stores MMM content to file
    bool saveXML(const std::string &filename);

    //! Stores MMM content to file
    template <typename M, typename std::enable_if<std::is_base_of<AbstractMotion, M>::value>::type* = nullptr>
    static bool saveXML(const std::vector<std::shared_ptr<M>> &motions, const std::string &filename) {
        std::string content;
        for (unsigned int i=0; i<motions.size(); i++)
        {
            content += motions[i]->toXML();
        }
        return saveXML(filename, content);
    }

protected:
    static bool saveXML(const std::string &filename, const std::string &content);

	//! Custom Entries are stored in this map
	std::map<std::string, MotionEntryPtr> motionEntries;
	
};


}

#endif 
