#include "LegacyMotionReaderXML.h"
#include "MMM/Model/ModelProcessorFactory.h"
#include "MMM/Model/ModelReaderXML.h"
#include "MMM/Model/LoadModelStrategy.h"
#include "MMM/Model/ProcessedModelWrapper.h"
#include "LegacyMotion.h"

#include <algorithm>
#include <fstream>
#include <filesystem>
#include <SimoxUtility/xml/rapidxml/rapidxml.hpp>
#include <SimoxUtility/algorithm/string.h>

namespace MMM
{

    LegacyMotionReaderXML::LegacyMotionReaderXML(bool mmmOnly, bool logging) :
        mmmOnly(mmmOnly),
        logging(logging),
        mr(ModelReaderXMLPtr(new ModelReaderXML()))
    {
        registerMotionXMLTag("comments", this);
        registerMotionXMLTag("model", this);
        registerMotionXMLTag("modelprocessorconfig", this);
        registerMotionXMLTag("jointorder", this);
        registerMotionXMLTag("motionframes", this);
        registerMotionFrameXMLTag("TimeStep", this);
        registerMotionFrameXMLTag("RootPosition", this);
        registerMotionFrameXMLTag("RootPositionVelocity", this);
        registerMotionFrameXMLTag("RootPositionAcceleration", this);
        registerMotionFrameXMLTag("RootRotation", this);
        registerMotionFrameXMLTag("RootRotationVelocity", this);
        registerMotionFrameXMLTag("RootRotationAcceleration", this);
        registerMotionFrameXMLTag("JointPosition", this);
        registerMotionFrameXMLTag("JointVelocity", this);
        registerMotionFrameXMLTag("JointAcceleration", this);
    }

    LegacyMotionPtr LegacyMotionReaderXML::loadMotion(const std::filesystem::path& xmlFile, const std::string& motionName)
    {

        // load file
        std::ifstream in(xmlFile.c_str());

        if (!in.is_open())
        {
            MMM_ERROR << "Could not open XML file:" << xmlFile << std::endl;
            return LegacyMotionPtr();
        }

        std::stringstream buffer;
        buffer << in.rdbuf();
        std::string motionXML(buffer.str());
        in.close();

        LegacyMotionPtr res = createMotionFromString(motionXML, motionName, xmlFile);
        if (!res)
        {
            MMM_ERROR << "Error while parsing file " << xmlFile << std::endl;
        }
        else if (res->getName() == "NO_ERROR") {// quick fix to not produce an error
            return LegacyMotionPtr();
        }

        return res;
    }

    LegacyMotionList LegacyMotionReaderXML::loadAllMotions(const std::filesystem::path& xmlFile)
    {
        std::vector < std::string > motionNames = getMotionNames(xmlFile);

        if (motionNames.size() == 0)
        {
            MMM_ERROR << "no motions in file " << std::endl;
            return std::vector<LegacyMotionPtr>();
        }

        std::vector<LegacyMotionPtr> result;

        int frameCount = -1;
        for (const auto& motionName : motionNames)
        {
            LegacyMotionPtr motion = loadMotion(xmlFile, motionName);
            if (!motion)
            {
                continue;
            }
            if (logging) MMM_INFO << "Loading motion: " << motion->getName() << std::endl;
            if (frameCount != -1 && static_cast<std::size_t>(frameCount) != motion->getNumFrames())
            {
                MMM_ERROR << "Error whole loading motions: the frame count between two motions do not match. All motions need to have the same framecount." << std::endl;
                continue;
            }
            else
            {
                frameCount = motion->getNumFrames();
            }
            result.push_back(motion);
        }

        return result;
    }

    LegacyMotionList LegacyMotionReaderXML::loadAllMotionsFromString(const std::string& xmlDataString)
    {
        std::vector < std::string > motionNames = getMotionNamesFromXMLString(xmlDataString);

        if (motionNames.size() == 0)
        {
            MMM_ERROR << "no motions in file " << std::endl;
            return std::vector<LegacyMotionPtr>();
        }

        std::vector<LegacyMotionPtr> result;

        int frameCount = -1;
        for (const auto& motionName : motionNames)
        {
            LegacyMotionPtr motion = createMotionFromString(xmlDataString, motionName);
            if (!motion)
            {
                continue;
            }
            if (logging) MMM_INFO << "Loading motion: " << motion->getName() << std::endl;
            if (frameCount != -1 && static_cast<std::size_t>(frameCount) != motion->getNumFrames())
            {
                MMM_ERROR << "Error whole loading motions: the frame count between two motions do not match. All motions need to have the same framecount." << std::endl;
                continue;
            }
            else
            {
                frameCount = motion->getNumFrames();
            }
            result.push_back(motion);
        }

        return result;
    }

    std::vector<std::string> LegacyMotionReaderXML::getMotionNamesFromXMLString(std::string motionXML) const
    {
        std::vector<std::string> res;
        try
        {
            rapidxml::xml_document<char> doc;   // character type defaults to char
            char* y = doc.allocate_string(motionXML.c_str());
            doc.parse<0>(y);                    // 0 means default parse flags

            rapidxml::xml_node<>* node = doc.first_node("MMM", 0, false);
            if (node)
            {
                node = node->first_node("motion", 0, false);
            }
            else
            {
                MMM_ERROR << "Could not find MMM node" << std::endl;
            }

            while (node)
            {
                rapidxml::xml_attribute<>* attr = node->first_attribute("name");
                if (attr)
                {
                    res.push_back(attr->value());
                }
                else
                {
                    MMM_WARNING << "Found Motion without name-attribute - skipping" << std::endl;
                }
                node = node->next_sibling("motion", 0, false);
            }
        }
        catch (rapidxml::parse_error& e)
        {

            MMM_ERROR << "Could not parse data in XML definition" << std::endl
                      << "Error message:" << e.what() << std::endl
                      << "Position: " << std::endl << e.where<char>() << std::endl;
            return res;
        }
        catch (std::exception& e)
        {

            MMM_ERROR << "Error while parsing XML definition" << std::endl
                      << "Error code:" << e.what() << std::endl;
            return res;
        }
        catch (...)
        {

            MMM_ERROR << "Error while parsing XML definition" << std::endl;
            return res;
        }
        return res;

    }

    std::vector<std::string> LegacyMotionReaderXML::getMotionNames(const std::filesystem::path& xmlFile) const
    {
        // load file
        std::ifstream in(xmlFile.c_str());
        std::vector<std::string> res;
        if (!in.is_open())
        {
            MMM_ERROR << "Could not open XML file:" << xmlFile << std::endl;
            return res;
        }

        std::stringstream buffer;
        buffer << in.rdbuf();
        std::string motionXML(buffer.str());
        in.close();
        res = getMotionNamesFromXMLString(motionXML);


        return res;
    }


    LegacyMotionPtr LegacyMotionReaderXML::createMotionFromString(const std::string& xmlString, const std::string& motionName, const std::filesystem::path& motionFilePath)
    {
        try
        {
            rapidxml::xml_document<char> doc;   // character type defaults to char
            char* y = doc.allocate_string(xmlString.c_str());
            doc.parse<0>(y);                    // 0 means default parse flags

            // get <MMM> tag
            rapidxml::xml_node<char>* node = doc.first_node("MMM", 0, false);
            if (node)
            {
                node = node->first_node();
            }
            else
            {
                MMM_ERROR << "Could not find MMM node" << std::endl;
                return LegacyMotionPtr();
            }


            // process <Motion> tags
            while (node)
            {
                std::string nodeName = simox::alg::to_lower(node->name());
                if (nodeName == "motion")
                {
                    rapidxml::xml_attribute<>* attr = node->first_attribute("name");
                    if (motionName.empty() || (attr && attr->value() == motionName))
                    {
                        return loadMotion(node, motionName, motionFilePath);
                    }

                }

                node = node->next_sibling();
            }
            if (!node)
            {
                MMM_ERROR << "Could not find Motion with name " << motionName << ", or no Motion defined" << std::endl;
                return LegacyMotionPtr();
            }
        }
        catch (rapidxml::parse_error& e)
        {
            MMM_ERROR << "Could not parse data in XML definition" << std::endl
                      << "Error message:" << e.what() << std::endl
                      << "Position: " << std::endl << e.where<char>() << std::endl;
        }
        catch (std::exception& e)
        {
            MMM_ERROR << "Error while parsing XML definition" << std::endl
                      << "Error code:" << e.what() << std::endl;
        }
        catch (...)
        {
            MMM_ERROR << "Error while parsing XML definition" << std::endl;
        }

        return LegacyMotionPtr();
    }

    LegacyMotionPtr LegacyMotionReaderXML::loadMotion(const rapidxml::xml_node<char>* motionNode, const std::string &motionName, const std::filesystem::path& motionFilePath) {
        try {
            LegacyMotionPtr motion(new LegacyMotion(motionName));
            motion->setMotionFilePath(motionFilePath);
            motion->setMotionFileName(motionFilePath.filename()); //added to be able to track the original motion file

            // now process the motion data set entries
            auto node = motionNode->first_node();
            while (node)
            {
                std::string nodeName = simox::alg::to_lower(node->name());
                if (xmlProcessors.find(nodeName) != xmlProcessors.end())
                {
                    if (!xmlProcessors[nodeName]->processMotionXMLTag(node, motion))
                    {
                        MMM_ERROR << "Error while processing XML tag <" << nodeName << ">" << std::endl;
                        return LegacyMotionPtr();
                    }
                }
                else
                {
                    //MMM_INFO << "Ignoring unknown XML tag <" << nodeName << ">" << std::endl;
                }
                node = node->next_sibling();
            }

            if (mmmOnly && motion->getProcessedModelWrapper() && motion->getProcessedModelWrapper()->getOriginalModelFileName() != "mmm") {
                MMM_INFO << "[MMMOnly] Ignoring motion " + motionFilePath.stem().generic_string()
                            + " as " + motion->getProcessedModelWrapper()->getOriginalModelFileName() + " is not the mmm reference model " << std::endl;
                return LegacyMotionPtr(new LegacyMotion("NO_ERROR")); // quick fix to not produce an error
            }

            if (LoadModelStrategy::isModelProcessed() && motion->getProcessedModelWrapper()) {
                motion->getProcessedModelWrapper()->processModel();
            }

            //motion->calculateVelocities();
            //motion->calculateAccelerations();


            return motion;
        }
        catch (rapidxml::parse_error& e)
        {
            MMM_ERROR << "Could not parse data in XML definition" << std::endl
                      << "Error message:" << e.what() << std::endl
                      << "Position: " << std::endl << e.where<char>() << std::endl;
        }
        catch (std::exception& e)
        {
            MMM_ERROR << "Error while parsing XML definition" << std::endl
                      << "Error code:" << e.what() << std::endl;
        }
        catch (...)
        {
            MMM_ERROR << "Error while parsing XML definition" << std::endl;
        }

        return LegacyMotionPtr();

    }

    bool LegacyMotionReaderXML::processMotionXMLTag(rapidxml::xml_node<char>* tag, LegacyMotionPtr motion)
    {
        if (!tag)
        {
            return false;
        }
        std::string name = simox::alg::to_lower(tag->name());
        if (name == "comments")
        {
            return processCommentsTag(tag, motion);
        }
        if (name == "jointorder")
        {
            return processJointOrderTag(tag, motion);
        }
        if (name == "model")
        {
            return processModelTag(tag, motion);
        }
        if (name == "modelprocessorconfig")
        {
            return processModelProcessorConfigTag(tag, motion);
        }

        if (name == "motionframes")
        {
            return processMotionFramesTag(tag, motion);
        }

        MMM_ERROR << "Unknown XML tag:" << tag->name() << std::endl;
        return false;
    }


    bool LegacyMotionReaderXML::processMotionFrameXMLTag(rapidxml::xml_node<char>* tag, MotionFramePtr motionFrame)
    {
        if (!tag || !motionFrame)
        {
            return false;
        }
        bool res = false;
        std::string name = simox::alg::to_lower(tag->name());

        try {
            if (name == "rootposition")
            {
                res = motionFrame->setRootPos(simox::alg::to_eigen_vec<float, 3>(tag->value()));
            }
            else if (name == "rootpositionvelocity")
            {
                res = motionFrame->setRootPosVel(simox::alg::to_eigen_vec<float, 3>(tag->value()));
            }
            else if (name == "rootpositionacceleration")
            {
                res = motionFrame->setRootPosAcc(simox::alg::to_eigen_vec<float, 3>(tag->value()));
            }
            else if (name == "rootrotation")
            {
                res = motionFrame->setRootRot(simox::alg::to_eigen_vec<float, 3>(tag->value()));
            }
            else if (name == "rootrotationvelocity")
            {
                res = motionFrame->setRootRotVel(simox::alg::to_eigen_vec<float, 3>(tag->value()));
            }
            else if (name == "rootrotationacceleration")
            {
                res = motionFrame->setRootRotAcc(simox::alg::to_eigen_vec<float, 3>(tag->value()));
            }
            else if (name == "timestep")
            {
                motionFrame->timestep = simox::alg::to_<float>(tag->value());
                res = true;
            }
            else if (name == "jointposition")
            {
                motionFrame->joint = simox::alg::to_eigen_vec<float>(tag->value());
                res = true;
            }
            else if (name == "jointvelocity")
            {
                motionFrame->joint_vel = simox::alg::to_eigen_vec<float>(tag->value());
                res = true;
            }
            else if (name == "jointacceleration")
            {
                motionFrame->joint_acc = simox::alg::to_eigen_vec<float>(tag->value());
                res = true;
            }
            else
            {
                //MMM_INFO << "Ignoring unknown XML motion-data tag:" << tag->name() << std::endl;
                // return true, this is not an error, we just ignore this tag
                return true;
            }
        }
        catch (simox::error::SimoxError &e) {
            MMM_ERROR << e.what() << std::endl;
            return false;
        }
        return res;
    }


    bool LegacyMotionReaderXML::processCommentsTag(rapidxml::xml_node<char>* tag, LegacyMotionPtr motion)
    {
        if (!tag)
        {
            MMM_ERROR << "NULL comments tag in XML definition" << std::endl;
            return false;
        }
        if (!motion)
        {
            MMM_ERROR << "NULL motion ?!" << std::endl;
            return false;
        }
        CommentEntryPtr r(new CommentEntry());
        rapidxml::xml_node<>* node = tag->first_node("text", 0, false);
        while (node)
        {
            std::string text = node->value();
            r->comments.push_back(text);
            node = node->next_sibling("text", 0, false);
        }
        motion->addEntry("comments", r);
        return true;
    }


    bool LegacyMotionReaderXML::processModelTag(rapidxml::xml_node<char>* tag, LegacyMotionPtr motion)
    {
        if (!tag)
        {
            MMM_ERROR << "NULL comments tag in XML definition" << std::endl;
            return false;
        }
        if (!motion)
        {
            MMM_ERROR << "NULL motion ?!" << std::endl;
            return false;
        }

        rapidxml::xml_node<>* node = tag->first_node("file", 0, false);
        if (node)
        {
            std::filesystem::path filename = node->value();
            std::filesystem::path motionFilePath = motion->getMotionFilePath();

            if (mmmOnly) {
                std::string modelName = filename.stem();
                if (modelName != "mmm") {
                    motion->setModel(ProcessedModelWrapperPtr(new ProcessedModelWrapper(filename)));
                    return true; // this is not an error
                }
            }

            auto model = mr->loadMotionModel(filename, motionFilePath, nullptr);
            motion->setModel(model);
        }

        // we allow failed instantiations
        return true;
    }

    bool LegacyMotionReaderXML::processModelProcessorConfigTag(rapidxml::xml_node<char>* tag, LegacyMotionPtr motion)
    {
        if (!tag)
        {
            MMM_ERROR << "NULL comments tag in XML definition" << std::endl;
            return false;
        }
        if (!motion)
        {
            MMM_ERROR << "NULL motion ?!" << std::endl;
            return false;
        }

        ModelProcessorPtr mp = ModelProcessorFactory::getModelProcessorFromNode(tag);
        if (!mp)
        {
            MMM_WARNING << "Could not instantiate model processor" << std::endl;
        }
        else
        {
            motion->setModelProcessor(mp);
        }


        // we allow failed instantiations
        return true;
    }



    bool LegacyMotionReaderXML::processJointOrderTag(rapidxml::xml_node<char>* tag, LegacyMotionPtr motion)
    {
        if (!tag)
        {
            MMM_ERROR << "NULL joint_order tag in XML definition" << std::endl;
            return false;
        }
        if (!motion)
        {
            MMM_ERROR << "NULL motion ?!" << std::endl;
            return false;
        }

        std::vector<std::string> names;
        rapidxml::xml_node<>* node = tag->first_node("joint", 0, false);
        while (node)
        {
            rapidxml::xml_attribute<>* attr = node->first_attribute("name", 0, false);
            if (!attr)
            {
                MMM_ERROR << "Joint_order tag: <joint> needs attribute name" << std::endl;
                return false;
            }
            std::string jn = attr->value();
            if (jn.empty())
            {
                MMM_ERROR << "Joint_order tag: <joint> needs not null attribute name" << std::endl;
                return false;
            }
            names.push_back(jn);
            node = node->next_sibling("joint", 0, false);
        }
        motion->setJointOrder(names);
        return true;
    }

    bool LegacyMotionReaderXML::processMotionFramesTag(rapidxml::xml_node<char>* tag, LegacyMotionPtr motion)
    {
        if (!tag)
        {
            MMM_ERROR << "NULL motion tag in XML definition" << std::endl;
            return false;
        }
        if (!motion)
        {
            MMM_ERROR << "NULL motion ?!" << std::endl;
            return false;
        }

        // cycle through MotionFrame tags
        rapidxml::xml_node<>* node = tag->first_node("motionframe", 0, false);
        while (node)
        {
            processMotionFrameTag(node, motion);
            node = node->next_sibling("motionframe", 0, false);
        }

        return true;
    }

    bool LegacyMotionReaderXML::processMotionFrameTag(rapidxml::xml_node<char>* tag, LegacyMotionPtr motion)
    {
        if (!tag)
        {
            MMM_ERROR << "NULL motion data tag in XML definition" << std::endl;
            return false;
        }
        if (!motion)
        {
            MMM_ERROR << "NULL motion ?!" << std::endl;
            return false;
        }

        //  if (motion->getJointNames().size()==0)
        //  {
        //      MMM_ERROR << "No Joint name data in motion file" << std::endl;
        //      return false;
        //  }
        MotionFramePtr motionFrame(new MotionFrame(motion->getJointNames().size()));

        rapidxml::xml_node<>* node = tag->first_node();
        while (node)
        {
            std::string nodeName = simox::alg::to_lower(node->name());

            if (xmlProcessorsMotionFrame.find(nodeName) != xmlProcessorsMotionFrame.end())
            {
                if (!xmlProcessorsMotionFrame[nodeName]->processMotionFrameXMLTag(node, motionFrame))
                {
                    MMM_ERROR << "Error while processing XML tag <" << nodeName << ">" << std::endl;
                    return false;
                }
            }
            else
            {
                //MMM_INFO << "Ignoring unknown motion-data XML tag:" << nodeName << std::endl;
                // no error: we ignore unknown tags
                //return false;
            }

            node = node->next_sibling();
        }
        return motion->addMotionFrame(motionFrame);
    }


    bool LegacyMotionReaderXML::registerMotionXMLTag(const std::string& xmlTag, XMLMotionTagProcessor* processor)
    {
        // need to store pointers, since we call this method from the constructor (no shared_ptrs available)!
        std::string lc = simox::alg::to_lower(xmlTag.c_str());
        simox::alg::to_lower(lc);
        xmlProcessors[lc] = processor;
        return true;
    }

    bool LegacyMotionReaderXML::registerMotionXMLTag(const std::string& xmlTag, XMLMotionTagProcessorPtr processor)
    {
        processorList.push_back(processor); // just storing pointer to avoid deletion
        return registerMotionXMLTag(xmlTag, processor.get());
    }

    bool LegacyMotionReaderXML::registerMotionFrameXMLTag(const std::string& xmlTag, XMLMotionFrameTagProcessor* processor)
    {
        // need to store pointers, since we call this method from the constructor (no shared_ptrs available)!
        std::string lc = simox::alg::to_lower(xmlTag.c_str());
        simox::alg::to_lower(lc);
        xmlProcessorsMotionFrame[lc] = processor;
        return true;
    }

    bool LegacyMotionReaderXML::registerMotionFrameXMLTag(const std::string& xmlTag, XMLMotionFrameTagProcessorPtr processor)
    {
        processorListMotionFrame.push_back(processor); // just storing pointer to avoid deletion
        return registerMotionFrameXMLTag(xmlTag, processor.get());
    }

    bool LegacyMotionReaderXML::isModelLoaded() {
        return LoadModelStrategy::isModelLoaded();
    }

    void LegacyMotionReaderXML::setHandleMissingModelFile(const std::function<void(std::filesystem::path&)> &handleMissingModelFile) {
        mr->handleModelFilePath = handleMissingModelFile;
    }


}
