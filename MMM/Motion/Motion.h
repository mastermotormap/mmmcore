﻿/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_Motion_H_
#define __MMM_Motion_H_

#include "MMM/MMMCore.h"
#include "MMM/Model/ProcessedModelWrapper.h"
#include "MMM/Motion/Sensor/Sensor.h"
#include "MotionType.h"
#include "MotionObserver.h"

#include <set>

namespace MMM
{

/*! @brief Stores a recorded motion with a corresponding virtual model and real measurements */
class MMM_IMPORT_EXPORT Motion : public std::enable_shared_from_this<Motion>
{
    friend class MotionReaderXML;

public:
    Motion(const std::string &name, ModelPtr originalModel, ModelProcessorPtr modelProcessor = nullptr, const std::filesystem::path &originFilePath = std::string(), const std::filesystem::path &modelFilePath = std::string(), MotionType motionType = MotionType::UNKNOWN);

    Motion(const std::string &name, ModelPtr originalModel, ModelPtr processedModel, ModelProcessorPtr modelProcessor, const std::filesystem::path &originFilePath = std::string(), const std::filesystem::path &modelFilePath = std::string(), MotionType motionType = MotionType::UNKNOWN);

    Motion(const std::string &name, ProcessedModelWrapperPtr model = ProcessedModelWrapperPtr(new ProcessedModelWrapper()), const std::filesystem::path &originFilePath = std::string(), MotionType motionType = MotionType::UNKNOWN);

    Motion(MotionPtr motion);

    ~Motion() = default;

    /*! Adds all information of this class to the given motion object */
    MotionPtr clone(bool cloneSensors = true, const std::string &newName = std::string()) const;

    void setName(const std::string& name);

    std::string getName();

    /*! Returns origin path when loaded from file. */
    std::filesystem::path getOriginFilePath() const;

    /*! Return the model. If required loads the model from original model file path
        @param processed Indicates weather the processed or the original model should be returned (If no modelProcessor is specified, the originalModel is always returned).
        @return The robot model if no model is specified. */
    ModelPtr getModel(bool processed = true) const;

    ModelPtr getModelThrow(bool processed = true) const;

    ModelPtr getModelCloneWithVisualization(bool processed = true) const;

    ModelPtr getModelCloneWithVisualizationThrow(bool processed = true) const;
    
    /*! Return the ModelProcessor (if specified). */
    ModelProcessorPtr getModelProcessor() const;

    /*! \brief Returns wether the motion contains a model processor */
    bool hasModelProcessor() const;

    //! get a segmented motion with name 'MOTIONNAME_segmented_STARTTIMESTEPf-ENDTIMESTEPf' according to timesteps.
    MotionPtr getSegmentMotion(float startTimestep, float endTimestep, bool changeTimestep = false) const;

    //! get a segmented motion with the given name according to timesteps.
    MotionPtr getSegmentMotion(float startTimestep, float endTimestep, std::string segmentMotionName, bool changeTimestep = false) const;

    //! Returns the sensor with the given name in the sensor data map.
    SensorPtr getSensorByName(const std::string &name) const;

    //! Returns the first sensor with the given sensor type (or nullptr) and cast it to a specific subclass sensor.
    template <typename S, typename std::enable_if<std::is_base_of<Sensor, S>::value>::type* = nullptr>
    std::shared_ptr<S> getSensorByType() const {
        return std::dynamic_pointer_cast<S>(getSensorByType(S::TYPE));
    }

    //! Returns the first sensor with the given sensor type or nullptr.
    SensorPtr getSensorByType(const std::string &type) const;

    //! Returns all sensor with the given sensor type and cast them to the specific subclass sensor. Removes nullptr after cast.
    template <typename S, typename std::enable_if<std::is_base_of<Sensor, S>::value>::type* = nullptr>
    std::vector<std::shared_ptr<S> > getSensorsByType() const {
        std::vector<std::shared_ptr<S> > sensors;
        for (auto sensor : getSensorsByType(S::TYPE)) {
            std::shared_ptr<S> cast_sensor = std::dynamic_pointer_cast<S>(sensor);
            if (cast_sensor) sensors.push_back(cast_sensor);
        }
        return sensors;
    }

    //! Returns all sensor with the given sensor type.
    SensorList getSensorsByType(const std::string &type) const;

    //! Returns true if the motion contains at least one sensor with the given type.
    bool hasSensor(const std::string &type) const;

    //! Returns true if the motion contains any sensor.
    bool hasSensor() const;

    //! Returns a map of the motion's sensor names pointing to their sensor measurement at the given timestep (if present).
    std::map<std::string, SensorMeasurementPtr> getAllMeasurementsForTimeStep(float timestep) const;

    //! Returns the sensor data as map.
    std::map<std::string, SensorPtr> getSensorData() const;

    //! Get Sensors sorted after their xml priority (descending order).
    std::vector<SensorPtr> getPrioritySortedSensorData() const;

    /*! Adds a new sensor to the motion. If present the name of the sensor in the motion matches the unique name of the sensor, else the sensor type is set as name.
        Ascending numbers starting with 2 are added if the motion already contains a sensor matching the sensor type.
        @param sensor The sensor.
        @param delta Timeshift of the sensor compared to the sensors of the motion.
        @throws MMMException if a sensor with the same configuration is already contained or the sensor not matching the model. */
    virtual void addSensor(SensorPtr sensor, float delta = 0.0f, bool checkModel = true);

    //! Returns a sensor in the motion having the same configuration.
    SensorPtr getSensor(SensorPtr sensor) const;

    //! Joins two motions by joining their sensors and returning a corresponding joined Motion.
    static MotionPtr join(MotionPtr motion1, MotionPtr motion2, std::string name = std::string());

    //! Returns the minimum timestep of all sensors' measurements. If no timesteps are found, returns 0.0f.
    float getMinTimestep() const;

    //! Returns the maximum timestep of all sensors' measurements. If no timesteps are found, returns 0.0f.
    float getMaxTimestep() const;

    //! Returns the common minimum timestep of all sensors' measurements. If no timesteps are found, returns 0.0f.
    float getCommonMinTimestep(const std::set<std::string> &ignoreSensorNames = std::set<std::string>()) const;

    //! Returns the common maximum timestep of all sensors' measurements. If no timesteps are found, returns 0.0f.
    float getCommonMaxTimestep(const std::set<std::string> &ignoreSensorNames = std::set<std::string>()) const;

    //! Returns if the sensors in the motion have the same minimum timestep
    bool hasSameMinTimestep() const;

    //! Returns if the sensors in the motion have the same maximum timestep
    bool hasSameMaxTimestep() const;

    //! Synchronizes all possible sensormeasurements on a specific time frequency via linear interpolation. Removes all sensors, that are not linear interpolatable.
    void synchronizeSensorMeasurements(float timeFrequency);

    //! Synchronizes all possible sensormeasurements on a specific time frequency via linear interpolation. Removes all sensors, that are not linear interpolatable.
    void synchronizeSensorMeasurements(float timeFrequency, float startTimestep, float endTimestep);

    //! Returns if the motion is synchronized
    bool isSynchronized() const;

    //! Extend kinematic and model pose sensors containing only a single measurement
    void extendKinematic();

    //! Returns all sensor types contained the motion
    std::set<std::string> getSensorTypes() const;

    std::string getModelName() const;

    std::string getModelFileName() const;

    std::filesystem::path getModelFilePath() const;

    /*! @brief Returns if the motion is based on the mmm reference model decided by the file name (mmm.xml) */
    bool isReferenceModelMotion() const;

    bool processModel(ModelProcessorPtr modelProcessor, ModelPtr optionalModel = nullptr);

    virtual bool eraseSensorIfEqual(Sensor& sensor);

    virtual int eraseSensors(const std::string &type);

    simox::xml::RapidXMLWrapperNodePtr createMotionRoot();

    void appendMotion(simox::xml::RapidXMLWrapperNodePtr root, const std::filesystem::path &path = std::string());

    std::string toXML(const std::filesystem::path &path = std::string(), bool indent = true);

    void saveXML(const std::filesystem::path &path, bool indent = true);

    MotionType getMotionType() const;

    void setMotionType(MotionType type);

    void setMotionType(const std::string &type);

    /*! method to get motion type from model name if not yet specified */
    void setMotionTypeFromModel();
    void setMotionTypeFromModelIfUnknown();

    /*! Compare motion after MotionType, e.g. subjects before objects */
    bool operator <(MotionPtr m) const;

    bool isSubject() const;

    /*! Only shifts the measurements of this motion. Does not notify other members of MotionRecording about shift.  */
    void shiftMeasurements(float delta);

    bool scaleMotion(float targetHeight, bool shiftMotion, ModelPtr optionalModel = nullptr);

    void shiftMotion(const Eigen::Vector3f &positionDelta);

    bool isMirrorSupported();

    bool mirrorMotion(const Eigen::Matrix4f &referencePose, int referenceAxis, bool ignoreNotSupported = false);

    /** @brief Returns model pose or motion capture marker timesteps if available */
    std::vector<float> getTimesteps();
    
    std::vector<std::string> getActuatedJointNames();
    std::map<std::string, std::vector<std::string>> getActuatedJointNamesBySensorName();

    void initializeModel(ModelPtr model, float timestep, bool extend = false, bool update = true);

    void initializeModel(ModelPtr model, float timestep, float delta, bool extend = false, bool update = true);

    Eigen::Matrix4f getRootPose(float timestep, bool extend = false);
    Eigen::VectorXf getJointAngles(float timestep);
    std::map<std::string, Eigen::VectorXf> getJointAnglesBySensorName(float timestep);
    std::map<std::string, float> getJointAngleMap(float timestep);

    bool getReferencePose(Eigen::Matrix4f &referencePose, int &referenceAxis);

    void attach(MotionObserver* obs);

    void detach(MotionObserver* obs);

    simox::xml::RapidXMLWrapperNodePtr getXML(const std::filesystem::path &path = std::string());

protected:
    bool addSensor(std::string name, SensorPtr sensor, float delta);
    bool setProcessedModel(ModelPtr model);

    std::string createSegmentName(float startTimestep, float endTimestep) const;
    MotionType getMotionTypeFromModel() const;
    virtual void setProcessedModelWrapper(ProcessedModelWrapperPtr modelContainer);

    std::string name;
    std::filesystem::path originFilePath;
    ProcessedModelWrapperPtr model;
    MotionType motionType;

    std::map<std::string, SensorPtr> sensorData;

private:

    bool isNameChangeAllowed(const std::string &name) {
        bool allowed = false;
        for (auto obs : motionObservers) {
            allowed |= obs->isNameChangeAllowed(name);
        }
        return allowed;
    }

    void notifyIntervalChange(float prevMinTimestep, float prevMaxTimestep) {
        if (prevMinTimestep != getMinTimestep() || prevMaxTimestep != getMaxTimestep())
            for (auto o : motionObservers)
                o->notifyIntervalChange(shared_from_this());
    }

    std::vector<MotionObserver*> motionObservers;
};

}

#endif
