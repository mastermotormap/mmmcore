#include "MotionSegment.h"

#include "Segmentation.h"
#include "../MotionRecording.h"

#include "../../Exceptions.h"
#include "MMM/Motion/Annotation/Annotation.h"
#include "MMM/Motion/Annotation/AnnotationFactory.h"
#include "MMM/Motion/Segmentation/SegmentationFactory.h"
#include "../Motion.h"
#include "MotionRecordingSegment.h"

#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>

namespace MMM {

MotionRecordingPtr MotionSegment::getMotionRecordingSegment(bool changeTimestep) {
    MotionRecordingPtr segmentedMotions;
    if (auto m = motions.lock())
        for (auto motion : *m) segmentedMotions->addMotion(getSegmentedMotion(motion, changeTimestep));
    auto motionRecordingSegment = createMotionRecordingSegment();
    if (changeTimestep)
        motionRecordingSegment->shiftTimesteps(-getStartTimestep());
    segmentedMotions->setMotionSegment(motionRecordingSegment);
    return segmentedMotions;
}

MotionRecordingPtr MotionSegment::getMotionRecordingTargetSegment(bool changeTimestep) {
    MotionRecordingPtr segmentedMotions;
    if (auto m = motions.lock()) {
        for (auto motion : *m) {
            auto parent = getParentSegmentation();
            if (!parent || parent->isTarget(motion))
                segmentedMotions->addMotion(getSegmentedMotion(motion, changeTimestep));
        }
    }
    auto motionRecordingSegment = createMotionRecordingSegment();
    if (changeTimestep)
        motionRecordingSegment->shiftTimesteps(-getStartTimestep());
    segmentedMotions->setMotionSegment(motionRecordingSegment);
    return segmentedMotions;
}


MotionPtr MotionSegment::getSegmentedMotion(MotionPtr motion, bool changeTimestep) {
    return motion->getSegmentMotion(getStartTimestep(), getEndTimestep(), changeTimestep);
}

MotionPtr MotionSegment::getSegmentedMotionTarget(bool changeTimestep) {
    auto motion = getMotionTarget();
    if (motion) return getSegmentedMotion(motion, changeTimestep);
    else return nullptr;
}

MotionPtr MotionSegment::getMotionTarget() {
    if (auto parent = getParentSegmentation())
        if (auto m = motions.lock())
            return m->getMotion(parent->getMotionTarget());
    return nullptr;
}

MotionRecordingSegmentPtr MotionSegment::createMotionRecordingSegment() {
    MotionRecordingSegmentPtr segment(new MotionRecordingSegment(motions.lock()));
    segment->addAnnotations(shared_from_this());
    segment->addSegmentations(shared_from_this());
    return segment;
}

void MotionSegment::shiftTimesteps(float timestepDelta) {
    if (auto parent = getParentSegmentation()) {
        auto nodeHandler = parent->segments.extract(getStartTimestep());
        nodeHandler.key() = getStartTimestep() + timestepDelta;
        parent->segments.insert(std::move(nodeHandler));
    }

    startTimestep += timestepDelta;
    endTimestep += timestepDelta;
    notifySegmentResized();
    AbstractMotionSegment::shiftTimesteps(timestepDelta);
}

MotionSegment::MotionSegment(float startTimestep, float endTimestep, const std::string &name, MotionRecordingPtr motions) :
    AbstractMotionSegment(name, motions),
    startTimestep(startTimestep),
    endTimestep(endTimestep)
{
    if (endTimestep <= startTimestep) throw Exception::MMMException("End timestep of motion segment has to be bigger than start timestep!");
}

MotionSegment::MotionSegment(simox::xml::RapidXMLWrapperNodePtr node, MotionRecordingPtr motions, MotionSegmentationPtr parent) :
    AbstractMotionSegment(node->attribute_value_or_default(xml::attribute::NAME, ""), motions),
    startTimestep(parent ? node->attribute_value_(simox::xml::attribute::XMLAttribute<float>("start")) : motions->getMinTimestep()),
    endTimestep(parent ? node->attribute_value_(simox::xml::attribute::XMLAttribute<float>("end")) : motions->getMaxTimestep()),
    parentSegmentation(parent)
{
}

float MotionSegment::getStartTimestep() const
{
    return startTimestep;
}

float MotionSegment::getEndTimestep() const
{
    return endTimestep;
}

void MotionSegment::setStartTimestep(float timestep) {
    if (auto parent = getParentSegmentation()) {
        auto nodeHandler = parent->segments.extract(getStartTimestep());
        nodeHandler.key() = timestep;
        parent->segments.insert(std::move(nodeHandler));
    }
    startTimestep = timestep;
    notifySegmentResized();
}

void MotionSegment::setEndTimestep(float timestep) {
    endTimestep = timestep;
    notifySegmentResized();
}


bool MotionSegment::getResizeLeftInterval(float &minTimestep, float &maxTimestep) {
    bool valid = true;
    if (auto parent = getParentSegmentation()) {
        minTimestep = parent->getMinimumTimestep();
        maxTimestep = parent->getMaximumTimestep();
        auto prevSegment = getPrevSegment();
        if (prevSegment) {
            prevSegment->getChildResizeRight(minTimestep);
        }
        else valid = false;
    }
    else {
        minTimestep = getStartTimestep();
        maxTimestep = getEndTimestep();
    }
    getChildResizeLeft(maxTimestep);
    return valid;
}

bool MotionSegment::getResizeRightInterval(float &minTimestep, float &maxTimestep) {
    bool valid = true;
    if (auto parent = getParentSegmentation()) {
        minTimestep = parent->getMinimumTimestep();
        maxTimestep = parent->getMaximumTimestep();
        auto nextSegment = getNextSegment();
        if (nextSegment) {
            nextSegment->getChildResizeLeft(maxTimestep);
        }
        else valid = false;
    }
    else {
        minTimestep = getStartTimestep();
        maxTimestep = getEndTimestep();
    }
    getChildResizeRight(minTimestep);
    return valid;
}


bool MotionSegment::resizeLeft(float timestep, bool checkTimestep, bool setEditor) {
    if (checkTimestep) {
        float minTimestep, maxTimestep;
        if (!getResizeLeftInterval(minTimestep, maxTimestep)) return false;
        timestep = std::min(maxTimestep, std::max(minTimestep, timestep));
    }
    setStartTimestep(timestep);
    resizeChildLeft(timestep, setEditor);
    if (auto parent = getParentSegmentation()) {
        auto prevSegment = getPrevSegment();
        if (prevSegment) {
            prevSegment->setEndTimestep(timestep);
            prevSegment->resizeChildRight(timestep, setEditor);
        }
        if (setEditor)
            parent->setEdited();
    }
    return true;
}

bool MotionSegment::resizeRight(float timestep, bool checkTimestep, bool setEditor) {
    if (checkTimestep) {
        float minTimestep, maxTimestep;
        if (!getResizeRightInterval(minTimestep, maxTimestep)) return false;
        timestep = std::min(maxTimestep, std::max(minTimestep, timestep));
    }
    setEndTimestep(timestep);
    resizeChildRight(timestep, setEditor);
    if (auto parent = getParentSegmentation()) {
        auto nextSegment = getNextSegment();
        if (nextSegment) {
            nextSegment->setStartTimestep(timestep);
            nextSegment->resizeChildLeft(timestep, setEditor);
        }
        if (setEditor)
            parent->setEdited();
    }
    return true;
}

MotionSegmentPtr MotionSegment::getPrevSegment() {
    if (auto parent = getParentSegmentation())
        return parent->getPrevSegment(shared_from_base<MotionSegment>());
    else return nullptr;
}

MotionSegmentPtr MotionSegment::getNextSegment() {
    if (auto parent = getParentSegmentation())
        return parent->getNextSegment(shared_from_base<MotionSegment>());
    else return nullptr;
}

MotionSegmentationPtr MotionSegment::getParentSegmentation() {
    return parentSegmentation.lock();
}

MotionSegmentationPtr MotionSegment::createChildSegmentation(const std::string &childSegmentName) {
    MotionSegmentationPtr childSegmentation = AbstractMotionSegment::createChildSegmentation(childSegmentName);
    if (auto parent = getParentSegmentation()) {
        switch(parent->getMotionLevel()) {
        case MotionLevel::Task:
            childSegmentation->setMotionLevel(MotionLevel::Motion);
            break;
        case MotionLevel::Motion:
            childSegmentation->setMotionLevel(MotionLevel::Action);
            break;
        case MotionLevel::Action:
            childSegmentation->setMotionLevel(MotionLevel::Primitive);
            break;
        default:
            break;
        }
    }
    return childSegmentation;
}


void MotionSegment::getChildResizeRight(float &minTimestep) {
    if (childSegmentations.size() > 0) {
        for (auto childSegmentation : childSegmentations) {
            childSegmentation->segments.rbegin()->second->getChildResizeRight(minTimestep);
        }
    }
    else minTimestep = std::max(minTimestep, getStartTimestep() + MINIMUM_SEGMENT_SIZE);
}

void MotionSegment::getChildResizeLeft(float &maxTimestep) {
    if (childSegmentations.size() > 0) {
        for (auto childSegmentation : childSegmentations) {
            childSegmentation->segments.begin()->second->getChildResizeLeft(maxTimestep);
        }
    }
    else maxTimestep = std::min(maxTimestep, getEndTimestep() - MINIMUM_SEGMENT_SIZE);
}

void MotionSegment::resizeChildLeft(float timestep, bool setEditor) {
    for (auto childSegmentation : childSegmentations) {
        auto segment = childSegmentation->segments.begin()->second;
        segment->setStartTimestep(timestep);
        segment->resizeChildLeft(timestep, setEditor);
        if (setEditor)
            childSegmentation->setEdited();
    }
}

void MotionSegment::resizeChildRight(float timestep, bool setEditor) {
    for (auto childSegmentation : childSegmentations) {
        auto segment = childSegmentation->segments.rbegin()->second;
        segment->setEndTimestep(timestep);
        segment->resizeChildRight(timestep, setEditor);
        if (setEditor)
            childSegmentation->setEdited();
    }
}

MotionSegmentPtr MotionSegment::clone(MotionSegmentationPtr parentSegmentation, bool deepCopy) {
    return clone(parentSegmentation, getStartTimestep(), getEndTimestep(), deepCopy);
}

MotionSegmentPtr MotionSegment::clone(MotionSegmentationPtr parentSegmentation, float startTimestep, float endTimestep, bool deepCopy,  bool copyAnnotations) {
    MotionSegmentPtr motionSegment(new MotionSegment(startTimestep > getStartTimestep() ? startTimestep : getStartTimestep(),
                                                     endTimestep < getEndTimestep() ? endTimestep : getEndTimestep(), name, motions.lock()));
    if ((startTimestep <= getStartTimestep() && endTimestep >= getEndTimestep()) || copyAnnotations)
        motionSegment->addAnnotations(shared_from_this());
    if (deepCopy)
        motionSegment->addSegmentations(shared_from_this(), startTimestep, endTimestep, deepCopy, copyAnnotations);
    motionSegment->parentSegmentation = parentSegmentation;
    motionSegment->motions = motions;
    motionSegment->name = name;
    return motionSegment;
}

bool MotionSegment::merge(MotionSegmentPtr segment, bool appendChildSegmentations) {
    if (segment->getEndTimestep() == getStartTimestep()) {
        setStartTimestep(segment->getStartTimestep());
    }
    else if (segment->getStartTimestep() == getEndTimestep()) {
        setEndTimestep(segment->getEndTimestep());
    }
    else {
        MMM_ERROR << "Cannot merge segment!" << std::endl;
        return false;
    }

    if (appendChildSegmentations) {
        for (auto segmentation : segment->childSegmentations) {
            auto seg = getSegmentationByConfiguration(segmentation);
            if (seg) {
                if (!seg->overlaps(segmentation)) {
                    for (auto segment : segmentation->segments) {
                        seg->addSegment(segment.second, false);
                    }
                }
                else {
                    MMM_ERROR << "Segmentation with same configuration and overlapping timesteps already available in segment" << std::endl;
                }
            }
            else {
                segmentation->parent = this->shared_from_this();
                childSegmentations.push_back(segmentation);
            }
        }
    }
    for (auto segmentation : childSegmentations) {
        if (segmentation->getFirstSegment()->getStartTimestep() != segmentation->getMinimumTimestep()) {
            segmentation->addSegment(segmentation->getMinimumTimestep(), segmentation->getFirstSegment()->getStartTimestep());
        }
        if (segmentation->getLastSegment()->getEndTimestep() != segmentation->getMaximumTimestep()) {
            segmentation->addSegment(segmentation->getLastSegment()->getEndTimestep(), segmentation->getMaximumTimestep());
        }
    }
    return true;
}

}
