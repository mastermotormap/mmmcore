#include "AbstractMotionSegment.h"

#include "Segmentation.h"
#include "../MotionRecording.h"

#include "../../Exceptions.h"
#include "MMM/Motion/Annotation/Annotation.h"
#include "MMM/Motion/Annotation/AnnotationFactory.h"
#include "MMM/Motion/Segmentation/SegmentationFactory.h"
#include "../Motion.h"
#include "MotionSegment.h"

#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>

namespace MMM {

float AbstractMotionSegment::MINIMUM_SEGMENT_SIZE = 0.01f;

AbstractMotionSegment::AbstractMotionSegment(const std::string &name, MotionRecordingPtr motions) :
    name(name),
    motions(motions)
{
}

float AbstractMotionSegment::getLength() const {
    return getEndTimestep()  - getStartTimestep();
}

void AbstractMotionSegment::addAnnotation(MotionAnnotationPtr annotation) {
    annotations.push_back(annotation);
}

bool AbstractMotionSegment::addSegmentation(MotionSegmentationPtr segmentation) {
    if (!containsSegmentationByConfiguration(segmentation)) {
        segmentation->parent = this->shared_from_this();
        childSegmentations.push_back(segmentation);
        return true;
    }
    else {
        MMM_ERROR << "Segmentation with same configuration already available in segment" << std::endl;
        return false;
    }
}

MotionSegmentationPtr AbstractMotionSegment::getSegmentationByConfiguration(MotionSegmentationPtr segmentation) {
    for (auto child : childSegmentations) {
        if (child->equals(segmentation)) {
            return child;
        }
    }
    return nullptr;
}

bool AbstractMotionSegment::containsSegmentationByConfiguration(MotionSegmentationPtr segmentation) {
    return getSegmentationByConfiguration(segmentation) != nullptr;
}

MotionAnnotationList AbstractMotionSegment::getAllAnnotations() const {
    return annotations;
}

MotionAnnotationList AbstractMotionSegment::getAnnotations(const std::string &type, const std::string &style) const {
    MotionAnnotationList annotations;
    for (auto annotation : this->annotations) {
        if (annotation->has(type, style))
            annotations.push_back(annotation);
    }
    return annotations;
}

bool AbstractMotionSegment::removeAnnotations(const std::string &type, const std::string &style) {
    bool removed = false;
    for (auto it = annotations.begin(); it != annotations.end();) {
        if (type.empty() || type == (*it)->getType()) {
            if (style.empty() || style == (*it)->getStyle()) {
                it = annotations.erase(it);
                removed = true;
                continue;
            }
        }
         it++;
    }
    return removed;
}

bool AbstractMotionSegment::removeSegmentation(MotionSegmentationPtr segmentation) {
    for (auto it = childSegmentations.begin(); it != childSegmentations.end(); it++) {
        if (*it == segmentation) {
            childSegmentations.erase(it);
            return true;
        }
    }
    return false;
}

void AbstractMotionSegment::shiftTimesteps(float timestepDelta) {
    for (auto segmentation : childSegmentations)
        segmentation->shiftTimesteps(timestepDelta);
}


/*MotionSegmentPtr MotionSegment::getSegmentedSegment(float startTimestep, float endTimestep, MotionSegmentationPtr newParent) {
    if (startTimestep < getEndTimestep()) {
        MotionSegmentPtr clonedSegment(new MotionSegment(this->startTimestep, this->endTimestep, name));
        clonedSegment->parentSegmentation = newParent;
        if (startTimestep > getStartTimestep()) {
            clonedSegment->startTimestep = startTimestep;
            // TODO
        }
        if (endTimestep < getEndTimestep()) {
            clonedSegment->endTimestep = endTimestep;
            // TODO
        }
        for (auto segment : childSegmentations) {

        }
        return clonedSegment;
    }
    else
        return nullptr;
}
*/

std::string AbstractMotionSegment::getAnnotationAsXML(const std::string &nodeName) {
    auto node = simox::xml::RapidXMLWrapperRootNode::createRootNode(nodeName);
    for (auto annotation : annotations)
        annotation->appendAnnotationXML(node);
    return node->print();
}

bool AbstractMotionSegment::overlaps(float startTimestep, float endTimestep) {
    return overlaps(getStartTimestep(), getEndTimestep(), startTimestep, endTimestep);
}

bool AbstractMotionSegment::overlaps(AbstractMotionSegmentPtr other) {
    return overlaps(other->getStartTimestep(), other->getEndTimestep());
}

std::vector<std::vector<MotionSegmentationPtr> > AbstractMotionSegment::collectSegmentations() {
    std::vector<std::vector<MotionSegmentationPtr>> collected = collectSegmentationsReverse();
    std::reverse(collected.begin(), collected.end());
    return collected;
}

std::vector<std::vector<MotionSegmentationPtr>> AbstractMotionSegment::collectSegmentationsReverse() {
    std::vector<std::vector<MotionSegmentationPtr> > collectedSegmentations;
    auto childSegmentations = getSegmentations();
    for (auto it = childSegmentations.rbegin(); it != childSegmentations.rend(); it++) {
        std::vector<std::vector<MotionSegmentationPtr> > collectedSegmentations_sub;
        for (const auto &segment : (*it)->getSegments()) {
            auto collectedChildSegmentations = segment.second->collectSegmentationsReverse();
            // Merge if possible
            merge(collectedSegmentations_sub, collectedChildSegmentations);
        }
        collectedSegmentations_sub.push_back({*it});
        merge(collectedSegmentations, collectedSegmentations_sub);
    }
    return collectedSegmentations;
}

MotionRecordingPtr AbstractMotionSegment::getMotionRecording() {
    return motions.lock();
}

void AbstractMotionSegment::addSegmentObserver(std::weak_ptr<MotionSegmentObserver> observer) {
    observers.push_back(observer);
}

bool AbstractMotionSegment::removeSegmentObserver(std::weak_ptr<MotionSegmentObserver> observer) {
    for (auto it = observers.begin(); it != observers.end(); it++) {
        if (!it->owner_before(observer) && !observer.owner_before(*it) ) {
            observers.erase(it);
            return true;
        }
    }
    return false;
}

bool AbstractMotionSegment::operator <(AbstractMotionSegmentPtr segment) const {
    if (getStartTimestep() == segment->getStartTimestep()) {
        return getEndTimestep() < segment->getEndTimestep();
    }
    else return getStartTimestep() < segment->getStartTimestep();
}

bool AbstractMotionSegment::overlaps(float a1, float a2, float b1, float b2) {
    return a1 < b2 && b1 < a2;
}

void AbstractMotionSegment::addAnnotations(AbstractMotionSegmentPtr fromSegment) {
    for (auto annotation : fromSegment->annotations)
        addAnnotation(annotation->clone());
}

void AbstractMotionSegment::addSegmentations(AbstractMotionSegmentPtr fromSegment, bool deepCopy, bool copyAnnotations) {
    return addSegmentations(fromSegment, getStartTimestep(), getEndTimestep(), deepCopy, copyAnnotations);
}

void AbstractMotionSegment::addSegmentations(AbstractMotionSegmentPtr fromSegment, float startTimestep, float endTimestep, bool deepCopy, bool copyAnnotations) {
    for (auto segmentation : fromSegment->childSegmentations) {
        auto clonedSegmentation = segmentation->clone(shared_from_this(), startTimestep, endTimestep, deepCopy, copyAnnotations);
        addSegmentation(clonedSegmentation);
    }
}

void AbstractMotionSegment::appendSegmentXML(simox::xml::RapidXMLWrapperNodePtr node) {
    if (!node) return;
    auto segmentNode = node->append_node("Segment");
    if (!name.empty())
        segmentNode->append_attribute(xml::attribute::NAME, name);
    if (getParentSegmentation()) {
        segmentNode->append_attribute("start", getStartTimestep());
        segmentNode->append_attribute("end", getEndTimestep());
    }
    for (auto annotation : annotations)
        annotation->appendAnnotationXML(segmentNode);
    for (auto segmentation : childSegmentations)
        segmentation->appendSegmentationXML(segmentNode);
}

void AbstractMotionSegment::addAnnotations(simox::xml::RapidXMLWrapperNodePtr node) {
    if (node->has_node("Annotation")) {
        for (auto annotationNode : node->nodes("Annotation")) {
            auto type = annotationNode->attribute_value(xml::attribute::TYPE);
            AnnotationFactoryPtr annotationFactory = AnnotationFactory::fromName(type, NULL);
            // TODO extend with plugins
            if (!annotationFactory) {
                MMM_WARNING << "Annotation with name " << type << " not known. "
                            << "This annotation will be loaded but some functions of the subclass will not be available" << std::endl;
                UnknownMotionAnnotationPtr unknownAnnotation(new UnknownMotionAnnotation(type, annotationNode, annotationNode->attribute_value_or_default("style", "")));
                this->addAnnotation(unknownAnnotation);
            }
            else {
                auto annotation = annotationFactory->createAnnotation(annotationNode);
                if (annotation)
                    this->addAnnotation(annotation);
            }
        }
    }
}

void AbstractMotionSegment::addSegmentations(simox::xml::RapidXMLWrapperNodePtr node) {
    if (node->has_node("Segmentation")) {
        for (auto segmentationNode : node->nodes("Segmentation")) {
            auto type = segmentationNode->attribute_value(xml::attribute::TYPE);
            SegmentationFactoryPtr segmentationFactory = SegmentationFactory::fromName(type, NULL);
            // TODO extend with plugins
            if (segmentationFactory)
                this->addSegmentation(segmentationFactory->createSegmentation(segmentationNode));
            else {
                bool unknownConfiguration = type != MotionSegmentation::MANUAL_SEGMENTATION_TYPE;
                if (unknownConfiguration)
                    MMM_WARNING << "Segmentation with name " << type << " not known. "
                                << "The segmentation will be loaded but some functions of the subclass will not be available" << std::endl;
                MotionSegmentationPtr segmentation(new MotionSegmentation(segmentationNode, unknownConfiguration));
                segmentation->addSegments(segmentationNode, motions.lock());
                this->addSegmentation(segmentation);
            }
        }
    }
}


void AbstractMotionSegment::notifySegmentResized() {
    for (auto it = observers.begin(); it != observers.end();) {
        if (auto o = it->lock()) {
            o->notifySegmentResized();
            it++;
        }
        else {
            MMM_WARNING << "Motion segment observer has not been deleted!" << std::endl;
            it = observers.erase(it);
        }
    }
}


void AbstractMotionSegment::setMotionRecording(MotionRecordingPtr motions) {
    this->motions = motions;
    for (auto segmentation : childSegmentations) {
        segmentation->setMotionRecording(motions);
    }
}

void AbstractMotionSegment::notifyNameChange(MotionPtr motion, const std::string &oldMotionName) {
    for (auto segmentation : childSegmentations) {
        segmentation->notifyNameChange(motion, oldMotionName);
    }
}

MotionSegmentationPtr AbstractMotionSegment::createChildSegmentation(const std::string &childSegmentName) {
    MotionSegmentationPtr childSegmentation(new MotionSegmentation(shared_from_this()));
    childSegmentation->addSegment(getStartTimestep(), getEndTimestep(), childSegmentName);
    addSegmentation(childSegmentation);
    return childSegmentation;
}

MotionSegmentationList AbstractMotionSegment::getSegmentations() {
    return childSegmentations;
}

void AbstractMotionSegment::merge(std::vector<std::vector<MotionSegmentationPtr> > &a, const std::vector<std::vector<MotionSegmentationPtr> > &b) {
    if (a.size() > 0) {
        for (auto it_b = b.begin(); it_b != b.end(); it_b++) {
            bool added = false;
            for (auto it_a = a.begin(); it_a != a.end(); it_a++) {
                if (it_b->size() > 0 && it_a->size() > 0 && it_b->front()->equals(it_a->front())) {
                    // check overlap
                    bool overlaps = false;
                    for (auto a_seg : *it_b) {
                        for (auto b_seg : *it_a) {
                            if (a_seg->overlaps(b_seg)) {
                                overlaps = true;
                            }
                        }
                    }
                    if (!overlaps) {
                        it_a->insert(it_a->end(), it_b->begin(), it_b->end());
                        added = true;
                        break;
                    }
                }
            }
            if (!added) {
                a.push_back(*it_b);
            }
        }
    }
    else a = b;
}

}
