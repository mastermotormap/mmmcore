#include "Segmentation.h"

#include "MotionSegment.h"
#include "../Motion.h"
#include "../XMLTools.h"

#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>
#include <SimoxUtility/algorithm/string.h>
#include <VirtualRobot/Robot.h>
#include "MMM/Exceptions.h"

namespace MMM {

std::string MotionSegmentation::AUTHOR_ID = MotionSegmentation::UNKNOWN_AUTHOR_ID;

MotionLevel MotionSegmentation::DEFAULT_LEVEL = MotionLevel::Unset;

MMM::MotionSegmentation::MotionSegmentation(AbstractMotionSegmentPtr parent, const std::string &type, MotionLevel level, const std::string &description,
                                            const std::string &target, const std::string &subtarget) :
    parent(parent),
    type(type),
    level(level),
    description(description),
    target(target),
    subtarget(subtarget),
    creatorID(AUTHOR_ID),
    lastEditorID(creatorID),
    created(std::time(0)),
    lastEdited(created),
    editors({creatorID})
{
}

MMM::MotionSegmentation::MotionSegmentation(AbstractMotionSegmentPtr parent, MotionPtr target, const std::string &type, MotionLevel level,
                                            const std::string &description, const std::string &subtarget) :
    parent(parent),
    type(type),
    level(level),
    description(description),
    creatorID(AUTHOR_ID),
    lastEditorID(creatorID),
    created(std::time(0)),
    lastEdited(created),
    editors({creatorID})
{
    if (target) {
        this->target = target->getName();
    }
    else throw Exception::MMMException("Motion is nullptr!");
    if (!subtarget.empty()) {
        auto model = target->getModel();
        if (model) {
            if (model->hasRobotNode(subtarget)) {
                this->subtarget = subtarget;
            }
            else throw Exception::MMMException("Subtarget '" + subtarget + "' is not part of model '" + model->getName() + "' in motion '" + this->target + "' Available nodes are: " + simox::alg::join(model->getRobotNodeNames()));
        }
        else throw Exception::MMMException("Subtarget '" + subtarget + "' cannot be verified as model of motion '" + this->target + "' is empty");
    }
}

MotionLevel MMM::MotionSegmentation::getMotionLevel() {
    return level;
}

void MMM::MotionSegmentation::setMotionLevel(MotionLevel level) {
    this->level = level;
}

int MMM::MotionSegmentation::getHierarchyLevel() {
    if (auto parent = getParent()) {
        return parent->getParentSegmentation()->getHierarchyLevel() + 1;
    }
    else return 0;
}

MotionLevel MMM::MotionSegmentation::motionLevelfromString(const std::string &motionLevel) {
    if (motionLevel.empty())
        return DEFAULT_LEVEL;
    try {
        return MotionLevel::_from_string(motionLevel.c_str());
    } catch (std::runtime_error &e) {
        MMM_WARNING << e.what() << " '" << motionLevel  << "'. Set to MotionLevel::" << DEFAULT_LEVEL._to_string() << " instead." << std::endl;
        return DEFAULT_LEVEL;
    }
}

AbstractMotionSegmentPtr MotionSegmentation::getParent() const {
    return parent.lock();
}

MotionSegmentation::MotionSegmentation(simox::xml::RapidXMLWrapperNodePtr node, bool unknownConfiguration) :
    type(node->attribute_value("type")),
    level(motionLevelfromString(node->attribute_value_or_default("level", ""))),
    description(node->attribute_value_or_default("description", "")),
    target(node->attribute_value_or_default("target", "")),
    subtarget(node->attribute_value_or_default("subtarget", "")),
    unknownConfigurationNode(nullptr),
    creatorID(node->attribute_value_or_default("creator", UNKNOWN_AUTHOR_ID)),
    lastEditorID(node->attribute_value_or_default("lasteditor", creatorID)),
    created(simox::alg::to_time_t(node->attribute_value("created"), TIME_FORMAT)),
    lastEdited(node->has_attribute("lastedited") ? simox::alg::to_time_t(node->attribute_value("lastedited"), TIME_FORMAT) : created),
    editors(node->attribute_value_vec_str("Editors"))
{
    if (unknownConfiguration) {
        if (node->has_node(xml::tag::CONFIGURATION))
            unknownConfigurationNode = node->first_node(xml::tag::CONFIGURATION);
    }
}

std::string MotionSegmentation::getType() {
    return type;
}

std::string MotionSegmentation::getDescription() {
    return description;
}

std::string MotionSegmentation::getMotionTarget() {
    if (target.empty()) {
        if (auto parent = getParent()) {
            if (auto parentSegmentation = parent->getParentSegmentation()) {
                return parentSegmentation->getMotionTarget();
            }
        }
    }
    return target;
}

std::string MotionSegmentation::getMotionSubTarget() {
    if (subtarget.empty()) {
        if (auto parent = getParent()) {
            if (auto parentSegmentation = parent->getParentSegmentation()) {
                return parentSegmentation->getMotionSubTarget();
            }
        }
    }
    return subtarget;
}

MotionSegmentPtr MotionSegmentation::addSegment(float minTimestep, float maxTimestep, const std::string &name) {
    if (auto p = getParent()) {
        auto segment = MotionSegmentPtr(new MotionSegment(minTimestep, maxTimestep, name, p->motions.lock()));
        addSegment(segment);
        return segment;
    }
    else return nullptr;
}

bool MotionSegmentation::isTarget(MotionPtr motion) {
    return motion->getName() == getMotionTarget();
}

void MotionSegmentation::shiftTimesteps(float timestepDelta) {
    if (timestepDelta < 0) // if clause to not erase segments from map when shifting
        for (auto it = segments.begin(); it != segments.end(); it++)
            it->second->shiftTimesteps(timestepDelta);
    else
        for (auto it = segments.rbegin(); it != segments.rend(); it++)
            it->second->shiftTimesteps(timestepDelta);
}

/*MotionSegmentationPtr MotionSegmentation::getSegmentedSegmentation(float startTimestep, float endTimestep, MotionSegmentPtr newParent) {
    MotionSegmentationPtr segmentedSegmentation = clone(newParent, false);
    for (const auto &segment : segments) {
        auto newSegment = segment.second->getSegmentedSegment(startTimestep, endTimestep, segmentedSegmentation);
        if (newSegment)
            segmentedSegmentation->addSegment(newSegment, false);
    }
    return segmentedSegmentation;
}*/

void MotionSegmentation::addSegment(MotionSegmentPtr segment, bool check) {
    // Search if overlapping segments
    if (check) {
        auto it = segments.lower_bound(segment->getEndTimestep());
        if (it != segments.begin()) {
            it--;
            if ((*it).second->getEndTimestep() > segment->getStartTimestep()) {
                std::stringstream ss;
                ss << "Error adding segment: Overlapping segments! ";
                ss << "[" << segment->getStartTimestep() << "," << segment->getEndTimestep() << "]";
                ss << " and ";
                ss << "[" << (*it).second->getStartTimestep() << "," << (*it).second->getEndTimestep() << "]";
                throw Exception::MMMException(ss.str());
            }
        }
    }
    segment->parentSegmentation = this->shared_from_this();
    segments.insert({segment->getStartTimestep(), segment});
}

void MotionSegmentation::appendSegmentationXML(simox::xml::RapidXMLWrapperNodePtr node) {
    if (!node) return;
    auto segmentationNode = node->append_node("Segmentation");
    segmentationNode->append_attribute(xml::attribute::TYPE, type);
    // TODO append version, name, tag (?)
    if (level._value != MotionLevel::Unset)
        segmentationNode->append_attribute("level", level._to_string());
    if (!description.empty())
        segmentationNode->append_attribute(xml::attribute::DESCRIPTION, description);
    if (!target.empty())
        segmentationNode->append_attribute("target", target);
    if (!subtarget.empty())
        segmentationNode->append_attribute("subtarget", subtarget);
    if (creatorID != UNKNOWN_AUTHOR_ID)
        segmentationNode->append_attribute("creator", creatorID);
    if (lastEditorID != creatorID)
        segmentationNode->append_attribute("lasteditor", lastEditorID);
    auto created_str = simox::alg::to_string(created, TIME_FORMAT);
    auto lastEdited_str = simox::alg::to_string(lastEdited, TIME_FORMAT);
    segmentationNode->append_attribute("created", created_str);
    if (lastEdited_str != created_str)
        segmentationNode->append_attribute("lastedited", lastEdited_str);
    segmentationNode->append_attribute("editors", editors);

    if (unknownConfigurationNode) {
        segmentationNode->append_node(*unknownConfigurationNode.get());
    }
    else {
        auto configurationNode = segmentationNode->append_node("Configuration");
        appendConfigurationXML(configurationNode);
    }
    for (const auto &segment : segments)
        segment.second->appendSegmentXML(segmentationNode);

}

float MotionSegmentation::getMinimumTimestep() {
    if (auto p = getParent()) {
        return p->getStartTimestep();
    }
    else return -1.0f;
}

float MotionSegmentation::getMaximumTimestep() {
    if (auto p = getParent()) {
        return p->getEndTimestep();
    }
    else return -1.0f;
}

bool MotionSegmentation::split(float timestep, bool copyAnnotations) {
    std::ostringstream errorMessage;
    if (!contains(timestep))
        errorMessage << "Timestep not within segmentation boundaries.";
    else if (segments.find(timestep) != segments.end())
        errorMessage << "Already segmented at timestep.";
    else {
        auto it = getSegmentIterator(timestep);
        if (it != segments.end()) {
            auto leftSegment = it->second->clone(shared_from_this(), it->second->getStartTimestep(), timestep,  true, copyAnnotations);
            auto rightSegment = it->second->clone(shared_from_this(), timestep, it->second->getEndTimestep(), true, copyAnnotations);

            segments.erase(it);
            addSegment(leftSegment, false);
            addSegment(rightSegment, false);
            this->setEdited();
            return true;
        }
    }
    MMM_ERROR << "Cannot split motion segment at timestep " << timestep << ": " << errorMessage.str() << std::endl;
    return false;
}

bool MotionSegmentation::remove(float timestep, bool appendChildSegmentations, bool removeSegmentationIfLast) {
    std::ostringstream errorMessage;
    if (!contains(timestep))
        errorMessage << "Timestep not within segmentation boundaries.";
    else if (segments.size() == 1) {
        if (removeSegmentationIfLast) return remove();
        else errorMessage << "Last segment in segmentation cannot be removed."; // TODO implement removing segmentation
    }
    else {
        auto it = getSegmentIterator(timestep);
        if (it != segments.end()) {
            // Append to right if first element in segmentation OR if timestep nearer to right segment
            auto removedSegment = it->second;
            auto segment = it == segments.begin() || (std::next(it) != segments.end() && (timestep - removedSegment->getStartTimestep() > removedSegment->getEndTimestep() - timestep))
                    ? std::next(it)->second : std::prev(it)->second;
            segments.erase(it);
            segment->merge(removedSegment, appendChildSegmentations);
            this->setEdited();
            return true;
        }
    }
    MMM_ERROR << "Cannot remove motion segment at timestep " << timestep << ": " << errorMessage.str() << std::endl;
    return false;
}

bool MMM::MotionSegmentation::remove() {
    if (auto p = getParent())
        return p->removeSegmentation(shared_from_this());
    else return false;
}

const std::vector<std::string> &MMM::MotionSegmentation::getEditors() {
    return editors;
}

void MMM::MotionSegmentation::updateInterval(float minTimestep, float maxTimestep) {
    auto firstSegment = getFirstSegment();
    if (firstSegment->getStartTimestep() < minTimestep) {
        if (!firstSegment->resizeLeft(minTimestep, true, false)) {
            MMM_ERROR << "Error resizing after decreasing segmentation interval on the left side. Functionality to delete segments is currently not implemented." << std::endl;
        }
    }
    else if (firstSegment->getStartTimestep() > minTimestep) {
        firstSegment->resizeLeft(minTimestep, false, false);
    }

    auto lastSegment = getLastSegment();
    if (lastSegment->getEndTimestep() < maxTimestep) {
        lastSegment->resizeRight(maxTimestep, false, false);
    }
    else if (lastSegment->getEndTimestep() > maxTimestep) {
        if (!lastSegment->resizeRight(maxTimestep, true, false)) {
            MMM_ERROR << "Error resizing after decreasing segmentation interval on the right side. Functionality to delete segments is currently not implemented." << std::endl;
        }
    }
}

MotionSegmentPtr MMM::MotionSegmentation::getFirstSegment() {
    return segments.begin()->second;
}

MotionSegmentPtr MMM::MotionSegmentation::getLastSegment() {
    return segments.rbegin()->second;
}

MotionSegmentPtr MotionSegmentation::getSegment(float timestep) {
    auto it = getSegmentIterator(timestep);
    if (it != segments.end()) return it->second;
    else return nullptr;
}

MotionSegmentPtr MotionSegmentation::getPrevSegment(float timestep, bool allowChildElements) {
    MotionSegmentPtr prevSegment = nullptr;
    auto it = segments.lower_bound(timestep);
    if (it != segments.begin()) {
        auto currentSegmentIt = std::prev(it);
        if (allowChildElements) {
            for (auto childSegmentations : currentSegmentIt->second->getSegmentations()) {
                auto prevChildSegment = childSegmentations->getPrevSegment(timestep, allowChildElements);
                if (!prevSegment || prevChildSegment->getEndTimestep() >= prevSegment->getEndTimestep()) {
                    prevSegment = prevChildSegment;
                }
            }
        }
        if (currentSegmentIt != segments.begin()) {
            auto prevIt = std::prev(currentSegmentIt);
            if (!prevSegment || prevIt->second->getEndTimestep() >= prevSegment->getEndTimestep()) {
                prevSegment = prevIt->second;
            }
        }
    }
    return prevSegment;
}

MotionSegmentPtr MotionSegmentation::getNextSegment(float timestep, bool allowChildElements) {
    MotionSegmentPtr nextSegment = nullptr;
    auto it = segments.upper_bound(timestep);
    if (it == segments.begin()) {
        nextSegment = it->second;
    }
    else {
        if (allowChildElements) {
            auto currentSegment = std::prev(it);
            for (auto childSegmentations : currentSegment->second->getSegmentations()) {
                auto nextChildSegment = childSegmentations->getNextSegment(timestep, allowChildElements);
                if (!nextSegment || nextChildSegment->getStartTimestep() <= nextSegment->getStartTimestep()) {
                    nextSegment = nextChildSegment;
                }
            }
        }
        if (it != segments.end()) {
            if (!nextSegment || it->second->getStartTimestep() <= nextSegment->getStartTimestep()) {
                nextSegment = it->second;
            }
        }
    }
    return nextSegment;
}

MotionSegmentPtr MotionSegmentation::getPrevSegment(MotionSegmentPtr segment) {
    auto it = segments.find(segment->getStartTimestep());
    if (it->second == segment && it != segments.begin())
        return std::prev(it)->second;
    else return nullptr;
}

MotionSegmentPtr MotionSegmentation::getNextSegment(MotionSegmentPtr segment) {
    auto it = segments.find(segment->getStartTimestep());
    auto next_it = std::next(it);
    if (it->second == segment && next_it != segments.end())
        return next_it->second;
    else return nullptr;
}

bool MotionSegmentation::contains(float timestep) {
    return timestep > getMinimumTimestep() && timestep < getMaximumTimestep();
}

void MotionSegmentation::setMotionRecording(MotionRecordingPtr motions) {
    for (const auto &segment : segments) {
        segment.second->setMotionRecording(motions);
    }
}

void MotionSegmentation::notifyNameChange(MotionPtr motion, const std::string &oldMotionName) {
    if (target == oldMotionName)
        target = motion->getName();
    for (const auto &segment : segments) {
        segment.second->notifyNameChange(motion, oldMotionName);
    }
}

void MotionSegmentation::appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr /*configurationNode*/) {
    // Do nothing - handle in sub class if required
}

void MotionSegmentation::appendTo(MotionSegmentationPtr motionSegmentation, AbstractMotionSegmentPtr parent, float startTimestep, float endTimestep, bool deepCopy, bool copyAnnotations) {
    motionSegmentation->type = type;
    motionSegmentation->level = level;
    motionSegmentation->description = description;
    motionSegmentation->target = target;
    motionSegmentation->subtarget = subtarget;
    motionSegmentation->creatorID = creatorID;
    motionSegmentation->lastEditorID = lastEditorID;
    motionSegmentation->created = created;
    motionSegmentation->lastEdited = lastEdited;
    motionSegmentation->editors = editors;
    motionSegmentation->unknownConfigurationNode = unknownConfigurationNode;
    motionSegmentation->parent = parent;
    if (deepCopy) {
        for (const auto &segment : segments) {
            if (segment.second->overlaps(startTimestep, endTimestep))
                motionSegmentation->addSegment(segment.second->clone(motionSegmentation, startTimestep, endTimestep, deepCopy, copyAnnotations), false);
        }
    }
}

std::map<float, MotionSegmentPtr>::const_iterator MotionSegmentation::getSegmentIterator(float timestep) {
    auto it = segments.upper_bound(timestep);
    if (it == segments.begin())
        return segments.end();
    else if (it == segments.end()) {
        auto prev_it = std::prev(it);
        if (prev_it->second->getEndTimestep() < timestep)
            return segments.end();
    }
    return std::prev(it);
}

MotionSegmentation::MotionSegmentation() : level(MotionLevel::Unset)
{
}

std::string MotionSegmentation::getCreatorID() {
    return creatorID;
}

std::string MotionSegmentation::getLastEditorID() {
    return lastEditorID;
}

time_t MotionSegmentation::getCreationTime() {
    return created;
}

time_t MotionSegmentation::getLastEditedTime() {
    return lastEdited;
}

void MotionSegmentation::setEdited(const std::string &editorID) {
    lastEdited = std::time(0);
    lastEditorID = editorID;
    if (!hasEdited(editorID))
        editors.push_back(editorID);
}

bool MotionSegmentation::hasEdited(const std::string &editorID) {
    for (const auto& id : editors) {
        if (id == editorID)
            return true;
    }
    return false;
}

std::vector<MotionSegmentationPtr> MotionSegmentation::createChildSegmentations() {
    std::vector<MotionSegmentationPtr> childSegmentations;
    for (auto segment : segments)
        childSegmentations.push_back(segment.second->createChildSegmentation());
    return childSegmentations;
}

std::map<float, MotionSegmentPtr> MotionSegmentation::getSegments() {
    return segments;
}

void MotionSegmentation::addSegments(simox::xml::RapidXMLWrapperNodePtr segmentationNode, MotionRecordingPtr motions) {
    for (auto segmentNode : segmentationNode->nodes("Segment")) {
        MotionSegmentPtr segment(new MotionSegment(segmentNode, motions, shared_from_this()));
        segment->addAnnotations(segmentNode);
        segment->addSegmentations(segmentNode);
        segments[segment->getStartTimestep()] = segment;
    }
}

bool MotionSegmentation::equals(MotionSegmentationPtr other) {
    if (type == other->type && level == other->level && getMotionTarget() == other->getMotionTarget() && getMotionSubTarget() == other->getMotionSubTarget()
        && creatorID == other->creatorID) {
        return equalsConfiguration(other);
    }
    return false;
}

bool MotionSegmentation::equalsConfiguration(MotionSegmentationPtr /*other*/) {
    // Do in derived segmentations
    return true;
}

bool MotionSegmentation::overlaps(MotionSegmentationPtr other) {
    if (auto p = getParent())
        if (auto p2 = other->getParent())
            return p->overlaps(p2);
    return false;
}

MotionSegmentationPtr MotionSegmentation::clone(AbstractMotionSegmentPtr parent, float startTimestep, float endTimestep, bool deepCopy, bool copyAnnotations) {
    MotionSegmentationPtr clonedSegmentation(new MotionSegmentation());
    appendTo(clonedSegmentation, parent, startTimestep, endTimestep, deepCopy, copyAnnotations);
    return clonedSegmentation;
}

}
