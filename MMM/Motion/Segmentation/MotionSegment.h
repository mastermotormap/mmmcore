/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include <string>

#include "MMM/MMMCore.h"
#include "AbstractMotionSegment.h"

#ifndef __MMM_MOTION_SEGMENT_H_
#define __MMM_MOTION_SEGMENT_H_

namespace MMM
{

class MotionSegment : public AbstractMotionSegment {
    friend class MotionSegmentation;

public:
    MotionSegment(float startTimestep, float endTimestep, const std::string &name, MotionRecordingPtr motions);

    MotionSegment(simox::xml::RapidXMLWrapperNodePtr node, MotionRecordingPtr motions, MotionSegmentationPtr parent);

    float getStartTimestep() const override;

    float getEndTimestep() const override;

    void setStartTimestep(float timestep);

    void setEndTimestep(float timestep);

    void shiftTimesteps(float timestepDelta) override;

    MotionRecordingSegmentPtr createMotionRecordingSegment();

    MotionRecordingPtr getMotionRecordingSegment(bool changeTimestep = false);

    MotionRecordingPtr getMotionRecordingTargetSegment(bool changeTimestep = false);

    MotionPtr getSegmentedMotion(MotionPtr motion, bool changeTimestep = false);

    MotionPtr getSegmentedMotionTarget(bool changeTimestep = false);

    MotionPtr getMotionTarget();

    bool getResizeLeftInterval(float &minTimestep, float &maxTimestep);

    bool getResizeRightInterval(float &minTimestep, float &maxTimestep);

    bool resizeLeft(float timestep, bool checkTimestep = true, bool setEditor = true);

    bool resizeRight(float timestep, bool checkTimestep = true, bool setEditor = true);

    MotionSegmentPtr getPrevSegment();

    MotionSegmentPtr getNextSegment();

    MotionSegmentPtr clone(MotionSegmentationPtr parentSegmentation, bool deepCopy = true);

    MotionSegmentPtr clone(MotionSegmentationPtr parentSegmentation, float startTimestep, float endTimestep, bool deepCopy = true, bool copyAnnotations = true);

    MotionSegmentationPtr getParentSegmentation() override;

    MotionSegmentationPtr createChildSegmentation(const std::string &childSegmentName = std::string()) override;

protected:
    bool merge(MotionSegmentPtr segment, bool appendChildSegmentations = true);

private:
    void getChildResizeRight(float &minTimestep);
    void getChildResizeLeft(float &maxTimestep);
    void resizeChildLeft(float timestep, bool setEditor = true);
    void resizeChildRight(float timestep, bool setEditor = true);

    float startTimestep;
    float endTimestep;
    MotionSegmentationWeakPtr parentSegmentation;
};

}


#endif // __MMM_MOTION_SEGMENT_H_
