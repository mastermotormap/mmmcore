/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  20201 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include <string>

#include "MMM/MMMCore.h"
#include "AbstractMotionSegment.h"

#ifndef __MMM_MOTION_RECORDING_SEGMENT_H_
#define __MMM_MOTION_RECORDING_SEGMENT_H_

namespace MMM
{

class MotionRecordingSegment : public AbstractMotionSegment
{
public:
    MotionRecordingSegment(MotionRecordingPtr motions);

    float getStartTimestep() const override;

    float getEndTimestep() const override;

    //MotionRecordingSegmentPtr getSegmentedSegment(float startTimestep, float endTimestep);

    void setMotionRecording(MotionRecordingPtr motions);

    static MotionRecordingSegmentPtr createMotionRecordingSegment(simox::xml::RapidXMLWrapperNodePtr node, MotionRecordingPtr motions);

    static MotionRecordingSegmentPtr createMotionRecordingSegment(const std::string &path, MotionRecordingPtr motions);

    MotionRecordingSegmentPtr clone(bool deepCopy = true);

    void saveXML(const std::filesystem::path &path, bool indent = true);

    MotionSegmentationPtr getParentSegmentation() override;

    void updateInterval(float minTimestep, float maxTimestep);

protected:
    MotionRecordingSegment(simox::xml::RapidXMLWrapperNodePtr node, MotionRecordingPtr motions);

};
}


#endif // __MMM_MOTION_RECORDING_SEGMENT_H_
