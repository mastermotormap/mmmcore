/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SEGMENTATIONFACTORY_H_
#define __MMM_SEGMENTATIONFACTORY_H_

#include <MMM/AbstractFactoryMethod.h>
#include <MMM/MMMCore.h>
#include <MMM/MMMImportExport.h>
#include <SimoxUtility/xml/rapidxml/RapidXMLForwardDecl.h>

#include "Segmentation.h"

namespace MMM
{

class MMM_IMPORT_EXPORT SegmentationFactory : public AbstractFactoryMethod<SegmentationFactory, void*>
{
public:
    SegmentationFactory() { }

    virtual ~SegmentationFactory() { }

    /*! Creates a segmentation object from an xml representation.
        @param segmentationNode The segmentation's xml node. */
    virtual MotionSegmentationPtr createSegmentation(simox::xml::RapidXMLWrapperNodePtr segmentationNode) = 0;

    /*! Returns the annotation type as name of the factory. */
    virtual std::string getName() = 0;

    static constexpr const char* VERSION = "1.0";
};

typedef std::shared_ptr<SegmentationFactory> SegmentationFactoryPtr;

}

#endif // __MMM_SEGMENTATIONFACTORY_H_
