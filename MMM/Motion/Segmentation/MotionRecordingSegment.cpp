#include "MotionRecordingSegment.h"

#include "Segmentation.h"
#include "../MotionRecording.h"

#include "../../Exceptions.h"
#include "MMM/Motion/Annotation/Annotation.h"
#include "MMM/Motion/Annotation/AnnotationFactory.h"
#include "MMM/Motion/Segmentation/SegmentationFactory.h"
#include "../Motion.h"

#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>

namespace MMM {

MotionRecordingSegment::MotionRecordingSegment(MotionRecordingPtr motions)
    : AbstractMotionSegment("Whole", motions)
{
}

float MotionRecordingSegment::getStartTimestep() const {
    if (auto m = motions.lock()) {
        return m->getMinTimestep();
    }
    else return -1.0f;
}

float MotionRecordingSegment::getEndTimestep() const  {
    if (auto m = motions.lock()) {
        return m->getMaxTimestep();
    }
    else return -1.0f;
}

MotionRecordingSegment::MotionRecordingSegment(simox::xml::RapidXMLWrapperNodePtr node, MotionRecordingPtr motions)
    : AbstractMotionSegment(node->attribute_value_or_default(xml::attribute::NAME, ""), motions)
{
}

/*MotionRecordingSegmentPtr MotionRecordingSegment::getSegmentedSegment(float startTimestep, float endTimestep) {
    if (startTimestep < getEndTimestep()) {
        return clone(nullptr, startTimestep, endTimestep, true, false);
    }
    else
        return nullptr;
}*/

MotionRecordingSegmentPtr MotionRecordingSegment::createMotionRecordingSegment(const std::string &path, MotionRecordingPtr motions) {
    try {
        auto segmentNode = simox::xml::RapidXMLWrapperRootNode::FromFile(path)->first_node("Segment");
        return createMotionRecordingSegment(segmentNode, motions);
    } catch (simox::error::SimoxError &e) {
        throw MMM::Exception::MMMFormatException(e.what());
    }

}

MotionRecordingSegmentPtr MotionRecordingSegment::createMotionRecordingSegment(simox::xml::RapidXMLWrapperNodePtr node, MotionRecordingPtr motions) {
    if (node && motions) {
        MotionRecordingSegmentPtr segment(new MotionRecordingSegment(node, motions));
        segment->addAnnotations(node);
        segment->addSegmentations(node);
        return segment;
    }
    else
        return nullptr;
}

MotionRecordingSegmentPtr MotionRecordingSegment::clone(bool deepCopy) {
    MotionRecordingSegmentPtr motionSegment(new MotionRecordingSegment(motions.lock()));
    motionSegment->addAnnotations(shared_from_this());
    if (deepCopy)
        motionSegment->addSegmentations(shared_from_base<MotionRecordingSegment>());
    return motionSegment;
}

void MotionRecordingSegment::saveXML(const std::filesystem::path &path, bool indent) {
    auto root = simox::xml::RapidXMLWrapperRootNode::createRootNode(xml::tag::MMM_ROOT);
    root->append_attribute(xml::attribute::VERSION, "2.0");
    appendSegmentXML(root);
    root->saveToFile(path, indent);
}

MotionSegmentationPtr MotionRecordingSegment::getParentSegmentation() {
    return nullptr;
}

void MotionRecordingSegment::updateInterval(float minTimestep, float maxTimestep) {
    for (auto childSegmentation : childSegmentations) {
        childSegmentation->updateInterval(minTimestep, maxTimestep);
    }
}

}
