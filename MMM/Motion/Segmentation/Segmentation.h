﻿/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include <string>
#include <set>
#include <map>

#include "MMM/MMMCore.h"
#include "MMM/Enum.h"

#ifndef __MMM_SEGMENTATION_H_
#define __MMM_SEGMENTATION_H_

namespace MMM
{

BETTER_ENUM(MotionLevel, short, Task = 1, Motion = 2, Action = 3, Primitive = 4, Grasping = 5, Unset = 10)

class MMM_IMPORT_EXPORT MotionSegmentation : public std::enable_shared_from_this<MotionSegmentation>
{
    friend class MotionSegment;
    friend class MotionRecordingSegment;
    friend class AbstractMotionSegment;

public:
    MotionSegmentation(AbstractMotionSegmentPtr parent, const std::string &type = MANUAL_SEGMENTATION_TYPE, MotionLevel level = DEFAULT_LEVEL,
                       const std::string &description = std::string(), const std::string &target = std::string(),
                       const std::string &subtarget = std::string());

    MotionSegmentation(AbstractMotionSegmentPtr parent, MotionPtr target, const std::string &type = MANUAL_SEGMENTATION_TYPE, MotionLevel level = DEFAULT_LEVEL,
                       const std::string &description = std::string(), const std::string &subtarget = std::string());

    MotionSegmentation(simox::xml::RapidXMLWrapperNodePtr node, bool unknownConfiguration = false);

    /*! Returns the type of the segmentation. */
    std::string getType();

    /*! Returns optional description of the segmentation. */
    std::string getDescription();

    /*! Returns optional target motion names of the segmentation. E.g. when multiple subjects are available. */
    std::string getMotionTarget();

    std::string getMotionSubTarget();

    MotionLevel getMotionLevel();

    void setMotionLevel(MotionLevel level);

    int getHierarchyLevel();

    MotionSegmentPtr addSegment(float minTimestep, float maxTimestep, const std::string &name = std::string());

    bool isTarget(MotionPtr motion);

    void shiftTimesteps(float timestepDelta);

    virtual MotionSegmentationPtr clone(AbstractMotionSegmentPtr parent, float startTimestep, float endTimestep, bool deepCopy = true, bool copyAnnotations = true);

    void appendSegmentationXML(simox::xml::RapidXMLWrapperNodePtr node);

    float getMinimumTimestep();

    float getMaximumTimestep();

    bool split(float timestep, bool copyAnnotations = true);

    bool remove(float timestep, bool appendChildSegmentations = true, bool removeSegmentationIfLast = true);

    /*! Removes segmentation from parent */
    bool remove();

    bool contains(float timestep);

    MotionSegmentPtr getFirstSegment();

    MotionSegmentPtr getLastSegment();

    MotionSegmentPtr getSegment(float timestep);

    MotionSegmentPtr getPrevSegment(float timestep, bool allowChildElements = true);

    MotionSegmentPtr getNextSegment(float timestep, bool allowChildElements = true);

    MotionSegmentPtr getPrevSegment(MotionSegmentPtr segment);

    MotionSegmentPtr getNextSegment(MotionSegmentPtr segment);

    std::string getCreatorID();

    std::string getLastEditorID();

    std::time_t getCreationTime();

    std::time_t getLastEditedTime();

    const std::vector<std::string>& getEditors();

    void setEdited(const std::string &editorID = AUTHOR_ID);

    bool hasEdited(const std::string &editorID);

    std::vector<MotionSegmentationPtr> createChildSegmentations();

    std::map<float, MotionSegmentPtr> getSegments();

    void addSegments(simox::xml::RapidXMLWrapperNodePtr segmentationNode, MotionRecordingPtr motions);

    bool equals(MotionSegmentationPtr other);

    virtual bool equalsConfiguration(MotionSegmentationPtr other);

    bool overlaps(MotionSegmentationPtr other);

    static std::string AUTHOR_ID;

    static constexpr const char* UNKNOWN_AUTHOR_ID = "unknown";

    static constexpr const char* MANUAL_SEGMENTATION_TYPE = "Manual";

    static constexpr const char* TIME_FORMAT = "%Y-%m-%d";

    static MotionLevel DEFAULT_LEVEL;

    AbstractMotionSegmentPtr getParent() const;

    static MotionLevel motionLevelfromString(const std::string &motionLevel);

protected:
    //MotionSegmentationPtr getSegmentedSegmentation(float startTimestep, float endTimestep, MotionSegmentPtr newParent);

    void addSegment(MotionSegmentPtr segment, bool check = true);

    void setMotionRecording(MotionRecordingPtr motions);

    void notifyNameChange(MotionPtr motion, const std::string &oldMotionName);

    virtual void appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr configurationNode);

    void appendTo(MotionSegmentationPtr motionSegmentation, AbstractMotionSegmentPtr parent, float startTimestep, float endTimestep, bool deepCopy = true, bool copyAnnotations = true);

    std::map<float, MotionSegmentPtr>::const_iterator getSegmentIterator(float timestep);

    void updateInterval(float minTimestep, float maxTimestep);

    AbstractMotionSegmentWeakPtr parent;
    std::string type;
    MotionLevel level;
    std::string description;
    std::string target;
    std::string subtarget;
    std::map<float, MotionSegmentPtr> segments;
    simox::xml::RapidXMLWrapperNodePtr unknownConfigurationNode;

private:
    MotionSegmentation();

    std::string creatorID;
    std::string lastEditorID;
    std::time_t created;
    std::time_t lastEdited;
    std::vector<std::string> editors;
};

}


#endif // __MMM_SEGMENTATION_H_
