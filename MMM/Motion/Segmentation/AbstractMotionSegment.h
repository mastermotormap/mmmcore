/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include <string>

#include "MMM/MMMCore.h"
#include "SegmentObserver.h"

#ifndef __MMM_ABSTRACT_MOTION_SEGMENT_H_
#define __MMM_ABSTRACT_MOTION_SEGMENT_H_

namespace MMM
{

class MMM_IMPORT_EXPORT AbstractMotionSegment : public std::enable_shared_from_this<AbstractMotionSegment>
{
    friend class MotionSegmentation;

public:
    const std::string& getName() const {
        return name;
    }

    /*! Returns segment start timestep in seconds */
    virtual float getStartTimestep() const = 0;

    /*! Returns segment end timestep in seconds */
    virtual float getEndTimestep() const = 0;

    /*! Returns segment length in seconds */
    float getLength() const;

    void addAnnotation(MotionAnnotationPtr annotation);

    void addAnnotations(AbstractMotionSegmentPtr fromSegment);

    bool addSegmentation(MotionSegmentationPtr segmentation);

    void addSegmentations(AbstractMotionSegmentPtr fromSegment, bool deepCopy = true, bool copyAnnotations = true);

    void addSegmentations(AbstractMotionSegmentPtr fromSegment, float startTimestep, float endTimestep, bool deepCopy = true, bool copyAnnotations = true);

    MotionSegmentationPtr getSegmentationByConfiguration(MotionSegmentationPtr segmentation);

    bool containsSegmentationByConfiguration(MotionSegmentationPtr segmentation);

    MotionAnnotationList getAllAnnotations() const ;

    MotionAnnotationList getAnnotations(const std::string &type, const std::string &style = std::string()) const;

    template <typename A, typename std::enable_if<std::is_base_of<MotionAnnotation, A>::value>::type* = nullptr>
    std::vector<std::shared_ptr<A>> getDerivedAnnotations(const std::string &style = std::string()) const {
        std::vector<std::shared_ptr<A>> derivedAnnotations;
        for (auto annotation : this->getAnnotations(A::TYPE, style)) {
            if (std::shared_ptr<A> a = std::dynamic_pointer_cast<A>(annotation))
                derivedAnnotations.push_back(a);
            else MMM_ERROR << "Deriving annotation failed! Should not occur!" << std::endl;
        }
        return derivedAnnotations;
    }

    template <typename A, typename std::enable_if<std::is_base_of<MotionAnnotation, A>::value>::type* = nullptr>
    std::shared_ptr<A> getDerivedAnnotation(const std::string &style = std::string()) const {
        std::vector<std::shared_ptr<A>> derivedAnnotations = getDerivedAnnotations<A>(style);
        if (derivedAnnotations.size() > 0) return derivedAnnotations.front();
        else return nullptr;
    }

    bool removeAnnotations(const std::string &type = std::string(), const std::string &style = std::string());

    bool removeSegmentation(MotionSegmentationPtr segmentation);

    virtual void shiftTimesteps(float timestepDelta);

    void appendSegmentXML(simox::xml::RapidXMLWrapperNodePtr node);

    void setMotionRecording(MotionRecordingPtr motions);

    void notifyNameChange(MotionPtr motion, const std::string &oldMotionName);

    virtual MotionSegmentationPtr createChildSegmentation(const std::string &childSegmentName = std::string());

    MotionSegmentationList getSegmentations();

    std::string getAnnotationAsXML(const std::string &nodeName = "Annotations");

    bool overlaps(float startTimestep, float endTimestep);

    bool overlaps(AbstractMotionSegmentPtr other);

    std::vector<std::vector<MotionSegmentationPtr>> collectSegmentations();

    std::vector<std::vector<MotionSegmentationPtr>> collectSegmentationsReverse();

    MotionRecordingPtr getMotionRecording();

    void addSegmentObserver(std::weak_ptr<MotionSegmentObserver> observer);

    bool removeSegmentObserver(std::weak_ptr<MotionSegmentObserver> observer);

    bool operator <(AbstractMotionSegmentPtr segment) const;

    virtual MotionSegmentationPtr getParentSegmentation() = 0;

    static bool overlaps(float a1, float a2, float b1, float b2);

    static float MINIMUM_SEGMENT_SIZE;

protected:
    template <typename Derived>
    std::shared_ptr<Derived> shared_from_base()
    {
        return std::static_pointer_cast<Derived>(shared_from_this());
    }
    //MotionSegmentPtr getSegmentedSegment(float startTimestep, float endTimestep, MotionSegmentationPtr newParent);

    AbstractMotionSegment(const std::string &name = std::string(), MotionRecordingPtr motions = nullptr);

    void addAnnotations(simox::xml::RapidXMLWrapperNodePtr node);

    void addSegmentations(simox::xml::RapidXMLWrapperNodePtr node);

    void notifySegmentResized();

    static void merge(std::vector<std::vector<MotionSegmentationPtr>> &a, const std::vector<std::vector<MotionSegmentationPtr>> &b);

    std::string name;
    MotionAnnotationList annotations;
    MotionSegmentationList childSegmentations;
    MotionRecordingWeakPtr motions;
    std::vector<std::weak_ptr<MotionSegmentObserver>> observers;
};

}


#endif // __MMM_ABSTRACT_MOTION_SEGMENT_H_
