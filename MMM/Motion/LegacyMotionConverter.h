/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_LEGACYMOTIONCONVERTER_H_
#define __MMM_LEGACYMOTIONCONVERTER_H_

#include "MMM/MMMCore.h"

#include "MMM/Motion/MotionRecording.h"

#include <set>
#include <filesystem>

namespace MMM {

//! Converter for create a motion from the model pose and kinematics of a legacy motion
class MMM_IMPORT_EXPORT LegacyMotionConverter
{
public:
    /*! @param additionalLibPaths Paths to sensor plug-in folders. No need to include the standard folder.
        @param ignoreStandardLibPaths Just use the given additional library Paths.
        @param mmmOnly Only load motions with mmm reference model (mmm.xml) */
    LegacyMotionConverter(bool mmmOnly = false, const std::vector<std::filesystem::path> &additionalLibPaths = std::vector<std::filesystem::path>(), bool ignoreStandardLibPaths = false);

    /*! @param sensorFactories The sensor factory plug-ins.
     *  @param mmmOnly Only load motions with mmm reference model (mmm.xml) */
    LegacyMotionConverter(const std::map<std::string, std::shared_ptr<SensorFactory> > &sensorFactories, bool mmmOnly = false);


    MotionRecordingPtr convert(const std::string &xml, bool xmlIsPath = true, const std::function<void(std::filesystem::path&)> &handleMissingModelFile = NULL);
    
    MotionPtr convert(const rapidxml::xml_node<char>* motionNode, const std::string &motionName, const std::filesystem::path &motionFilePath, const std::function<void(std::filesystem::path&)> &handleMissingModelFile = NULL);
    
    MotionPtr convert(simox::xml::RapidXMLWrapperNodePtr motionNode, const std::string &motionName, const std::filesystem::path &motionFilePath, const std::function<void(std::filesystem::path&)> &handleMissingModelFile = NULL);

    MotionRecordingPtr convert(const std::vector<LegacyMotionPtr> &legacyMotions, const std::filesystem::path &motionFilePath = std::filesystem::path(), bool checkModel = true);

    MotionPtr convertMotion(LegacyMotionPtr legacyMotion, const std::filesystem::path &motionFilePath, bool checkModel = true);

    std::vector<SensorPtr> createSensors(LegacyMotionPtr legacyMotion);

    //! Denotes the version of the legacy motion
    static const std::string VERSION;

    static std::filesystem::path getStandardLibPath();

protected:
    LegacyMotionList loadMotions(const std::string &xml, bool xmlIsPath = true, const std::function<void(std::filesystem::path&)> &handleMissingModelFile = NULL);

    std::map<std::string, std::shared_ptr<SensorFactory> > sensorFactories;
    bool mmmOnly;
};

}

#endif // __MMM_LEGACYMOTIONCONVERTER_H_
