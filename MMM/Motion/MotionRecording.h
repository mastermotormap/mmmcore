/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MotionList_H_
#define __MMM_MotionList_H_

#include "MMM/MMMCore.h"

#include <string>
#include <vector>
#include <map>
#include "Motion.h"
#include "MotionObserver.h"
#include "XMLTools.h"
#include <iterator>

namespace MMM
{

struct ModelMotionDistance {
    std::string motionName;
    std::string modelNodeName;
    bool includesChildNodes;
    std::vector<float> timesteps;
    std::map<std::string, std::map<float, float>> minDistancesToMotions; // at timestep
};

typedef std::shared_ptr<ModelMotionDistance> ModelMotionDistancePtr;

class MotionRecording : public MotionObserver, public std::enable_shared_from_this<MotionRecording>
{

public:
    MotionRecording(std::filesystem::path filePath = "", const std::string &name = std::string(), const std::string &description = std::string()) :
        originFilePath(filePath),
        name(name),
        description(description),
        motions(std::vector<MotionPtr>()),
        segment(nullptr)
    {
    }

    virtual ~MotionRecording();

    //! Create a new empty motion recording
    static MotionRecordingPtr EmptyRecording();

    /**
     * @brief clone Create a clone of this motion recording
     * @param cloneSensors If sensor measurement should be cloned
     * @return cloned motion recording
     */
    MotionRecordingPtr clone(bool cloneSensors = true);

    MotionRecordingPtr cloneConfiguration();

    //! Add a motion
    bool addMotion(MotionPtr motion);

    //! Replace a motion with the same name in a list of motions with the given motion. Returns true if a motion was replaced
    bool replaceMotionByName(MotionPtr motion);

    //! Replace a motion with the same name in a list of motions with the given motion. Otherwise the motion will just be added. Returns true if a motion was replaced.
    bool addReplaceMotion(MotionPtr motion);

    //! Return the common minimum timestep of all motions
    float getCommonMinTimestep();

    //! Return the common maximum timestep of all motions
    float getCommonMaxTimestep();

    //! Return the common minimum and maximum timestep of all motions
    std::tuple<float, float> getCommonTimestepInterval();

    //! Return the common minimum and maximum timestep of all motions
    void getCommonTimestepInterval(float &minTimestep, float &maxTimestep);

    //! Return the minimum timestep of all motions
    float getMinTimestep(MotionPtr ignoreMotion = nullptr);

    //! Return the maximum timestep of all motions
    float getMaxTimestep(MotionPtr ignoreMotion = nullptr);

    //! Return the minimum and timestep of all motions
    std::tuple<float, float> getMinMaxTimesteps(MotionPtr ignoreMotion = nullptr);

    //! Return the minimum and timestep of all motions
    void getMinMaxTimesteps(float &minTimestep, float &maxTimestep);

    //! Return all motion names
    std::vector<std::string> getMotionNames();

    /**
     * @brief toXML Returns the containing motions as xml-based MMM dataformat
     * @param path Where the motion will be stcalculateContactChangesored later to create relative paths
     * @param indent Use indents
     * @return string in MMM data format
     */
    std::string toXML(const std::filesystem::path &path = std::string(), bool indent = true);

    /**
     * @brief saveXML Save containing motions as xml-based MMM dataformat
     * @param path Where to store the xml file
     * @param indent Use indents
     */
    void saveXML(const std::filesystem::path &path, bool indent = true);

    //! Replaces the motion with the same name and store at given filepath
    void replaceAndSave(MotionPtr motion, const std::filesystem::path &motionFilePath);

    /*! Returns the first mmm model reference motion in the list */
    MotionPtr getReferenceModelMotion();

    //! Returns the number of mmm model reference motions in this recording
    size_t countRefereceModelMotion();

    //! Returns the number of motions present
    size_t size();

    //! Returns the motion with the given name or nullptr if not available
    MotionPtr getMotion(const std::string &motionName);

    //! Returns the motion at given index or nullptr if size does not match
    MotionPtr getMotion(unsigned int index);

    //! Returns the motion with the given name. Throws MMMException if not available
    MotionPtr getMotionThrow(const std::string &motionName);

    //! Returns a motion with the same name as given motion or nullptr if not available
    MotionPtr getMotionByName(MotionPtr motion);

    //! Returns 'True' if a motion is present with the given name
    bool contains(const std::string &motionName);

    //! Returns 'True' if a motion is present having the same name as the given motion
    bool containsMotionByName(MotionPtr motion);

    //! Synchronizes all possible sensormeasurements on a specific time frequency via linear interpolation. Removes all sensors, that are not linear interpolatable.
    void synchronizeSensorMeasurements(float timeFrequency);

    //! Returns 'True' if no motions are in this motion recording
    bool empty();

    //! Returns all motions which contain a model
    MotionRecordingPtr motionsWithModel(bool logging = false);

    //! Returns all motions which contain a specific sensor type
    MotionRecordingPtr motionsWithSensor(const std::string &type, bool logging = false);

    //! Returns 'True' if any motion contains sensor information
    bool containsMotionWithAnySensor();

    //! Returns 'True' if any motion contains sensor information with a given type
    bool containsMotionWithSensor(const std::string &type);

    std::vector<MotionPtr>::iterator begin();

    std::vector<MotionPtr>::iterator end();

    //! Returns the first motion in recording
    MotionPtr getFirstMotion();

    //! Sets the name of this recording
    void setName(const std::string &name);

    //! Returns the name of this recording
    std::string getName();

    //! Set the description of this recording
    void setDescription(const std::string &description);

    //! Returns the description of this recording
    std::string getDescription();

    MotionRecordingPtr getMotionRecordingSegment(float startTimestep, float endTimestep, bool changeTimestep);

    /*! Returns a motion recording where min and max timestep match approximately */
    MotionRecordingPtr getReducedMotion(bool changeTimestep);

    //! Synchronizes all possible sensormeasurements on a specific time frequency via linear interpolation. Removes all sensors, that are not linear interpolatable.
    void synchronizeMeasurements(float timeFrequency);

    bool getReferencePose(Eigen::Matrix4f &referencePose, int &referenceAxis);

    MotionRecordingPtr getMirroredMotionRecording();

    void mirrorMotions();

    void setMotionSegment(MotionRecordingSegmentPtr segment);

    std::vector<MotionPtr>::iterator remove(MotionPtr motion);

    MotionRecordingSegmentPtr getSegment() {
        return segment;
    }

    std::filesystem::path getOriginFilePath() {
        return originFilePath;
    }

    std::map<std::string, ModelMotionDistancePtr> calculateDistanceMap(const std::string &motionName, const std::set<std::string> &nodeNames, bool includeChildNodes = false);

    std::map<std::string, std::map<float, std::string>> calculateMultiContactChanges
    (const std::string &motionName, const std::set<std::string> &nodeNames, bool includeChildNodes = false,
     float distanceThresholdMM = DEFAULT_CONTACT_DISTANCE_TRESHOLD_MM, float allowedTimeDifferenceSec = DEFAULT_CONTACT_ALLOWED_TIME_DIFFERENCE_SEC,
     const std::set<std::string> &environment = std::set<std::string>());

    std::map<std::string, std::map<float, std::string>> calculateMultiContacts
    (const std::string &motionName, const std::set<std::string> &nodeNames, bool includeChildNodes = false,
     float distanceThresholdMM = DEFAULT_CONTACT_DISTANCE_TRESHOLD_MM,
     const std::set<std::string> &environment = std::set<std::string>());

    static std::map<float, std::string> calculateContactChanges(ModelMotionDistancePtr modelMotionDistance, float distanceThresholdMM = DEFAULT_CONTACT_DISTANCE_TRESHOLD_MM, float allowedTimeDifferenceSec = DEFAULT_CONTACT_ALLOWED_TIME_DIFFERENCE_SEC, const std::set<std::string> &environment = std::set<std::string>());
    static std::map<float, std::string> calculateContactChanges(const std::map<float, std::string> &contacts, float allowedTimeDifferenceSec = DEFAULT_CONTACT_ALLOWED_TIME_DIFFERENCE_SEC);
    static std::map<float, std::string> calculateContacts(ModelMotionDistancePtr modelMotionDistance, float distanceThresholdMM = DEFAULT_CONTACT_DISTANCE_TRESHOLD_MM, const std::set<std::string> &environment = std::set<std::string>());

    static const float DEFAULT_CONTACT_DISTANCE_TRESHOLD_MM;
    static const float DEFAULT_CONTACT_ALLOWED_TIME_DIFFERENCE_SEC;

protected:
    void add(MotionPtr motion);

    // MotionObserver
    bool isNameChangeAllowed(const std::string &name) override;
    void notifyNameChange(MotionPtr motion, const std::string &oldName) override;
    void notifyMeasurementShift(MotionPtr motion, float delta) override;
    void notifyIntervalChange(MotionPtr motion) override;

    simox::xml::RapidXMLWrapperNodePtr getXML(const std::filesystem::path &path);

    std::filesystem::path originFilePath;
    std::string name;
    std::string description;
    std::vector<MotionPtr> motions;
    MotionRecordingSegmentPtr segment;
};

}

#endif
