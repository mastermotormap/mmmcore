#include "MotionReaderXML.h"

#include "MMM/FactoryPluginLoader.h"
#include "MMM/Model/ModelProcessorFactory.h"
#include "MMM/Model/ModelReaderXML.h"
#include "MMM/Model/LoadModelStrategy.h"
#include "Sensor/UnknownSensor.h"
#include "LegacyMotionConverter.h"
#include "Motion.h"
#include "MotionRecording.h"
#include "MMM/Motion/Sensor/SensorFactory.h"
#include "MMM/Exceptions.h"
#include "XMLTools.h"
#include "Segmentation/MotionRecordingSegment.h"

#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>

#include <map>
#include <vector>
#include <set>
#include <thread>
#include <filesystem>
#include <ctime>
#include <chrono>

namespace MMM
{

const std::string MotionReaderXML::ERROR_MESSAGE = "Could not load motion file! ";
const std::string MotionReaderXML::VERSION = "2.0";

MotionReaderXML::MotionReaderXML(bool convertLegacyMotions, bool mmmOnly, const std::vector<std::filesystem::path> &additionalLibPaths, bool loadUnknownSensors, bool ignoreStandardLibPaths) :
    mmmOnly(mmmOnly),
    converter(convertLegacyMotions ? LegacyMotionConverterPtr(new LegacyMotionConverter(mmmOnly, additionalLibPaths, ignoreStandardLibPaths)) : nullptr),
    mr(new ModelReaderXML()),
    loadUnknownSensors(loadUnknownSensors)
{
    std::vector<std::filesystem::path> libPaths;
    if (!ignoreStandardLibPaths) libPaths.push_back(getStandardLibPath());
    for (const auto &additionalLibPath : additionalLibPaths) libPaths.push_back(additionalLibPath);
    std::shared_ptr<FactoryPluginLoader<SensorFactory> > factoryPluginLoader = std::shared_ptr<FactoryPluginLoader<SensorFactory> >(new FactoryPluginLoader<SensorFactory>(libPaths));
    sensorFactories = factoryPluginLoader->getFactories();
}

MotionReaderXML::MotionReaderXML(const std::map<std::string, std::shared_ptr<SensorFactory> > &sensorFactories, bool convertLegacyMotions, bool mmmOnly, bool loadUnknownSensors) :
    sensorFactories(sensorFactories),
    mmmOnly(mmmOnly),
    converter(convertLegacyMotions ? LegacyMotionConverterPtr(new LegacyMotionConverter(sensorFactories, mmmOnly)) : nullptr),
    mr(new ModelReaderXML()),
    loadUnknownSensors(loadUnknownSensors)
{
}

/*! Load a specific motion from an mmm dataformat xml document.
    @param xml An xml string or a path to an xml document.
    @param motionName The name of the motion to load from the xml document.
    @param xmlIsPath Xml parameter is a path or string.
    @throws XMLFormatException When the mmm data format is not applicable.*/
MotionPtr MotionReaderXML::loadMotion(const std::string &xml, const std::string &motionName, bool xmlIsPath) {
    std::string version;
    for (auto motionNode : getMotionNodes(xml::getRoot(xml, xmlIsPath), version)) {
        if (getName(motionNode) == motionName) {
            return loadMotion(xml, motionName, xmlIsPath);
        }
    }
    return nullptr;
}

std::vector<simox::xml::RapidXMLWrapperNodePtr> MotionReaderXML::getMotionNodes(simox::xml::RapidXMLWrapperRootNodePtr root, std::string &version) {
   std::vector<simox::xml::RapidXMLWrapperNodePtr> motionNodes;
    try {
        if (root && root->name() == xml:: tag::MMM_ROOT) {
            version = root->has_attribute(xml::attribute::VERSION) ? root->attribute_value(xml::attribute::VERSION) : "1.0";
            motionNodes = root->nodes(xml::tag::MOTION);
        }
        else error("Node 'MMM' does not exist in " + root->getPath());
    }
    catch (simox::error::XMLFormatError &e) {
        error(e.what());
    }
    return motionNodes;
}

MotionPtr MotionReaderXML::loadMotion(simox::xml::RapidXMLWrapperNodePtr motionNode, const std::string &version, const std::filesystem::path filePath) {
    if (version == VERSION)
        return loadMotion(motionNode, filePath);
    else if (version == LegacyMotionConverter::VERSION && converter)
        return converter->convert(motionNode, getName(motionNode), filePath, mr->handleModelFilePath);
    else
        return nullptr;
}

MotionPtr MotionReaderXML::loadMotion(simox::xml::RapidXMLWrapperNodePtr motionNode, const std::filesystem::path &motionFilePath) {
    try {
        MotionPtr motion(new Motion(getName(motionNode)));
        motion->originFilePath = motionFilePath;

        ProcessedModelWrapperPtr model = nullptr;
        if (motionNode->has_node(xml::tag::MODEL)) {
            simox::xml::RapidXMLWrapperNodePtr modelNode = motionNode->first_node(xml::tag::MODEL);
            ModelProcessorPtr modelProcessor;
            if (modelNode->has_node(xml::tag::MODELPROCESSOR)) {
                modelProcessor = ModelProcessorFactory::getModelProcessorFromNode(modelNode->first_node(xml::tag::MODELPROCESSOR)->get_node_ptr());
            }
            std::filesystem::path modelFilePath = modelNode->attribute_value(xml::attribute::PATH);
            if (mmmOnly) {
                std::string modelName = modelFilePath.stem();
                if (modelName != "mmm") {
                    MMM_INFO << "[MMMOnly] Ignoring motion " + motion->getName() + " as " + modelName + " is not the mmm reference model " << std::endl;
                    return nullptr;
                }
            }
            model = mr->loadMotionModel(modelFilePath, motionFilePath, modelProcessor);
        }
        else if (mmmOnly) {
            MMM_INFO << "[MMMOnly] Ignoring motion " + motion->getName() + " as it does not contain a model." << std::endl;
            return nullptr;
        }
        motion->setProcessedModelWrapper(model);

        if (motionNode->has_attribute(xml::attribute::TYPE))
            motion->setMotionType(motionNode->attribute_value(xml::attribute::TYPE));
        else {
            motion->setMotionTypeFromModel();
        }

        for (auto sensorNode : motionNode->first_node("Sensors")->nodes(xml::tag::SENSOR)) {
           std::string sensorVersion = "1.0";
           if (sensorNode->has_attribute(xml::attribute::VERSION)) sensorVersion = sensorNode->attribute_value(xml::attribute::VERSION);
           std::string sensorType = sensorNode->attribute_value(xml::attribute::TYPE);
           SensorFactoryPtr factory = sensorFactories[sensorType + "_v" + sensorVersion];
           if (factory) {
               auto sensor = factory->createSensor(sensorNode, motionFilePath);
               if (sensor) motion->addSensor(sensor, 0.0f, LoadModelStrategy::isModelLoaded() && model && model->getModel()); // Exception will be forwarded. Nullptr are ignored here
           }
           else if (loadUnknownSensors) {
                UnknownSensorPtr sensor = UnknownSensor::loadSensorXML(sensorNode, motionFilePath);
                if (sensor) {
                    motion->addSensor(sensor, 0.0f, LoadModelStrategy::isModelLoaded() && model && model->getModel()); // Exception will be forwarded. Nullptr are ignored here
                    MMM_INFO << "Cannot find plugin with id '" << sensorType << "' and version '" << sensorVersion << "'. Loading as unknown sensor instead!" << std::endl;
                }
            }
            else MMM_INFO << "Cannot find plugin with id '" << sensorType << "' and version '" << sensorVersion << "'" << std::endl;
        }

        return motion;
    }
    catch (simox::error::SimoxError &e) {
        throw Exception::MMMException(e.what());
    }
}

/*! Load all motions from an mmm dataformat xml document.
    @param xml An xml string or a path to an xml document.
    @param xmlIsPath Xml parameter is a path or string.
    @throws XMLFormatException When the mmm data format is not applicable.*/
MotionRecordingPtr MotionReaderXML::loadMotionRecording(const std::string &xml, bool xmlIsPath) {
    try {
        auto root = xmlIsPath ? simox::xml::RapidXMLWrapperRootNode::FromFile(xml) : simox::xml::RapidXMLWrapperRootNode::FromXmlString(xml);
        std::string version;
        std::filesystem::path filePath = xmlIsPath ? xml : std::string();
        MotionRecordingPtr motions(new MotionRecording(filePath, root->attribute_value_or_default(xml::attribute::NAME, filePath.stem()),
                                                       root->attribute_value_or_default(xml::attribute::DESCRIPTION, "")));
        for (auto motionNode : getMotionNodes(root, version)) {
            MotionPtr motion = loadMotion(motionNode, version, filePath);
            if (motion) {
                if (!motions->addMotion(motion)) error("Name " + getName(motionNode) + " not unique!");
            }
        }
        if (root->has_node("Segment")) {
            auto segmentNode = root->first_node("Segment");
            motions->setMotionSegment(MotionRecordingSegment::createMotionRecordingSegment(segmentNode, motions));
        }
        return motions;
    }
    catch (simox::error::SimoxError &e) {
        throw Exception::MMMException(e.what());
    }
}

std::vector<MotionRecordingPtr> MotionReaderXML::loadAllMotionRecordingsFromDirectory(const std::filesystem::path &directoryPath, bool recursive, unsigned int threads, const std::string &regex, int maxNumber, unsigned int startIndex, bool printLog) {
    auto paths = getMotionPathsFromDirectory(directoryPath, recursive, regex, startIndex, printLog);
    if (paths.empty()) {
        MMM_ERROR << "No suitable candidates found!\n";
        return std::vector<MotionRecordingPtr>();
    }
    else return loadAllMotionRecordingsFromPaths(paths, threads, maxNumber, printLog);
}

std::vector<std::filesystem::path> MotionReaderXML::getMotionPathsFromDirectory(const std::filesystem::path &directoryPath, bool recursive, const std::string &regex, unsigned int startIndex, bool printLog) {
    if (!MMM::xml::isValid(directoryPath)) error(directoryPath.generic_string() + " is not a valid directory!");

    std::regex r = std::regex(regex);
    std::vector<std::filesystem::path> paths;
    unsigned int index = 0;
    if (recursive) {
        for(auto &file : std::filesystem::recursive_directory_iterator(directoryPath))
            addXMLFile(file.path(), paths, index, r, startIndex, printLog);
    }
    else {
        for(auto &file : std::filesystem::directory_iterator(directoryPath))
            addXMLFile(file.path(), paths, index, r, startIndex, printLog);
    }
    return paths;
}

void MotionReaderXML::addXMLFile(const std::filesystem::path &filePath, std::vector<std::filesystem::path> &paths, unsigned int &index, const std::regex &regex, unsigned int startIndex, bool printLog) {
    auto fileExtension = filePath.extension();
    if (fileExtension == ".xml") {
        if (std::regex_search(filePath.stem().generic_string(), regex)) {
            if (index++ >= startIndex) {
                paths.push_back(filePath);
                return; // No log is printed
            }
        }
    }
    if (printLog)
        MMM_INFO << "Ignoring file " + filePath.generic_string() + "\n";
}

std::filesystem::path MotionReaderXML::getStandardLibPath() {
    return SensorFactory::getStandardLibPath();
}

void MotionReaderXML::error(const std::string &msg) {
    throw Exception::MMMFormatException(ERROR_MESSAGE + msg);
}

void MotionReaderXML::setHandleMissingModelFile(const std::function<void(std::filesystem::path&)> &handleMissingModelFile) {
    mr->handleModelFilePath = handleMissingModelFile;
}

std::string MotionReaderXML::getName(simox::xml::RapidXMLWrapperNodePtr motionNode) {
    return motionNode->attribute_value(xml::attribute::NAME);
}

std::vector<MotionRecordingPtr> MotionReaderXML::loadAllMotionRecordingsFromPaths(const std::vector<std::filesystem::path> &paths, unsigned int threads, int maxNumber, bool printLog) {
    std::vector<MotionRecordingPtr> mmmMotions;
    std::atomic<int> n = 0;
    unsigned int maximum = (maxNumber > 0 && maxNumber <= (int)paths.size()) ? maxNumber : paths.size();
    auto start = std::chrono::system_clock::now();
    if (threads <= 1) {
        MMM_INFO << "Loading motion files with one thread from " + std::to_string(paths.size()) + " candidates" << std::endl;
        for (const std::string &filePath : paths) {
            if (n >= (int) maximum) break;
            try {
                mmmMotions.push_back(loadMotionRecording(filePath, true));
                n++;
                if (printLog && n % 50 == 0) {
                    auto current = std::chrono::system_clock::now();
                    std::chrono::duration<double> elapsed_seconds = current - start;
                    MMM_INFO << std::to_string(n) + " of " + std::to_string(maximum) + " mmm motions loaded. Elapsed time: " + std::to_string(elapsed_seconds.count()) << std::endl;
                }
            }
            catch (Exception::MMMException &e) {
                if (printLog) MMM_INFO << "Ignoring file " + filePath + "! " + e.what() + "\n";
            }
        }
    }
    else {
        if (threads > std::thread::hardware_concurrency()) threads = std::thread::hardware_concurrency();
        MMM_INFO << "Loading motion files in parallel with " + std::to_string(threads) + " threads " + std::to_string(paths.size()) + " candidates" << std::endl;
        int split = maximum / threads + 1;
        mmmMotions.resize(maximum);
        std::vector<std::thread> threadPool;
        for (unsigned int i = 0; i < maximum; i += split) {
            threadPool.push_back(std::thread([this,split,i,paths,printLog,start,maximum]
                                             (std::vector<MotionRecordingPtr> &motions, std::atomic<int> &n) {
                for (int j = 0; j < split; j++) {
                    int index = i + j;
                    if (index >= (int) motions.size()) break;
                    auto path = paths[index];
                    try {
                        motions[index] = loadMotionRecording(path, true);
                        int number = ++n;
                        if (printLog && number % 50 == 0) {
                            auto current = std::chrono::system_clock::now();
                            std::chrono::duration<double> elapsed_seconds = current - start;
                            MMM_INFO << number << " of " << maximum << " mmm motions loaded. Elapsed time: "
                                     << elapsed_seconds.count() << std::endl;
                        }
                    }
                    catch (Exception::MMMException &e) {
                        if (printLog) MMM_ERROR << "Ignoring file " + path.generic_string() + "! " + e.what() + "\n";
                    }
                }
            }, std::ref(mmmMotions), std::ref(n)));
        }
        for (auto &thread : threadPool) {
            thread.join();
        }
    }

    std::vector<MotionRecordingPtr> mmmMotionsResized;
    std::copy_if (mmmMotions.begin(), mmmMotions.end(), std::back_inserter(mmmMotionsResized), [](MotionRecordingPtr m){return m && m->size() > 0;} ); // remove empty motions
    return mmmMotionsResized;
}

}
