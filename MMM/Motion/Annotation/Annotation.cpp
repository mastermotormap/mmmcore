#include "Annotation.h"

std::string MMM::MotionAnnotation::getType() {
    return type;
}

std::string MMM::MotionAnnotation::getStyle() {
    return style;
}

bool MMM::MotionAnnotation::has(const std::string &type, const std::string &style) {
    return getType() == type && style.empty() ? true : getStyle() == style;
}

void MMM::MotionAnnotation::appendAnnotationXML(simox::xml::RapidXMLWrapperNodePtr node) {
    if (!node) return;
    auto annotationNode = node->append_node("Annotation");
    annotationNode->append_attribute(xml::attribute::TYPE, type);
    if (!style.empty())
        annotationNode->append_attribute("style", style);
    append(annotationNode);
}

void MMM::MotionAnnotation::notifyNameChange(MMM::MotionPtr /*motion*/, const std::string &/*oldMotionName*/) {
    // Do something in derived classes
}

MMM::MotionAnnotation::MotionAnnotation(const std::string &type, const std::string &style) :
    type(type),
    style(style)
{
}


// ---------------------------------------------------------------------------------


MMM::UnknownMotionAnnotation::UnknownMotionAnnotation(const std::string &type, simox::xml::RapidXMLWrapperNodePtr node, const std::string &style) :
    MotionAnnotation(type, style),
    node(node)
{
}

std::string MMM::UnknownMotionAnnotation::getAnnotation() {
    return node->value();
}

MMM::MotionAnnotationPtr MMM::UnknownMotionAnnotation::clone() {
    return MotionAnnotationPtr(new UnknownMotionAnnotation(type, node, style));
}

simox::xml::RapidXMLWrapperNodePtr MMM::UnknownMotionAnnotation::getUnknownAnnotationNode() {
    return node;
}

void MMM::UnknownMotionAnnotation::appendAnnotationXML(simox::xml::RapidXMLWrapperNodePtr node) {
    node->append_node(*node.get());
}
