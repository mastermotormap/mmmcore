/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MOTIONDESCRIPTIONANNOTATION_H_
#define __MMM_MOTIONDESCRIPTIONANNOTATION_H_

#include "../Annotation.h"

namespace MMM
{

BETTER_ENUM(MotionDescriptionStyle, short, LOCOMOTION = 1, MANIPULATION = 2, GRASP = 3, GESTICULATION = 4, SPORT = 5, DIRECTION = 6, SPEED = 7, REPETITION = 8,
            PERTURBATION = 9, EMOTION = 10, BODYPARTS = 11, POSES = 12, HANDEDNESS = 13, SPECIFICATION = 14)

class MMM_IMPORT_EXPORT MotionDescriptionAnnotation : public MotionAnnotation
{
public:
    MotionDescriptionAnnotation(MotionDescriptionStyle style, simox::xml::RapidXMLWrapperNodePtr node);

    MotionDescriptionAnnotation(MotionDescriptionStyle style, const std::string &description);

    std::string getAnnotation() override;

    std::string getDescription();

    static constexpr const char* TYPE = "MotionDescription";

protected:
    void append(simox::xml::RapidXMLWrapperNodePtr annotationNode) override;

private:
    std::string description;
};

}

#endif // __MMM_MOTIONDESCRIPTIONANNOTATION_H_
