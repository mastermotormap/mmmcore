#include "MotionDescriptionAnnotation.h"

namespace MMM
{

MotionDescriptionAnnotation::MotionDescriptionAnnotation(MotionDescriptionStyle style, simox::xml::RapidXMLWrapperNodePtr node) : MotionDescriptionAnnotation(style, node->value())
{
}

MotionDescriptionAnnotation::MotionDescriptionAnnotation(MotionDescriptionStyle style, const std::string &description) : MotionAnnotation(TYPE, style._name()), description(description)
{
}

std::string MotionDescriptionAnnotation::getAnnotation() {
    return getDescription();
}

std::string MotionDescriptionAnnotation::getDescription() {
    return description;
}

void MotionDescriptionAnnotation::append(simox::xml::RapidXMLWrapperNodePtr annotationNode) {
    annotationNode->append_data_node(description);
}

}
