#include "BimanualAnnotation.h"

#include "MMM/Motion/Motion.h"

namespace MMM
{

BimanualAnnotation::BimanualAnnotation(simox::xml::RapidXMLWrapperNodePtr node) :
    BimanualAnnotation(BimanualLabel::_from_string(node->value().c_str()))
{
}

BimanualAnnotation::BimanualAnnotation(BimanualLabel label)
    : MotionAnnotation(TYPE), label(label)
{
}

std::string BimanualAnnotation::getAnnotation() {
    return getLabel()._name();
}

BimanualLabel BimanualAnnotation::getLabel() {
    return label;
}

void BimanualAnnotation::append(simox::xml::RapidXMLWrapperNodePtr annotationNode) {
    annotationNode->append_data_node(label._to_string());
}



MotionAnnotationPtr BimanualAnnotation::clone(){
    return std::shared_ptr<BimanualAnnotation>(new BimanualAnnotation(label));
}


void BimanualAnnotation::setLabel(BimanualLabel label) {
    this->label = label;
}

}
