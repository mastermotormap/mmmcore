#include "BimanualAnnotation.h"

#include <boost/extension/extension.hpp>

#include "BimanualAnnotationFactory.h"

namespace MMM
{

// register this factory
AnnotationFactory::SubClassRegistry BimanualAnnotationFactory::registry(BimanualAnnotation::TYPE, &BimanualAnnotationFactory::createInstance);

BimanualAnnotationFactory::BimanualAnnotationFactory() : AnnotationFactory() {}

BimanualAnnotationFactory::~BimanualAnnotationFactory() = default;

MotionAnnotationPtr BimanualAnnotationFactory::createAnnotation(simox::xml::RapidXMLWrapperNodePtr annotationNode)
{
    return MotionAnnotationPtr(new BimanualAnnotation(annotationNode));
}


std::string BimanualAnnotationFactory::getName()
{
    return BimanualAnnotation::TYPE;
}

AnnotationFactoryPtr BimanualAnnotationFactory::createInstance(void *)
{
    return AnnotationFactoryPtr(new BimanualAnnotationFactory());
}

}
