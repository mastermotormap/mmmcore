#pragma once

#include "../Annotation.h"


namespace MMM
{


BETTER_ENUM(BimanualLabel, short, no_motion = 0, unimanual_right = 1, unimanual_left = 2,
            loosely_coupled = 3, tightly_coupled_sym = 4, tightly_coupled_asym_r = 5, tightly_coupled_asym_l = 6, undefined = 7)

class MMM_IMPORT_EXPORT BimanualAnnotation : public MotionAnnotation
{
public:
    BimanualAnnotation(simox::xml::RapidXMLWrapperNodePtr node);

    BimanualAnnotation(BimanualLabel label);

    std::string getAnnotation() override;

    MotionAnnotationPtr clone() override;

    void setLabel(BimanualLabel label);
    BimanualLabel getLabel();


    static constexpr const char* TYPE = "BimanualLabel";

protected:
    void append(simox::xml::RapidXMLWrapperNodePtr annotationNode) override;

private:
    BimanualLabel label;
};

}
