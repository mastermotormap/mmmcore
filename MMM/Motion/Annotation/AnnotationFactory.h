/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_ANNOTATIONFACTORY_H_
#define __MMM_ANNOTATIONFACTORY_H_

#include <MMM/AbstractFactoryMethod.h>
#include <MMM/MMMCore.h>
#include <MMM/MMMImportExport.h>
#include <SimoxUtility/xml/rapidxml/RapidXMLForwardDecl.h>

#include "Annotation.h"

namespace MMM
{

class MMM_IMPORT_EXPORT AnnotationFactory : public AbstractFactoryMethod<AnnotationFactory, void*>
{
public:
    AnnotationFactory() { }

    virtual ~AnnotationFactory() { }

    /*! Creates a annotation from an xml representation.
        @param annotationNode The annotation xml node. */
    virtual MotionAnnotationPtr createAnnotation(simox::xml::RapidXMLWrapperNodePtr annotationNode) = 0;

    /*! Returns the annotation type as name of the factory. */
    virtual std::string getName() = 0;

    static constexpr const char* VERSION = "1.0";
};

typedef std::shared_ptr<AnnotationFactory> AnnotationFactoryPtr;

}

#endif // __MMM_ANNOTATIONFACTORY_H_
