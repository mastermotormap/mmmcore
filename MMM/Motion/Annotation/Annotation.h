/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include "MMM/MMMCore.h"
#include "MMM/Enum.h"
#include "MMM/Motion/XMLTools.h"

#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>

#ifndef __MMM_ANNOTATION_H_
#define __MMM_ANNOTATION_H_

namespace MMM
{

/*! \brief The public interface for the annotation classes */
class MMM_IMPORT_EXPORT MotionAnnotation
{
public:
    std::string getType();

    std::string getStyle();

    bool has(const std::string &type, const std::string &style = std::string());

    virtual std::string getAnnotation() = 0;

    virtual MotionAnnotationPtr clone() = 0;

    virtual void appendAnnotationXML(simox::xml::RapidXMLWrapperNodePtr node);

    virtual void notifyNameChange(MotionPtr motion, const std::string &oldMotionName);

protected:
    MotionAnnotation(const std::string &type, const std::string &style = std::string());

    virtual void append(simox::xml::RapidXMLWrapperNodePtr annotationNode) = 0;

    std::string type;
    std::string style;
};


class MMM_IMPORT_EXPORT UnknownMotionAnnotation : public MotionAnnotation
{
public:
    UnknownMotionAnnotation(const std::string &type, simox::xml::RapidXMLWrapperNodePtr node, const std::string &style = std::string());

    std::string getAnnotation() override;

    MotionAnnotationPtr clone() override;

    simox::xml::RapidXMLWrapperNodePtr getUnknownAnnotationNode();

    virtual void appendAnnotationXML(simox::xml::RapidXMLWrapperNodePtr node) override;

private:
    void append(simox::xml::RapidXMLWrapperNodePtr /*node*/) override {
        // Not used here
    }

    simox::xml::RapidXMLWrapperNodePtr node;
};

typedef std::shared_ptr<UnknownMotionAnnotation> UnknownMotionAnnotationPtr;


}

#endif // __MMM_ANNOTATION_H_
