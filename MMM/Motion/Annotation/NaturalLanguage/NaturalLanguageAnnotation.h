/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_NATURALLANGUAGEANNOTATION_H_
#define __MMM_NATURALLANGUAGEANNOTATION_H_

#include "../Annotation.h"

namespace MMM
{

BETTER_ENUM(NaturalLanguageStyle, short, INSTRUCTION = 1, FIRSTPERSON = 2, THIRDPERSON = 3)

class MMM_IMPORT_EXPORT NaturalLanguageAnnotation : public MotionAnnotation
{
public:
    NaturalLanguageAnnotation(NaturalLanguageStyle style, simox::xml::RapidXMLWrapperNodePtr node);

    NaturalLanguageAnnotation(NaturalLanguageStyle style, const std::string &naturalLanguageTerm);

    std::string getAnnotation() override;

    std::string getNaturalLanguageTerm();

    static constexpr const char* TYPE = "NaturalLanguage";

protected:
    void append(simox::xml::RapidXMLWrapperNodePtr annotationNode) override;

private:
    std::string naturalLanguageTerm;
};
}

#endif // __MMM_NATURALLANGUAGEANNOTATION_H_
