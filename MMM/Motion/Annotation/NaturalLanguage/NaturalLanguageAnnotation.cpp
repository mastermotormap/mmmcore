#include "NaturalLanguageAnnotation.h"

namespace MMM
{

NaturalLanguageAnnotation::NaturalLanguageAnnotation(NaturalLanguageStyle style, simox::xml::RapidXMLWrapperNodePtr node) : NaturalLanguageAnnotation(style, node->value())
{
}

NaturalLanguageAnnotation::NaturalLanguageAnnotation(NaturalLanguageStyle style, const std::string &naturalLanguageTerm) : MotionAnnotation(TYPE, style._name()), naturalLanguageTerm(naturalLanguageTerm)
{
}

std::string NaturalLanguageAnnotation::getAnnotation() {
    return getNaturalLanguageTerm();
}

std::string NaturalLanguageAnnotation::getNaturalLanguageTerm() {
    return naturalLanguageTerm;
}

void NaturalLanguageAnnotation::append(simox::xml::RapidXMLWrapperNodePtr annotationNode) {
    annotationNode->append_data_node(naturalLanguageTerm);
}

}
