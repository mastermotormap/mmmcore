#include "ActionLabelAnnotationFactory.h"

#include <boost/extension/extension.hpp>

#include "ActionLabelAnnotation.h"

namespace MMM
{

// register this factory
AnnotationFactory::SubClassRegistry ActionLabelAnnotationFactory::registry(ActionLabelAnnotation::TYPE, &ActionLabelAnnotationFactory::createInstance);

ActionLabelAnnotationFactory::ActionLabelAnnotationFactory() : AnnotationFactory() {}

ActionLabelAnnotationFactory::~ActionLabelAnnotationFactory() = default;

MotionAnnotationPtr ActionLabelAnnotationFactory::createAnnotation(simox::xml::RapidXMLWrapperNodePtr annotationNode)
{
    return MotionAnnotationPtr(new ActionLabelAnnotation(annotationNode));
}


std::string ActionLabelAnnotationFactory::getName()
{
    return ActionLabelAnnotation::TYPE;
}

AnnotationFactoryPtr ActionLabelAnnotationFactory::createInstance(void *)
{
    return AnnotationFactoryPtr(new ActionLabelAnnotationFactory());
}

}
