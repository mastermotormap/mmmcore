#include "ActionLabelAnnotation.h"

#include "MMM/Motion/Motion.h"

namespace MMM
{

ActionLabel ActionLabelFromString(const std::string &label) {
    better_enums::optional<ActionLabel> optionalLabel = ActionLabel::_from_string_nocase_nothrow(label.c_str());
    if (!optionalLabel)
        throw Exception::MMMException(label + " is not a valid ActionLabel!");
    return optionalLabel.value();
}

Failure FailureFromString(const std::string &failure) {
    if (failure.empty()) return Failure::None;

    better_enums::optional<Failure> optionalFailure = Failure::_from_string_nocase_nothrow(failure.c_str());
    if (!optionalFailure)
        throw Exception::MMMException(failure + " is not a valid Failure!");
    return optionalFailure.value();
}

ActionObject::ActionObject(const std::string &specifier)
{
    std::vector<std::string> splitted = simox::alg::split(specifier, SEPARATOR);
    if (splitted.size() == 2) {
        name = splitted.at(0);
        node = splitted.at(1);
    }
    else if (splitted.size() == 1) {
        name = splitted.at(0);
    }
    else throw Exception::MMMException("Wrong action object specifier: " + specifier);
}

const std::string &ActionObject::getName() const
{
    return name;
}

void ActionObject::setName(const std::string &newName)
{
    name = newName;
}

const std::string &ActionObject::getNode() const
{
    return node;
}

void ActionObject::setNode(const std::string &newNode)
{
    node = newNode;
}

bool ActionObject::matches(const std::string &name) {
    return this->name == name;
}

bool ActionObject::notifyNameChange(const std::string &newName, const std::string &oldName) {
    if (!matches(oldName)) return false;
    name = newName;
    return true;
}

std::string ActionObject::str() const {
    if (node.empty())
        return name;
    else
        return name + SEPARATOR + node;
}

std::optional<ActionObject> ActionObject::fromString(const std::string &specifier) {
    std::optional<ActionObject> o;
    if (!specifier.empty())
        o = ActionObject(specifier);
    return o;
}

ActionLabelAnnotation::ActionLabelAnnotation(simox::xml::RapidXMLWrapperNodePtr node) :
    ActionLabelAnnotation(ActionLabelFromString(node->value().c_str()),
                          node->attribute_value_or_default("main", std::string()),
                          node->attribute_value_or_default("source", std::string()),
                          node->attribute_value_or_default("target", std::string()),
                          node->attribute_value_or_default("main-second", std::string()),
                          FailureFromString(node->attribute_value_or_default("failure", std::string())))
{
}

ActionLabelAnnotation::ActionLabelAnnotation(ActionLabel label, const std::string &mainObject, const std::string &source,
                                             const std::string &target, const std::string &secondMainObject, Failure failure)
    : ActionLabelAnnotation(label, ActionObject::fromString(mainObject), ActionObject::fromString(source),
                            ActionObject::fromString(target), ActionObject::fromString(secondMainObject), failure)
{
}

ActionLabelAnnotation::ActionLabelAnnotation(ActionLabel label, const std::optional<ActionObject> &mainObject, const std::optional<ActionObject> &source,
                                             const std::optional<ActionObject> &target, const std::optional<ActionObject> &secondMainObject, Failure failure) :
    MotionAnnotation(TYPE),
    label(label),
    mainObject(mainObject),
    secondMainObject(secondMainObject),
    source(source),
    target(target),
    failure(failure)
{
}

std::string ActionLabelAnnotation::getAnnotation() {
    return getLabel()._name();
}

const ActionLabel& ActionLabelAnnotation::getLabel() const {
    return label;
}
void ActionLabelAnnotation::setLabel(const ActionLabel &label) {
    this->label = label;
}

void ActionLabelAnnotation::setMainObject(const ActionObject &value) {
    mainObject = value;
}

void ActionLabelAnnotation::setSecondMainObject(const ActionObject &value) {
    secondMainObject = value;
}

void ActionLabelAnnotation::setSource(const ActionObject &value) {
    source = value;
}

void ActionLabelAnnotation::setTarget(const ActionObject &value) {
    target = value;
}

const std::optional<ActionObject>& ActionLabelAnnotation::getMainObject() const
{
    return mainObject;
}

void ActionLabelAnnotation::setMainObject(const std::string &value)
{
    mainObject = ActionObject::fromString(value);
}

const std::optional<ActionObject>& ActionLabelAnnotation::getSecondMainObject() const
{
    return secondMainObject;
}

void ActionLabelAnnotation::setSecondMainObject(const std::string &value)
{
    secondMainObject = ActionObject::fromString(value);
}

const std::optional<ActionObject>& ActionLabelAnnotation::getSource() const
{
    return source;
}

void ActionLabelAnnotation::setSource(const std::string &value)
{
    source = ActionObject::fromString(value);
}

const std::optional<ActionObject>& ActionLabelAnnotation::getTarget() const
{
    return target;
}

void ActionLabelAnnotation::setTarget(const std::string &value)
{
    target = ActionObject::fromString(value);
}

MotionAnnotationPtr ActionLabelAnnotation::clone(){
    return std::shared_ptr<ActionLabelAnnotation>(new ActionLabelAnnotation(label, mainObject, source, target, secondMainObject));
}

void ActionLabelAnnotation::notifyNameChange(MotionPtr motion, const std::string &oldMotionName) {
    mainObject->notifyNameChange(motion->getName(), oldMotionName);
    secondMainObject->notifyNameChange(motion->getName(), oldMotionName);
    source->notifyNameChange(motion->getName(), oldMotionName);
    target->notifyNameChange(motion->getName(), oldMotionName);
}

void ActionLabelAnnotation::append(simox::xml::RapidXMLWrapperNodePtr annotationNode) {
    annotationNode->append_data_node(label._to_string());
    if (mainObject.has_value()) annotationNode->append_attribute("main", mainObject->str());
    if (secondMainObject.has_value()) annotationNode->append_attribute("main-second", secondMainObject->str());
    if (source.has_value()) annotationNode->append_attribute("source", source->str());
    if (target.has_value()) annotationNode->append_attribute("target", target->str());
    if (failure._value != Failure::None) annotationNode->append_attribute("failure", failure._to_string());
}

const Failure& ActionLabelAnnotation::getFailure() const {
    return failure;
}

void ActionLabelAnnotation::setFailure(const std::string &failure) {
    this->failure = FailureFromString(failure);
}

void ActionLabelAnnotation::setFailure(const Failure &failure) {
    this->failure = failure;
}

bool ActionLabelAnnotation::isFailure() const {
    return failure._value > 0;
}

std::string ActionLabelAnnotation::getMainObjectStr() const {
    return mainObject.has_value() ? mainObject.value().str() : std::string();
}

std::string ActionLabelAnnotation::getSecondMainObjectStr() const {
    return secondMainObject.has_value() ? secondMainObject.value().str() : std::string();
}

std::string ActionLabelAnnotation::getSourceStr() const {
    return source.has_value() ? source.value().str() : std::string();
}

std::string ActionLabelAnnotation::getTargetStr() const {
    return target.has_value() ? target.value().str() : std::string();
}

std::string ActionLabelAnnotation::getLabelStr() const {
    return label._to_string();
}

std::string ActionLabelAnnotation::getFailureStr(bool noneAsEmpty) const {
    if (noneAsEmpty && !isFailure())
        return std::string();
    return failure._to_string();
}

}
