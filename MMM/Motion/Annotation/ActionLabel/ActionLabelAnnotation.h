/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_ACTIONLABELANNOTATION_H_
#define __MMM_ACTIONLABELANNOTATION_H_

#include "../Annotation.h"

namespace MMM
{

BETTER_ENUM(ActionLabel, short, Idle = 0, Approach = 1, Retreat = 2, // Actions without object in hand
            Lift = 10, Place = 11, // only supported
            Hold = 16,
            Move = 20,
            Cut = 30, Peel = 31, Wipe = 32, Mix = 33, Stir = 34, Scoop = 35, Sweep = 36, Pour = 37,
            Transfer = 38, Open = 39, Close = 40, RollOut = 41, Shake = 42,
            PreAction = 51, PostAction = 52,
            Regrasp = 60,
            Unnamed = 100)

ActionLabel ActionLabelFromString(const std::string &label);

BETTER_ENUM(Failure, short, None = 0,
            Slip = 1, Drop = 2,
            Planning=10,
            Other = 100)

Failure FailureFromString(const std::string &failure);

class ActionObject {

public:
    ActionObject() = default;

    ActionObject(const std::string &specifier);

    std::string str() const;

    bool matches(const std::string &name);

    bool notifyNameChange(const std::string &newName, const std::string &oldName);

    static std::optional<ActionObject> fromString(const std::string &specifier);

    static constexpr const char* SEPARATOR = "::";

    const std::string &getName() const;
    void setName(const std::string &newName);
    const std::string &getNode() const;
    void setNode(const std::string &newNode);

private:
    std::string name;
    std::string node;
};


class MMM_IMPORT_EXPORT ActionLabelAnnotation : public MotionAnnotation
{
public:
    ActionLabelAnnotation(simox::xml::RapidXMLWrapperNodePtr node);                         

    ActionLabelAnnotation(ActionLabel label, const std::string &mainObject = std::string(), const std::string &source = std::string(),
                          const std::string &target = std::string(), const std::string &secondMainObject = std::string(), Failure failure = Failure::None);

    ActionLabelAnnotation(ActionLabel label, const std::optional<ActionObject> &mainObject, const std::optional<ActionObject> &source,
                          const std::optional<ActionObject> &target, const std::optional<ActionObject> &secondMainObject, Failure failure = Failure::None);

    std::string getAnnotation() override;

    MotionAnnotationPtr clone() override;

    void setLabel(const ActionLabel& label);
    const ActionLabel& getLabel() const;
    std::string getLabelStr() const;

    const std::optional<ActionObject>& getMainObject() const;
    std::string getMainObjectStr() const;
    void setMainObject(const std::string &value);
    void setMainObject(const ActionObject &value);

    const std::optional<ActionObject>& getSecondMainObject() const;
    std::string getSecondMainObjectStr() const;
    void setSecondMainObject(const std::string &value);
    void setSecondMainObject(const ActionObject &value);

    const std::optional<ActionObject>& getSource() const;
    std::string getSourceStr() const;
    void setSource(const std::string &value);
    void setSource(const ActionObject &value);

    const std::optional<ActionObject>& getTarget() const;
    std::string getTargetStr() const;
    void setTarget(const std::string &value);
    void setTarget(const ActionObject &value);

    const Failure& getFailure() const;
    std::string getFailureStr(bool noneAsEmpty = true) const;
    void setFailure(const std::string &failure);
    void setFailure(const Failure &failure);
    bool isFailure() const;

    void notifyNameChange(MotionPtr motion, const std::string &oldMotionName) override;

    static constexpr const char* TYPE = "ActionLabel";

protected:
    void append(simox::xml::RapidXMLWrapperNodePtr annotationNode) override;

private:
    ActionLabel label;
    std::optional<ActionObject> mainObject;
    std::optional<ActionObject> secondMainObject;
    std::optional<ActionObject> source;
    std::optional<ActionObject> target;
    Failure failure;
};

}

#endif // __MMM_ACTIONLABELANNOTATION_H_
