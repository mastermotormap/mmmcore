#include "MotionRecording.h"

#include "MMM/Motion/Sensor/BasicModelPoseSensor.h"
#include "Segmentation/MotionRecordingSegment.h"
#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/CollisionDetection/CDManager.h>

#include <limits>

namespace MMM
{


const float MotionRecording::DEFAULT_CONTACT_DISTANCE_TRESHOLD_MM = 10;
const float MotionRecording::DEFAULT_CONTACT_ALLOWED_TIME_DIFFERENCE_SEC = 0.1;

std::string MotionRecording::toXML(const std::filesystem::path &path, bool indent) {
    return getXML(path)->print(indent);
}

void MotionRecording::saveXML(const std::filesystem::path &path, bool indent) {
    getXML(path)->saveToFile(path, indent);
}

void MotionRecording::replaceAndSave(MotionPtr motion, const std::filesystem::path &motionFilePath) {
    replaceMotionByName(motion);
    saveXML(motionFilePath);
}

MotionPtr MotionRecording::getReferenceModelMotion() {
    MotionPtr refMotion = nullptr;
    for (auto motion : motions) {
        if (motion->isReferenceModelMotion()) {
            refMotion = motion;
            break;
        }
    }
    return refMotion;
}

size_t MotionRecording::countRefereceModelMotion() {
    size_t count = 0;
    for (auto motion : motions) {
        if (motion->isReferenceModelMotion()) count++;
    }
    return count;
}

size_t MotionRecording::size() {
    return motions.size();
}

MotionPtr MotionRecording::getMotion(const std::string &motionName) {
    for (auto motion : motions) {
        if (motion->getName() == motionName)
            return motion;
    }
    return nullptr;
}

MotionPtr MotionRecording::getMotion(unsigned int index) {
    if (index < size()) {
        return motions[index];
    }
    else return nullptr;
}

MotionPtr MotionRecording::getMotionThrow(const std::string &motionName) {
    auto motion = getMotion(motionName);
    if (motion) return motion;
        else throw Exception::MMMException("Motion " + motionName + " not included in MotionList " + name);
}

MotionPtr MotionRecording::getMotionByName(MotionPtr motion) {
    return getMotion(motion->getName());
}

bool MotionRecording::contains(const std::string &motionName) {
    return getMotion(motionName) != nullptr;
}

bool MotionRecording::containsMotionByName(MotionPtr motion) {
    return getMotionByName(motion) != nullptr;
}

void MotionRecording::synchronizeSensorMeasurements(float timeFrequency) {
    for (auto motion : motions) {
            motion->synchronizeSensorMeasurements(timeFrequency);
        }
}

bool MotionRecording::empty() {
    return size() == 0;
}

MotionRecordingPtr MotionRecording::motionsWithModel(bool logging) {
    MotionRecordingPtr motionsWithModel(new MotionRecording());
    for (auto motion : motions) {
        if (motion->getModel()) motionsWithModel->addMotion(motion);
        else if (logging) MMM_INFO << "Ignoring motion with name " + motion->getName() + ", because is does not have a model!";
    }
    if (segment) motionsWithModel->segment = segment->clone();
    return motionsWithModel;
}

MotionRecordingPtr MotionRecording::motionsWithSensor(const std::string &type, bool logging) {
    MotionRecordingPtr motionsWithType(new MotionRecording());
    for (auto motion : motions) {
        if (motion->hasSensor(type)) motionsWithType->addMotion(motion);
        else if (logging) MMM_INFO << "Ignoring motion with name " + motion->getName() + ", because is does not have type " + type << std::endl;
    }
    if (segment) motionsWithType->segment = segment->clone();
    return motionsWithType;
}

bool MotionRecording::containsMotionWithAnySensor() {
    for (auto motion : motions) {
        if (motion->hasSensor()) return true;
    }
    return false;
}

bool MotionRecording::containsMotionWithSensor(const std::string &type) {
    for (auto motion : motions) {
        if (motion->hasSensor(type)) return true;
    }
    return false;
}

std::vector<MotionPtr>::iterator MotionRecording::begin() {
    return motions.begin();
}

std::vector<MotionPtr>::iterator MotionRecording::end() {
    return motions.end();
}

MotionPtr MotionRecording::getFirstMotion() {
    if (!empty())
        return motions.at(0);
    else
        return nullptr;
}

void MotionRecording::setName(const std::string &name) {
    this->name = name;
}

std::string MotionRecording::getName() {
    return name;
}

void MotionRecording::setDescription(const std::string &description) {
    this->description = description;
}

std::string MotionRecording::getDescription() {
    return description;
}

MotionRecordingPtr MotionRecording::getMotionRecordingSegment(float startTimestep, float endTimestep, bool changeTimestep) {
    MotionRecordingPtr segment = EmptyRecording();
    for (auto m : motions) {
        segment->addMotion(m->getSegmentMotion(startTimestep, endTimestep, changeTimestep));
    }
    //TODO: segment->segment->clone(nullptr, startTimestep, endTimestep, true, false);
    return segment;
}

MotionRecordingPtr MotionRecording::getReducedMotion(bool changeTimestep) {
    float minTimestep;
    float maxTimestep;
    getCommonTimestepInterval(minTimestep, maxTimestep);
    return getMotionRecordingSegment(minTimestep, maxTimestep, changeTimestep);
}

void MotionRecording::synchronizeMeasurements(float timeFrequency) {
    float minTimestep;
    float maxTimestep;
    getCommonTimestepInterval(minTimestep, maxTimestep);
    for (auto m : motions) {
        m->synchronizeSensorMeasurements(timeFrequency, minTimestep, maxTimestep);
    }
}

MotionRecording::~MotionRecording() {
    for (auto motion : motions) {
        motion->detach(this);
    }
}

MotionRecordingPtr MotionRecording::EmptyRecording() {
    return std::make_shared<MotionRecording>();
}

MotionRecordingPtr MotionRecording::clone(bool cloneSensors) {
    MotionRecordingPtr clonedRecording = cloneConfiguration();
    for (auto m : motions) {
        clonedRecording->addMotion(m->clone(cloneSensors));
    }
    return clonedRecording;
}

MotionRecordingPtr MotionRecording::cloneConfiguration() {
    MotionRecordingPtr clonedRecording = EmptyRecording();
    clonedRecording->name = name;
    clonedRecording->description = description;
    if (segment) clonedRecording->segment = segment->clone();
    return clonedRecording;
}

bool MotionRecording::addMotion(MotionPtr motion) {
    if (containsMotionByName(motion))
        return false;
    add(motion);
    return true;
}

bool MotionRecording::replaceMotionByName(MotionPtr motion) {
    auto old = getMotionByName(motion);
    if (old) {
        remove(old);
        add(motion);
        return true;
    }
    return false;
}

bool MotionRecording::addReplaceMotion(MotionPtr motion) {
    bool replaced = false;
    auto old = getMotionByName(motion);
    if (old) {
        remove(old);
        replaced = true;
    }
    add(motion);
    return replaced;
}

float MotionRecording::getCommonMinTimestep() {
    if (motions.empty()) return 0.0f;
    float min = std::numeric_limits<float>::max();
    for (auto motion : motions) {
        float minMotionTimestep = motion->getMinTimestep();
        if (min < minMotionTimestep) min = minMotionTimestep;
    }
    return min;
}

float MotionRecording::getCommonMaxTimestep() {
    if (motions.empty()) return 0.0f;
    float max = std::numeric_limits<float>::min();
    for (auto motion : motions) {
        float maxMotionTimestep = motion->getMaxTimestep();
        if (max > maxMotionTimestep) max = maxMotionTimestep;
    }
    return max;
}

std::tuple<float, float> MotionRecording::getCommonTimestepInterval() {
    float minTimestep = getCommonMinTimestep();
    float maxTimestep = getCommonMaxTimestep();
    if (minTimestep > maxTimestep + 0.00001) {
        minTimestep = 0.0f;
        maxTimestep = 0.0f;
    }
    return std::tuple<float, float>(minTimestep, maxTimestep);
}

void MotionRecording::getCommonTimestepInterval(float &minTimestep, float &maxTimestep) {
    auto tuple = getCommonTimestepInterval();
    minTimestep = std::get<0>(tuple);
    maxTimestep = std::get<1>(tuple);
}

float MotionRecording::getMinTimestep(MotionPtr ignoreMotion) {
    if (motions.empty()) return 0.0f;
    float min = std::numeric_limits<float>::max();
    for (auto motion : motions) {
        if (ignoreMotion && motion == ignoreMotion) continue;
        float minMotionTimestep = motion->getMinTimestep();
        if (min > minMotionTimestep) min = minMotionTimestep;
    }
    return min;
}

float MotionRecording::getMaxTimestep(MotionPtr ignoreMotion) {
    if (motions.empty()) return 0.0f;
    float max = std::numeric_limits<float>::min();
    for (auto motion : motions) {
        if (ignoreMotion && motion == ignoreMotion) continue;
        float maxMotionTimestep = motion->getMaxTimestep();
        if (max < maxMotionTimestep) max = maxMotionTimestep;
    }
    return max;
}

std::tuple<float, float> MotionRecording::getMinMaxTimesteps(MotionPtr ignoreMotion) {
    float minTimestep = getMinTimestep(ignoreMotion);
    float maxTimestep = getMaxTimestep(ignoreMotion);
    if (minTimestep > maxTimestep + 0.00001) {
        minTimestep = 0.0f;
        maxTimestep = 0.0f;
    }
    return std::tuple<float, float>(minTimestep, maxTimestep);
}

void MotionRecording::getMinMaxTimesteps(float &minTimestep, float &maxTimestep) {
    auto timesteps = getMinMaxTimesteps();
    minTimestep = std::get<0>(timesteps);
    maxTimestep = std::get<1>(timesteps);
}

std::vector<std::string> MotionRecording::getMotionNames() {
    std::vector<std::string> motionNames;
    for (auto motion : motions) {
        motionNames.push_back(motion->getName());
    }
    return motionNames;
}


bool MotionRecording::getReferencePose(Eigen::Matrix4f &referencePose, int &referenceAxis) {
    for (auto motion : motions) {
        if (motion->getReferencePose(referencePose, referenceAxis)) {
            return true;
        }
    }
    return false;
}

MotionRecordingPtr MotionRecording::getMirroredMotionRecording() {
    MotionRecordingPtr mirroredMotionRecording = cloneConfiguration();
    Eigen::Matrix4f referencePose;
    int referenceAxis;
    if (getReferencePose(referencePose, referenceAxis)) {
        for (auto motion : motions) {
            MotionPtr clonedMotion = motion->clone();
            if (clonedMotion->mirrorMotion(referencePose, referenceAxis))
                mirroredMotionRecording->addMotion(clonedMotion);
        }
    }
    // TODO Reversion ! mirroredMotionRecording->segment = segment->clone();
    return mirroredMotionRecording;
}

void MotionRecording::mirrorMotions() {
    Eigen::Matrix4f referencePose;
    int referenceAxis;
    if (getReferencePose(referencePose, referenceAxis)) {
        auto motions = this->motions;
        this->motions.clear();
        for (auto motion : motions) {
            if (motion->mirrorMotion(referencePose, referenceAxis))
                addMotion(motion);
        }
    }
}


std::map<std::string, std::map<float, std::string>>
 MotionRecording::calculateMultiContactChanges(const std::string &motionName, const std::set<std::string> &nodeNames, bool includeChildNodes, float distanceThresholdMM, float allowedTimeDifferenceSec, const std::set<std::string> &environment) {
    std::map<std::string, std::map<float, std::string>> contactChanges;
    auto modelMotionDistances = calculateDistanceMap(motionName, nodeNames, includeChildNodes);
    for (const auto &modelMotionDistance : modelMotionDistances)
        contactChanges[modelMotionDistance.second->motionName] = calculateContactChanges(modelMotionDistance.second, distanceThresholdMM, allowedTimeDifferenceSec, environment);
    return contactChanges;
}

std::map<std::string, std::map<float, std::string>>
 MotionRecording::calculateMultiContacts(const std::string &motionName, const std::set<std::string> &nodeNames, bool includeChildNodes, float distanceThresholdMM, const std::set<std::string> &environment) {
    std::map<std::string, std::map<float, std::string>> contacts;
    auto modelMotionDistances = calculateDistanceMap(motionName, nodeNames, includeChildNodes);
    for (const auto &modelMotionDistance : modelMotionDistances)
        contacts[modelMotionDistance.second->motionName] = calculateContacts(modelMotionDistance.second, distanceThresholdMM, environment);
    return contacts;
}

std::map<float, std::string> MotionRecording::calculateContactChanges(ModelMotionDistancePtr modelMotionDistance, float distanceThresholdMM, float allowedTimeDifferenceSec, const std::set<std::string> &environment) {
    return calculateContactChanges(calculateContacts(modelMotionDistance, distanceThresholdMM, environment), allowedTimeDifferenceSec);
}

std::map<float, std::string> MotionRecording::calculateContactChanges(const std::map<float, std::string> &contacts, float allowedTimeDifferenceSec) {
    std::map<float, std::string> contacts_pruned;
    std::string lastContactName = "";
    float lastContactTimestep = -1;
    for (const auto &c : contacts) {
        float timestep = c.first;
        std::string contactName = c.second;
        if (!contactName.empty()) {
            if (lastContactName != contactName) {
                lastContactName = contactName;
                contacts_pruned[timestep] = contactName;
            }
            else lastContactTimestep = timestep;
        }
        else if (lastContactTimestep >= 0 && timestep - lastContactTimestep > allowedTimeDifferenceSec) {
            contacts_pruned[lastContactTimestep] = "";
            lastContactName = "";
            lastContactTimestep = -1;
        }
    }
    if (lastContactTimestep >= 0 && contacts.end()->first - lastContactTimestep > 0) {
        contacts_pruned[lastContactTimestep] = "";
    }
    return contacts_pruned;
}

std::map<float, std::string> MotionRecording::calculateContacts(ModelMotionDistancePtr modelMotionDistance, float distanceThresholdMM, const std::set<std::string> &environment) {
    std::map<float, std::string> contacts;
    std::map<float, float> minDistances;

    float lastTimestep = -1.0;
    for (float timestep : modelMotionDistance->timesteps) {
        contacts[timestep] = "";
        minDistances[timestep] = std::numeric_limits<float>::max();
        std::tuple<std::string, float> minDistanceEnvironment = std::make_tuple("", std::numeric_limits<float>::max());

        for (auto it = modelMotionDistance->minDistancesToMotions.begin(); it != modelMotionDistance->minDistancesToMotions.end(); it++) {
            std::string motionName = it->first;
            float distance = it->second.at(timestep);

            // environment contacts are second in line
            if (environment.find(motionName) != environment.end()) {
                if (distance < std::get<1>(minDistanceEnvironment) && distance < distanceThresholdMM * 0.5) {
                    minDistanceEnvironment = std::make_tuple(motionName, distance);
                }
                continue;
            }

            if (lastTimestep >= 0.0f &&
                     distance < distanceThresholdMM &&
                     contacts[lastTimestep] == motionName) {
                 // if multiple collisions, choose the same contact as in last frame
                contacts[timestep] = motionName;
                minDistances[timestep] = distance;
                break;
            }
            else if (distance < minDistances[timestep] && distance < distanceThresholdMM) {
                contacts[timestep] = motionName;
                minDistances[timestep] = distance;
            }
        }

        if (contacts[timestep].empty() && !std::get<0>(minDistanceEnvironment).empty()) {
            contacts[timestep] = std::get<0>(minDistanceEnvironment);
            minDistances[timestep] = std::get<1>(minDistanceEnvironment);
        }
        lastTimestep = timestep;
    }

    return contacts;
}

void MotionRecording::setMotionSegment(MotionRecordingSegmentPtr segment) {
    // TODO check
    this->segment = segment;
}

void MotionRecording::add(MotionPtr motion) {
    motions.push_back(motion);
    motion->attach(this);
}

std::vector<MotionPtr>::iterator MotionRecording::remove(MotionPtr motion) {
    motion->detach(this);
    for (auto it = motions.begin(); it != motions.end(); it++) {
        if (*it == motion) {
            return motions.erase(it);
        }
    }
    return motions.end();
}

std::map<std::string, ModelMotionDistancePtr>
MotionRecording::calculateDistanceMap(const std::string &motionName, const std::set<std::string> &nodeNames, bool includeChildNodes) {
    std::map<std::string, ModelMotionDistancePtr> modelMotionDistances;
    auto motion = getMotionThrow(motionName);
    auto timesteps = motion->getTimesteps();
    auto model = motion->getModelCloneWithVisualizationThrow();
    for (auto nodeName : nodeNames) {
        auto node = model->getRobotNode(nodeName);
        if (!node)
            throw Exception::MMMException("Node " + nodeName + " not part of model " + model->getName() + " in motion " + motion->getName());

        ModelMotionDistancePtr modelMotionDistance(new ModelMotionDistance());
        modelMotionDistance->motionName = motion->getName();
        modelMotionDistance->modelNodeName = nodeName;
        modelMotionDistance->includesChildNodes = includeChildNodes;
        modelMotionDistance->timesteps = timesteps;

//        VirtualRobot::CDManagerPtr cdm(new VirtualRobot::CDManager());
//        VirtualRobot::SceneObjectSetPtr collisionNodes(new VirtualRobot::SceneObjectSet());
//        if (includeChildNodes) {
//            std::vector<VirtualRobot::RobotNodePtr> allNodes;
//            node->collectAllRobotNodes(allNodes);
//            for (auto n : allNodes) {
//                collisionNodes->addSceneObject(n);
//            }
//        }
//        else {
//            collisionNodes->addSceneObject(node);
//        }

//        if (collisionNodes->getSize() == 0)
//            throw Exception::MMMException("Node " + nodeName + " of model " + model->getName() + " in motion " + motion->getName() + " contains no collision models!");

        for (const auto &otherMotion : motions) {
            if (motion == otherMotion)
                continue;
            MMM_INFO << "Calculating distances between " + nodeName + (includeChildNodes ? " and subnodes" : "") + " of motion " + motionName
                        + " and motion " + otherMotion->getName() << std::endl;

            auto otherModel = otherMotion->getModelCloneWithVisualization();
            if (!otherModel) {
                MMM_INFO << "Skipping because " + otherMotion->getName() + " has no model" << std::endl;
                continue;
            }

//            VirtualRobot::SceneObjectSetPtr collisionNodes_other(new VirtualRobot::SceneObjectSet());
//            for (auto node : otherModel->getRobotNodes()) {
//                if (!node->getCollisionModel()) continue;
//                else collisionNodes_other->addSceneObject(node);
//            }
//            if (collisionNodes->getSize() == 0) {
//                MMM_INFO << "Skipping because " + otherMotion->getName() + " contains no collision models" << std::endl;
//                continue;
//            }
//            cdm->addCollisionModelPair(collisionNodes, collisionNodes_other);

            for (float timestep : timesteps) {
                motion->initializeModel(model, timestep);
                otherMotion->initializeModel(otherModel, timestep);

                // ____________________________________________________

                VirtualRobot::CDManagerPtr cdm(new VirtualRobot::CDManager());
                VirtualRobot::SceneObjectSetPtr collisionNodes(new VirtualRobot::SceneObjectSet());
                if (includeChildNodes) {
                    std::vector<VirtualRobot::RobotNodePtr> allNodes;
                    node->collectAllRobotNodes(allNodes);
                    for (auto n : allNodes) {
                        collisionNodes->addSceneObject(n);
                    }
                }
                else {
                    collisionNodes->addSceneObject(node);
                }

                if (collisionNodes->getSize() == 0)
                    throw Exception::MMMException("Node " + nodeName + " of model " + model->getName() + " in motion " + motion->getName() + " contains no collision models!");

                VirtualRobot::SceneObjectSetPtr collisionNodes_other(new VirtualRobot::SceneObjectSet());
                for (auto node : otherModel->getRobotNodes()) {
                    if (!node->getCollisionModel()) continue;
                    else collisionNodes_other->addSceneObject(node);
                }
                if (collisionNodes->getSize() == 0) {
                    MMM_INFO << "Skipping because " + otherMotion->getName() + " contains no collision models" << std::endl;
                    continue;
                }
                cdm->addCollisionModelPair(collisionNodes, collisionNodes_other);

                // ---------------------------------------------------------------------------


                float distance = cdm->getDistance();
                if (distance >= 0) {
                    modelMotionDistance->minDistancesToMotions[otherMotion->getName()][timestep] = distance;
                }
            }
        }
        modelMotionDistances[nodeName] = modelMotionDistance;
    }
    return modelMotionDistances;
}

simox::xml::RapidXMLWrapperNodePtr MotionRecording::getXML(const std::filesystem::path &path) {
    if (empty())
        throw Exception::MMMException("Empty motion list");
    simox::xml::RapidXMLWrapperNodePtr root = nullptr;
    for (auto motion : motions) {
        if (!root) root = motion->createMotionRoot();
        motion->appendMotion(root, path);
    }
    if (root) {
        if (!name.empty())
            root->append_attribute("name", name);
        if (!description.empty())
            root->append_attribute("description", description);
        if (segment)
            segment->appendSegmentXML(root);
    }
    return root;
}


bool MotionRecording::isNameChangeAllowed(const std::string &name) {
    return !contains(name);
}

void MotionRecording::notifyNameChange(MotionPtr motion, const std::string &oldName) {
    if (segment)
        segment->notifyNameChange(motion, oldName);
}

void MotionRecording::notifyMeasurementShift(MotionPtr motion, float delta) {
    float minTimestepSensor = motion->getMinTimestep();
    float otherDelta = minTimestepSensor + delta;
    if (otherDelta > 0.0f) {
        for (auto m : motions) {
            if (m != motion)
                m->shiftMeasurements(delta);
        }
        if (segment)
            segment->shiftTimesteps(delta);
    }
    else {
        // Motion should start with positive timestep
        for (auto m : motions) {
            if (m != motion)
                m->shiftMeasurements(-minTimestepSensor);
            else
                m->shiftMeasurements(-otherDelta);
        }
        if (segment)
            segment->shiftTimesteps(-minTimestepSensor);
    }
}

void MotionRecording::notifyIntervalChange(MotionPtr motion) {
    if ((size() == 1 && getMotion(0) == motion) ||
            (getMinTimestep(motion) > motion->getMinTimestep()) ||
            (getMaxTimestep(motion) < motion->getMaxTimestep())) {
        if (segment) {
            segment->updateInterval(getMinTimestep(), getMaxTimestep());
        }
    }
}

}
