#include "XMLTools.h"

#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>
#include "MMM/Exceptions.h"
#include "MMM/MMMCore.h"

namespace MMM::xml
{

simox::xml::RapidXMLWrapperRootNodePtr getRoot(const std::string xml, bool xmlIsPath) {
    try {
        return xmlIsPath ? simox::xml::RapidXMLWrapperRootNode::FromFile(xml) : simox::xml::RapidXMLWrapperRootNode::FromXmlString(xml);
    }
    catch (simox::error::SimoxError &e) {
        throw Exception::MMMException(e.what());
    }
}

bool isValid(const std::filesystem::path &filePath, bool checkPermissions, bool logging) {
    std::error_code errorCode;
    auto status = std::filesystem::status(filePath, errorCode);
    if (!errorCode && std::filesystem::exists(status)) {
        /*if (checkPermissions) {
            auto permissions = status.permissions();
            return ((permissions & std::filesystem::perms::owner_read) != std::filesystem::perms::none &&
                    (permissions & std::filesystem::perms::group_read) != std::filesystem::perms::none &&
                    (permissions & std::filesystem::perms::others_read) != std::filesystem::perms::none);
        }
        else */return true;
    }
    if (logging) MMM_ERROR << "Error loading " << filePath << "! " << errorCode.message() << std::endl;
    return false;
}

}
