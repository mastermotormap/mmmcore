#include "MotionType.h"

#include <set>
#include <SimoxUtility/algorithm/string/string_conversion.h>

namespace MMM
{

MotionType getMotionTypeFromModel(const std::string &modelName) {
    if (modelName.empty()) return MotionType::UNKNOWN; // no model
    // The following helper code should be loaded from some .txt file
    std::string help = simox::alg::to_lower(modelName);
    std::set<std::string> mmmNames = {"mmm"};
    std::set<std::string> humanNames = {"lhand", "rhand"};
    std::set<std::string> exoNames = {"prosthesis"};
    std::set<std::string> robotNames = {"armar3", "armar4", "armar6", "hrp4", "kit_gripper", "kuka_lwr", "youbot"};
    std::set<std::string> envObjects = {"barricade", "beam", "floor", "slope", "stairs", "table"};
    if (mmmNames.find(help) != mmmNames.end()) return MotionType::MMM;
    else if (humanNames.find(help) != humanNames.end()) return MotionType::HUMAN;
    else if (robotNames.find(help) != robotNames.end()) return MotionType::ROBOT;
    else if (exoNames.find(help) != exoNames.end()) return MotionType::EXOSKELETON;
    else if (envObjects.find(help) != envObjects.end()) return MotionType::ENVIRONMENT;
    else return MotionType::OBJECT;
}

}

