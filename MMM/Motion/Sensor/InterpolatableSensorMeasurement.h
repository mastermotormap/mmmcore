/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2018 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_INTERPOLATABLESENSORMEASUREMENT_H_
#define __MMM_INTERPOLATABLESENSORMEASUREMENT_H_

#include "SensorMeasurement.h"

namespace MMM
{

template <typename M>
/*! \brief Template for interpolatable sensor measurements */
class MMM_IMPORT_EXPORT InterpolatableSensorMeasurement : public SensorMeasurement
{
protected:
    InterpolatableSensorMeasurement(float timestep, SensorMeasurementType type = SensorMeasurementType::MEASURED) :
        SensorMeasurement(timestep, type)
    {
    }

    virtual std::shared_ptr<M> interpolate(std::shared_ptr<M> other, float timestep) = 0;

public:
    virtual ~InterpolatableSensorMeasurement() = default;

    std::shared_ptr<M> interpolateMeasurement(std::shared_ptr<M> other, float timestep) {
        auto interpolatedMeasurement = interpolate(other, timestep);
        if (type == +SensorMeasurementType::EXTENDED || other->type == +SensorMeasurementType::EXTENDED)
            interpolatedMeasurement->type = SensorMeasurementType::EXTENDED;
        return interpolatedMeasurement;
    }

    //! Linear Interpolation of Vector3f
    static Eigen::Vector3f linearInterpolation(const Eigen::Vector3f &value1, float v1_timestep, const Eigen::Vector3f &value2, float v2_timestep, float timestep)  {
        Eigen::Vector3f interpolatedVector;
        for (int i = 0; i < 3; i++) {
            interpolatedVector(i) = linearInterpolation(value1(i), v1_timestep, value2(i), v2_timestep, timestep);
        }
        return interpolatedVector;
    }

    //! Linear Angle Interpolation of Vector3f
    static Eigen::Vector3f linearAngleInterpolation(const Eigen::Vector3f &value1, float v1_timestep, const Eigen::Vector3f &value2, float v2_timestep, float timestep)  {
        Eigen::Vector3f interpolatedVector;
        for (int i = 0; i < 3; i++) {
            if (std::abs(value1(i) - value2(i)) > 6 && std::abs(v1_timestep - v2_timestep) < 0.100001) {
                if (std::abs(value1(i)) > std::abs(value2(i))) {
                    if (value1(i) > value2(i))
                        interpolatedVector(i) = linearInterpolation(value1(i) - 2*M_PI, v1_timestep, value2(i), v2_timestep, timestep);
                    else
                        interpolatedVector(i) = linearInterpolation(value1(i) + 2*M_PI, v1_timestep, value2(i), v2_timestep, timestep);
                } else {
                    if (value1(i) > value2(i))
                        interpolatedVector(i) = linearInterpolation(value1(i), v1_timestep, value2(i) + 2*M_PI, v2_timestep, timestep);
                    else
                        interpolatedVector(i) = linearInterpolation(value1(i), v1_timestep, value2(i) - 2*M_PI, v2_timestep, timestep);
                }
            }
            else interpolatedVector(i) = linearInterpolation(value1(i), v1_timestep, value2(i), v2_timestep, timestep);
        }
        return interpolatedVector;
    }

    //! Linear Interpolation of float values
    static float linearInterpolation(float value1, float v1_timestep, float value2, float v2_timestep, float timestep) {
        return value1 + (value2 - value1) / (v2_timestep - v1_timestep) * (timestep - v1_timestep);
    }
};

}

#endif // __MMM_INTERPOLATABLESENSORMEASUREMENT_H_
