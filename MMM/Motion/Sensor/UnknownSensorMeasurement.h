/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_UNKNOWNSENSORMEASUREMENT_H_
#define __MMM_UNKNOWNSENSORMEASUREMENT_H_

#include "MMM/MMMCore.h"
#include "MMM/Enum.h"
#include "SensorMeasurement.h"
#include <map>

namespace MMM
{


class UnknownSensorMeasurement;

typedef std::shared_ptr<UnknownSensorMeasurement> UnknownSensorMeasurementPtr;

/*! \brief Models unknown sensor measurements loaded from XML. */
class MMM_IMPORT_EXPORT UnknownSensorMeasurement : public SensorMeasurement, SMCloneable<UnknownSensorMeasurement>
{
public:
    UnknownSensorMeasurement(float timestep, SensorMeasurementType type = SensorMeasurementType::MEASURED);

    UnknownSensorMeasurement(float timestep, simox::xml::RapidXMLWrapperNodePtr measurementNode, SensorMeasurementType type = SensorMeasurementType::MEASURED);

    virtual ~UnknownSensorMeasurement() = default;

    SensorMeasurementPtr clone();

    UnknownSensorMeasurementPtr clone(float newTimestep);

    bool equals(SensorMeasurementPtr sensorMeasurement);

    simox::xml::RapidXMLWrapperNodePtr getUnknownSensorMeasurementNode();

protected:
    void appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode);

    simox::xml::RapidXMLWrapperNodePtr node;
};

}
#endif // __MMM_UNKNOWNSENSORMEASUREMENT_H_

