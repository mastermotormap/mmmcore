#include "UnknownSensor.h"

#include "UnknownSensorMeasurement.h"

#include <VirtualRobot/Robot.h>
#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>
#include "MMM/Motion/XMLTools.h"

namespace MMM
{

UnknownSensorPtr UnknownSensor::loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    UnknownSensorPtr sensor(new UnknownSensor());
    sensor->loadSensor(node, filePath);
    return sensor;
}

bool UnknownSensor::checkModel(ModelPtr /*model*/) {
    return true;
}

std::shared_ptr<BasicSensor<UnknownSensorMeasurement> > UnknownSensor::cloneConfiguration() {
    return std::shared_ptr<UnknownSensor>(new UnknownSensor(node, description));
}

bool UnknownSensor::equalsConfiguration(SensorPtr other) {
    std::shared_ptr<UnknownSensor> ptr = std::dynamic_pointer_cast<UnknownSensor>(other);
    if (ptr) {
        return node == ptr->node;
    }
    else return false;

}

simox::xml::RapidXMLWrapperNodePtr UnknownSensor::getUnknownSensorConfigurationNode() {
    return node;
}

std::string UnknownSensor::getType() {
    return TYPE;
}

std::string UnknownSensor::getUnknownType() {
    return unknownType;
}

std::string UnknownSensor::getVersion() {
    return VERSION;
}

int UnknownSensor::getPriority() {
    return 0;
}

UnknownSensor::UnknownSensor(const std::string &description) : UnknownSensor(nullptr, description)
{
}

UnknownSensor::UnknownSensor(simox::xml::RapidXMLWrapperNodePtr unknownNode, const std::string &description) :
    BasicSensor(description),
    node(unknownNode)
{
}

void UnknownSensor::loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    assert(node);
    assert(node->name() == "Configuration");

    this->node = node;
}

void UnknownSensor::loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type) {
    assert(node);
    assert(node->name() == xml::tag::MEASUREMENT);

    UnknownSensorMeasurementPtr m(new UnknownSensorMeasurement(timestep, node, type));
    addSensorMeasurement(m);
}

void UnknownSensor::appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    for (auto configNodes : getUnknownSensorConfigurationNode()->nodes()) {
        node->append_node(*configNodes.get());
    }
}

bool UnknownSensor::hasSensorConfigurationContent() const {
    return true;
}

void UnknownSensor::loadSensor(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    Sensor::loadSensor(node, filePath);
    unknownType = node->attribute_value(xml::attribute::TYPE);
}

void UnknownSensor::appendTypeXML(simox::xml::RapidXMLWrapperNodePtr node) {
    node->append_attribute(xml::attribute::TYPE, getUnknownType());
}

}

