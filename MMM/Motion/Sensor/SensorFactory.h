/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SENSORFACTORY_H_
#define __MMM_SENSORFACTORY_H_

#include "MMM/AbstractFactoryMethod.h"

namespace MMM
{

class MMM_IMPORT_EXPORT SensorFactory : public AbstractFactoryMethod<SensorFactory, void*>
{
public:
    SensorFactory() { }

    virtual ~SensorFactory() { }

    /*! Creates a sensor from an xml representation.
        @param sensorNode The sensor xml node. */
    virtual SensorPtr createSensor(simox::xml::RapidXMLWrapperNodePtr sensorNode, const std::filesystem::path &filePath) = 0;

    /*! Creates sensors from a legacy motion if possible. */
    virtual std::vector<SensorPtr> createSensors(LegacyMotionPtr /*motion*/) {
        return std::vector<SensorPtr>();
    }

    /*! Returns the sensor type as name of the factory. */
    virtual std::string getName() = 0;

    static constexpr const char* VERSION = "1.0";

    static std::filesystem::path getStandardLibPath() {
        std::filesystem::path standardLibDir(std::string(SENSOR_PLUGIN_LIB_DIR));
        if (!is_directory(standardLibDir)) return "/usr/lib/MMMSensorPlugins/"; // MMMCore is installed on system
        return standardLibDir;
    }
};

typedef std::shared_ptr<SensorFactory> SensorFactoryPtr;

template<class Sensor>
std::string NAME_STR(){
    return std::string(Sensor::TYPE) + "_v" + std::string(Sensor::VERSION);
}

}

#endif
