﻿/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_BASICKINEMATICSENSOR_H_
#define __MMM_BASICKINEMATICSENSOR_H_

#include "MMM/MMMCore.h"
#include "Sensor.h"

namespace MMM
{
class MMM_IMPORT_EXPORT BasicKinematicSensor : public virtual Sensor
{
public:
    virtual ~BasicKinematicSensor() = default;

    virtual std::vector<std::string> getJointNames() = 0;

    virtual Eigen::VectorXf getJointAngles(float timestep) = 0;

    virtual std::map<std::string, float> getJointAngleMap(float timestep) = 0;

    virtual std::map<float, float> getJointValues(const std::string &jointName) = 0;

    virtual int getJointIndex(const std::string &jointName) = 0;

    virtual void setJointOffset(const std::string &jointName, float offset) = 0;

    virtual Eigen::VectorXf getJointOffset() = 0;

    virtual bool hasJoint(const std::string &jointName) = 0;

    virtual bool removeJoint(const std::string &name) = 0;

    static constexpr const char* TYPE = "Kinematic";
};

typedef std::shared_ptr<BasicKinematicSensor> BasicKinematicSensorPtr;

}

#endif // __MMM_BASICKINEMATICSENSOR_H_
