/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_UNKNOWNSENSOR_H_
#define __MMM_UNKNOWNSENSOR_H_

#include "MMM/MMMCore.h"

#include "BasicSensor.h"
#include "UnknownSensorMeasurement.h"

#include <string>
#include <vector>
#include <map>

namespace MMM
{

class UnknownSensor;

typedef std::shared_ptr<UnknownSensor> UnknownSensorPtr;

/*! \brief Models unknown sensor types loaded from XML */
class MMM_IMPORT_EXPORT UnknownSensor : public BasicSensor<UnknownSensorMeasurement>
{
public:
    static UnknownSensorPtr loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    virtual ~UnknownSensor() = default;

    bool checkModel(ModelPtr model);

    std::shared_ptr<BasicSensor<UnknownSensorMeasurement> > cloneConfiguration();

    bool equalsConfiguration(SensorPtr other);

    simox::xml::RapidXMLWrapperNodePtr getUnknownSensorConfigurationNode();

    std::string getType();

    std::string getUnknownType();

    std::string getVersion();

    int getPriority();

    static constexpr const char* TYPE = "Unknown";

    static constexpr const char* VERSION = "1.0";

protected:
    UnknownSensor(const std::string &description = std::string());

    UnknownSensor(simox::xml::RapidXMLWrapperNodePtr unknownNode, const std::string &description = std::string());

    void loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    void loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type);

    void appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    bool hasSensorConfigurationContent() const;

    void loadSensor(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) override;

    void appendTypeXML(simox::xml::RapidXMLWrapperNodePtr node) override;

    simox::xml::RapidXMLWrapperNodePtr node;

    std::string unknownType;
};

}

#endif // __MMM_UNKNOWNSENSOR_H_
