/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2018 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_ATTACHEDSENSOR_H_
#define __MMM_ATTACHEDSENSOR_H_

#include "InterpolatableSensor.h"
#include "MMM/Exceptions.h"


namespace MMM {

class IAttachedSensor;

typedef std::shared_ptr<IAttachedSensor> IAttachedSensorPtr;

class MMM_IMPORT_EXPORT IAttachedSensor {
public:
    virtual ~IAttachedSensor() = default;

    static void loadSensorConfiguration(IAttachedSensorPtr attachedSensor, simox::xml::RapidXMLWrapperNodePtr configurationNode, const std::filesystem::path &filePath) {
        attachedSensor->loadConfigurationXML(configurationNode, filePath);
    }

    void saveSensorConfiguration(const std::string &filePath) {
        simox::xml::RapidXMLWrapperRootNodePtr root = simox::xml::RapidXMLWrapperRootNode::createRootNode("Configuration");
        appendConfigurationElementsXML(root);
        root->saveToFile(filePath, true);
    }

    virtual void loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) = 0;

    void setSegment(const std::string &segment) {
        this->segment = segment;
    }

    std::string getSegment() {
        return segment;
    }

    void setOffsetUnit(const std::string &offsetUnit) {
        getUnitScalingToCM(offsetUnit); // just to check if correct unit
        this->offsetUnit = offsetUnit;
    }

    std::string getOffsetUnit() {
        return offsetUnit;
    }

    void setOffset(const Eigen::Matrix4f &offset) {
        this->offset = offset;
    }

    Eigen::Matrix4f getOffset() {
        return offset;
    }

    //! Returns the offset transformation matrix in centi meter
    Eigen::Matrix4f getOffsetInCM() const {
        Eigen::Matrix4f offsetCM = offset;
        float scaling = getUnitScalingToCM(offsetUnit);
        offsetCM(0,3) *= scaling;
        offsetCM(1,3) *= scaling;
        offsetCM(2,3) *= scaling;
        return offsetCM;
    }

    void appendConfigurationElementsXML(simox::xml::RapidXMLWrapperNodePtr configurationNode) {
        configurationNode->append_node("Segment")->append_attribute("name", segment);
        simox::xml::RapidXMLWrapperNodePtr offsetNode = configurationNode->append_node("Offset");
        offsetNode->append_attribute("unit", offsetUnit);
        offsetNode->append_data_node(offset, "Matrix4x4");
    }

    Eigen::Matrix4f getGlobalPose(ModelPtr model);

protected:
    IAttachedSensor()
    {
    }

    IAttachedSensor(const std::string &segment, const std::string &offsetUnit = "mm", const Eigen::Matrix4f &offset = Eigen::Matrix4f::Identity()) :
        segment(segment),
        offsetUnit(offsetUnit),
        offset(offset)
    {
    }

    float getUnitScalingToCM(const std::string &unit) const {
        std::string v = simox::alg::to_lower(unit);
        if (v=="mm" || v=="millimeter")             return 0.1f;
        else if (v == "cm" || v == "centimeter")    return 1.0f;
        else if (v == "dm" || v == "decimeter")     return 10.0f;
        else if (v == "m" || v == "meter")          return 100.0f;
        else if (v == "km" || v == "kilometer")     return 100000.0f;
        else throw Exception::MMMException(unit + " no valid unit for offset transformation matrix!");
    }

    std::string segment;
    std::string offsetUnit;
    Eigen::Matrix4f offset;
};

template <typename M>
class MMM_IMPORT_EXPORT AttachedSensor : public virtual InterpolatableSensor<M>, public IAttachedSensor {
public:
    virtual ~AttachedSensor() = default;

    bool checkModel(ModelPtr model) override {
        return hasNode(model, segment);
    }

    bool equalsConfiguration(SensorPtr other) override {
        std::shared_ptr<AttachedSensor<M> > ptr = std::dynamic_pointer_cast<AttachedSensor<M>>(other);
        if (ptr) {
            return getSegment() == ptr->getSegment() && getOffset() == ptr->getOffset() && getOffsetUnit() == ptr->getOffsetUnit();
        }
        else return false;
    }

    bool hasSensorConfigurationContent() const override {
        return true;
    }

protected:
    AttachedSensor() : InterpolatableSensor<M>(), IAttachedSensor()
    {
    }

    AttachedSensor(const std::string &segment, const std::string &offsetUnit = "mm", const Eigen::Matrix4f &offset = Eigen::Matrix4f::Identity(), const std::string &description = std::string()) :
        InterpolatableSensor<M>(description),
        IAttachedSensor(segment, offsetUnit, offset)
    {
    }

    void loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &/*filePath*/) override {
        assert(node->name() == "Configuration");

        segment = node->first_node("Segment")->attribute_value("name");
        if (node->has_node("Offset")) {
            simox::xml::RapidXMLWrapperNodePtr offsetNode = node->first_node("Offset");
            setOffsetUnit(offsetNode->attribute_value("unit"));
            setOffset(offsetNode->first_node("Matrix4x4")->value_matrix4f());
        } else {
            setOffsetUnit("mm");
            setOffset(Eigen::Matrix4f::Identity());
        }
    }

    void appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &/*filePath*/) override {
        appendConfigurationElementsXML(node);
    }

};

}

#endif // __MMM_ATTACHEDSENSOR_H_
