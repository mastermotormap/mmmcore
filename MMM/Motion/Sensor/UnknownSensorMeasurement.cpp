#include "UnknownSensorMeasurement.h"

#include "MMM/Motion/XMLTools.h"
#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>

namespace MMM
{

SensorMeasurementPtr UnknownSensorMeasurement::clone() {
    return clone(timestep);
}

UnknownSensorMeasurementPtr UnknownSensorMeasurement::clone(float newTimestep) {
    return UnknownSensorMeasurementPtr(new UnknownSensorMeasurement(newTimestep, node, type));
}

bool UnknownSensorMeasurement::equals(SensorMeasurementPtr sensorMeasurement) {
    UnknownSensorMeasurementPtr ptr = std::dynamic_pointer_cast<UnknownSensorMeasurement>(sensorMeasurement);
    if (ptr) {
        return node == ptr->node;
    }
    return false;
}

simox::xml::RapidXMLWrapperNodePtr UnknownSensorMeasurement::getUnknownSensorMeasurementNode() {
    return node;
}

UnknownSensorMeasurement::UnknownSensorMeasurement(float timestep, SensorMeasurementType type)
    : UnknownSensorMeasurement(timestep, nullptr, type)
{
}

UnknownSensorMeasurement::UnknownSensorMeasurement(float timestep, simox::xml::RapidXMLWrapperNodePtr measurementNode, SensorMeasurementType type)
    : SensorMeasurement(timestep, type), node(measurementNode)
{
}

void UnknownSensorMeasurement::appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode) {
    if (node)
        for (auto &n : node->nodes())
            measurementNode->append_node(*n.get());
}

}
