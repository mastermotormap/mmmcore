#include "AttachedSensor.h"

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Nodes/RobotNode.h>

namespace MMM
{

Eigen::Matrix4f IAttachedSensor::getGlobalPose(ModelPtr model) {
    if (model->hasRobotNode(segment)) {
        auto node = model->getRobotNode(segment);
        return node->getGlobalPose(offset);
    }
    else throw MMM::Exception::MMMException("Segment " + segment + " not part of model " + model->getName());
}

}
