#include "Sensor.h"

#include "SensorMeasurement.h"

#include <VirtualRobot/Robot.h>
#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>
#include "MMM/Motion/XMLTools.h"
#include "UnknownSensor.h"

namespace MMM
{

std::function<bool (std::filesystem::path &)> Sensor::handleSensorFilePath = NULL;

bool hasNode(ModelPtr model, const std::string &nodeName) {
    if (!model) {
        MMM_ERROR << "Attached sensor at segment " + nodeName + " does not contain a model!"  << std::endl;
        return false;
    }
    return model->hasRobotNode(nodeName);
}

Sensor::Sensor(const std::string &description) :
    description(description)
{
}

void Sensor::appendTypeXML(simox::xml::RapidXMLWrapperNodePtr node) {
    node->append_attribute(xml::attribute::TYPE, getType());
}

std::string Sensor::getName() {
    return name;
}

void Sensor::setName(const std::string &name) {
    this->name = name;
}

std::string Sensor::getUniqueName() {
    return uniqueName;
}

void Sensor::setUniqueName(const std::string &uniqueName) {
    this->uniqueName = uniqueName;
}

std::string Sensor::getDescription() {
    return description;
}

void Sensor::setDescription(const std::string &description) {
    this->description = description;
}

bool Sensor::isInterpolatable() {
    return false;
}

int Sensor::getPriority() {
    return 0;
}

SensorPtr Sensor::join(SensorPtr sensor1, SensorPtr sensor2) {
    return sensor1->joinSensor(sensor2);
}

void Sensor::synchronizeSensorMeasurements(float timeFrequency) {
    synchronizeSensorMeasurements(timeFrequency, getMinTimestep(), getMaxTimestep());
}

void Sensor::synchronizeSensorMeasurements(float /*timeFrequency*/, float /*minTimestep*/, float /*maxTimestep*/) {
    // do nothing - implementation optional for subsensor
}

void Sensor::scaleMotionData(float /*scaleFactor*/) {
    // Do nothing - implementation optional for subsensor
}

void Sensor::shiftMotionData(const Eigen::Vector3f &/*positionDelta*/) {
    // Do nothing - implementation optional for subsensor
}

bool Sensor::isMirrorSupported(MotionType /*type*/) {
    // Do nothing - implementation optional for subsensor
    return true;
}

void Sensor::mirrorMotionData(const Eigen::Matrix4f &/*referencePose*/, int /*referenceAxis*/) {
    // Do nothing - implementation optional for subsensor
}

std::vector<std::string> Sensor::getSensorSpecificInformation(const std::string &/*name*/)
{
    return std::vector<std::string>(); // Return "nothing" - implementation optional for subsensor
}

void Sensor::initializeModel(ModelPtr model, float timestep, bool extend, bool update) {
    SensorMeasurementPtr measurement = nullptr;
    if (extend && timestep > getMaxTimestep()) {
        measurement = getMeasurement(getMaxTimestep());
    }
    if (extend && timestep < getMinTimestep()) {
        measurement = getMeasurement(getMinTimestep());
    }
    else {
        measurement = getMeasurement(timestep);
    }

    if (measurement)
        measurement->initializeModel(model, update);
}

void Sensor::initializeModel(ModelPtr model, float timestep, float delta, bool extend, bool update) {
    SensorMeasurementPtr measurement = nullptr;
    if (extend && timestep > getMaxTimestep()) {
        measurement = getMeasurement(getMaxTimestep());
    }
    if (extend && timestep < getMinTimestep()) {
        measurement = getMeasurement(getMinTimestep());
    }
    else {
        measurement = getMeasurement(timestep, delta);
    }

    if (measurement)
        measurement->initializeModel(model, update);
}

void Sensor::appendSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath)
{
    assert(node);

    if(!hasSensorConfigurationContent()) {
        return;
    }

    simox::xml::RapidXMLWrapperNodePtr sensorNode = node->append_node(xml::tag::SENSOR);
    appendTypeXML(sensorNode);
    sensorNode->append_attribute(xml::attribute::VERSION, getVersion());
    auto name = getName();
    if (!name.empty()) {
        sensorNode->append_attribute(xml::attribute::NAME, name);
    }
    auto description = getDescription();
    if (!description.empty()) {
        sensorNode->append_attribute(xml::attribute::DESCRIPTION, description);
    }
    appendConfigurationXML(sensorNode->append_node(xml::tag::CONFIGURATION), filePath);
    appendDataXML(sensorNode);
}

void Sensor::loadSensor(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath)
{
    assert(node);
    assert(node->name() == xml::tag::SENSOR);
    if (getType() != UnknownSensor::TYPE)
        assert(node->attribute_value(xml::attribute::TYPE) == getType());

    this->name = node->attribute_value_or_default(xml::attribute::NAME, std::string());
    this->description = node->attribute_value_or_default(xml::attribute::DESCRIPTION, std::string());

    loadConfigurationXML(node->first_node(xml::tag::CONFIGURATION), filePath);

    if (!node->has_node(xml::tag::DATA)) return;
    auto dataNode = node->first_node(xml::tag::DATA);
    if (dataNode->has_node(xml::tag::MEASUREMENT))
        for (auto measurementNode : dataNode->nodes(xml::tag::MEASUREMENT))
            loadMeasurementNodeXML(measurementNode);
}

}

