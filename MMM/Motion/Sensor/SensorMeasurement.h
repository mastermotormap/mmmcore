/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SENSORMEASUREMENT_H_
#define __MMM_SENSORMEASUREMENT_H_

#include "MMM/MMMCore.h"
#include "MMM/Enum.h"
#include <map>

namespace MMM
{

BETTER_ENUM(SensorMeasurementType, int, MEASURED = 1, INTERPOLATED = 2, EXTENDED = 3, UNKNOWN = 4)

/*! \brief The public interface for the sensor measurement classes. */
class MMM_IMPORT_EXPORT SensorMeasurement
{
public:
    virtual ~SensorMeasurement() = default;

    /*! Appends this sensor measurement as an xml representation to a given xml node.
        @param node The xml node.*/
    virtual void appendDataXML(simox::xml::RapidXMLWrapperNodePtr node);

    /*! Returns a copy of this sensor measurement. */
    virtual SensorMeasurementPtr clone() = 0;

    /*! Checks if specific sensor measurement values are equal. (not checking timestep or interpolated) */
    virtual bool equals(SensorMeasurementPtr sensorMeasurement) = 0;

    /*! Returns an xml string representation of this sensor measurement. */
    std::string toXMLString();

    float getTimestep();

    bool isInterpolated();

    bool isExtended();

    void setType(SensorMeasurementType type);

    SensorMeasurementType getType();

    virtual void initializeModel(ModelPtr /*model*/, bool /*update*/) {
        // Do nothing (not required for every sensor)
    }

    template <typename Map>
    static bool compare(Map const &m1, Map const &m2) {
        auto pred = [] (decltype(*m1.begin()) a, decltype(a) b)
                           { return a.first == b.first && a.second == b.second; };

            return m1.size() == m2.size()
                && std::equal(m1.begin(), m1.end(), m2.begin(), pred);
    }

    template <typename M, typename std::enable_if<std::is_base_of<SensorMeasurement, M>::value>::type* = nullptr>
    static std::shared_ptr<M> getDerivedMeasurement(const std::map<float, std::shared_ptr<M>> &measurements, float timestep, float delta = 0.0001) {
        if (measurements.find(timestep) != measurements.end())
            return measurements.at(timestep);
        else {
            auto it = measurements.lower_bound(timestep);
            if (it != measurements.end()) {
                if (it != measurements.begin()) {
                    auto prev = std::prev(it);
                    float delta1 = std::abs(it->first - timestep);
                    float delta2 = std::abs(prev->first - timestep);
                    std::shared_ptr<M> measurement = (delta1 < delta2) ? it->second : prev->second;
                    if (std::abs(measurement->getTimestep() - timestep) < delta + 0.000001) return measurement;

                }
            }
        }
        return nullptr;
    }

protected:
    SensorMeasurement(float timestep, SensorMeasurementType type = SensorMeasurementType::MEASURED);

    virtual void appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode) = 0;

    /*! Timestep of the sensor measurement.*/
    float timestep;

    /*! Wheter this sensor measurement is measured value, interpolated or artifically extended. */
    SensorMeasurementType type;
};

template <typename T>
class SMCloneable
{
public:
    virtual std::shared_ptr<T> clone(float newTimestep) = 0;
};

}
#endif // __MMM_SENSORMEASUREMENT_H_

