#include "SensorMeasurement.h"

#include "MMM/Motion/XMLTools.h"
#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>

namespace MMM
{

SensorMeasurement::SensorMeasurement(float timestep, SensorMeasurementType type):
    timestep(timestep),
    type(type)
{
}

void SensorMeasurement::appendDataXML(simox::xml::RapidXMLWrapperNodePtr node) {
    simox::xml::RapidXMLWrapperNodePtr measurementNode = node->append_attribute(xml::attribute::TIMESTEP, timestep);
    if (type != +SensorMeasurementType::MEASURED) measurementNode->append_attribute(xml::attribute::TYPE, simox::alg::to_lower(type._to_string()));
    appendMeasurementDataXML(measurementNode);
}

std::string SensorMeasurement::toXMLString() {
    auto measurementNode = simox::xml::RapidXMLWrapperRootNode::createRootNode(xml::tag::MEASUREMENT);
    appendDataXML(measurementNode);
    return measurementNode->print(true);
}

float SensorMeasurement::getTimestep() {
    return timestep;
}

bool SensorMeasurement::isInterpolated() {
    return type == +SensorMeasurementType::INTERPOLATED;
}

bool SensorMeasurement::isExtended() {
    return type._to_integral() == SensorMeasurementType::EXTENDED;
}

SensorMeasurementType SensorMeasurement::getType() {
    return type;
}

void SensorMeasurement::setType(SensorMeasurementType type) {
    this->type = type;
}

}
