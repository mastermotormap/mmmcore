﻿/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_BASICMODELPOSESENSOR_H_
#define __MMM_BASICMODELPOSESENSOR_H_

#include "MMM/MMMCore.h"
#include "Sensor.h"

namespace MMM
{

class MMM_IMPORT_EXPORT BasicModelPoseSensor : public virtual Sensor
{
public:
    virtual ~BasicModelPoseSensor() = default;

    virtual Eigen::Matrix4f getRootPose(float timestep, bool extend = false) = 0;

    virtual std::map<float, float> getPositions(unsigned int index) = 0;

    virtual std::map<float, float> getAngles(unsigned int index) = 0;

    virtual std::map<float, Eigen::Matrix4f> getRootPoses() = 0;

    virtual void setOffset(const Eigen::Matrix4f &offset) = 0;

    virtual void transform(const Eigen::Matrix4f &offsetTransformation) = 0;

    virtual Eigen::Matrix4f getOffset() = 0;

    /**
     * @brief setFixPose Sets all measurements to a fixed pose
     * @param rootPose The new fixed pose
     * @param reduceMeasurements Reduces the measurements to minimum and maximum timestep
     */
    virtual void setFixedRootPose(const Eigen::MatrixXf &rootPose, bool reduceMeasurements = true) = 0;

    static constexpr const char* TYPE = "ModelPose";
};

typedef std::shared_ptr<BasicModelPoseSensor> BasicModelPoseSensorPtr;

}

#endif // __MMM_BASICMODELPOSESENSOR_H_
