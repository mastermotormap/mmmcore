#include <cassert>
#include <map>

#include "WholeBodyDynamicSensor.h"

namespace MMM
{

WholeBodyDynamicSensorPtr WholeBodyDynamicSensor::loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    WholeBodyDynamicSensorPtr sensor(new WholeBodyDynamicSensor());
    sensor->loadSensor(node, filePath);
    return sensor;
}

WholeBodyDynamicSensor::WholeBodyDynamicSensor(const std::string &description) : InterpolatableSensor(description)
{
}

bool WholeBodyDynamicSensor::checkModel(ModelPtr model) {
    return true;
}

std::shared_ptr<BasicSensor<WholeBodyDynamicSensorMeasurement> > WholeBodyDynamicSensor::cloneConfiguration() {
    WholeBodyDynamicSensorPtr m(new WholeBodyDynamicSensor(description));
    return m;
}

void WholeBodyDynamicSensor::loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    assert(node);
    assert(node->name() == "Configuration");
}

void WholeBodyDynamicSensor::loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type)
{
    assert(node);
    assert(node->name() == xml::tag::MEASUREMENT);

    simox::xml::RapidXMLWrapperNodePtr comNode = node->first_node("CenterOfMass");
    simox::xml::RapidXMLWrapperNodePtr amNode = node->first_node("AngularMomentum");
    WholeBodyDynamicSensorMeasurementPtr m(new WholeBodyDynamicSensorMeasurement(timestep, comNode->value_eigen_vec3(), amNode->value_eigen_vec3(), type));
    addSensorMeasurement(m);
}

void WholeBodyDynamicSensor::appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr /*node*/, const std::filesystem::path &filePath) {
    // Do nothing
}


bool WholeBodyDynamicSensor::hasSensorConfigurationContent() const
{
    return true; // TODO
}

bool WholeBodyDynamicSensor::equalsConfiguration(SensorPtr other) {
    WholeBodyDynamicSensorPtr ptr = std::dynamic_pointer_cast<WholeBodyDynamicSensor>(other);
    if (ptr) {
        return true;
    }
    else return false;
}

std::string WholeBodyDynamicSensor::getType() {
    return TYPE;
}

std::string WholeBodyDynamicSensor::getVersion() {
    return VERSION;
}

int WholeBodyDynamicSensor::getPriority() {
    return 80;
}

}

