/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_WHOLEBODYDYNAMICSENSORMEASUREMENT_H_
#define __MMM_WHOLEBODYDYNAMICSENSORMEASUREMENT_H_

#include <MMM/Motion/Sensor/SensorMeasurement.h>
#include <MMM/Motion/Sensor/InterpolatableSensorMeasurement.h>

#include <Eigen/Core>

namespace MMM
{
class WholeBodyDynamicSensorMeasurement;

typedef std::shared_ptr<WholeBodyDynamicSensorMeasurement> WholeBodyDynamicSensorMeasurementPtr;

class MMM_IMPORT_EXPORT WholeBodyDynamicSensorMeasurement : public InterpolatableSensorMeasurement<WholeBodyDynamicSensorMeasurement>, SMCloneable<WholeBodyDynamicSensorMeasurement>
{

public:
    WholeBodyDynamicSensorMeasurement(float timestep, const Eigen::Vector3f &centerOfMass, const Eigen::Vector3f &angularMomentum, SensorMeasurementType type = SensorMeasurementType::MEASURED);

    SensorMeasurementPtr clone();

    bool equals(SensorMeasurementPtr sensorMeasurement);

    WholeBodyDynamicSensorMeasurementPtr clone(float newTimestep);

    Eigen::Vector3f getCenterOfMass();

    Eigen::Vector3f getAngularMomentum();

protected:
    WholeBodyDynamicSensorMeasurementPtr interpolate(WholeBodyDynamicSensorMeasurementPtr other, float timestep);

    void appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode);

private:
    Eigen::Vector3f centerOfMass;
    Eigen::Vector3f angularMomentum;
};
}
#endif // __MMM_WholeBodyDynamicSENSORMEASUREMENT_H_
