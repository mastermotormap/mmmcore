#include "WholeBodyDynamicSensorMeasurement.h"

#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>

namespace MMM
{

WholeBodyDynamicSensorMeasurement::WholeBodyDynamicSensorMeasurement(float timestep, const Eigen::Vector3f &centerOfMass, const Eigen::Vector3f &angularMomentum, SensorMeasurementType type) :
    InterpolatableSensorMeasurement(timestep, type),
    centerOfMass(centerOfMass),
    angularMomentum(angularMomentum)
{
}

SensorMeasurementPtr WholeBodyDynamicSensorMeasurement::clone() {
    return clone(timestep);
}

bool WholeBodyDynamicSensorMeasurement::equals(SensorMeasurementPtr sensorMeasurement) {
    WholeBodyDynamicSensorMeasurementPtr ptr = std::dynamic_pointer_cast<WholeBodyDynamicSensorMeasurement>(sensorMeasurement);
    if (ptr) {
        return centerOfMass == ptr->centerOfMass && angularMomentum == ptr->angularMomentum;
    }
    return false;
}

WholeBodyDynamicSensorMeasurementPtr WholeBodyDynamicSensorMeasurement::clone(float newTimestep) {
    WholeBodyDynamicSensorMeasurementPtr clonedSensorMeasurement(new WholeBodyDynamicSensorMeasurement(newTimestep, centerOfMass, angularMomentum, type));
    return clonedSensorMeasurement;
}

void WholeBodyDynamicSensorMeasurement::appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode) {
    measurementNode->append_node("CenterOfMass")->append_data_node(centerOfMass);
    measurementNode->append_node("AngularMomentum")->append_data_node(angularMomentum);
}

WholeBodyDynamicSensorMeasurementPtr WholeBodyDynamicSensorMeasurement::interpolate(WholeBodyDynamicSensorMeasurementPtr other, float timestep) {
    Eigen::Vector3f interpolatedCenterOfMass = InterpolatableSensorMeasurement::linearInterpolation(this->centerOfMass, this->timestep, other->centerOfMass, other->timestep, timestep);
    Eigen::Vector3f interpolatedAngularMomentum = InterpolatableSensorMeasurement::linearInterpolation(this->angularMomentum, this->timestep, other->angularMomentum, other->timestep, timestep);
    WholeBodyDynamicSensorMeasurementPtr interpolatedSensorMeasurement(new WholeBodyDynamicSensorMeasurement(timestep, interpolatedCenterOfMass, interpolatedAngularMomentum, SensorMeasurementType::INTERPOLATED));
    return interpolatedSensorMeasurement;
}

Eigen::Vector3f WholeBodyDynamicSensorMeasurement::getCenterOfMass() {
    return centerOfMass;
}

Eigen::Vector3f WholeBodyDynamicSensorMeasurement::getAngularMomentum() {
    return angularMomentum;
}

}
