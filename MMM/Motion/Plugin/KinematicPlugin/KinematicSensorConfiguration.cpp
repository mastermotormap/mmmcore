#include "KinematicSensorConfiguration.h"

#include <algorithm>

namespace MMM {

KinematicSensorConfiguration::KinematicSensorConfiguration(const std::vector<std::string> &jointNames) : jointNames(jointNames), jointOffset(Eigen::VectorXf::Zero(jointNames.size()))
{
}

const std::vector<std::string>& KinematicSensorConfiguration::getJointNames() const {
    return jointNames;
}

unsigned int KinematicSensorConfiguration::getJointNumber() const {
    return jointNames.size();
}

bool KinematicSensorConfiguration::equals(KinematicSensorConfigurationPtr configuration) const {
    if (configuration) {
        if (jointNames == configuration->jointNames) return true;
        else {
            for (auto jointName : configuration->jointNames) {
                if (std::find(jointNames.begin(), jointNames.end(), jointName) == jointNames.end()) return false; // TODO
            }
            return false;
        }
    }
    return false;
}

bool KinematicSensorConfiguration::equals(KinematicSensorConfigurationPtr a, KinematicSensorConfigurationPtr b) {
    if (a)
        return a->equals(b);
    else
        return b == nullptr;
}

int KinematicSensorConfiguration::removeJoint(const std::string &name) {
    int index = 0;
    for (auto it = jointNames.begin(); it != jointNames.end(); it++) {
        if (*it == name) {
            jointNames.erase(it);
            if (index < jointOffset.rows())
                jointOffset.block(index, 0, jointOffset.rows() - index - 1, 1) = jointOffset.bottomRows(jointOffset.rows() - index - 1);
            jointOffset.conservativeResize(jointOffset.rows() - 1, 1);
            return index;
        }
        index++;
    }
    return -1;
}

}
