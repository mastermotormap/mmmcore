/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_KINEMATICSENSORMEASUREMENT_H_
#define __MMM_KINEMATICSENSORMEASUREMENT_H_

#include <MMM/Motion/Sensor/SensorMeasurement.h>
#include <MMM/Motion/Sensor/InterpolatableSensorMeasurement.h>
#include "KinematicSensorConfiguration.h"

#include <map>

namespace MMM
{
class KinematicSensorMeasurement;

typedef std::shared_ptr<KinematicSensorMeasurement> KinematicSensorMeasurementPtr;

class MMM_IMPORT_EXPORT KinematicSensorMeasurement : public InterpolatableSensorMeasurement<KinematicSensorMeasurement>, SMCloneable<KinematicSensorMeasurement>
{

public:
    KinematicSensorMeasurement(float timestep, const Eigen::VectorXf &jointAngles, SensorMeasurementType type = SensorMeasurementType::MEASURED);

    SensorMeasurementPtr clone() override;

    bool equals(SensorMeasurementPtr sensorMeasurement) override;

    KinematicSensorMeasurementPtr clone(float newTimestep) override;

    Eigen::VectorXf getJointAngles();

    std::map<std::string, float> getJointAngleMap();

    void initializeModel(ModelPtr model, bool update) override;

    bool invertJointValue(int index);

    bool setConfiguration(KinematicSensorConfigurationPtr configuration);

    void removeJoint(int index);

protected:
    KinematicSensorMeasurementPtr interpolate(KinematicSensorMeasurementPtr other, float timestep) override;

    void appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode) override;

private:
    Eigen::VectorXf jointAngles;
    KinematicSensorConfigurationPtr configuration;
};
}
#endif // __MMM_KINEMATICSENSORMEASUREMENT_H_
