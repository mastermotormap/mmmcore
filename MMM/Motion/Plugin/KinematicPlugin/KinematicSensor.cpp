#include <cassert>

#include "MMM/Motion/Legacy/LegacyMotion.h"
#include <VirtualRobot/Robot.h>

#include "KinematicSensor.h"

namespace MMM
{

KinematicSensorPtr KinematicSensor::loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    KinematicSensorPtr sensor(new KinematicSensor());
    sensor->loadSensor(node, filePath);
    return sensor;
}

KinematicSensorPtr KinematicSensor::createFromLegacy(LegacyMotionPtr motion)
{
    auto jointNames = motion->getJointNames();

    if (jointNames.size() == 0)
        return nullptr;

    KinematicSensorPtr kinematicSensor(new KinematicSensor(motion->getJointNames()));
    for (auto motionFrame : motion->getMotionFrames()) {
        KinematicSensorMeasurementPtr kMeasurement(new KinematicSensorMeasurement(motionFrame->timestep, motionFrame->joint));
        kinematicSensor->addSensorMeasurement(kMeasurement);
    }
    return kinematicSensor;
}

KinematicSensor::KinematicSensor() : InterpolatableSensor()
{
}

KinematicSensor::KinematicSensor(const std::vector<std::string> &jointNames, const std::string &description) :
    InterpolatableSensor(description),
    configuration(new KinematicSensorConfiguration(jointNames))
{
}

KinematicSensor::KinematicSensor(const std::vector<std::string> &jointNames, const std::map<float, Eigen::VectorXf> &jointValues, const std::string &description) :
    KinematicSensor(jointNames, description)
{
    addSensorMeasurements(jointValues);
}

bool KinematicSensor::checkModel(ModelPtr model) {
    if (!model) {
        MMM_ERROR << "Sensor '" << uniqueName << "' needs a model."  << std::endl;
        return false;
    }
    for (auto jointName : configuration->getJointNames()) {
        if (!hasNode(model, jointName)) {
            MMM_ERROR << "Joint name '" << jointName << "' is not part of model file " << model->getFilename() << std::endl;
            return false;
        }
    }
    return true;
}

std::shared_ptr<BasicSensor<KinematicSensorMeasurement> > KinematicSensor::cloneConfiguration() {
    KinematicSensorPtr k(new KinematicSensor(configuration->getJointNames(), description));
    return k;
}

void KinematicSensor::loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    assert(node);
    assert(node->name() == "Configuration");

    std::vector<std::string> jointNames;
    for(auto jointNode : node->nodes("Joint")) {
        std::string jointName = jointNode->attribute_value(xml::attribute::NAME);
        if (std::find(jointNames.begin(), jointNames.end(), jointName) == jointNames.end()) jointNames.push_back(jointName);
        else throw Exception::MMMFormatException("Duplicate of joint '" + jointName + "' in Kinematics sensor configuration");
    }
    configuration = KinematicSensorConfigurationPtr(new KinematicSensorConfiguration(jointNames));
}

void KinematicSensor::loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type)
{
    assert(node);
    assert(node->name() == xml::tag::MEASUREMENT);

    KinematicSensorMeasurementPtr k(new KinematicSensorMeasurement(timestep, node->first_node("JointPosition")->value_eigen_vec(configuration->getJointNames().size()), type));
    addSensorMeasurement(k); //never false, because the error is catched shortly before
}

void KinematicSensor::appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath)
{
    for(auto joint : configuration->getJointNames()) {
        node->append_node("Joint")->append_attribute(xml::attribute::NAME, joint);
    }
}

bool KinematicSensor::hasSensorConfigurationContent() const
{
    return this->configuration && !this->configuration->getJointNames().empty();
}

bool KinematicSensor::equalsConfiguration(SensorPtr other) {
    KinematicSensorPtr kinSensor = std::dynamic_pointer_cast<KinematicSensor>(other);
    if (kinSensor) {
        return KinematicSensorConfiguration::equals(configuration, kinSensor->configuration);
    }
    else return false;
}

bool KinematicSensor::addSensorMeasurement(KinematicSensorMeasurementPtr measurement)
{
    if (static_cast<std::size_t>(measurement->getJointAngles().rows()) != configuration->getJointNumber()) {
        MMM_ERROR << "Could not add kinematic sensor measurement, because the jointAngles are not matching the jointNames" << std::endl;
        return false;
    }
    measurements.insert(std::pair<float, KinematicSensorMeasurementPtr>(measurement->getTimestep(), measurement));
    return measurement->setConfiguration(configuration);
}

bool KinematicSensor::addSensorMeasurements(const std::map<float, Eigen::VectorXf> &jointValues, float timestepDelta) {
    bool success = true;
    for (const auto &v : jointValues) {
        if (!addSensorMeasurement(std::make_shared<KinematicSensorMeasurement>(v.first + timestepDelta, v.second)))
            success = false;
    }
    return success;
}

std::vector<std::string> KinematicSensor::getJointNames() {
    if (!configuration)
        return std::vector<std::string>();
    else
        return configuration->getJointNames();
}

std::string KinematicSensor::getType() {
    return BasicKinematicSensor::TYPE;
}

std::string KinematicSensor::getVersion() {
    return VERSION;
}

int KinematicSensor::getPriority() {
    return 90;
}

bool KinematicSensor::isMirrorSupported(MotionType type) {
    if (type._value != MotionType::MMM)
        return false;
    else
        return true;
}

std::set<std::string> mmmJointNamesToInvert = {"LSCz_joint", "LSCy_joint", "LSz_joint", "LSy_joint", "LEz_joint", "LWy_joint",
                                                           "LHz_joint", "LHy_joint", "LAz_joint", "LAy_joint", "LMrot_joint",
                                                           "RSCz_joint", "RSCy_joint", "RSz_joint", "RSy_joint", "REz_joint", "RWy_joint",
                                                           "RHz_joint", "RHy_joint", "RAz_joint", "RAy_joint", "RMrot_joint",
                                                           "BPz_joint", "BPy_joint", "BTz_joint", "BTy_joint",
                                                           "BUNz_joint", "BUNy_joint", "BLNz_joint", "BLNy_joint"};

void KinematicSensor::mirrorMotionData(const Eigen::Matrix4f &/*referencePose*/, int /*referenceAxis*/) {
    // TODO currently only mmm supported!
    std::vector<std::string> newJointNames;
    std::vector<int> invertIndices;
    int index = 0;
    for (const auto &jointName : getJointNames()) {
        std::string newJointName = jointName;
        if (jointName.substr(0,4) == "Right") {
            newJointName = "Left" + jointName.substr(5);
        }
        else if (jointName.substr(0,3) == "Left") {
            newJointName = "Right" + jointName.substr(4);
        }
        else if (jointName.at(0) == 'R') {
            newJointName = "L" + jointName.substr(1);
        }
        else if (jointName.at(0) == 'L') {
            newJointName = "R" + jointName.substr(1);
        }
        newJointNames.push_back(newJointName);
        if (mmmJointNamesToInvert.find(newJointName) != mmmJointNamesToInvert.end())
            invertIndices.push_back(index);
        index++;
    }

    for (auto timestep : getTimesteps()) {
        auto measurement = getDerivedMeasurement(timestep);
        for (auto index : invertIndices) {
            measurement->invertJointValue(index);
        }
    }

    configuration->setJointNames(newJointNames);
}

KinematicSensorPtr KinematicSensor::join(std::vector<KinematicSensorPtr> sensors) {
    std::vector<float> timesteps;
    int max_jSize = 0;
    for (auto sensor : sensors) {
        int jSize = sensor->getJointNames().size();
        if (jSize > max_jSize) {
            max_jSize = jSize;
            timesteps = sensor->getTimesteps();
        }
    }
    return join(sensors, timesteps);
}

KinematicSensorPtr KinematicSensor::join(std::vector<KinematicSensorPtr> sensors, const std::vector<float> &timesteps) {
    if (sensors.size() == 0) throw Exception::MMMException("Could not join kinematic sensors, because there are none!");

    std::vector<std::string> jointNames;
    for (auto sensor : sensors) {
        for (auto jointName : sensor->getJointNames()) {
            if (std::find(jointNames.begin(), jointNames.end(), jointName) != jointNames.end())
                throw Exception::MMMException("Could not join kinematic sensors, because the joint names of different kinematic sensors are the same.");
            jointNames.push_back(jointName);
        }
    }
    KinematicSensorPtr joinedSensor(new KinematicSensor(jointNames, "Joined sensor."));

    for (auto timestep : timesteps) {
        Eigen::VectorXf jointAngles(jointNames.size());
        SensorMeasurementType type = SensorMeasurementType::MEASURED;
        int j = 0;
        for (auto sensor : sensors) {
            KinematicSensorMeasurementPtr sensorMeasurement = sensor->getDerivedMeasurement(timestep);
            // set not interpolatable sensor measurements zero. maybe TODO other method!
            if (!sensorMeasurement) sensorMeasurement = KinematicSensorMeasurementPtr(new KinematicSensorMeasurement(timestep, Eigen::VectorXf::Zero(sensor->getJointNames().size(), true)));
            if (type._value != SensorMeasurementType::EXTENDED) {
                if (sensorMeasurement->isExtended()) type = SensorMeasurementType::EXTENDED;
                else if (sensorMeasurement->isInterpolated()) type = SensorMeasurementType::INTERPOLATED;
            }
            Eigen::VectorXf sensorMeasurement_jointAngles = sensorMeasurement->getJointAngles();
            for (unsigned int i = 0; i < sensorMeasurement_jointAngles.rows(); i++, j++)
                jointAngles(j) = sensorMeasurement_jointAngles(i);
        }
        KinematicSensorMeasurementPtr joinedSensorMeasurement(new KinematicSensorMeasurement(timestep, jointAngles, type));
        joinedSensor->addSensorMeasurement(joinedSensorMeasurement);
    }

    return joinedSensor;
}

std::vector<std::string> KinematicSensor::getSensorSpecificInformation(const std::string &name) {
    if (name == "JointNames")
        return getJointNames();
    else
        return std::vector<std::string>();
}

Eigen::VectorXf KinematicSensor::getJointAngles(float timestep) {
    auto measurement = getDerivedMeasurement(timestep);
    if (measurement)
        return measurement->getJointAngles();
    else
        throw Exception::MMMException("Kinematic Sensor " + uniqueName + " has no measurement at timestep " + simox::alg::to_string(timestep));
}

std::map<std::string, float> KinematicSensor::getJointAngleMap(float timestep) {
    auto measurement = getDerivedMeasurement(timestep);
    if (measurement)
        return measurement->getJointAngleMap();
    else
        return std::map<std::string, float>();
}

std::map<float, float> KinematicSensor::getJointValues(const std::string &jointName) {
    std::map<float, float> jointValues;

    int index = getJointIndex(jointName);
    if (index < 0) {
        MMM_ERROR << "Joint " + jointName + " not found in kinematic sensor " + uniqueName << std::endl;
    }
    else {
        for (float timestep : getTimesteps()) {
            MMM::KinematicSensorMeasurementPtr measurement = getDerivedMeasurement(timestep);
            jointValues[timestep] = measurement->getJointAngles()[index];
        }
    }

    return jointValues;
}

int KinematicSensor::getJointIndex(const std::string &jointName) {
    int index = -1;
    for (unsigned int i = 0; i < getJointNames().size(); i++) {
        if (getJointNames()[i] == jointName) {
            index = i;
            break;
        }
    }
    return index;
}

bool KinematicSensor::hasJoint(const std::string &jointName) {
    return getJointIndex(jointName) >= 0;
}

void KinematicSensor::setJointOffset(const std::string &jointName, float offset) {
    if (configuration && hasJoint(jointName)) {
        configuration->setJointOffset(getJointIndex(jointName), offset);
    }
}

Eigen::VectorXf KinematicSensor::getJointOffset() {
    if (!configuration) return Eigen::VectorXf::Zero(0);
    else return configuration->getJointOffset();
}

bool KinematicSensor::removeJoint(const std::string &name) {
    int index = configuration->removeJoint(name);
    if (index >= 0) {
        for (auto &measurement : measurements) {
            measurement.second->removeJoint(index);
        }
        return true;
    }
    return false;
}

}
