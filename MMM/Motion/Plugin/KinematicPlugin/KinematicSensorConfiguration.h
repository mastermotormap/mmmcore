/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_KINEMATICSENSORCONFIGURATION_H_
#define __MMM_KINEMATICSENSORCONFIGURATION_H_

#include <memory>
#include <vector>
#include <string>
#include <Eigen/Core>

namespace MMM {

class KinematicSensorConfiguration;
typedef std::shared_ptr<KinematicSensorConfiguration> KinematicSensorConfigurationPtr;

class KinematicSensorConfiguration {
public:
    KinematicSensorConfiguration(const std::vector<std::string> &jointNames);

    const std::vector<std::string>& getJointNames() const;

    unsigned int getJointNumber() const;

    bool equals(KinematicSensorConfigurationPtr configuration) const;

    static bool equals(KinematicSensorConfigurationPtr a, KinematicSensorConfigurationPtr b);

    void setJointNames(const std::vector<std::string> &jointNames) {
        this->jointNames = jointNames;
    }

    void setJointOffset(Eigen::VectorXf jointOffset) {
       this->jointOffset = jointOffset;
    }

    Eigen::VectorXf getJointOffset() {
        return jointOffset;
    }

    void setJointOffset(int jointIndex, float offset) {
        jointOffset(jointIndex) = offset;
    }

    int removeJoint(const std::string &name);

private:
    std::vector<std::string> jointNames;
    Eigen::VectorXf jointOffset;
};



}

#endif // __MMM_KINEMATICSENSORCONFIGURATION_H_
