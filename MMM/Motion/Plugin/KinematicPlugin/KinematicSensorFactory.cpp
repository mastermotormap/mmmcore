#include "KinematicSensorFactory.h"

#include <boost/extension/extension.hpp>

#include "KinematicSensor.h"

namespace MMM
{

// register this factory
SensorFactory::SubClassRegistry KinematicSensorFactory::registry(NAME_STR<KinematicSensor>(), &KinematicSensorFactory::createInstance);

KinematicSensorFactory::KinematicSensorFactory() : SensorFactory() {}

KinematicSensorFactory::~KinematicSensorFactory() = default;

SensorPtr KinematicSensorFactory::createSensor(simox::xml::RapidXMLWrapperNodePtr sensorNode, const std::filesystem::path &filePath)
{
    return KinematicSensor::loadSensorXML(sensorNode, filePath);
}

std::vector<SensorPtr> KinematicSensorFactory::createSensors(LegacyMotionPtr motion) {
    KinematicSensorPtr sensor = KinematicSensor::createFromLegacy(motion);
    if (sensor)
        return { sensor };
    else
        return std::vector<SensorPtr>();
}

std::string KinematicSensorFactory::getName()
{
    return NAME_STR<KinematicSensor>();
}

SensorFactoryPtr KinematicSensorFactory::createInstance(void *)
{
    return SensorFactoryPtr(new KinematicSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorFactoryPtr getFactory() {
    return SensorFactoryPtr(new KinematicSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorFactory::VERSION;
}

}
