#include "KinematicSensorMeasurement.h"

#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Nodes/RobotNode.h>

namespace MMM
{

KinematicSensorMeasurement::KinematicSensorMeasurement(float timestep, const Eigen::VectorXf &jointAngles, SensorMeasurementType type) :
    InterpolatableSensorMeasurement(timestep, type),
    jointAngles(jointAngles),
    configuration(nullptr)
{
}

SensorMeasurementPtr KinematicSensorMeasurement::clone() {
    return clone(timestep);
}

bool KinematicSensorMeasurement::equals(SensorMeasurementPtr sensorMeasurement) {
    KinematicSensorMeasurementPtr ptr = std::dynamic_pointer_cast<KinematicSensorMeasurement>(sensorMeasurement);
    if (ptr) {
        return getJointAngles() == ptr->getJointAngles();
    }
    return false;
}

KinematicSensorMeasurementPtr KinematicSensorMeasurement::clone(float newTimestep) {
    KinematicSensorMeasurementPtr clonedSensorMeasurement(new KinematicSensorMeasurement(newTimestep, jointAngles, type));
    clonedSensorMeasurement->setConfiguration(configuration);
    return clonedSensorMeasurement;
}

void KinematicSensorMeasurement::appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode) {
    measurementNode->append_node("JointPosition")->append_data_node(getJointAngles());
}

Eigen::VectorXf KinematicSensorMeasurement::getJointAngles() {
    if (configuration) return jointAngles + configuration->getJointOffset();
    else return jointAngles;
}

KinematicSensorMeasurementPtr KinematicSensorMeasurement::interpolate(KinematicSensorMeasurementPtr other, float timestep) {
    int vSize = jointAngles.rows();
    Eigen::VectorXf interpolatedJointAngles(vSize);
    Eigen::VectorXf jointAngles = getJointAngles();
    Eigen::VectorXf otherJointAngles = other->getJointAngles();
    for (int i = 0; i < vSize; i++) {
        //if (i < 10) std::cout << this->timestep << "  " << timestep << "  " << other->timestep << std::endl;
        interpolatedJointAngles(i) = InterpolatableSensorMeasurement::linearInterpolation(jointAngles(i), this->timestep, otherJointAngles(i), other->timestep, timestep);
    }

    KinematicSensorMeasurementPtr interpolatedSensorMeasurement(new KinematicSensorMeasurement(timestep, interpolatedJointAngles, SensorMeasurementType::INTERPOLATED));
    interpolatedSensorMeasurement->setConfiguration(configuration);
    return interpolatedSensorMeasurement;
}

bool KinematicSensorMeasurement::invertJointValue(int index) {
    if (jointAngles.rows() <= index) return false;
    jointAngles(index) *= -1;
    return true;
}

void KinematicSensorMeasurement::initializeModel(ModelPtr model, bool update) {
    const Eigen::VectorXf jointAngles = getJointAngles();
    const std::vector<std::string> jointNames = configuration->getJointNames();
    for (unsigned int i = 0; i < jointNames.size(); i++) {
        const auto node = model->getRobotNode(jointNames[i]);
        if (node) node->setJointValueNoUpdate(jointAngles(i));
    }
    if (update) model->updatePose();
}

std::map<std::string, float> KinematicSensorMeasurement::getJointAngleMap() {
    std::map<std::string, float> jointAngleMap;
    if (configuration) {
        Eigen::VectorXf jointAngles = getJointAngles();
        for (unsigned int i = 0; i < configuration->getJointNumber(); i++) {
            jointAngleMap[configuration->getJointNames()[i]] = jointAngles(i);
        }
    }
    else {
        MMM_ERROR << "No configuration in kinematic sensor measurement!" << std::endl;
    }
    return jointAngleMap;
}

bool KinematicSensorMeasurement::setConfiguration(KinematicSensorConfigurationPtr configuration) {
    if (configuration->getJointNumber() != jointAngles.rows())
        return false;
    else {
        this->configuration = configuration;
        return true;
    }
}

void KinematicSensorMeasurement::removeJoint(int index) {
    if (index >= 0 && jointAngles.size() - 1 == configuration->getJointNumber()) {
        if (index < jointAngles.rows())
            jointAngles.block(index, 0, jointAngles.rows() - index - 1, 1) = jointAngles.bottomRows(jointAngles.rows() - index - 1);
        jointAngles.conservativeResize(jointAngles.rows() - 1, 1);
    }
}

}
