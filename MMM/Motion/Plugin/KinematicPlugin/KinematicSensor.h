/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_KINEMATICSENSOR_H_
#define __MMM_KINEMATICSENSOR_H_

#include <MMM/Motion/Sensor/BasicKinematicSensor.h>
#include <MMM/Motion/Sensor/InterpolatableSensor.h>
#include <MMM/Motion/Sensor/SensorMeasurement.h>
#include "KinematicSensorMeasurement.h"
#include "KinematicSensorConfiguration.h"

#include <string>
#include <vector>
#include <map>
#include <set>
#include <iostream>

namespace MMM
{

class KinematicSensor;
typedef std::shared_ptr<KinematicSensor> KinematicSensorPtr;
typedef std::vector<KinematicSensorPtr> KinematicSensorList;

class MMM_IMPORT_EXPORT KinematicSensor : public InterpolatableSensor<KinematicSensorMeasurement>, public BasicKinematicSensor
{

public:

    static KinematicSensorPtr loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    static KinematicSensorPtr createFromLegacy(LegacyMotionPtr motion);

    KinematicSensor(const std::vector<std::string> &jointNames, const std::string &description = std::string());

    KinematicSensor(const std::vector<std::string> &jointNames, const std::map<float, Eigen::VectorXf> &jointValues,
                    const std::string &description = std::string());

    bool checkModel(ModelPtr model) override;

    std::shared_ptr<BasicSensor<KinematicSensorMeasurement> > cloneConfiguration() override;

    bool equalsConfiguration(SensorPtr other) override;

    std::string getType() override;

    std::string getVersion() override;

    int getPriority() override;

    bool isMirrorSupported(MotionType type) override;

    void mirrorMotionData(const Eigen::Matrix4f &referencePose, int referenceAxis) override;

    std::vector<std::string> getJointNames() override;

    bool addSensorMeasurement(KinematicSensorMeasurementPtr measurement) override;

    bool addSensorMeasurements(const std::map<float, Eigen::VectorXf> &jointValues, float timestepDelta = 0.0f);

    Eigen::VectorXf getJointAngles(float timestep) override;

    std::map<std::string, float> getJointAngleMap(float timestep) override;

    std::map<float, float> getJointValues(const std::string &jointName) override;

    int getJointIndex(const std::string &jointName) override;

    bool hasJoint(const std::string &jointName) override;

    void setJointOffset(const std::string &jointName, float offset) override;

    Eigen::VectorXf getJointOffset() override;

    bool removeJoint(const std::string &name) override;

    static constexpr const char* TYPE = BasicKinematicSensor::TYPE;

    static constexpr const char* VERSION = "1.0";

    /*! Joins all kinematic sensors together on the timestep measurements of the biggest sensor if possible for all given sensors via linear interpolation. */
    static KinematicSensorPtr join(std::vector<KinematicSensorPtr> sensors);

    /*! Joins all kinematic sensors together the given timestep if possible via linear interpolation. */
    static KinematicSensorPtr join(std::vector<KinematicSensorPtr> sensors, const std::vector<float> &timesteps);

    std::vector<std::string> getSensorSpecificInformation(const std::string &name) override;

private:

    KinematicSensor();

    void loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) override;

    void loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type) override;

    void appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) override;

    bool hasSensorConfigurationContent() const override;

    KinematicSensorConfigurationPtr configuration;
};
}
#endif // __MMM_KINEMATICSENSOR_H_

