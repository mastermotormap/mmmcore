#include "MoCapMarkerSensorFactory.h"

#include <boost/extension/extension.hpp>

#include "MoCapMarkerSensor.h"

#include <cstring>

namespace MMM
{

// register this factory
SensorFactory::SubClassRegistry MoCapMarkerSensorFactory::registry(NAME_STR<MoCapMarkerSensor>(), &MoCapMarkerSensorFactory::createInstance);

MoCapMarkerSensorFactory::MoCapMarkerSensorFactory() : SensorFactory() {}

MoCapMarkerSensorFactory::~MoCapMarkerSensorFactory() = default;

SensorPtr MoCapMarkerSensorFactory::createSensor(simox::xml::RapidXMLWrapperNodePtr sensorNode, const std::filesystem::path &filePath)
{
    return MoCapMarkerSensor::loadSensorXML(sensorNode, filePath);
}

std::string MoCapMarkerSensorFactory::getName()
{
    return NAME_STR<MoCapMarkerSensor>();
}

SensorFactoryPtr MoCapMarkerSensorFactory::createInstance(void *)
{
    return SensorFactoryPtr(new MoCapMarkerSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorFactoryPtr getFactory() {
    return SensorFactoryPtr(new MoCapMarkerSensorFactory);
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorFactory::VERSION;
}

}
