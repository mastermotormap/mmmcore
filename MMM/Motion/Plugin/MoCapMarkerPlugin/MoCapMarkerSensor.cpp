#include <cassert>
#include <map>

#include "../../../Exceptions.h"

#include "MoCapMarkerSensor.h"

namespace MMM
{

MoCapMarkerSensorPtr MoCapMarkerSensor::loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    MoCapMarkerSensorPtr sensor(new MoCapMarkerSensor());
    sensor->loadSensor(node, filePath);
    return sensor;
}

MoCapMarkerSensor::MoCapMarkerSensor(const std::string &description) : InterpolatableSensor(description)
{
}

bool MoCapMarkerSensor::checkModel(ModelPtr model) {
    return true;
}

std::shared_ptr<BasicSensor<MoCapMarkerSensorMeasurement> > MoCapMarkerSensor::cloneConfiguration() {
    MoCapMarkerSensorPtr m(new MoCapMarkerSensor(description));
    return m;
}

void MoCapMarkerSensor::loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    assert(node);
    assert(node->name() == "Configuration");
}

void MoCapMarkerSensor::loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type)
{
    assert(node);
    assert(node->name() == xml::tag::MEASUREMENT);

    std::map<std::string, Eigen::Vector3f> markers;
    std::vector<Eigen::Vector3f> unlabeledMarkers;
    for (auto markerNode : node->nodes("MarkerPosition")) {
        std::string name = markerNode->attribute_value_or_default("name", "");
        Eigen::Vector3f markerPosition = markerNode->value_eigen_vec3();
        if (name.empty()) unlabeledMarkers.push_back(markerPosition);
        else if (markers.find(name) == markers.end()) markers[name] = markerPosition;
        else throw Exception::MMMFormatException("Duplicate of marker '" + name + "' at timestep '" + simox::alg::to_string(timestep) + "' in MoCapMarker sensor data");
    }
    MoCapMarkerSensorMeasurementPtr m(new MoCapMarkerSensorMeasurement(timestep, markers, unlabeledMarkers, type));
    addSensorMeasurement(m);
}

void MoCapMarkerSensor::appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr /*node*/, const std::filesystem::path &filePath) {
    // Do nothing
}

bool MoCapMarkerSensor::hasSensorConfigurationContent() const
{
    return true; // TODO
}

bool MoCapMarkerSensor::equalsConfiguration(SensorPtr other) {
    MoCapMarkerSensorPtr ptr = std::dynamic_pointer_cast<MoCapMarkerSensor>(other);
    if (ptr) {
        return true;
    }
    else return false;
}

std::map<float, std::map<std::string, Eigen::Vector3f> > MoCapMarkerSensor::getLabeledMarkerData() {
    std::map<float, std::map<std::string, Eigen::Vector3f>> m;
    for (const auto &measurement : measurements) m.insert(std::pair<float, std::map<std::string, Eigen::Vector3f>>(measurement.first, measurement.second->getLabeledMarker()));
    return m;
}

std::string MoCapMarkerSensor::getType() {
    return TYPE;
}

std::string MoCapMarkerSensor::getVersion() {
    return VERSION;
}

int MoCapMarkerSensor::getPriority() {
    return 20;
}

void MoCapMarkerSensor::shiftMotionData(const Eigen::Vector3f &positionDelta) {
    for (auto measurement : measurements) {
        measurement.second->shiftMotion(positionDelta);
    }
}

void MoCapMarkerSensor::scaleMotionData(float scaleFactor) {
    for (auto measurement : measurements) {
        measurement.second->scaleMotion(scaleFactor);
    }
}


}
