#include <cassert>
#include <map>

#include "CustomSensor.h"
#include <SimoxUtility/math/convert.h>

namespace MMM
{

CustomSensorPtr CustomSensor::loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    CustomSensorPtr sensor(new CustomSensor());
    sensor->loadSensor(node, filePath);
    return sensor;
}

CustomSensor::CustomSensor(const std::string &description) : InterpolatableSensor(description)
{
}

bool CustomSensor::checkModel(ModelPtr /*model*/) {
    return true; // TODO
}

std::shared_ptr<BasicSensor<CustomSensorMeasurement> > CustomSensor::cloneConfiguration() {
    CustomSensorPtr m(new CustomSensor(description));
    return m;
}

void CustomSensor::loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &/*filePath*/) {
    assert(node);
    assert(node->name() == "Configuration");
}

void CustomSensor::loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type)
{
    assert(node);
    assert(node->name() == xml::tag::MEASUREMENT);

    std::map<std::string, CustomPosePtr> customPoses;
    for (const auto &p : node->nodes("Pose")) {
        auto pos = p->first_node("Position")->value_eigen_vec3();
        auto ori = Eigen::Quaternionf(1,0,0,0);
        if (p->has_node("Orientation")) {
            ori = simox::math::rpy_to_quat(p->first_node("Orientation")->value_eigen_vec3());
        }
        auto id = p->attribute_value("id");
        std::string local = "";
        if (p->has_attribute("local")) {
            local = p->attribute_value("local");
        }
        customPoses[id] = CustomPosePtr(new CustomPose(id, pos, ori, local));
    }

    CustomSensorMeasurementPtr m(new CustomSensorMeasurement(timestep, customPoses, type));
    addSensorMeasurement(m);
}

void CustomSensor::appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr /*node*/, const std::filesystem::path &/*filePath*/) {
    // Do nothing
}

bool CustomSensor::hasSensorConfigurationContent() const
{
    return true; // TODO
}

bool CustomSensor::equalsConfiguration(SensorPtr other) {
    CustomSensorPtr ptr = std::dynamic_pointer_cast<CustomSensor>(other);
    if (ptr) {
        return true;
    }
    else return false;
}

std::string CustomSensor::getType() {
    return TYPE;
}

std::string CustomSensor::getVersion() {
    return VERSION;
}

int CustomSensor::getPriority() {
    return 0;
}

}

