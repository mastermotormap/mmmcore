/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_CustomSensor_H_
#define __MMM_CustomSensor_H_

#include <MMM/Motion/Sensor/InterpolatableSensor.h>
#include <MMM/Motion/Sensor/SensorMeasurement.h>
#include "CustomSensorMeasurement.h"

#include <vector>

namespace MMM
{

class CustomSensor;

typedef std::shared_ptr<CustomSensor> CustomSensorPtr;
typedef std::vector<CustomSensorPtr> CustomSensorList;

class MMM_IMPORT_EXPORT CustomSensor : public InterpolatableSensor<CustomSensorMeasurement>
{

public:
    static CustomSensorPtr loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    CustomSensor(const std::string &description = std::string());

    bool checkModel(ModelPtr model);

    std::shared_ptr<BasicSensor<CustomSensorMeasurement> > cloneConfiguration();

    bool equalsConfiguration(SensorPtr other);

    std::string getType();

    std::string getVersion();

    int getPriority();

    static constexpr const char* TYPE = "Custom";

    static constexpr const char* VERSION = "1.0";

private:
    void loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    void loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type);

    void appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    bool hasSensorConfigurationContent() const;
};


}
#endif // __MMM_CustomSensor_H_

