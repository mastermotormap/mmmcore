/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_CustomSensorMEASUREMENT_H_
#define __MMM_CustomSensorMEASUREMENT_H_

#include <MMM/Motion/Sensor/SensorMeasurement.h>
#include <MMM/Motion/Sensor/InterpolatableSensorMeasurement.h>

#include <Eigen/Core>
#include <Eigen/Dense>
#include <map>

namespace MMM
{

class CustomPose {
public:
    CustomPose(const std::string &id, const Eigen::Vector3f &position, const Eigen::Quaternionf &orientation = Eigen::Quaternionf(1, 0, 0, 0), const std::string &localCoordSystem = std::string()) :
        id(id),
        position(position),
        orientation(orientation),
        localCoordSystem(localCoordSystem)
    {
    }

    std::string id;
    Eigen::Vector3f position;
    Eigen::Quaternionf orientation;
    std::string localCoordSystem;
};

typedef std::shared_ptr<CustomPose> CustomPosePtr;

class CustomSensorMeasurement;

typedef std::shared_ptr<CustomSensorMeasurement> CustomSensorMeasurementPtr;

class MMM_IMPORT_EXPORT CustomSensorMeasurement : public InterpolatableSensorMeasurement<CustomSensorMeasurement>, SMCloneable<CustomSensorMeasurement>
{

public:
    CustomSensorMeasurement(float timestep, const std::map<std::string, CustomPosePtr> &customPoses = std::map<std::string, CustomPosePtr>(), SensorMeasurementType type = SensorMeasurementType::MEASURED);

    SensorMeasurementPtr clone();

    bool equals(SensorMeasurementPtr sensorMeasurement);

    CustomSensorMeasurementPtr clone(float newTimestep);

    void addCustomPose(CustomPosePtr customPose);

    std::map<std::string, CustomPosePtr> getCustomPoses();

    CustomPosePtr getCustomPose(const std::string &id);

protected:
    CustomSensorMeasurementPtr interpolate(CustomSensorMeasurementPtr other, float timestep);

    void appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode);

private:
    std::map<std::string, CustomPosePtr> customPoses;
};
}
#endif // __MMM_CustomSensorMEASUREMENT_H_
