#include "CustomSensorMeasurement.h"

#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>
#include <SimoxUtility/math/convert.h>

namespace MMM
{

CustomSensorMeasurement::CustomSensorMeasurement(float timestep,  const std::map<std::string, CustomPosePtr> &customPoses, SensorMeasurementType type) :
    InterpolatableSensorMeasurement(timestep, type),
    customPoses(customPoses)
{
}

SensorMeasurementPtr CustomSensorMeasurement::clone() {
    return clone(timestep);
}

bool CustomSensorMeasurement::equals(SensorMeasurementPtr sensorMeasurement) {
    CustomSensorMeasurementPtr ptr = std::dynamic_pointer_cast<CustomSensorMeasurement>(sensorMeasurement);
    if (ptr) {
        return customPoses.size() == ptr->customPoses.size(); // TODO!
    }
    return false;
}

CustomSensorMeasurementPtr CustomSensorMeasurement::clone(float newTimestep) {
    CustomSensorMeasurementPtr clonedSensorMeasurement(new CustomSensorMeasurement(newTimestep, customPoses, type));
    return clonedSensorMeasurement;
}

void CustomSensorMeasurement::appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode) {
    for (const auto &c : customPoses) {
        auto node = measurementNode->append_node("Pose");
        node->append_attribute("id", c.first);
        if (!c.second->localCoordSystem.empty()) node->append_attribute("local", c.second->localCoordSystem);
        node->append_node("Position")->append_data_node(c.second->position);
        node->append_node("Orientation")->append_data_node(simox::math::quat_to_rpy(c.second->orientation));
    }
}

CustomSensorMeasurementPtr CustomSensorMeasurement::interpolate(CustomSensorMeasurementPtr other, float timestep) {
    std::map<std::string, CustomPosePtr> interpolatedPosesMap;
    for (const auto &m : customPoses) {
        if (other->customPoses.find(m.first) != other->customPoses.end()) {
            auto position = InterpolatableSensorMeasurement::linearInterpolation(m.second->position, this->timestep, other->customPoses[m.first]->position, other->timestep, timestep);
            auto orientation = m.second->orientation.slerp((timestep - this->timestep) / (other->timestep - this->timestep), other->customPoses[m.first]->orientation);
            interpolatedPosesMap[m.first] = CustomPosePtr(new CustomPose(m.first, position, orientation, m.second->localCoordSystem));
        }
    }
    CustomSensorMeasurementPtr interpolatedSensorMeasurement(new CustomSensorMeasurement(timestep, interpolatedPosesMap, SensorMeasurementType::INTERPOLATED));
    return interpolatedSensorMeasurement;
}

void CustomSensorMeasurement::addCustomPose(CustomPosePtr customPose) {
    customPoses[customPose->id] = customPose;
}

CustomPosePtr CustomSensorMeasurement::getCustomPose(const std::string &id) {
    if (customPoses.find(id) != customPoses.end())
        return customPoses.at(id);
    else throw Exception::MMMException(id + " unknown!");
}

std::map<std::string, CustomPosePtr> CustomSensorMeasurement::getCustomPoses() {
    return customPoses;
}

}
