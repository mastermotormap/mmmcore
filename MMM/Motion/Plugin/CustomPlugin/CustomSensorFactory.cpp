#include "CustomSensorFactory.h"

#include <boost/extension/extension.hpp>

#include "CustomSensor.h"

namespace MMM
{

// register this factory
SensorFactory::SubClassRegistry CustomSensorFactory::registry(NAME_STR<CustomSensor>(), &CustomSensorFactory::createInstance);

CustomSensorFactory::CustomSensorFactory() : SensorFactory() {}

CustomSensorFactory::~CustomSensorFactory() = default;

SensorPtr CustomSensorFactory::createSensor(simox::xml::RapidXMLWrapperNodePtr sensorNode, const std::filesystem::path &filePath)
{
    return CustomSensor::loadSensorXML(sensorNode, filePath);
}

std::string CustomSensorFactory::getName()
{
    return NAME_STR<CustomSensor>();
}

SensorFactoryPtr CustomSensorFactory::createInstance(void *)
{
    return SensorFactoryPtr(new CustomSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorFactoryPtr getFactory() {
    return SensorFactoryPtr(new CustomSensorFactory);
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorFactory::VERSION;
}

}
