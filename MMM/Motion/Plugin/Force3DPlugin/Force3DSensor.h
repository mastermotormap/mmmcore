/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_Force3DSENSOR_H_
#define __MMM_Force3DSENSOR_H_

#include <MMM/Motion/Sensor/AttachedSensor.h>
#include <MMM/Motion/Sensor/SensorMeasurement.h>
#include "Force3DSensorMeasurement.h"

#include <vector>

namespace MMM
{

class Force3DSensor;

typedef std::shared_ptr<Force3DSensor> Force3DSensorPtr;
typedef std::vector<Force3DSensorPtr> Force3DSensorList;

class MMM_IMPORT_EXPORT Force3DSensor : public AttachedSensor<Force3DSensorMeasurement>
{

public:
    static Force3DSensorPtr loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    Force3DSensor();

    Force3DSensor(const std::string &segment, const std::string &offsetUnit = "mm", const Eigen::Matrix4f &offset = Eigen::Matrix4f::Identity(), const std::string &description = std::string());

    std::shared_ptr<BasicSensor<Force3DSensorMeasurement> > cloneConfiguration();

    std::string getType();

    std::string getVersion();

    int getPriority();

    static constexpr const char* TYPE = "Force3D";

    static constexpr const char* VERSION = "1.0";

private:
    void loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type);
};


}
#endif // __MMM_Force3DSENSOR_H_

