#include "Force3DSensorFactory.h"

#include <boost/extension/extension.hpp>

#include "Force3DSensor.h"

namespace MMM
{

// register this factory
SensorFactory::SubClassRegistry Force3DSensorFactory::registry(NAME_STR<Force3DSensor>(), &Force3DSensorFactory::createInstance);

Force3DSensorFactory::Force3DSensorFactory() : SensorFactory() {}

Force3DSensorFactory::~Force3DSensorFactory() {}

SensorPtr Force3DSensorFactory::createSensor(simox::xml::RapidXMLWrapperNodePtr sensorNode, const std::filesystem::path &filePath)
{
    return Force3DSensor::loadSensorXML(sensorNode, filePath);
}

std::string Force3DSensorFactory::getName()
{
    return NAME_STR<Force3DSensor>();
}

SensorFactoryPtr Force3DSensorFactory::createInstance(void *)
{
    return SensorFactoryPtr(new Force3DSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorFactoryPtr getFactory() {
    return SensorFactoryPtr(new Force3DSensorFactory);
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorFactory::VERSION;
}

}
