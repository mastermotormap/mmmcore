#include <cassert>
#include <map>

#include "Force3DSensor.h"

namespace MMM
{

Force3DSensorPtr Force3DSensor::loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    Force3DSensorPtr sensor(new Force3DSensor());
    sensor->loadSensor(node, filePath);
    return sensor;
}

Force3DSensor::Force3DSensor() : AttachedSensor()
{
}

Force3DSensor::Force3DSensor(const std::string &segment, const std::string &offsetUnit, const Eigen::Matrix4f &offset, const std::string &description) :
    AttachedSensor(segment, offsetUnit, offset, description)
{
}

std::shared_ptr<BasicSensor<Force3DSensorMeasurement> > Force3DSensor::cloneConfiguration() {
    Force3DSensorPtr m(new Force3DSensor(segment, offsetUnit, offset, description));
    return m;
}

void Force3DSensor::loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type)
{
    assert(node);
    assert(node->name() == xml::tag::MEASUREMENT);

    simox::xml::RapidXMLWrapperNodePtr forceNode = node->first_node("Force");
    Force3DSensorMeasurementPtr m(new Force3DSensorMeasurement(timestep, forceNode->value_eigen_vec3(), type));
    addSensorMeasurement(m);
}

std::string Force3DSensor::getType() {
    return TYPE;
}

std::string Force3DSensor::getVersion() {
    return VERSION;
}

int Force3DSensor::getPriority() {
    return 60;
}

}
