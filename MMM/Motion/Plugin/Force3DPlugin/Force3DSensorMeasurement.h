/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_Force3DSENSORMEASUREMENT_H_
#define __MMM_Force3DSENSORMEASUREMENT_H_

#include <MMM/Motion/Sensor/SensorMeasurement.h>
#include <MMM/Motion/Sensor/InterpolatableSensorMeasurement.h>

#include <Eigen/Core>

namespace MMM
{
class Force3DSensorMeasurement;

typedef std::shared_ptr<Force3DSensorMeasurement> Force3DSensorMeasurementPtr;

class MMM_IMPORT_EXPORT Force3DSensorMeasurement : public InterpolatableSensorMeasurement<Force3DSensorMeasurement>, SMCloneable<Force3DSensorMeasurement>
{

public:
    Force3DSensorMeasurement(float timestep, const Eigen::Vector3f &force, SensorMeasurementType type = SensorMeasurementType::MEASURED);

    SensorMeasurementPtr clone();

    bool equals(SensorMeasurementPtr sensorMeasurement);

    Force3DSensorMeasurementPtr clone(float newTimestep);

    Eigen::Vector3f getForce();

protected:
    Force3DSensorMeasurementPtr interpolate(Force3DSensorMeasurementPtr other, float timestep);

    void appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode);

private:
    Eigen::Vector3f force;
};
}
#endif // __MMM_Force3DSENSORMEASUREMENT_H_
