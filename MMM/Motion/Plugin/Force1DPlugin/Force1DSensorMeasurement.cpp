#include "Force1DSensorMeasurement.h"

#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>

namespace MMM
{

Force1DSensorMeasurement::Force1DSensorMeasurement(float timestep, float force, SensorMeasurementType type) :
    InterpolatableSensorMeasurement(timestep, type),
    force(force)
{
}

SensorMeasurementPtr Force1DSensorMeasurement::clone() {
    return clone(timestep);
}

bool Force1DSensorMeasurement::equals(SensorMeasurementPtr sensorMeasurement) {
    Force1DSensorMeasurementPtr ptr = std::dynamic_pointer_cast<Force1DSensorMeasurement>(sensorMeasurement);
    if (ptr) {
        return std::abs(force - ptr->force) < 0.000001;
    }
    return false;
}

Force1DSensorMeasurementPtr Force1DSensorMeasurement::clone(float newTimestep) {
    Force1DSensorMeasurementPtr clonedSensorMeasurement(new Force1DSensorMeasurement(newTimestep, force, type));
    return clonedSensorMeasurement;
}

void Force1DSensorMeasurement::appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode) {
    measurementNode->append_node("Force")->append_data_node(force);
}

Force1DSensorMeasurementPtr Force1DSensorMeasurement::interpolate(Force1DSensorMeasurementPtr other, float timestep) {
    float interpolatedForce = InterpolatableSensorMeasurement::linearInterpolation(this->force, this->timestep, other->force, other->timestep, timestep);
    Force1DSensorMeasurementPtr interpolatedSensorMeasurement(new Force1DSensorMeasurement(timestep, interpolatedForce, SensorMeasurementType::INTERPOLATED));
    return interpolatedSensorMeasurement;
}

float Force1DSensorMeasurement::getForce() {
    return force;
}

}
