/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_FORCE1DSENSOR_H_
#define __MMM_FORCE1DSENSOR_H_

#include <MMM/Motion/Sensor/AttachedSensor.h>
#include <MMM/Motion/Sensor/SensorMeasurement.h>
#include "Force1DSensorMeasurement.h"

#include <vector>

namespace MMM
{

class Force1DSensor;

typedef std::shared_ptr<Force1DSensor> Force1DSensorPtr;
typedef std::vector<Force1DSensorPtr> Force1DSensorList;

class MMM_IMPORT_EXPORT Force1DSensor : public AttachedSensor<Force1DSensorMeasurement>
{

public:
    static Force1DSensorPtr loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    Force1DSensor();

    Force1DSensor(const std::string &segment, const std::string &offsetUnit = "mm", const Eigen::Matrix4f &offset = Eigen::Matrix4f::Identity(), const std::string &description = std::string());

    std::shared_ptr<BasicSensor<Force1DSensorMeasurement> > cloneConfiguration();

    std::string getType();

    std::string getVersion();

    int getPriority();

    static constexpr const char* TYPE = "Force1D";

    static constexpr const char* VERSION = "1.0";

private:
    void loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type);
};


}
#endif // __MMM_Force1DSENSOR_H_

