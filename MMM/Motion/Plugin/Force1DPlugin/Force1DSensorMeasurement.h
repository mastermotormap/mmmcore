/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_FORCE1DSENSORMEASUREMENT_H_
#define __MMM_FORCE1DSENSORMEASUREMENT_H_

#include <MMM/Motion/Sensor/SensorMeasurement.h>
#include <MMM/Motion/Sensor/InterpolatableSensorMeasurement.h>

#include <Eigen/Core>

namespace MMM
{
class Force1DSensorMeasurement;

typedef std::shared_ptr<Force1DSensorMeasurement> Force1DSensorMeasurementPtr;

class MMM_IMPORT_EXPORT Force1DSensorMeasurement : public InterpolatableSensorMeasurement<Force1DSensorMeasurement>, SMCloneable<Force1DSensorMeasurement>
{

public:
    Force1DSensorMeasurement(float timestep, float force, SensorMeasurementType type = SensorMeasurementType::MEASURED);

    SensorMeasurementPtr clone();

    bool equals(SensorMeasurementPtr sensorMeasurement);

    Force1DSensorMeasurementPtr clone(float newTimestep);

    float getForce();

protected:
    Force1DSensorMeasurementPtr interpolate(Force1DSensorMeasurementPtr other, float timestep);

    void appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode);

private:
    float force;
};
}
#endif // __MMM_Force1DSENSORMEASUREMENT_H_
