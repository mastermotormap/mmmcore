#include <cassert>
#include <map>

#include "Force1DSensor.h"

namespace MMM
{

Force1DSensorPtr Force1DSensor::loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    Force1DSensorPtr sensor(new Force1DSensor());
    sensor->loadSensor(node, filePath);
    return sensor;
}

Force1DSensor::Force1DSensor() : AttachedSensor()
{
}

Force1DSensor::Force1DSensor(const std::string &segment, const std::string &offsetUnit, const Eigen::Matrix4f &offset, const std::string &description) :
    AttachedSensor(segment, offsetUnit, offset, description)
{
}

std::shared_ptr<BasicSensor<Force1DSensorMeasurement> > Force1DSensor::cloneConfiguration() {
    Force1DSensorPtr m(new Force1DSensor(segment, offsetUnit, offset, description));
    return m;
}

void Force1DSensor::loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type)
{
    assert(node);
    assert(node->name() == xml::tag::MEASUREMENT);

    simox::xml::RapidXMLWrapperNodePtr forceNode = node->first_node("Force");
    Force1DSensorMeasurementPtr m(new Force1DSensorMeasurement(timestep, forceNode->value_<float>(), type));
    addSensorMeasurement(m);
}

std::string Force1DSensor::getType() {
    return TYPE;
}

std::string Force1DSensor::getVersion() {
    return VERSION;
}

int Force1DSensor::getPriority() {
    return 60;
}

}
