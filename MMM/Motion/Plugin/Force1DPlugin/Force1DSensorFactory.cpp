#include "Force1DSensorFactory.h"

#include <boost/extension/extension.hpp>

#include "Force1DSensor.h"

namespace MMM
{

// register this factory
SensorFactory::SubClassRegistry Force1DSensorFactory::registry(NAME_STR<Force1DSensor>(), &Force1DSensorFactory::createInstance);

Force1DSensorFactory::Force1DSensorFactory() : SensorFactory() {}

Force1DSensorFactory::~Force1DSensorFactory() = default;

SensorPtr Force1DSensorFactory::createSensor(simox::xml::RapidXMLWrapperNodePtr sensorNode, const std::filesystem::path &filePath)
{
    return Force1DSensor::loadSensorXML(sensorNode, filePath);
}

std::string Force1DSensorFactory::getName()
{
    return NAME_STR<Force1DSensor>();
}

SensorFactoryPtr Force1DSensorFactory::createInstance(void *)
{
    return SensorFactoryPtr(new Force1DSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorFactoryPtr getFactory() {
    return SensorFactoryPtr(new Force1DSensorFactory);
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorFactory::VERSION;
}

}
