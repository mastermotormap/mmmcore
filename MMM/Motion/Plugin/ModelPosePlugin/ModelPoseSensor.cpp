#include <cassert>
#include <map>

#include <MMM/Motion/Legacy/LegacyMotion.h>
#include <SimoxUtility/math/pose/invert.h>

#include "ModelPoseSensor.h"

namespace MMM
{

ModelPoseSensorPtr ModelPoseSensor::loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    ModelPoseSensorPtr sensor(new ModelPoseSensor());
    sensor->loadSensor(node, filePath);
    return sensor;
}

ModelPoseSensorPtr ModelPoseSensor::createFromLegacy(LegacyMotionPtr motion) {
    ModelPoseSensorPtr sensor(new ModelPoseSensor());
    for (auto motionFrame : motion->getMotionFrames()) {
        ModelPoseSensorMeasurementPtr mpMeasurement(new ModelPoseSensorMeasurement(motionFrame->timestep, motionFrame->getRootPos(), motionFrame->getRootRot()));
        sensor->addSensorMeasurement(mpMeasurement);
    }
    return sensor;
}

ModelPoseSensor::ModelPoseSensor(const std::string &description) : InterpolatableSensor(description)
{
}

bool ModelPoseSensor::checkModel(ModelPtr model) {
    if (!model) {
        MMM_ERROR << "Sensor '" << uniqueName << "' needs a model."  << std::endl;
        return false;
    }
    return true;
}

std::shared_ptr<BasicSensor<ModelPoseSensorMeasurement> > ModelPoseSensor::cloneConfiguration() {
    ModelPoseSensorPtr m(new ModelPoseSensor(description));
    return m;
}

void ModelPoseSensor::loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    assert(node);
    assert(node->name() == "Configuration");
}

void ModelPoseSensor::loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type)
{
    assert(node);
    assert(node->name() == xml::tag::MEASUREMENT);

    simox::xml::RapidXMLWrapperNodePtr posNode = node->first_node("RootPosition");
    simox::xml::RapidXMLWrapperNodePtr rotNode = node->first_node("RootRotation");
    ModelPoseSensorMeasurementPtr m(new ModelPoseSensorMeasurement(timestep, posNode->value_eigen_vec3(), rotNode->value_eigen_vec3(), type));
    addSensorMeasurement(m);
}

void ModelPoseSensor::appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr /*node*/, const std::filesystem::path &filePath) {
    // Do nothing
}

bool ModelPoseSensor::hasSensorConfigurationContent() const
{
    return true; // TODO
}

bool ModelPoseSensor::equalsConfiguration(SensorPtr other) {
    ModelPoseSensorPtr ptr = std::dynamic_pointer_cast<ModelPoseSensor>(other);
    if (ptr) {
        return true;
    }
    else return false;
}

std::string ModelPoseSensor::getType() {
    return TYPE;
}

std::string ModelPoseSensor::getVersion() {
    return VERSION;
}

int ModelPoseSensor::getPriority() {
    return 100;
}

void ModelPoseSensor::shiftMotionData(const Eigen::Vector3f &positionDelta) {
    for (auto measurement : measurements) {
        measurement.second->shiftMotion(positionDelta);
    }
}

void ModelPoseSensor::scaleMotionData(float scaleFactor) {
    for (auto measurement : measurements) {
        measurement.second->scaleMotion(scaleFactor);
    }
}

void ModelPoseSensor::mirrorMotionData(const Eigen::Matrix4f &referencePose, int referenceAxis) {
    // Better performance to do calculation here
    auto invertedReferencePose = simox::math::inverted_pose(referencePose);
    Eigen::Matrix4f reflectionMatrix = Eigen::Matrix4f::Identity();
    reflectionMatrix(referenceAxis, referenceAxis) = -1;
    for (auto measurement : measurements) {
        measurement.second->mirrorMotion(referencePose, invertedReferencePose, reflectionMatrix);
    }
}

Eigen::Matrix4f ModelPoseSensor::getRootPose(float timestep, bool extend) {
    auto measurement = getDerivedMeasurement(timestep);
    if (measurement)
        return measurement->getRootPose();
    else if (extend && measurements.size() > 0) {
        if (timestep < measurements.begin()->first)
            return measurements.begin()->second->getRootPose();
        else
            return measurements.rbegin()->second->getRootPose();
    }
    else
        throw Exception::MMMException("Model Pose Sensor " + uniqueName + " has no measurement at timestep " + simox::alg::to_string(timestep));
}

std::map<float, float> ModelPoseSensor::getPositions(unsigned int index) {
    std::map<float, float> positions;
    if (index <= 2) {
        for (auto timestep : getTimesteps()) {
            positions[timestep] = getDerivedMeasurement(timestep)->getRootPosition()[index];
        }
    }
    return positions;

}

std::map<float, float> ModelPoseSensor::getAngles(unsigned int index) {
    std::map<float, float> angles;
    if (index >= 0 && index <= 2) {
        for (auto timestep : getTimesteps()) {
            angles[timestep] = getDerivedMeasurement(timestep)->getRootPosition()[index];
        }
    }
    return angles;
}

std::map<float, Eigen::Matrix4f> ModelPoseSensor::getRootPoses() {
    std::map<float, Eigen::Matrix4f> poses;
    for (auto timestep : getTimesteps()) {
        poses[timestep] = getDerivedMeasurement(timestep)->getRootPose();
    }
    return poses;
}

void ModelPoseSensor::setOffset(const Eigen::Matrix4f &offset) {
    for (auto m : measurements) {
        m.second->setOffset(offset);
    }
}

void ModelPoseSensor::transform(const Eigen::Matrix4f &offsetTransformation) {
    for (auto m : measurements) {
        m.second->transform(offsetTransformation);
    }
}

Eigen::Matrix4f ModelPoseSensor::getOffset() {
    if (measurements.size() > 0) return measurements.begin()->second->getOffset();
    return Eigen::Matrix4f::Identity();
}

void ModelPoseSensor::setFixedRootPose(const Eigen::MatrixXf &rootPose, bool reduceMeasurements) {
    auto firstIt = measurements.begin();
    auto lastIt = std::prev(measurements.end());
    for (auto it = firstIt; it != measurements.end();) {
        if (reduceMeasurements && it != firstIt && it != lastIt) {
            it = measurements.erase(it);
        }
        else {
            it->second->setRootPose(rootPose);
            it++;
        }
    }
}

}
