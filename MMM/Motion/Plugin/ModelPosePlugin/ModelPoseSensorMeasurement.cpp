#include "ModelPoseSensorMeasurement.h"

#include <SimoxUtility/math/convert.h>
#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>
#include <VirtualRobot/Robot.h>

#include <Eigen/Geometry>

namespace MMM
{


ModelPoseSensorMeasurement::ModelPoseSensorMeasurement(float timestep, const Eigen::Matrix4f &rootPose, SensorMeasurementType type) :
    InterpolatableSensorMeasurement(timestep, type),
    rootPose(rootPose), offset(Eigen::Matrix4f::Identity())
{
}

ModelPoseSensorMeasurement::ModelPoseSensorMeasurement(float timestep, const Eigen::Vector3f &rootPosition, const Eigen::Vector3f &rootRotation, SensorMeasurementType type) :
    InterpolatableSensorMeasurement(timestep, type),
    rootPose(simox::math::pos_rpy_to_mat4f(rootPosition, rootRotation)), offset(Eigen::Matrix4f::Identity())
{
}

ModelPoseSensorMeasurement::ModelPoseSensorMeasurement(float timestep, const Eigen::Vector3f &rootPosition, const Eigen::Quaternionf &rootRotation, SensorMeasurementType type) :
    InterpolatableSensorMeasurement(timestep, type),
    rootPose(simox::math::pos_quat_to_mat4f(rootPosition, rootRotation)), offset(Eigen::Matrix4f::Identity())
{
}


SensorMeasurementPtr ModelPoseSensorMeasurement::clone() {
    return clone(timestep);
}

bool ModelPoseSensorMeasurement::equals(SensorMeasurementPtr sensorMeasurement) {
    ModelPoseSensorMeasurementPtr ptr = std::dynamic_pointer_cast<ModelPoseSensorMeasurement>(sensorMeasurement);
    if (ptr) {
        return getRootPose() == ptr->getRootPose();
    }
    else return false;
}

ModelPoseSensorMeasurementPtr ModelPoseSensorMeasurement::clone(float newTimestep) {
    ModelPoseSensorMeasurementPtr clonedSensorMeasurement(new ModelPoseSensorMeasurement(newTimestep, getRootPose(), type));
    return clonedSensorMeasurement;
}

void ModelPoseSensorMeasurement::appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode) {
    measurementNode->append_node("RootPosition")->append_data_node(getRootPosition());
    measurementNode->append_node("RootRotation")->append_data_node(getRootRotationRPY());
}

Eigen::Vector3f ModelPoseSensorMeasurement::getRootPosition() {
    return simox::math::mat4f_to_pos(getRootPose());
}

Eigen::Vector3f ModelPoseSensorMeasurement::getRootRotationRPY() {
    return simox::math::mat4f_to_rpy(getRootPose());
}

Eigen::Quaternionf ModelPoseSensorMeasurement::getRootRotation() {
     return simox::math::mat4f_to_quat(getRootPose());
}

Eigen::Matrix4f ModelPoseSensorMeasurement::getRootPose() {
    return rootPose * offset;
}

ModelPoseSensorMeasurementPtr ModelPoseSensorMeasurement::interpolate(ModelPoseSensorMeasurementPtr other, float timestep) {
    float scalar = (timestep - this->timestep) / (other->timestep - this->timestep);
    Eigen::Vector3f interpolatedRootPosition = InterpolatableSensorMeasurement::linearInterpolation(this->getRootPosition(), this->timestep, other->getRootPosition(), other->timestep, timestep);
    Eigen::Quaternionf interpolatedRootRotation = getRootRotation().slerp(scalar, other->getRootRotation());
    ModelPoseSensorMeasurementPtr interpolatedSensorMeasurement(new ModelPoseSensorMeasurement(timestep, interpolatedRootPosition, interpolatedRootRotation, SensorMeasurementType::INTERPOLATED));
    return interpolatedSensorMeasurement;
}

void ModelPoseSensorMeasurement::shiftMotion(const Eigen::Vector3f &positionDelta) {
    rootPose.block(0,3,3,1) += positionDelta;
}

Eigen::Matrix4f ModelPoseSensorMeasurement::getOffset() {
    return offset;
}

void ModelPoseSensorMeasurement::setOffset(const Eigen::Matrix4f &offset) {
   this->offset = offset;
}

void ModelPoseSensorMeasurement::transform(const Eigen::Matrix4f &offsetTransformation) {
    offset *= offsetTransformation;
}

void ModelPoseSensorMeasurement::scaleMotion(float scaleFactor) {
    rootPose.block(0,3,3,1) *= scaleFactor;
}

void ModelPoseSensorMeasurement::setRootPose(const Eigen::Matrix4f &rootPose, bool resetOffset) {
    this->rootPose = rootPose;
    if (resetOffset)
        setOffset(Eigen::Matrix4f::Identity());
}

void ModelPoseSensorMeasurement::mirrorMotion(const Eigen::Matrix4f &referencePose, const Eigen::Matrix4f &invertedReferencePose, const Eigen::Matrix4f &reflectionMatrix) {
    auto localPose = invertedReferencePose * rootPose;

    Eigen::Matrix4f reflectedOrientation = Eigen::Matrix4f::Identity();
    reflectedOrientation = (referencePose * reflectionMatrix) * (localPose * reflectionMatrix);

    Eigen::Matrix4f reflectedPosition = Eigen::Matrix4f::Identity();
    reflectedPosition.block(0,3,3,1) = localPose.block(0,3,3,1);
    reflectedPosition.block(0,3,1,1) *= -1;
    reflectedPosition = referencePose * reflectedPosition;

    rootPose.block(0,3,3,1) = reflectedPosition.block(0,3,3,1);
    rootPose.block(0,0,3,3) = reflectedOrientation.block(0,0,3,3);
}

void ModelPoseSensorMeasurement::initializeModel(ModelPtr model, bool update) {
    model->setGlobalPose(getRootPose(), update);
}

}
