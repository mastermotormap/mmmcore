#include "ModelPoseSensorFactory.h"

#include <boost/extension/extension.hpp>

#include "ModelPoseSensor.h"

namespace MMM
{

// register this factory
SensorFactory::SubClassRegistry ModelPoseSensorFactory::registry(NAME_STR<ModelPoseSensor>(), &ModelPoseSensorFactory::createInstance);

ModelPoseSensorFactory::ModelPoseSensorFactory() : SensorFactory() {}

ModelPoseSensorFactory::~ModelPoseSensorFactory() = default;

SensorPtr ModelPoseSensorFactory::createSensor(simox::xml::RapidXMLWrapperNodePtr sensorNode, const std::filesystem::path &filePath)
{
    return ModelPoseSensor::loadSensorXML(sensorNode, filePath);
}

std::vector<SensorPtr> ModelPoseSensorFactory::createSensors(LegacyMotionPtr motion) {
    return { ModelPoseSensor::createFromLegacy(motion) };
}

std::string ModelPoseSensorFactory::getName()
{
    return NAME_STR<ModelPoseSensor>();
}

SensorFactoryPtr ModelPoseSensorFactory::createInstance(void *)
{
    return SensorFactoryPtr(new ModelPoseSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorFactoryPtr getFactory() {
    return SensorFactoryPtr(new ModelPoseSensorFactory);
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorFactory::VERSION;
}

}
