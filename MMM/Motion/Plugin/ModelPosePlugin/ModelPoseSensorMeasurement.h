/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MODELPOSESENSORMEASUREMENT_H_
#define __MMM_MODELPOSESENSORMEASUREMENT_H_

#include <MMM/Motion/Sensor/SensorMeasurement.h>
#include <MMM/Motion/Sensor/InterpolatableSensorMeasurement.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

namespace MMM
{
class ModelPoseSensorMeasurement;

typedef std::shared_ptr<ModelPoseSensorMeasurement> ModelPoseSensorMeasurementPtr;

class MMM_IMPORT_EXPORT ModelPoseSensorMeasurement : public InterpolatableSensorMeasurement<ModelPoseSensorMeasurement>, SMCloneable<ModelPoseSensorMeasurement>
{

public:
    ModelPoseSensorMeasurement(float timestep, const Eigen::Matrix4f &rootPose, SensorMeasurementType type = SensorMeasurementType::MEASURED);

    ModelPoseSensorMeasurement(float timestep, const Eigen::Vector3f &rootPosition, const Eigen::Vector3f &rootRotation, SensorMeasurementType type = SensorMeasurementType::MEASURED);

    ModelPoseSensorMeasurement(float timestep, const Eigen::Vector3f &rootPosition, const Eigen::Quaternionf &rootRotation, SensorMeasurementType type = SensorMeasurementType::MEASURED);

    SensorMeasurementPtr clone() override;

    bool equals(SensorMeasurementPtr sensorMeasurement) override;

    ModelPoseSensorMeasurementPtr clone(float newTimestep) override;

    Eigen::Vector3f getRootPosition();

    Eigen::Vector3f getRootRotationRPY();

    Eigen::Quaternionf getRootRotation();

    Eigen::Matrix4f getRootPose();

    Eigen::Matrix4f getRootPoseWithoutOffset() {
        return rootPose;
    }

    void shiftMotion(const Eigen::Vector3f &positionDelta);

    Eigen::Matrix4f getOffset();

    void setOffset(const Eigen::Matrix4f &offset);

    void transform(const Eigen::Matrix4f &offsetTransformation);

    void scaleMotion(float scaleFactor);

    void mirrorMotion(const Eigen::Matrix4f &referencePose, const Eigen::Matrix4f &invertedReferencePose, const Eigen::Matrix4f &reflectionMatrix);

    void setRootPose(const Eigen::Matrix4f &rootPose, bool resetOffset = true);

    void initializeModel(ModelPtr model, bool update) override;

protected:
    ModelPoseSensorMeasurementPtr interpolate(ModelPoseSensorMeasurementPtr other, float timestep) override;

    void appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode) override;

private:
    Eigen::Matrix4f rootPose;
    Eigen::Matrix4f offset;
};
}
#endif // __MMM_MODELPOSESENSORMEASUREMENT_H_
