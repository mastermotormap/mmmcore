/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MODELPOSESENSOR_H_
#define __MMM_MODELPOSESENSOR_H_

#include <MMM/Motion/Sensor/BasicModelPoseSensor.h>
#include <MMM/Motion/Sensor/InterpolatableSensor.h>
#include <MMM/Motion/Sensor/SensorMeasurement.h>
#include "ModelPoseSensorMeasurement.h"

#include <vector>

namespace MMM
{

class ModelPoseSensor;

typedef std::shared_ptr<ModelPoseSensor> ModelPoseSensorPtr;
typedef std::vector<ModelPoseSensorPtr> ModelPoseSensorList;

class MMM_IMPORT_EXPORT ModelPoseSensor : public InterpolatableSensor<ModelPoseSensorMeasurement>, public BasicModelPoseSensor
{

public:

    static ModelPoseSensorPtr loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    static ModelPoseSensorPtr createFromLegacy(LegacyMotionPtr motion);

    ModelPoseSensor(const std::string &description = std::string());

    virtual ~ModelPoseSensor() = default;

    bool checkModel(ModelPtr model) override;

    std::shared_ptr<BasicSensor<ModelPoseSensorMeasurement> > cloneConfiguration() override;

    bool equalsConfiguration(SensorPtr other) override;

    Eigen::Matrix4f getRootPose(float timestep, bool extend = false) override;

    std::string getType() override;

    std::string getVersion() override;

    int getPriority() override;

    void shiftMotionData(const Eigen::Vector3f &positionDelta) override;

    void scaleMotionData(float scaleFactor) override;

    void mirrorMotionData(const Eigen::Matrix4f &referencePose, int referenceAxis) override;

    std::map<float, float> getPositions(unsigned int index) override;

    std::map<float, float> getAngles(unsigned int index) override;

    std::map<float, Eigen::Matrix4f> getRootPoses() override;

    virtual void setOffset(const Eigen::Matrix4f &offset) override;

    virtual void transform(const Eigen::Matrix4f &offsetTransformation) override;

    virtual Eigen::Matrix4f getOffset() override;

    virtual void setFixedRootPose(const Eigen::MatrixXf &rootPose, bool reduceMeasurements = true) override;

    static constexpr const char* TYPE = BasicModelPoseSensor::TYPE;

    static constexpr const char* VERSION = "1.0";

private:
    void loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) override;

    void loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type) override;

    void appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) override;

    bool hasSensorConfigurationContent() const override;
};


}
#endif // __MMM_ModelPoseSENSOR_H_

