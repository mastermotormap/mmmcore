#include "IMUSensorFactory.h"

#include <boost/extension/extension.hpp>

#include "IMUSensor.h"

namespace MMM
{

// register this factory
SensorFactory::SubClassRegistry IMUSensorFactory::registry(NAME_STR<IMUSensor>(), &IMUSensorFactory::createInstance);

IMUSensorFactory::IMUSensorFactory() : SensorFactory() {}

IMUSensorFactory::~IMUSensorFactory() = default;

SensorPtr IMUSensorFactory::createSensor(simox::xml::RapidXMLWrapperNodePtr sensorNode, const std::filesystem::path &filePath)
{
    return IMUSensor::loadSensorXML(sensorNode, filePath);
}

std::string IMUSensorFactory::getName()
{
    return NAME_STR<IMUSensor>();
}

SensorFactoryPtr IMUSensorFactory::createInstance(void *)
{
    return SensorFactoryPtr(new IMUSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorFactoryPtr getFactory() {
    return SensorFactoryPtr(new IMUSensorFactory);
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorFactory::VERSION;
}

}
