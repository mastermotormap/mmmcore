#include "IMUSensorMeasurement.h"

#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>

namespace MMM
{


IMUSensorMeasurement::IMUSensorMeasurement(float timestep, const Eigen::Vector3f &acceleration, const Eigen::Vector3f &gyroscope, const Eigen::Vector3f &magnetometer, SensorMeasurementType type):
    InterpolatableSensorMeasurement(timestep, type),
    acceleration(acceleration)
{
    this->gyroscope = std::optional<Eigen::Vector3f>{gyroscope};
    this->magnetometer = std::optional<Eigen::Vector3f>{magnetometer};
}

IMUSensorMeasurement::IMUSensorMeasurement(float timestep, const Eigen::Vector3f &acceleration, SensorMeasurementType type):
InterpolatableSensorMeasurement(timestep, type),
acceleration(acceleration)
{

}

IMUSensorMeasurement::IMUSensorMeasurement(float timestep, const Eigen::Vector3f &acceleration, const std::optional<Eigen::Vector3f> &gyroscope, const std::optional<Eigen::Vector3f> &magnetometer, SensorMeasurementType type):
    InterpolatableSensorMeasurement(timestep, type),
    acceleration(acceleration),
    gyroscope(gyroscope),
    magnetometer(magnetometer)
{

}


SensorMeasurementPtr IMUSensorMeasurement::clone() {
    return clone(timestep);
}

bool IMUSensorMeasurement::equals(SensorMeasurementPtr sensorMeasurement) {
    IMUSensorMeasurementPtr ptr = std::dynamic_pointer_cast<IMUSensorMeasurement>(sensorMeasurement);
    if (ptr) {
        if(gyroscope && magnetometer){
            return (acceleration == ptr->acceleration) && (gyroscope.value() == ptr->gyroscope.value()) && (magnetometer.value() == ptr->magnetometer.value());
        }else{
            return (acceleration == ptr->acceleration);
        }

    }
    return false;
}

IMUSensorMeasurementPtr IMUSensorMeasurement::clone(float newTimestep) {
    IMUSensorMeasurementPtr clonedSensorMeasurement(new IMUSensorMeasurement(newTimestep, acceleration, gyroscope, magnetometer, type));
    return clonedSensorMeasurement;
}

void IMUSensorMeasurement::appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode) {
    measurementNode->append_node("Acceleration")->append_data_node(acceleration);
    if(gyroscope) measurementNode->append_node("Gyroscope")->append_data_node(gyroscope.value());
    if(magnetometer) measurementNode->append_node("Magnetometer")->append_data_node(magnetometer.value());
}

IMUSensorMeasurementPtr IMUSensorMeasurement::interpolate(IMUSensorMeasurementPtr other, float timestep) {
    Eigen::Vector3f interpolatedAcceleration;
    std::optional<Eigen::Vector3f> interpolatedGyroscope;
    std::optional<Eigen::Vector3f> interpolatedMagnetometer;


    interpolatedAcceleration = InterpolatableSensorMeasurement::linearInterpolation(this->acceleration, this->timestep, other->acceleration, other->timestep, timestep);

    if(other->gyroscope){
        interpolatedGyroscope = std::optional<Eigen::Vector3f>{InterpolatableSensorMeasurement::linearInterpolation(this->gyroscope.value(), this->timestep, other->gyroscope.value(), other->timestep, timestep)};
    }
    if(other->magnetometer){
        interpolatedMagnetometer = std::optional<Eigen::Vector3f>{InterpolatableSensorMeasurement::linearInterpolation(this->magnetometer.value(), this->timestep, other->magnetometer.value(), other->timestep, timestep)};
    }
    IMUSensorMeasurementPtr interpolatedSensorMeasurement(new IMUSensorMeasurement(timestep, interpolatedAcceleration, interpolatedGyroscope, interpolatedMagnetometer, SensorMeasurementType::INTERPOLATED));
    return interpolatedSensorMeasurement;
}

Eigen::Vector3f IMUSensorMeasurement::getAcceleration() {
    return acceleration;
}

std::optional<Eigen::Vector3f> IMUSensorMeasurement::getGyroscope()
{
    return gyroscope;
}

std::optional<Eigen::Vector3f> IMUSensorMeasurement::getMagnetometer()
{
    return magnetometer;
}

}
