#include <cassert>
#include <map>

#include "IMUSensor.h"

namespace MMM
{

IMUSensorPtr IMUSensor::loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    IMUSensorPtr sensor(new IMUSensor());
    sensor->loadSensor(node, filePath);
    return sensor;
}

IMUSensorPtr IMUSensor::loadSensorConfigurationXML(simox::xml::RapidXMLWrapperNodePtr configurationNode, const std::filesystem::path &filePath) {
    IMUSensorPtr sensor(new IMUSensor());
    sensor->loadConfigurationXML(configurationNode, filePath);
    return sensor;
}

IMUSensor::IMUSensor() : AttachedSensor()
{
}

IMUSensor::IMUSensor(const std::string &segment, const std::string &offsetUnit, const Eigen::Matrix4f &offset, const std::string &description) :
    AttachedSensor(segment, offsetUnit, offset, description)
{
}

std::shared_ptr<BasicSensor<IMUSensorMeasurement> > IMUSensor::cloneConfiguration() {
    IMUSensorPtr m(new IMUSensor(segment, offsetUnit, offset, description));
    return m;
}

void IMUSensor::loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type)
{
    assert(node);
    assert(node->name() == xml::tag::MEASUREMENT);
    Eigen::Vector3f acceleration;
    std::optional<Eigen::Vector3f> gyroscope, magnetometer;
    IMUSensorMeasurementPtr m;

    acceleration = node->first_node("Acceleration")->value_eigen_vec3();
    if(node->has_node("Gyroscope")){gyroscope = std::optional<Eigen::Vector3f>{node->first_node("Gyroscope")->value_eigen_vec3()};}
    if(node->has_node("Magnetometer")){magnetometer = std::optional<Eigen::Vector3f>{node->first_node("Magnetometer")->value_eigen_vec3()};}

    if(node->has_node("Acceleration") && node->has_node("Gyroscope") && node->has_node("Magnetometer")){
        m.reset(new IMUSensorMeasurement(timestep, acceleration, gyroscope, magnetometer, type));
    }else{
        m.reset(new IMUSensorMeasurement(timestep, acceleration, type));
    }
    addSensorMeasurement(m);
}

std::string IMUSensor::getType() {
    return TYPE;
}

std::string IMUSensor::getVersion() {
    return VERSION;
}

int IMUSensor::getPriority() {
    return 70;
}

}
