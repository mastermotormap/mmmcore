/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_IMUSENSOR_H_
#define __MMM_IMUSENSOR_H_

#include <MMM/Motion/Sensor/AttachedSensor.h>
#include <MMM/Motion/Sensor/SensorMeasurement.h>
#include "IMUSensorMeasurement.h"

#include <Eigen/Core>
#include <vector>

namespace MMM
{

class IMUSensor;

typedef std::shared_ptr<IMUSensor> IMUSensorPtr;
typedef std::vector<IMUSensorPtr> IMUSensorList;

class MMM_IMPORT_EXPORT IMUSensor : public AttachedSensor<IMUSensorMeasurement>
{

public:
    static IMUSensorPtr loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    static IMUSensorPtr loadSensorConfigurationXML(simox::xml::RapidXMLWrapperNodePtr configurationNode, const std::filesystem::path &filePath);

    IMUSensor();

    IMUSensor(const std::string &segment, const std::string &offsetUnit = "mm", const Eigen::Matrix4f &offset = Eigen::Matrix4f::Identity(), const std::string &description = std::string());

    std::shared_ptr<BasicSensor<IMUSensorMeasurement> > cloneConfiguration();

    std::string getType();

    std::string getVersion();

    int getPriority();

    static constexpr const char* TYPE = "IMU";

    static constexpr const char* VERSION = "1.0";

private:
    void loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type);
};


}
#endif // __MMM_IMUSENSOR_H_

