/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_IMUSENSORMEASUREMENT_H_
#define __MMM_IMUSENSORMEASUREMENT_H_

#include <MMM/Motion/Sensor/SensorMeasurement.h>
#include <MMM/Motion/Sensor/InterpolatableSensorMeasurement.h>
#include <optional>
#include <Eigen/Core>

namespace MMM
{
class IMUSensorMeasurement;

typedef std::shared_ptr<IMUSensorMeasurement> IMUSensorMeasurementPtr;

class MMM_IMPORT_EXPORT IMUSensorMeasurement : public InterpolatableSensorMeasurement<IMUSensorMeasurement>, SMCloneable<IMUSensorMeasurement>
{

public:
    IMUSensorMeasurement(float timestep, const Eigen::Vector3f &acceleration, const Eigen::Vector3f &gyroscope, const Eigen::Vector3f &magnetometer, SensorMeasurementType type = SensorMeasurementType::MEASURED);
    IMUSensorMeasurement(float timestep, const Eigen::Vector3f &acceleration, SensorMeasurementType type = SensorMeasurementType::MEASURED);
    IMUSensorMeasurement(float timestep, const Eigen::Vector3f &acceleration, const std::optional<Eigen::Vector3f> &gyroscope, const std::optional<Eigen::Vector3f> &magnetometer, SensorMeasurementType type = SensorMeasurementType::MEASURED);
    SensorMeasurementPtr clone();

    bool equals(SensorMeasurementPtr sensorMeasurement);

    IMUSensorMeasurementPtr clone(float newTimestep);

    Eigen::Vector3f getAcceleration();
    std::optional<Eigen::Vector3f> getGyroscope();
    std::optional<Eigen::Vector3f> getMagnetometer();

protected:
    IMUSensorMeasurementPtr interpolate(IMUSensorMeasurementPtr other, float timestep);

    void appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode);

private:
    Eigen::Vector3f acceleration;
    std::optional<Eigen::Vector3f> gyroscope;
    std::optional<Eigen::Vector3f> magnetometer;
};
}
#endif // __MMM_IMUSENSORMEASUREMENT_H_
