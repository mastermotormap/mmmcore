#include "EnvironmentalContactSensorMeasurement.h"

#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>

namespace MMM
{

EnvironmentalContactSensorMeasurement::EnvironmentalContactSensorMeasurement(float timestep, const std::map<std::string, std::string> &EnvironmentalContact, SensorMeasurementType type) :
    SensorMeasurement(timestep, type),
    EnvironmentalContact(EnvironmentalContact)
{
}

SensorMeasurementPtr EnvironmentalContactSensorMeasurement::clone() {
    return clone(timestep);
}

bool EnvironmentalContactSensorMeasurement::equals(SensorMeasurementPtr sensorMeasurement) {
    EnvironmentalContactSensorMeasurementPtr ptr = std::dynamic_pointer_cast<EnvironmentalContactSensorMeasurement>(sensorMeasurement);
    if (ptr) {
        return SensorMeasurement::compare<std::map<std::string, std::string> >(EnvironmentalContact, ptr->EnvironmentalContact);
    }
    return false;
}

EnvironmentalContactSensorMeasurementPtr EnvironmentalContactSensorMeasurement::clone(float newTimestep) {
    EnvironmentalContactSensorMeasurementPtr clonedSensorMeasurement(new EnvironmentalContactSensorMeasurement(newTimestep, EnvironmentalContact, type));
    return clonedSensorMeasurement;
}

void EnvironmentalContactSensorMeasurement::appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode) {
    for (const auto &contact : EnvironmentalContact) {
        measurementNode->append_node("Contact")->append_attribute("segment", contact.first)->append_data_node(contact.second);
    }
}

std::map<std::string, std::string> EnvironmentalContactSensorMeasurement::getEnvironmentalContact() {
    return EnvironmentalContact;
}

}
