/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_ENVIRONMENTALCONTACTSENSORMEASUREMENT_H_
#define __MMM_ENVIRONMENTALCONTACTSENSORMEASUREMENT_H_

#include <MMM/Motion/Sensor/SensorMeasurement.h>

#include <Eigen/Core>
#include <map>

namespace MMM
{
class EnvironmentalContactSensorMeasurement;

typedef std::shared_ptr<EnvironmentalContactSensorMeasurement> EnvironmentalContactSensorMeasurementPtr;

class MMM_IMPORT_EXPORT EnvironmentalContactSensorMeasurement : public SensorMeasurement, SMCloneable<EnvironmentalContactSensorMeasurement>
{

public:
    EnvironmentalContactSensorMeasurement(float timestep, const std::map<std::string, std::string> &EnvironmentalContact, SensorMeasurementType type = SensorMeasurementType::MEASURED);

    SensorMeasurementPtr clone();

    bool equals(SensorMeasurementPtr sensorMeasurement);

    EnvironmentalContactSensorMeasurementPtr clone(float newTimestep);

    std::map<std::string, std::string> getEnvironmentalContact();

protected:
    void appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode);

private:
    std::map<std::string, std::string> EnvironmentalContact;
};
}
#endif // __MMM_EnvironmentalContactSENSORMEASUREMENT_H_
