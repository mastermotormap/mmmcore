#include "EnvironmentalContactSensorFactory.h"

#include <boost/extension/extension.hpp>

#include "EnvironmentalContactSensor.h"

namespace MMM
{

// register this factory
SensorFactory::SubClassRegistry EnvironmentalContactSensorFactory::registry(NAME_STR<EnvironmentalContactSensor>(), &EnvironmentalContactSensorFactory::createInstance);

EnvironmentalContactSensorFactory::EnvironmentalContactSensorFactory() : SensorFactory() {}

EnvironmentalContactSensorFactory::~EnvironmentalContactSensorFactory() = default;

SensorPtr EnvironmentalContactSensorFactory::createSensor(simox::xml::RapidXMLWrapperNodePtr sensorNode, const std::filesystem::path &filePath)
{
    return EnvironmentalContactSensor::loadSensorXML(sensorNode, filePath);
}

std::string EnvironmentalContactSensorFactory::getName()
{
    return NAME_STR<EnvironmentalContactSensor>();
}

SensorFactoryPtr EnvironmentalContactSensorFactory::createInstance(void *)
{
    return SensorFactoryPtr(new EnvironmentalContactSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorFactoryPtr getFactory() {
    return SensorFactoryPtr(new EnvironmentalContactSensorFactory);
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorFactory::VERSION;
}

}
