#include <cassert>
#include <map>

#include "EnvironmentalContactSensor.h"

namespace MMM
{

EnvironmentalContactSensorPtr EnvironmentalContactSensor::loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    EnvironmentalContactSensorPtr sensor(new EnvironmentalContactSensor());
    sensor->loadSensor(node, filePath);
    return sensor;
}

EnvironmentalContactSensor::EnvironmentalContactSensor(const std::string &description) : BasicSensor(description)
{
}

bool EnvironmentalContactSensor::checkModel(ModelPtr model) {
    if (!model) {
        MMM_ERROR << "Sensor '" << uniqueName << "' needs a model."  << std::endl;
        return false;
    }
    for (const auto &measurement : measurements) {
        for (const auto &contact : measurement.second->getEnvironmentalContact()) {
            if (!hasNode(model, contact.first)) return false;
        }
    }
    return true;
}

std::shared_ptr<BasicSensor<EnvironmentalContactSensorMeasurement> > EnvironmentalContactSensor::cloneConfiguration() {
    EnvironmentalContactSensorPtr m(new EnvironmentalContactSensor(description));
    return m;
}

void EnvironmentalContactSensor::loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    assert(node);
    assert(node->name() == "Configuration");
}

void EnvironmentalContactSensor::loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type)
{
    assert(node);
    assert(node->name() == xml::tag::MEASUREMENT);

    std::map<std::string, std::string> EnvironmentalContact;
    if (node->has_node("Contact")) {
        for (auto contactNode : node->nodes("Contact")) {
            std::string name = contactNode->attribute_value("segment");
            if (EnvironmentalContact.find(name) == EnvironmentalContact.end()) EnvironmentalContact[name] = contactNode->value();
            else throw Exception::MMMFormatException("Duplicate of contact at segment '" + name + "' at timestep '" + simox::alg::to_string(timestep) + "' in EnvironmentalContact sensor data");
        }
    }
    EnvironmentalContactSensorMeasurementPtr m(new EnvironmentalContactSensorMeasurement(timestep, EnvironmentalContact, type));
    addSensorMeasurement(m);
}

void EnvironmentalContactSensor::appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr /*node*/, const std::filesystem::path &filePath) {
    // Do nothing
}

bool EnvironmentalContactSensor::hasSensorConfigurationContent() const
{
    return true; // TODO
}

bool EnvironmentalContactSensor::equalsConfiguration(SensorPtr other) {
    EnvironmentalContactSensorPtr ptr = std::dynamic_pointer_cast<EnvironmentalContactSensor>(other);
    if (ptr) return true;
    else return false;
}

EnvironmentalContactSensorMeasurementPtr EnvironmentalContactSensor::getDerivedMeasurement(float timestep) {
    if (hasMeasurement(timestep)) return measurements[timestep];
    else {
        auto it = measurements.lower_bound(timestep);
        if (it != measurements.end()) {
            EnvironmentalContactSensorMeasurementPtr found = it->second;
            return EnvironmentalContactSensorMeasurementPtr(new EnvironmentalContactSensorMeasurement(found->getTimestep(), found->getEnvironmentalContact(), SensorMeasurementType::EXTENDED));
        }
        else return nullptr;
    }
}

EnvironmentalContactSensorMeasurementPtr EnvironmentalContactSensor::getDerivedMeasurement(float timestep, float /*delta*/) {
    return getDerivedMeasurement(timestep);
}

std::string EnvironmentalContactSensor::getType() {
    return TYPE;
}

std::string EnvironmentalContactSensor::getVersion() {
    return VERSION;
}

int EnvironmentalContactSensor::getPriority() {
    return 50;
}

}
