/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_ENVIRONMENTALCONTACTSENSOR_H_
#define __MMM_ENVIRONMENTALCONTACTSENSOR_H_

#include <MMM/Motion/Sensor/BasicSensor.h>
#include <MMM/Motion/Sensor/SensorMeasurement.h>
#include "EnvironmentalContactSensorMeasurement.h"

#include <vector>

namespace MMM
{

class EnvironmentalContactSensor;

typedef std::shared_ptr<EnvironmentalContactSensor> EnvironmentalContactSensorPtr;
typedef std::vector<EnvironmentalContactSensorPtr> EnvironmentalContactSensorList;

class MMM_IMPORT_EXPORT EnvironmentalContactSensor : public BasicSensor<EnvironmentalContactSensorMeasurement>
{

public:
    static EnvironmentalContactSensorPtr loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    EnvironmentalContactSensor(const std::string &description = std::string());

    bool checkModel(ModelPtr model);

    std::shared_ptr<BasicSensor<EnvironmentalContactSensorMeasurement> > cloneConfiguration();

    bool equalsConfiguration(SensorPtr other);

    EnvironmentalContactSensorMeasurementPtr getDerivedMeasurement(float timestep);

    EnvironmentalContactSensorMeasurementPtr getDerivedMeasurement(float timestep, float delta);

    std::string getType();

    std::string getVersion();

    int getPriority();

    static constexpr const char* TYPE = "EnvironmentalContact";

    static constexpr const char* VERSION = "1.0";

private:
    void loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    void loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type);

    void appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    bool hasSensorConfigurationContent() const;
};


}
#endif // __MMM_ENVIRONMENTALCONTACTSENSOR_H_

