#include "Motion.h"

#include <VirtualRobot/Robot.h>
#include "MMM/Model/ModelProcessorWinter.h"
#include "MMM/Model/ModelReaderXML.h"
#include "MMM/Model/ProcessedModelWrapper.h"
#include "MMM/Model/ModelProcessor.h"
#include "MMM/Motion/Sensor/Sensor.h"
#include "MMM/Motion/Sensor/SensorMeasurement.h"
#include "MMM/Motion/Sensor/BasicModelPoseSensor.h"
#include "MMM/Motion/Sensor/BasicKinematicSensor.h"
#include "XMLTools.h"
#include <SimoxUtility/algorithm/string.h>
#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>
#include <SimoxUtility/math/convert.h>
#include <VirtualRobot/Visualization/VisualizationNode.h>

namespace MMM
{  

Motion::Motion(const std::string &name, ModelPtr originalModel, ModelPtr processedModel, ModelProcessorPtr modelProcessor, const std::filesystem::path &originFilePath, const std::filesystem::path &modelFilePath, MotionType motionType) :
    Motion(name, ProcessedModelWrapperPtr(new ProcessedModelWrapper(originalModel, processedModel, modelProcessor, modelFilePath)), originFilePath, motionType)
{
}

Motion::Motion(const std::string &name, ModelPtr originalModel, ModelProcessorPtr modelProcessor, const std::filesystem::path &originFilePath, const std::filesystem::path &modelFilePath, MotionType motionType) :
    Motion(name, ProcessedModelWrapperPtr(new ProcessedModelWrapper(originalModel, modelProcessor, modelFilePath)), originFilePath, motionType)
{
}

Motion::Motion(const std::string &name, ProcessedModelWrapperPtr model, const std::filesystem::path &originFilePath, MotionType motionType) :
    name(name),
    originFilePath(originFilePath),
    model(model),
    motionType(motionType)
{
    if (this->name.empty()) throw Exception::MMMException("The motion name should not be an empty string!");
}

Motion::Motion(MotionPtr motion) : Motion(motion->name, motion->model, motion->originFilePath, motion->motionType)
{
    for (const auto &sensor : sensorData)
        addSensor(sensor.second->clone(), 0.0f, false); // no exceptionHandling, because there should occure no Exception!
}

MotionPtr Motion::clone(bool cloneSensors, const std::string &newName) const {
    MotionPtr clonedMotion(new Motion(newName.empty() ? this->name : newName));
    clonedMotion->setName(newName.empty() ? name : newName);
    clonedMotion->setProcessedModelWrapper(model ? model->clone() : nullptr);
    clonedMotion->originFilePath = originFilePath;
    clonedMotion->motionType = motionType;
    if (cloneSensors)
        for (const auto &sensor : sensorData)
            clonedMotion->addSensor(sensor.second->clone(), 0.0f, false); // no exceptionHandling, because there should occure no Exception!
    return clonedMotion;
}

void Motion::setName(const std::string& name) {
    if (this->name != name) {
        if (name.empty())
            throw Exception::MMMException("Empty name");
        if (isNameChangeAllowed(name))
            throw Exception::MMMException(name + " is not unique.");
        auto oldName = this->name;
        this->name = name;
        for (auto obs : motionObservers) {
            obs->notifyNameChange(shared_from_this(), oldName);
        }
    }
}

std::string Motion::getName() {
    return name;
}

//! get a segmented motion with name 'MOTIONNAME_segmented_STARTTIMESTEPf-ENDTIMESTEPf' according to timesteps.
MotionPtr  Motion::getSegmentMotion(float startTimestep, float endTimestep, bool changeTimestep) const {
    return getSegmentMotion(startTimestep, endTimestep, createSegmentName(startTimestep, endTimestep), changeTimestep);
}

//! get a segmented motion with the given name according to timesteps.
MotionPtr  Motion::getSegmentMotion(float startTimestep, float endTimestep, std::string segmentMotionName, bool changeTimestep) const {
    if (startTimestep > endTimestep) {
        MMM_ERROR << "startTimestep needs to be bigger than endTimestep" << std::endl;
        return nullptr;
    }
    MotionPtr segmentMotion = clone(false, segmentMotionName);
    for (const auto &sensor : sensorData) {
        SensorPtr segmentSensor = sensor.second->getSegmentSensor(startTimestep, endTimestep, changeTimestep);
        if (segmentSensor->getTimesteps().size() != 0) segmentMotion->addSensor(segmentSensor); // no exceptionHandling, because there should occure no Exception!
        else  MMM_INFO << "Ignoring sensor '" << segmentSensor->getName() << "' by segmenting, because the sensor has no corresponding measurements." << std::endl;
    }
    if (segmentMotion->sensorData.size() == 0) MMM_INFO << "Segmented motion does not contain any sensor." << std::endl;
    return segmentMotion;
}

SensorPtr Motion::getSensorByName(const std::string &name) const {
    return sensorData.at(name);
}

SensorPtr Motion::getSensorByType(const std::string &type) const {
    for (const auto &sensor : sensorData) {
        if (sensor.second->getType() == type) return sensor.second;
    }
    return nullptr;
}


std::vector<SensorPtr> Motion::getSensorsByType(const std::string &type) const {
    std::vector<SensorPtr> sensors;
    for (const auto &sensor : sensorData) {
        if (sensor.second->getType() == type) sensors.push_back(sensor.second);
    }
    return sensors;
}

bool Motion::hasSensor() const {
    return sensorData.size() > 0;
}

bool Motion::hasSensor(const std::string &type) const {
    return getSensorByType(type) != nullptr;
}

std::filesystem::path Motion::getOriginFilePath() const {
    return originFilePath;
}

std::map<std::string, SensorPtr> Motion::getSensorData() const {
    return sensorData;
}

std::vector<SensorPtr> Motion::getPrioritySortedSensorData() const {
    std::vector<SensorPtr> sortedSensors;
    for (const auto &sensor : sensorData) {
      sortedSensors.push_back(sensor.second);
    }
    std::stable_sort(sortedSensors.begin(), sortedSensors.end(), [] (SensorPtr p1, SensorPtr p2) { return p1->getPriority() > p2->getPriority();});
    return sortedSensors;
}

ModelPtr Motion::getModel(bool processed) const {
    if (!model) return nullptr;
    return model->getModel(processed);
}

ModelPtr Motion::getModelThrow(bool processed) const {
    auto model = getModel(processed);
    if (model) return model;
    else throw Exception::MMMException("Model null in motion " + name);
}

ModelPtr Motion::getModelCloneWithVisualization(bool processed) const {
    auto model = getModel(processed);
    if (!model) return nullptr;
    auto clonedModel = model->cloneScaling();
    model->reloadVisualizationFromXML(true);
    return model;
}

ModelPtr Motion::getModelCloneWithVisualizationThrow(bool processed) const {
    auto model = getModelThrow(processed)->cloneScaling();
    model->reloadVisualizationFromXML(true);
    return model;
}

ModelProcessorPtr Motion::getModelProcessor() const {
    if (!model) return nullptr;
    return model->getModelProcessor();
}

bool Motion::hasModelProcessor() const {
    return getModelProcessor() != nullptr;
}

std::map<std::string, SensorMeasurementPtr> Motion::getAllMeasurementsForTimeStep(float timestep) const {
    std::map<std::string, SensorMeasurementPtr> measurements;
    for (const auto &sensor : sensorData) {
        SensorMeasurementPtr measurement = sensor.second->getMeasurement(timestep);
        if (measurement) measurements[sensor.first] = measurement;
    }
    return measurements;
}

void Motion::addSensor(SensorPtr sensor, float delta, bool checkModel) {
    if (!sensor) throw Exception::MMMException("Sensor is null!");
    if (getSensor(sensor)) throw Exception::MMMException("Sensor is already contained in motion!");
    if (checkModel && !sensor->checkModel(getModel(true))) throw Exception::MMMException("Sensor not suited for the model of the motion!");
    if (sensor->getName().empty()) {
        AddingByType:
        if (!addSensor(sensor->getType(), sensor, delta)) {
            int i = 2;
            while (!addSensor(sensor->getType() + simox::alg::to_string(i), sensor, delta)) i++;
        }
    } else {
        if (!addSensor(sensor->getName(), sensor, delta)) {
            MMM_INFO << "Already found a sensor with name '" << sensor->getName() << "'!"
                     << " Will add this sensor with '" << sensor->getType() << "' as part of his name." << std::endl;
            goto AddingByType;
        }
    }
}

bool Motion::addSensor(std::string name, SensorPtr sensor, float delta) {
    if (sensorData.find(name) == sensorData.end()) {
        float prevMinTimestep = getMinTimestep();
        float prevMaxTimestep = getMaxTimestep();

        sensor->setUniqueName(name);
        if (delta == 0.0f) sensorData[name] = sensor;
        else {
            // shift sensor data for synchronisation purpose
            float minTimestepSensor = sensor->getMinTimestep();
            float otherDelta = minTimestepSensor + delta;
            if (otherDelta > 0.0f) {
                sensor->shiftMeasurements(delta);
                sensorData[name] = sensor;
            } else {
                // Motion should start with positive timestep
                delta = -otherDelta;
                for (const auto &s : sensorData) {
                    s.second->shiftMeasurements(delta);
                }
                sensor->shiftMeasurements(-minTimestepSensor);
                sensorData[name] = sensor;
                // Adapt other motions in motion recording
                for (auto o : motionObservers)
                    o->notifyMeasurementShift(shared_from_this(), delta);
            }
        }

        notifyIntervalChange(prevMinTimestep, prevMaxTimestep);
        return true;
    }
    else return false;
}

SensorPtr Motion::getSensor(SensorPtr s) const {
    for (const auto &sensor : sensorData) {
        if(s->equalsConfiguration(sensor.second)) return s;
    }
    return nullptr;
}

MotionPtr Motion::join(MotionPtr motion1, MotionPtr motion2, std::string name) {
    if (!motion1->getModel(true)) return nullptr;
    std::string joinedMotionName = name.empty() ? motion1->name : name;
    MotionPtr joinedMotion(new Motion(joinedMotionName, motion1->model ? motion1->model->clone() : nullptr));
    std::vector<SensorPtr> sensorCollection;
    for (const auto &sensor : motion1->sensorData) { // Collect Sensors from motion1
        sensorCollection.push_back(sensor.second);
    }
    for (const auto &sensor : motion2->sensorData) { // Collect Sensors from motion2, join if needed
        bool added = false;
        for (auto it = sensorCollection.begin(); it != sensorCollection.end(); ++it) {
            if (sensor.second->equalsConfiguration(*it)) {
                SensorPtr joinedSensor = Sensor::join(sensor.second, *it);
                if (joinedSensor) {
                    sensorCollection.erase(it);
                    sensorCollection.push_back(joinedSensor);
                    added = true;
                    break;
                }
                else return nullptr;
            }
        }
        if (!added) sensorCollection.push_back(sensor.second);
    }
    for (auto sensor : sensorCollection) { // Try Add found Sensors
        try {
            joinedMotion->addSensor(sensor->clone());
        } catch (Exception::MMMException& e) {
            MMM_ERROR << "Error when adding sensors from joined motion! " << e.what() << std::endl;
            return nullptr;
        }
    }
    return joinedMotion;
}

float Motion::getMaxTimestep() const {
    if (sensorData.size() == 0) return 0.0f;
    float max = 0.0f;
    for (const auto &sensor : sensorData) {
        if (!sensor.second->hasMeasurement()) continue;
        float maxT = sensor.second->getMaxTimestep();
        if (maxT > max) max = maxT;
    }
    return max;
}

float Motion::getMinTimestep() const {
    if (sensorData.size() == 0) return 0.0f;
    float min = std::numeric_limits<float>::max();
    for (const auto &sensor : sensorData) {
        if (!sensor.second->hasMeasurement()) continue;
        float minT = sensor.second->getMinTimestep();
        if (minT < min) min = minT;
    }
    return min;
}

float Motion::getCommonMinTimestep(const std::set<std::string> &ignoreSensorNames) const {
    float max = 0.0f;
    for (const auto &sensor : sensorData) {
        if (!sensor.second->hasMeasurement()) continue;
        if (ignoreSensorNames.find(sensor.second->getUniqueName()) != ignoreSensorNames.end()) continue;
        float minT = sensor.second->getMinTimestep();
        if (minT > max) max = minT;
    }
    return max;
}

float Motion::getCommonMaxTimestep(const std::set<std::string> &ignoreSensorNames) const {
    float min = std::numeric_limits<float>::max();
    for (const auto &sensor : sensorData) {
        if (!sensor.second->hasMeasurement()) continue;
        if (ignoreSensorNames.find(sensor.second->getUniqueName()) != ignoreSensorNames.end()) continue;
        float maxT = sensor.second->getMaxTimestep();
        if (maxT < min) min = maxT;
    }
    if (min > std::numeric_limits<float>::max() - 1.0f) return 0.0f;
    else return min;
}

void Motion::extendKinematic() {
    auto sensors = getSensorsByType(BasicKinematicSensor::TYPE);
    sensors.push_back(getSensorByType(BasicModelPoseSensor::TYPE));
    std::vector<SensorPtr> extendSensors;
    std::set<std::string> extendSensorNames;
    for (SensorPtr sensor : sensors) {
        if (sensor->getTimesteps().size() == 1) {
            extendSensors.push_back(sensor);
            extendSensorNames.insert(sensor->getUniqueName());
        }
    }
    if (extendSensors.size() > 0) {
        float minTimestep = getCommonMinTimestep(extendSensorNames);
        float maxTimestep = getCommonMaxTimestep(extendSensorNames);
        for (SensorPtr sensor : extendSensors)
            sensor->extend(minTimestep, maxTimestep);
    }
}

void Motion::synchronizeSensorMeasurements(float timeFrequency) {
    float minTimestep = getCommonMinTimestep();
    float maxTimestep = getCommonMaxTimestep();
    synchronizeSensorMeasurements(timeFrequency, minTimestep, maxTimestep);
}

void Motion::synchronizeSensorMeasurements(float timeFrequency, float startTimestep, float endTimestep) {
    if (timeFrequency < 0.001) return;

    float prevMinTimestep = getMinTimestep();
    float prevMaxTimestep = getMaxTimestep();

    // erase non-interpolatable sensors
    for(auto it = sensorData.begin(); it != sensorData.end();) {
        if (!it->second->isInterpolatable()) {
            MMM_INFO << "Erasing sensor '" << it->first << "' from the motion, because it is not interpolatable." << std::endl;
            it = sensorData.erase(it);
        } else {
            it++;
        }
    }

    for (const auto &sensor : sensorData) {
        sensor.second->synchronizeSensorMeasurements(timeFrequency, startTimestep, endTimestep);
    }

    notifyIntervalChange(prevMinTimestep, prevMaxTimestep);
}

bool Motion::isSynchronized() const {
    // Faster to check minimum and maximum timestep first. As getTimestep() can be slow depending on the sensor
    if (!hasSameMinTimestep())
        return false;
    if (!hasSameMaxTimestep())
        return false;

    std::vector<float> timesteps;
    bool initialized = false;
    for (auto sensor : sensorData) {
        if (!initialized) {
            timesteps = sensor.second->getTimesteps();
            initialized = true;
        }
        else if (timesteps != sensor.second->getTimesteps()) return false;
    }
    // test if difference between timesteps is overall the same
    if (timesteps.size() > 2) {
        float timestepDiff = timesteps.at(1) - timesteps.at(0);
        for (unsigned int i = 2; i < timesteps.size(); i++) {
            if (abs(timestepDiff - (timesteps.at(i) - timesteps.at(i - 1))) > 0.0001) return false;
        }
    }
    return true;
}

bool Motion::hasSameMinTimestep() const {
    float minTimestep;
    bool initialized = false;
    for (auto sensor : sensorData) {
        if (!initialized) {
            minTimestep = sensor.second->getMinTimestep();
            initialized = true;
        }
        else if (std::abs(minTimestep - sensor.second->getMinTimestep()) > 0.0001) return false;
    }
    return true;
}

bool Motion::hasSameMaxTimestep() const {
    float maxTimestep;
    bool initialized = false;
    for (auto sensor : sensorData) {
        if (!initialized) {
            maxTimestep = sensor.second->getMaxTimestep();
            initialized = true;
        }
        else if (std::abs(maxTimestep - sensor.second->getMaxTimestep()) > 0.0001) return false;
    }
    return true;
}

std::set<std::string> Motion::getSensorTypes() const {
    std::set<std::string> sensorTypes;
    for (const auto &data : sensorData) {
        sensorTypes.insert(data.second->getType());
    }
    return sensorTypes;
}

std::string Motion::getModelName() const {
    if (model) {
        auto m = model->getModel(false);
        if (m)
            return m->getName();
    }
    return std::string();
}

std::string Motion::getModelFileName() const {
    if (model) return model->getOriginalModelFileName();
    else return std::string();
}

std::filesystem::path Motion::getModelFilePath() const {
    if (model) return model->getOriginalModelFilePath();
    else return std::string();
}

bool Motion::isReferenceModelMotion() const {
    return (motionType._value == MotionType::MMM && hasSensor(BasicModelPoseSensor::TYPE)) || getModelFileName() == "mmm";
}

bool Motion::processModel(ModelProcessorPtr modelProcessor, ModelPtr optionalModel) {
    if (this->model) {
        this->model->setModelProcessor(modelProcessor);
        auto model = optionalModel ? optionalModel : getModel(false);
        return setProcessedModel(modelProcessor->convertModel(model));
    }
    else return false;
}

bool Motion::eraseSensorIfEqual(Sensor &sensor) {
    for(auto it = sensorData.begin(); it != sensorData.end();) {
        if (sensor.equalsConfiguration(it->second)) {
            it = sensorData.erase(it);
            return true;
        }
        else it++;
    }
    return false;
}

bool Motion::setProcessedModel(ModelPtr model) {
    if (this->model) {
        this->model->setProcessedModel(model);
        return true;
    }
    else return false;
}

int Motion::eraseSensors(const std::string &type) {
    float prevMinTimestep = getMinTimestep();
    float prevMaxTimestep = getMaxTimestep();

    int erasedSensors = 0;
    for(auto it = sensorData.begin(); it != sensorData.end();) {
        if (it->second->getType() == type) {
            it = sensorData.erase(it);
            erasedSensors++;
        }
        else it++;
    }

    notifyIntervalChange(prevMinTimestep, prevMaxTimestep);
    return erasedSensors;
}

void Motion::appendMotion(simox::xml::RapidXMLWrapperNodePtr root, const std::filesystem::path &path) {
    simox::xml::RapidXMLWrapperNodePtr motionNode = root->append_node(xml::tag::MOTION);
    motionNode->append_attribute(xml::attribute::NAME, getName().c_str());
    if (motionType && motionType._value != MotionType::UNKNOWN)
        motionNode->append_attribute(xml::attribute::TYPE, simox::alg::to_lower(motionType._to_string()));
    if (isSynchronized()) motionNode->append_attribute(xml::attribute::SYNCHRONIZED, true);

    if (model) {
        //write Model
        std::filesystem::path modelFilePath = model->getOriginalModelFilePath();
        if (modelFilePath.empty() && getModel(false)) modelFilePath = getModel(false)->getFilename();
        if (!modelFilePath.empty()) {
            if (!path.empty() && getOriginFilePath().parent_path() != path.parent_path()) {
                if (!MMM::xml::isValid(modelFilePath)) {
                    // if model file path does not exist
                    modelFilePath = modelFilePath.filename();
                }
                else if (MMM::xml::isValid(path.parent_path())) modelFilePath = std::filesystem::proximate(std::filesystem::canonical(modelFilePath), std::filesystem::canonical(path.parent_path()));
                else modelFilePath = std::filesystem::proximate(std::filesystem::canonical(modelFilePath), path.parent_path());
            }
            simox::xml::RapidXMLWrapperNodePtr modelNode = motionNode->append_node(xml::tag::MODEL)->append_attribute(xml::attribute::PATH, modelFilePath);
            // write Modelprocessor
            if (getModelProcessor()) getModelProcessor()->appendDataXML(modelNode);
        }
    }

    simox::xml::RapidXMLWrapperNodePtr sensorsNode = motionNode->append_node("Sensors");
    for (auto sensor : getPrioritySortedSensorData()) {
      sensor->appendSensorXML(sensorsNode, path);
    }
}

simox::xml::RapidXMLWrapperNodePtr Motion::createMotionRoot() {
    auto root = simox::xml::RapidXMLWrapperRootNode::createRootNode(xml::tag::MMM_ROOT);
    root->append_attribute(xml::attribute::VERSION, "2.0");
    return root;
}

std::string Motion::toXML(const std::filesystem::path &path, bool indent) {
    return getXML(path)->print(indent);
}

void Motion::saveXML(const std::filesystem::path &path, bool indent) {
    getXML(path)->saveToFile(path, indent);
}

simox::xml::RapidXMLWrapperNodePtr Motion::getXML(const std::filesystem::path &path) {
    simox::xml::RapidXMLWrapperNodePtr root = createMotionRoot();
    appendMotion(root, path);
    return root;
}

void Motion::setProcessedModelWrapper(ProcessedModelWrapperPtr modelContainer) {
    this->model = modelContainer;
}

std::string Motion::createSegmentName(float startTimestep, float endTimestep) const {
    return name + "_segmented_" + simox::alg::to_string(startTimestep) + "f-" + simox::alg::to_string(endTimestep) + "f";
}

MotionType Motion::getMotionType() const {
    return motionType;
}

void Motion::setMotionType(MotionType type) {
    motionType = type;
}

void Motion::setMotionType(const std::string &type) {
    auto maybe_type = MotionType::_from_string_nocase_nothrow(type.c_str());
    if (maybe_type)
        motionType = *maybe_type;
    else
        throw Exception::MMMException(type + " not a valid motion type!");
}

void Motion::setMotionTypeFromModelIfUnknown() {
    if (!motionType || motionType._value == MotionType::UNKNOWN)
        setMotionTypeFromModel();
}

bool Motion::operator <(MotionPtr m) const {
    if (motionType == m->motionType)
        return name < m->name;
    else return motionType < m->motionType;
}

bool Motion::isSubject() const {
    return motionType._value < 10;
}

void Motion::setMotionTypeFromModel() {
    motionType = getMotionTypeFromModel();
}

MotionType Motion::getMotionTypeFromModel() const {
    if (model)
        return MMM::getMotionTypeFromModel(model->getOriginalModelName());
    else
        return MotionType::UNKNOWN;
}

bool Motion::scaleMotion(float targetHeight, bool shiftMotion, ModelPtr optionalModel) {
    auto modelProcessor = getModelProcessor();
    if (modelProcessor) {
        ModelProcessorWinterPtr modelProcessorWinter = std::dynamic_pointer_cast<ModelProcessorWinter>(modelProcessor);
        if (modelProcessorWinter) {
            double actualHeight = modelProcessorWinter->getHeight();
            double actualMass = modelProcessorWinter->getMass();
            double factor = targetHeight / actualHeight;
            double targetMass = actualMass * factor;
            ModelProcessorWinterPtr processor(new ModelProcessorWinter(targetHeight, targetMass));
            processModel(processor, optionalModel);
            if (shiftMotion) {
                for (auto sensor : sensorData) {
                    sensor.second->scaleMotionData(factor);
                }
            }
            return true;
        }
    }
    return false;
}

/*! Only shifts the measurements of this motion. Does not notify other members of MotionRecording about shift.  */
void Motion::shiftMeasurements(float delta) {
    for (auto sensor : sensorData) {
        sensor.second->shiftMeasurements(delta);
    }
}

void Motion::shiftMotion(const Eigen::Vector3f &positionDelta) {
    for (auto sensor : sensorData) {
        sensor.second->shiftMotionData(positionDelta);
    }
}

bool Motion::isMirrorSupported() {
    for (auto sensor : sensorData) {
        if (!sensor.second->isMirrorSupported(motionType)) {
            MMM_ERROR << "Sensor " + sensor.second->getUniqueName() + " does not support mirroring!" << std::endl;
            return false;
        }
    }
    return true;
}

bool Motion::mirrorMotion(const Eigen::Matrix4f &referencePose, int referenceAxis, bool ignoreNotSupported) {
    bool mirrored = false;
    if (!ignoreNotSupported && isMirrorSupported()) {
        for (auto sensor : sensorData) {
            sensor.second->mirrorMotionData(referencePose, referenceAxis);
        }
        mirrored = true;
    }
    else if (ignoreNotSupported) {
        for (auto sensor : sensorData) {
            if (sensor.second->isMirrorSupported(motionType)) {
                sensor.second->mirrorMotionData(referencePose, referenceAxis);
            }
        }
        mirrored = true;
    }
    if (mirrored && (motionType._value != MotionType::MMM || motionType._value != MotionType::ROBOT)) {
        MMM_WARNING << "[NOT IMPLEMENTED YET] Mirror motions may require also to mirror the model if not symmetrical to origin coordinate system!" << std::endl;
    }
    return mirrored;
}

std::vector<float> Motion::getTimesteps() {
    SensorPtr modelPoseSensor = getSensorByType(BasicModelPoseSensor::TYPE);
    if (modelPoseSensor)
        return modelPoseSensor->getTimesteps();
    SensorPtr mocapSensor = getSensorByType("MoCapMarker");
    if (mocapSensor)
        return mocapSensor->getTimesteps();
    return std::vector<float>();
}

std::vector<std::string> Motion::getActuatedJointNames() {  
    std::vector<std::string> actuatedJointNames;
    auto sensors = getSensorsByType<BasicKinematicSensor>();
    for (auto sensor : sensors) {
        auto jointNames = sensor->getJointNames();
        actuatedJointNames.insert(actuatedJointNames.end(), jointNames.begin(), jointNames.end());
    }
    return actuatedJointNames;
}

std::map<std::string, std::vector<std::string>> Motion::getActuatedJointNamesBySensorName() {
    std::map<std::string, std::vector<std::string>> actuatedJointNames;
    auto sensors = getSensorsByType<BasicKinematicSensor>();
    for (auto sensor : sensors) {
        actuatedJointNames[sensor->getUniqueName()] = sensor->getJointNames();
    }
    return actuatedJointNames;
}

void Motion::initializeModel(ModelPtr model, float timestep, bool extend, bool update) {
    if (model) {
        model->setPropagatingJointValuesEnabled(false);
        for (const auto &sensor : sensorData) {
            sensor.second->initializeModel(model, timestep, extend, update);
        }
        model->setPropagatingJointValuesEnabled(true);
        if (update) model->updatePose(true);
    }
}

void Motion::initializeModel(ModelPtr model, float timestep, float delta, bool extend, bool update) {
    if (model) {
        model->setPropagatingJointValuesEnabled(false);
        for (const auto &sensor : sensorData) {
            sensor.second->initializeModel(model, timestep, delta, extend);
        }
        model->setPropagatingJointValuesEnabled(true);
        if (update) model->updatePose(true);
    }
}

Eigen::Matrix4f Motion::getRootPose(float timestep, bool extend) {
    auto sensor = getSensorByType<BasicModelPoseSensor>();
    if (sensor)
        return sensor->getRootPose(timestep, extend);
    else
        throw Exception::MMMException("No sensor with type " + std::string(BasicModelPoseSensor::TYPE) + " in motion " + getName());
}

Eigen::VectorXf Motion::getJointAngles(float timestep) {
    auto sensors = getSensorsByType<BasicKinematicSensor>();
    if (sensors.size() > 0) {
        size_t size = 0;
        std::vector<Eigen::VectorXf> v;
        for (auto sensor : sensors) {
            auto jointAngles = sensor->getJointAngles(timestep);
            size += jointAngles.rows();
            v.push_back(jointAngles);
        }
        Eigen::VectorXf jointAngles(size);
        for (const auto &v_i : v) {
            jointAngles << v_i;
        }
        return jointAngles;
    }
    else
        throw Exception::MMMException("No sensor with type " + std::string(BasicKinematicSensor::TYPE) + " in motion " + getName());
}

std::map<std::string, Eigen::VectorXf> Motion::getJointAnglesBySensorName(float timestep) {
    std::map<std::string, Eigen::VectorXf> jointAngles;
    auto sensors = getSensorsByType<BasicKinematicSensor>();
    for (auto sensor : sensors) {
        jointAngles[sensor->getUniqueName()] = sensor->getJointAngles(timestep);
    }
    return jointAngles;
}

std::map<std::string, float> Motion::getJointAngleMap(float timestep) {
    auto sensors = getSensorsByType<BasicKinematicSensor>();
    std::map<std::string, float> jointAngleMap_all;
    for (auto sensor : sensors) {
        auto jointAngleMap = sensor->getJointAngleMap(timestep);
        jointAngleMap_all.insert(jointAngleMap.begin(), jointAngleMap.end());
    }
    return jointAngleMap_all;
}

bool Motion::getReferencePose(Eigen::Matrix4f &referencePose, int &referenceAxis) {
    if (isReferenceModelMotion()) {
        auto modelPoseSensor = getSensorByType<BasicModelPoseSensor>();
        if (modelPoseSensor) {
            Eigen::Vector3f position = simox::math::mat4f_to_pos(getRootPose(modelPoseSensor->getMinTimestep()));
            Eigen::Vector2f startPosition = position.block(0,0,2,1);
            Eigen::Vector2f endPosition = simox::math::mat4f_to_pos(getRootPose(modelPoseSensor->getMaxTimestep())).block(0,0,2,1);
            if ((startPosition - endPosition).norm() < 500) {
                // find position further away to span coordinate system
                float maxDistance = 0;
                for (auto timestep : modelPoseSensor->getTimesteps()) {
                    Eigen::Vector2f p = simox::math::mat4f_to_pos(getRootPose(timestep)).block(0,0,2,1);
                    float d = (startPosition - p).norm();
                    if (d > maxDistance) {
                        endPosition = p;
                        maxDistance = d;
                    }
                }
            }

            Eigen::Vector3f z(0, 0, 1);
            Eigen::Vector3f y;
            if ((startPosition - endPosition).norm() < 500) {
                y = getRootPose(modelPoseSensor->getMinTimestep()).block(0,1,3,1);
                y(2) = 0;
                y.normalize();
            }
            else {
                y = Eigen::Vector3f(endPosition(0) - startPosition(0), endPosition(1) - startPosition(1), 0);
                y.normalize();
            }
            Eigen::Vector3f x = z.cross(y);
            referencePose = Eigen::Matrix4f::Identity();
            referencePose.block(0,0,3,1) = x;
            referencePose.block(0,1,3,1) = y;
            referencePose.block(0,2,3,1) = z;
            referencePose.block(0,3,3,1) = position;

            referenceAxis = 0; // x-axis

            return true;
        }
    }
    return false;
}

void Motion::attach(MotionObserver* obs) {
    motionObservers.push_back(obs);
}

void Motion::detach(MotionObserver* obs) {
    for (auto it = motionObservers.begin(); it != motionObservers.end(); it++) {
        if (*it == obs) {
            motionObservers.erase(it);
            break;
        }
    }
}

}
