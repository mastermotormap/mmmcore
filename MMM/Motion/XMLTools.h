/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/


#ifndef __MMM_XMLTOOLS_H_
#define __MMM_XMLTOOLS_H_

#include <SimoxUtility/xml/rapidxml/RapidXMLForwardDecl.h>

namespace MMM
{

namespace xml
{

simox::xml::RapidXMLWrapperRootNodePtr getRoot(const std::string xml, bool xmlIsPath = true);

namespace tag
{
    static constexpr const char* MMM_ROOT           = "MMM";
    static constexpr const char* MODEL              = "Model";
    static constexpr const char* MOTION             = "Motion";
    static constexpr const char* MEASUREMENT        = "Measurement";
    static constexpr const char* SENSOR             = "Sensor";
    static constexpr const char* CONFIGURATION      = "Configuration";
    static constexpr const char* DATA               = "Data";
    static constexpr const char* MODELPROCESSOR     = "ModelProcessorConfig";
}

namespace attribute
{
    static const simox::xml::attribute::XMLAttribute INTERPOLATED          = simox::xml::attribute::XMLAttribute<bool>("interpolated");
    static const simox::xml::attribute::XMLAttribute SYNCHRONIZED          = simox::xml::attribute::XMLAttribute<bool>("synchronized");
    static const simox::xml::attribute::XMLAttribute TIMESTEP              = simox::xml::attribute::XMLAttribute<float>("timestep");
    static constexpr const char* TYPE               = "type";
    static constexpr const char* NAME               = "name";
    static constexpr const char* ID                 = "id";
    static constexpr const char* DESCRIPTION        = "description";
    static constexpr const char* PATH               = "path";
    static constexpr const char* VERSION            = "version";
}

bool isValid(const std::filesystem::path& filePath, bool checkPermissions = true, bool logging = false);

}

}

#endif // __MMM_XMLTOOLS_H_
