#include "LegacyMotionConverter.h"

#include "MMM/Motion/Motion.h"
#include "MMM/Motion/MotionRecording.h"
#include "MMM/Motion/Legacy/LegacyMotion.h"
#include "MMM/Motion/Legacy/LegacyMotionReaderXML.h"
#include "MMM/Model/LoadModelStrategy.h"
#include "MMM/Exceptions.h"
#include "MMM/FactoryPluginLoader.h"
#include "MMM/Motion/Sensor/SensorFactory.h"
#include "XMLTools.h"
#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>

#include <algorithm>

namespace MMM
{

const std::string LegacyMotionConverter::VERSION = "1.0";

LegacyMotionConverter::LegacyMotionConverter(bool mmmOnly, const std::vector<std::filesystem::path> &additionalLibPaths, bool ignoreStandardLibPaths) :
    mmmOnly(mmmOnly)
{
    std::vector<std::filesystem::path> libPaths;
    if (!ignoreStandardLibPaths) libPaths.push_back(getStandardLibPath());
    for (const auto &additionalLibPath : additionalLibPaths) libPaths.push_back(additionalLibPath);
    std::shared_ptr<FactoryPluginLoader<SensorFactory> > factoryPluginLoader
            = std::shared_ptr<FactoryPluginLoader<SensorFactory> >(new FactoryPluginLoader<SensorFactory>(libPaths));
    sensorFactories = factoryPluginLoader->getFactories();
}

LegacyMotionConverter::LegacyMotionConverter(const std::map<std::string, std::shared_ptr<SensorFactory> > &sensorFactories, bool mmmOnly) :
    sensorFactories(sensorFactories),
    mmmOnly(mmmOnly)
{
}

LegacyMotionList LegacyMotionConverter::loadMotions(const std::string &xml, bool xmlIsPath, const std::function<void(std::filesystem::path&)> &handleMissingModelFile) {
    auto motionFilePath = xmlIsPath ? xml : std::string();
    // check version
    simox::xml::RapidXMLWrapperNodePtr root = xml::getRoot(xml, xmlIsPath);
    if (root && root->name() == xml::tag::MMM_ROOT && root->has_attribute(xml::attribute::VERSION) && root->attribute_value(xml::attribute::VERSION) != VERSION)
        throw Exception::MMMFormatVersionException(root->attribute_value(xml::attribute::VERSION), "Please don't try to convert non legacy motions!");

    LegacyMotionReaderXMLPtr legacyMotionReader(new LegacyMotionReaderXML(mmmOnly, false));
    legacyMotionReader->setHandleMissingModelFile(handleMissingModelFile);
    auto legacyMotions = xmlIsPath ? legacyMotionReader->loadAllMotions(xml) : legacyMotionReader->loadAllMotionsFromString(xml);
    return legacyMotions;
}

MotionRecordingPtr LegacyMotionConverter::convert(const std::string &xml, bool xmlIsPath, const std::function<void(std::filesystem::path&)> &handleMissingModelFile)
{
    auto legacyMotions = loadMotions(xml, xmlIsPath, handleMissingModelFile);
    auto checkModel = LoadModelStrategy::isModelLoaded();

    return convert(legacyMotions, xmlIsPath ? xml : "", checkModel);
}

MotionPtr LegacyMotionConverter::convert(const rapidxml::xml_node<char>* motionNode, const std::string &motionName, const std::filesystem::path &motionFilePath, const std::function<void(std::filesystem::path&)> &handleMissingModelFile)
{
    LegacyMotionReaderXMLPtr legacyMotionReader(new LegacyMotionReaderXML(mmmOnly, false));
    legacyMotionReader->setHandleMissingModelFile(handleMissingModelFile);
    auto legacyMotion = legacyMotionReader->loadMotion(motionNode, motionName, motionFilePath);
    auto checkModel = legacyMotionReader->isModelLoaded();

    return convertMotion(legacyMotion, motionFilePath, checkModel);
}

MotionPtr LegacyMotionConverter::convert(simox::xml::RapidXMLWrapperNodePtr motionNode, const std::string &motionName, const std::filesystem::path &motionFilePath, const std::function<void(std::filesystem::path&)> &handleMissingModelFile)
{
    return convert(motionNode->getNode(), motionName, motionFilePath, handleMissingModelFile);
}

MotionRecordingPtr LegacyMotionConverter::convert(const std::vector<LegacyMotionPtr> &legacyMotions, const std::filesystem::path &motionFilePath, bool checkModel)
{
    MotionRecordingPtr motions = MotionRecording::EmptyRecording();
    for (auto legacyMotion : legacyMotions) {
        if (legacyMotion) {
            motions->addMotion(convertMotion(legacyMotion, motionFilePath.empty() ? legacyMotion->getMotionFilePath() : motionFilePath, checkModel));
        }
    }
    return motions;
}

MotionPtr LegacyMotionConverter::convertMotion(LegacyMotionPtr legacyMotion, const std::filesystem::path &motionFilePath, bool checkModel) {
    std::string motionName = legacyMotion->getName();
    auto model = legacyMotion->getProcessedModelWrapper();
    if (motionName.empty()) throw Exception::MMMException("Motion name is empty!");
    else if (checkModel && !legacyMotion->getModel()) throw Exception::MMMException("No model in motion '" + motionName + "'.");

    MotionPtr motion(new Motion(motionName, model, motionFilePath));
    for (auto sensor : createSensors(legacyMotion)) {
        if (sensor)
            motion->addSensor(sensor, 0.0f, checkModel);
    }
    return motion;
}

std::vector<SensorPtr> LegacyMotionConverter::createSensors(LegacyMotionPtr legacyMotion) {
    std::vector<SensorPtr> sensors;
    for (auto sensorFactory : sensorFactories) {
        auto s = sensorFactory.second->createSensors(legacyMotion);
        if (!s.empty())
            sensors.insert(sensors.end(), s.begin(), s.end());
    }
    return sensors;
}

std::filesystem::path LegacyMotionConverter::getStandardLibPath() {
    return SensorFactory::getStandardLibPath();
}

}
