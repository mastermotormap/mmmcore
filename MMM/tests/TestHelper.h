#ifndef TESTHELPER_H
#define TESTHELPER_H

#include "MMM/MMMCore.h"
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Nodes/RobotNodeFixed.h>

#include <set>

namespace MMM {

struct TestHelper {
    static ModelPtr createMockupModel(std::set<std::string> nodeNames) {
        ModelPtr model(new VirtualRobot::LocalRobot("Dummy"));
        auto rootNode = VirtualRobot::RobotNodePtr(new VirtualRobot::RobotNodeFixed(model, "ROOT", Eigen::Matrix4f()));
        model->setRootNode(rootNode);
        //model->registerRobotNode(rootNode);
        for (auto const &nodeName : nodeNames) {
            VirtualRobot::RobotNodePtr node(new VirtualRobot::RobotNodeFixed(model, nodeName, Eigen::Matrix4f()));
            rootNode->attachChild(node);
            model->registerRobotNode(node);
        }
        return model;
    }
};

}

#endif // TESTHELPER_H
