

#define BOOST_TEST_MODULE MMM_MODEL_READER_XML_TEST

#include <MMM/Motion/Legacy/LegacyMotion.h>
#include <MMM/Model/ModelReaderXML.h>
#include <MMM/Motion/Legacy/LegacyMotionReaderXML.h>
#include <string>

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Nodes/RobotNodeFixed.h>
#include <VirtualRobot/Nodes/RobotNodeRevolute.h>

#include <boost/test/unit_test.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>

BOOST_AUTO_TEST_CASE(testParseEmptyString)
{
    const std::string modelString = "";
    MMM::ModelReaderXMLPtr r(new MMM::ModelReaderXML());
    MMM::ModelPtr m = r->createModelFromString(modelString);
	BOOST_REQUIRE(!m);
}

BOOST_AUTO_TEST_CASE(testParseLoadModel)
{
	const std::string modelString = 
		"<?xml version='1.0' encoding='UTF-8'?>"
		"<Robot Type='DemoModel' RootNode='Joint1'>"
		"	<RobotNode name='Joint1'>"
		"		<Visualization enable='true'>"
		"			<File type='Inventor'>test.iv</File>"
		"			<CoordinateAxis type='Inventor' enable='true' scaling='1' text='Axis1'/>"
		"		</Visualization>"
		"		<CollisionModel>"
		"			<File type='Inventor'>test.iv</File>"
		"		</CollisionModel>"
		"		<Child name='Joint2'/>"
		"	</RobotNode>"
		"	<RobotNode name='Joint2'>"
		"		<Transform>"
		"			<Translation x='100' y='0' z='0' units='mm'/>"
		"		</Transform>"
		"		<Joint type='revolute' offset='0'>"
		"			<Limits unit='degree' lo='-90' hi='45'/>"
		"			<Axis x='0' y='0' z='1'/>"
		"		</Joint>"
		"		<Visualization enable='true'>"
		"			<File type='Inventor'>test.iv</File>"
		"		</Visualization>"
		"		<CollisionModel>"
		"			<File boundingbox='true' type='Inventor'>test.iv</File>"
		"		</CollisionModel>"
		"	</RobotNode>"
		"</Robot>";
    MMM::ModelReaderXMLPtr r(new MMM::ModelReaderXML());
    MMM::ModelPtr m = r->createModelFromString(modelString);
	BOOST_REQUIRE(m);
    BOOST_REQUIRE(m->getRootNode()->getName() =="Joint1");
	BOOST_REQUIRE(m->getName()=="DemoModel");
    BOOST_REQUIRE(m->getRobotNodes().size()==2);
    auto mod1 = m->getRobotNodes()[0];
	BOOST_REQUIRE(mod1);
    BOOST_REQUIRE(mod1->getName() =="Joint1");
    BOOST_REQUIRE(std::dynamic_pointer_cast<VirtualRobot::RobotNodeFixed>(mod1));
    auto mod2 = m->getRobotNodes()[1];
	BOOST_REQUIRE(mod2);
    BOOST_REQUIRE(mod2->getName() == "Joint2");
    auto _mod2 = std::dynamic_pointer_cast<VirtualRobot::RobotNodeRevolute>(mod2);
    BOOST_REQUIRE(_mod2);
    BOOST_REQUIRE(_mod2->getJointRotationAxisInJointCoordSystem().isApprox(Eigen::Vector3f(0,0,1.0f)));

	// check export and re-load data
    std::string modelString2 = "<? xml version = '1.0' ?>\n" + m->toXML();
	//std::cout << modelString2 << std::endl;
    MMM::ModelPtr m2 = r->createModelFromString(modelString2);
	BOOST_REQUIRE(m2);
    BOOST_REQUIRE(m2->getRootNode()->getName() =="Joint1");
    BOOST_REQUIRE(m2->getRobotNodes().size()==2);
    mod1 = m->getRobotNodes()[0];
    BOOST_REQUIRE(mod1);
    BOOST_REQUIRE(mod1->getName() =="Joint1");
    BOOST_REQUIRE(std::dynamic_pointer_cast<VirtualRobot::RobotNodeFixed>(mod1));
    mod2 = m->getRobotNodes()[1];
    BOOST_REQUIRE(mod2);
    BOOST_REQUIRE(mod2->getName() == "Joint2");
    _mod2 = std::dynamic_pointer_cast<VirtualRobot::RobotNodeRevolute>(mod2);
    BOOST_REQUIRE(_mod2);
    BOOST_REQUIRE(_mod2->getJointRotationAxisInJointCoordSystem().isApprox(Eigen::Vector3f(0,0,1.0f)));
}

// BOOST_AUTO_TEST_SUITE_END()
