

#define BOOST_TEST_MODULE MMM_MODELPROCESSOR_TEST

#include <MMM/Motion/Legacy/LegacyMotion.h>
#include <MMM/Model/ModelProcessorWinter.h>
#include <MMM/Model/ModelReaderXML.h>
#include <MMM/Model/ModelProcessorFactory.h>
#include <MMM/Motion/Legacy/LegacyMotionReaderXML.h>
#include <string>

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Nodes/RobotNodeFixed.h>
#include <VirtualRobot/Nodes/RobotNodeRevolute.h>

#include <boost/test/unit_test.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>

BOOST_AUTO_TEST_CASE(testModelProcessorWinter)
{
    const std::string modelString =
		"<?xml version='1.0' encoding='UTF-8'?>"
		"<Robot Type='DemoModel' RootNode='Joint1'>"
		"	<RobotNode name='Joint1'>"
		"		<Visualization enable='true'>"
		"			<File type='Inventor'>test.iv</File>"
		"			<CoordinateAxis type='Inventor' enable='true' scaling='1' text='Axis1'/>"
		"		</Visualization>"
		"		<CollisionModel>"
		"			<File type='Inventor'>test.iv</File>"
		"		</CollisionModel>"
		"		<Child name='Joint2'/>"
		"	</RobotNode>"
		"	<RobotNode name='Joint2'>"
		"		<Transform>"
		"			<Translation x='100' y='0' z='0' units='mm'/>"
		"		</Transform>"
		"		<Joint type='revolute' offset='0'>"
		"			<Limits unit='degree' lo='-90' hi='45'/>"
		"			<Axis x='0' y='0' z='1'/>"
		"		</Joint>"
		"		<Visualization enable='true'>"
		"			<File type='Inventor'>test.iv</File>"
		"		</Visualization>"
		"		<CollisionModel>"
		"			<File boundingbox='true' type='Inventor'>test.iv</File>"
		"		</CollisionModel>"
		"	</RobotNode>"
		"</Robot>";
    MMM::ModelReaderXMLPtr r(new MMM::ModelReaderXML());
    MMM::ModelPtr m = r->createModelFromString(modelString);
    BOOST_REQUIRE(m);
    BOOST_REQUIRE(m->getRootNode()->getName() =="Joint1");
    BOOST_REQUIRE(m->getName()=="DemoModel");
    BOOST_REQUIRE(m->getRobotNodes().size()==2);
    auto mod1 = m->getRobotNodes()[0];
    BOOST_REQUIRE(mod1);
    BOOST_REQUIRE(mod1->getName() =="Joint1");
    BOOST_REQUIRE(std::dynamic_pointer_cast<VirtualRobot::RobotNodeFixed>(mod1));
    auto mod2 = m->getRobotNodes()[1];
    BOOST_REQUIRE(mod2);
    BOOST_REQUIRE(mod2->getName() == "Joint2");
    auto _mod2 = std::dynamic_pointer_cast<VirtualRobot::RobotNodeRevolute>(mod1);
    BOOST_REQUIRE(_mod2);
    BOOST_REQUIRE(_mod2->getJointRotationAxisInJointCoordSystem().isApprox(Eigen::Vector3f(0,0,1.0f)));
    BOOST_REQUIRE_CLOSE(mod2->getLocalTransformation().block(0,3,3,1).norm(), 0.1f, 0.01f);

    MMM::ModelProcessorWinterPtr mpWinter(new MMM::ModelProcessorWinter(2.0, 50.0f));
    BOOST_REQUIRE(mpWinter);

    mpWinter->setupSegmentLength("Joint2", 0.3f);

    MMM::ModelPtr mmmModel2 = mpWinter->convertModel(m);
    BOOST_REQUIRE(mmmModel2);

    float height1 = m->getScaling();
    BOOST_REQUIRE_CLOSE(height1, 1.0f, 0.001f);
    float height2 = mmmModel2->getScaling();
    BOOST_REQUIRE_CLOSE(height2, 2.0f, 0.001f);

    auto n1 = m->getRobotNode("Joint2");
    BOOST_REQUIRE(n1);
    float l1 = n1->getLocalTransformation().block(0, 3, 3, 1).norm();
    BOOST_REQUIRE_CLOSE(l1, 0.1f, 0.001f);

    auto n2 = m->getRobotNode("Joint2");
    BOOST_REQUIRE(n2);
    float l2 = n2->getLocalTransformation().block(0, 3, 3, 1).norm();
    BOOST_REQUIRE_CLOSE(l2, 0.3f, 0.001f);


}


// BOOST_AUTO_TEST_SUITE_END()
