
#define BOOST_TEST_MODULE MMM_MOTION_TEST

#include "MMM/Motion/MotionReaderXML.h"
#include "MMM/Motion/MotionRecording.h"
#include "MMM/Motion/Motion.h"

#include "MMM/Model/LoadModelStrategy.h"
#include <boost/test/unit_test.hpp>

#include <iostream>

BOOST_AUTO_TEST_CASE( ReadMultiMotions )
{
    MMM::MotionReaderXMLPtr mr(new MMM::MotionReaderXML(true));
    MMM::LoadModelStrategy::LOAD_MODEL_STRATEGY = MMM::LoadModelStrategy::Strategy::LOAD_PROCESS_BUFFER;
    //auto motions = mr->loadAllMotionsFromDirectory("/home/andre/Documents/KITLanguageDataset", false, 12, "_mmm", 2000, 0, true);
    //auto motions = mr->loadAllMotionsFromDirectory("/home/andre/repos/MDB/motiondb_python_example/test", false, 12, "", 2000, 0, true);
    auto motions = mr->loadAllMotionRecordingsFromDirectory("/home/andre/repos/mmmtools/data/Motions", false, 12, "", 2000, 0, true);
    std::cout << motions.size() << "\n";
    for (auto ms : motions) {
        for (const std::string &name : ms->getMotionNames()) {
            auto motion = ms->getMotion(name);
            //std::cout << motion->getOriginFilePath() << " " <<  motion->getModel(true) << (motion->getModel(true) != nullptr ? " + " + std::to_string(motion->getModel(true)->getScaling()) : "") << std::endl;
        };
    }
    std::cout << std::endl;
}

// BOOST_AUTO_TEST_SUITE_END()
