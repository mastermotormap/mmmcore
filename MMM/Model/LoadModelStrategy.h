/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/
#ifndef __MMM_LOADMODELSTRATEGY_H_
#define __MMM_LOADMODELSTRATEGY_H_

#include "MMM/MMMCore.h"
#include <map>
#include <VirtualRobot/VirtualRobot.h>

namespace MMM
{

struct LoadModelStrategy {

    /*!
     * \brief Different possible stratgies for loading models from a motion depending on the required performance and needs.
     */
    enum class Strategy {
        NONE,                           /*!< No direct loading of model file */
        CHECK,                          /*!< No direct loading of model file, but check if valid file path */
        LOAD,                           /*!< Load model from file */
        LOAD_PROCESS,                   /*!< Load/process model from file */
        LOAD_BUFFER,                    /*!< Load model from file and buffer model */
        LOAD_PROCESS_BUFFER,            /*!< Load/process model from file and buffer model */
        LOAD_BUFFER_CLONE,              /*!< Load model from file and buffer model. Clone buffered model if model is needed */
        LOAD_PROCESS_BUFFER_CLONE       /*!< Load/process model from file and buffer model. Clone buffered model if model is needed */
    };

    static Strategy LOAD_MODEL_STRATEGY;
    static std::map<std::string, VirtualRobot::RobotPtr> bufferedModels;

    static bool isModelLoaded();
    static bool isModelBuffered();
    static bool isModelBufferedCloned();
    static bool isModelProcessed();
};

}

#endif // __MMM_LOADMODELSTRATEGY_H_
