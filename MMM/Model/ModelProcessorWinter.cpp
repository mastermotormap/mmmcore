#include "ModelProcessorWinter.h"

#include "ModelProcessorFactory.h"
#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>
#include <SimoxUtility/algorithm/string.h>
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <cmath>

namespace MMM
{

ModelProcessorWinter::ModelProcessorWinter(float height, float mass, float handLength/*, float handWidth*/,
                                           const std::string &rightHandRootName, const std::string &leftHandRootName) :
    ModelProcessor("Winter"),
    height(height),
    mass(mass),
    handLength(handLength),
    rightHandRootName(rightHandRootName),
    leftHandRootName(leftHandRootName)
{
}

ModelProcessorWinter::ModelProcessorWinter(simox::xml::RapidXMLWrapperNodePtr node) :
    ModelProcessor("Winter"),
    height(1.0),
    mass(1.0),
    handLength(-1.0),
    rightHandRootName("Hand R Root"),
    leftHandRootName("Hand L Root")
{
    try {
        if (node->has_node("Height")) {
            height = node->first_node("Height")->value_<float>();
        }
        if (node->has_node("Mass")) {
            mass = node->first_node("Mass")->value_<float>();
        }
        else if (node->has_node("Weight")) {
            mass = node->first_node("Weight")->value_<float>();
        }
        if (node->has_node("HandLength")) {
            if (node->has_attribute("rootr"))
                rightHandRootName = node->attribute_value("rootr");
            if (node->has_attribute("rootl"))
                leftHandRootName = node->attribute_value("rootl");
            handLength = node->first_node("HandLength")->value_<float>();
        }
        /*if (node->has_node("HandWidth")) {
            handWidth = node->first_node("HandWidth")->value_<float>();
        }*/
        if (node->has_node("SegmentLength")) {
            for (auto sn : node->nodes("SegmentLength")) {
                customSegmentLengths[sn->attribute_value("name")] = sn->value_<float>() * sn->getUnitsScalingToMeter();
            }
        }
    } catch (simox::error::XMLFormatError &e) {
        throw MMM::Exception::MMMFormatException(e.what());
    }
}

void ModelProcessorWinter::setup(float height, float mass, float handLength/*, float handWidth*/)
{
	this->height = height;
	this->mass = mass;
    this->handLength = handLength;
    /*this->handWidth = handWidth;*/
}

void ModelProcessorWinter::setupSegmentLength(const std::string &segmentName, float lengthM)
{
    customSegmentLengths[segmentName] = lengthM;
}


VirtualRobot::RobotPtr ModelProcessorWinter::convertModel(VirtualRobot::RobotPtr input)
{
	if (!input)
		return input;

    VirtualRobot::RobotPtr res = input->clone();

    std::map<std::string, float> scalingMap;

    if (handLength > 0)
    {
        // TODO: Scaling only for mmm model
        float scalingFactor = 1 / 10.8f;
        auto rightHand = res->getRobotNode(rightHandRootName);
        if (!rightHand) throw Exception::MMMException("Root node " + rightHandRootName + " not found for right hand");
        auto rightHandNodes = rightHand->getChildren();
        while (rightHandNodes.size() > 0) {
            auto node = rightHandNodes.back();
            rightHandNodes.pop_back();
            scalingMap[node->getName()] = handLength * scalingFactor / node->getScaling();
            for (auto child : node->getChildren())
                rightHandNodes.push_back(child);
        }

        auto leftHand = res->getRobotNode(leftHandRootName);
        if (!leftHand) throw Exception::MMMException("Root node " + leftHandRootName + " not found for left hand");
        auto leftHandNodes = leftHand->getChildren();
        while (leftHandNodes.size() > 0) {
            auto node = leftHandNodes.back();
            leftHandNodes.pop_back();
            scalingMap[node->getName()] = handLength * scalingFactor / node->getScaling();
            for (auto child : node->getChildren())
                leftHandNodes.push_back(child);
        }
    }

    for (auto model : res->getRobotNodes())
	{
        float scaling = height;

		// scale model
        auto custSegIt = customSegmentLengths.find(model->getName());
        if (custSegIt != customSegmentLengths.end())
        {
            float l = model->getLocalTransformation().block(0, 3, 3, 1).norm();
            if (l != 0.0f)
                scaling = 1.0f / l * custSegIt->second;
        }

        auto handSegIt = scalingMap.find(model->getName());
        if (handSegIt != scalingMap.end())
            scaling = handSegIt->second;

        for (auto sensor : model->getSensors())
        {
            Eigen::Matrix4f lt = sensor->getParentNodeToSensorTransformation();
            lt.block(0, 3, 3, 1) *= scaling;
            sensor->setRobotNodeToSensorTransformation(lt);
        }

        //std::cout << "Model " << model->getName() << " : " << scaling << std::endl;

        scale(model, scaling);
 	}
    res->setScaling(height);
	res->setMass(mass);
	return res;
}

void ModelProcessorWinter::scale(VirtualRobot::RobotNodePtr modelNode, float scaling) {
    auto localTransformation = modelNode->getLocalTransformation();
    localTransformation.block(0, 3, 3, 1) *= scaling;
    modelNode->setLocalTransformation(localTransformation);

    modelNode->setMass(modelNode->getMass() * mass);
    modelNode->setCoMLocal(modelNode->getCoMLocal() * scaling);
    modelNode->setInertiaMatrix(modelNode->getInertiaMatrix() * pow(scaling,2) * mass);
    modelNode->setScaling(scaling);

    // todo prismatic joints!
    // todo hand scaling
}

void ModelProcessorWinter::appendProcessorDataXML(simox::xml::RapidXMLWrapperNodePtr processorNode) {
    processorNode->append_node("Height")->append_data_node(height);
    processorNode->append_node("Mass")->append_data_node(mass);
    if (handLength > 0)
        processorNode->append_node("HandLength")->append_data_node(handLength);
    /*if (handWidth > 0)
        processorNode->append_node("HandWidth")->append_data_node(handWidth);*/
    for (auto & customSegmentLength : customSegmentLengths) {
        processorNode->append_node("SegmentLength")->append_attribute("name", customSegmentLength.first)->append_attribute("units", "m")->append_data_node(customSegmentLength.second);
    }
}

float ModelProcessorWinter::getMass()
{
	return mass;
}

float ModelProcessorWinter::getHeight()
{
	return height;
}


std::map<std::string, float> ModelProcessorWinter::getCustomSegmentLengths()
{
    return customSegmentLengths;
}
}
