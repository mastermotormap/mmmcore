#include "LoadModelStrategy.h"

namespace MMM
{

LoadModelStrategy::Strategy LoadModelStrategy::LOAD_MODEL_STRATEGY = LoadModelStrategy::Strategy::LOAD_PROCESS;
std::map<std::string, VirtualRobot::RobotPtr> LoadModelStrategy::bufferedModels = std::map<std::string, VirtualRobot::RobotPtr>();

bool LoadModelStrategy::isModelLoaded() {
    return LoadModelStrategy::LOAD_MODEL_STRATEGY != LoadModelStrategy::Strategy::NONE && LoadModelStrategy::LOAD_MODEL_STRATEGY != LoadModelStrategy::Strategy::CHECK;
}

bool LoadModelStrategy::isModelBuffered() {
    return LoadModelStrategy::isModelBufferedCloned() || LoadModelStrategy::LOAD_MODEL_STRATEGY == LoadModelStrategy::Strategy::LOAD_BUFFER || LoadModelStrategy::LOAD_MODEL_STRATEGY == LoadModelStrategy::Strategy::LOAD_PROCESS;
}

bool LoadModelStrategy::isModelBufferedCloned() {
    return LoadModelStrategy::LOAD_MODEL_STRATEGY == LoadModelStrategy::Strategy::LOAD_BUFFER_CLONE || LoadModelStrategy::LOAD_MODEL_STRATEGY == LoadModelStrategy::Strategy::LOAD_PROCESS_BUFFER_CLONE;
}

bool LoadModelStrategy::isModelProcessed() {
    return LoadModelStrategy::LOAD_MODEL_STRATEGY == LoadModelStrategy::Strategy::LOAD_PROCESS || LoadModelStrategy::LOAD_MODEL_STRATEGY == LoadModelStrategy::Strategy::LOAD_PROCESS_BUFFER
            || LoadModelStrategy::LOAD_MODEL_STRATEGY == LoadModelStrategy::Strategy::LOAD_PROCESS_BUFFER_CLONE;
}

}
