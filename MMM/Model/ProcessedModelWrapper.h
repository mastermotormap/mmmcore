/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/
#ifndef __MMM_PROCESSEDMODELWRAPPER_H_
#define __MMM_PROCESSEDMODELWRAPPER_H_

#include "MMM/MMMCore.h"

#include <filesystem>
#include <VirtualRobot/VirtualRobot.h>

namespace MMM
{

class MMM_IMPORT_EXPORT ProcessedModelWrapper {
public:
    ProcessedModelWrapper(const std::filesystem::path &modelFilePath = std::string()) : ProcessedModelWrapper(nullptr, nullptr, modelFilePath, false)
    {
    }

    ProcessedModelWrapper(ModelPtr model, ModelPtr processedModel, ModelProcessorPtr modelProcessor, const std::filesystem::path &modelFilePath);

    ProcessedModelWrapper(ModelPtr model, ModelProcessorPtr modelProcessor, const std::filesystem::path &modelFilePath, bool processModel = true);

    ModelPtr getModel(bool processed = true);

    void setModel(ModelPtr originalModel);
    void setProcessedModel(ModelPtr processedModel);

    ModelProcessorPtr getModelProcessor();
    void setModelProcessor(ModelProcessorPtr modelProcessor);

    std::filesystem::path getOriginalModelFilePath();

    std::string getOriginalModelName();
    std::string getOriginalModelFileName();

    ProcessedModelWrapperPtr clone();

    void processModel();

    //! Returns a reduced robot model by merging all robot nodes in subtrees below the given actuated joints of the model (e.g. hands of the MMM model).
    static ModelPtr createReducedModel(ModelPtr model, const std::vector<std::string> &actuatedJoints);

    static bool updateInertialMatricesFromModels(VirtualRobot::RobotPtr robot);

private:
    static std::vector<std::string> getUnitableSubnodes(VirtualRobot::RobotNodePtr robotNode, const std::vector<std::string> &actuatedJoints);

    ModelPtr originalModel;
    ModelPtr processedModel;
    ModelProcessorPtr modelProcessor;
    std::filesystem::path modelFilePath;
};

}

#endif
