/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Nikolaus Vahrenkamp
* @copyright  2013 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/
#ifndef __ModelReaderXML_H_
#define __ModelReaderXML_H_

#include "MMM/MMMCore.h"

#include <string> 
#include <vector>
#include <mutex>
#include <map>
#include <filesystem>
#include <VirtualRobot/VirtualRobot.h>

namespace MMM
{

/*!
	\brief This reader parses a model file in order to generate an MMM Model.
	The model syntax is compatible with the Simox::VirtualRobot package.
*/
class MMM_IMPORT_EXPORT ModelReaderXML : public std::enable_shared_from_this<ModelReaderXML>
{   

public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	ModelReaderXML();

    ModelPtr loadModel(const std::filesystem::path &xmlFile, bool loadVisualization = false);

    ModelPtr createModelFromString(const std::string &xmlFile, const std::string &basePath = "", bool loadVisualization = false);

    ProcessedModelWrapperPtr loadMotionModel(std::filesystem::path &modelFilePath, const std::filesystem::path &motionFilePath, ModelProcessorPtr modelProcessor = nullptr, bool loadVisualization = false);

    std::function<void(std::filesystem::path&)> handleModelFilePath;


private:
    std::mutex bufferMutex;
};

typedef std::shared_ptr<ModelReaderXML> ModelReaderXMLPtr;

}

#endif 
