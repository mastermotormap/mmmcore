#include "ModelProcessor.h"

#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>
#include <fstream>
#include <Eigen/Core>

namespace MMM
{

ModelProcessor::ModelProcessor(const std::string &name) : name(name)
{
}

std::string ModelProcessor::getName()
{
	return name;
}

void ModelProcessor::appendDataXML(simox::xml::RapidXMLWrapperNodePtr node) {
    simox::xml::RapidXMLWrapperNodePtr processorNode = node->append_node("ModelProcessorConfig");
    if (!name.empty())
    {
        processorNode->append_attribute("type", name);
        appendProcessorDataXML(processorNode);
    }
}

std::string ModelProcessor::toXML(bool indenting, int indents) {
    simox::xml::RapidXMLWrapperRootNodePtr root = simox::xml::RapidXMLWrapperRootNode::createRootNode("ModelProcessorConfig");
    root->append_attribute("type", name);
    appendProcessorDataXML(root);
    return root->print(indenting, indents);
}

}
