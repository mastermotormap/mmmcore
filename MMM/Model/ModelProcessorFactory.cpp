#include "ModelProcessorFactory.h"

#include "ModelProcessor.h"

#include <fstream>
#include <filesystem>

#include <SimoxUtility/xml/rapidxml/rapidxml.hpp>
#include <SimoxUtility/xml/rapidxml/rapidxml_print.hpp>
#include <SimoxUtility/algorithm/string.h>
#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>

namespace MMM
{

// Need to add a template specialization to the library, 
// otherwise we will get undefined references when we use the ModelProcessorFactory externally.
template class MMM_IMPORT_EXPORT AbstractFactoryMethod<ModelProcessorFactory, void*>;

simox::xml::RapidXMLWrapperNodePtr ModelProcessorFactory::getNode(const std::string &configFile) {
    try {
        simox::xml::RapidXMLWrapperRootNodePtr root = simox::xml::RapidXMLWrapperRootNode::FromFile(configFile);
        return root;
    }
    catch (simox::error::XMLFormatError &e) {
        throw MMM::Exception::MMMFormatException(e.what());
    }
}

ModelProcessorPtr ModelProcessorFactory::createModelProcessor(const std::string &configFile) {
    return createModelProcessor(getNode(configFile));
}

ModelProcessorPtr ModelProcessorFactory::fromConfigFile(const std::string &configFile)
{
    return getModelProcessorFromNode(getNode(configFile));
}

ModelProcessorPtr ModelProcessorFactory::getModelProcessorFromNode(simox::xml::RapidXMLWrapperNodePtr node) {
    std::string factoryName;
    try {
        factoryName = node->attribute_value("type");
    }
    catch (simox::error::XMLFormatError &e) {
        throw MMM::Exception::MMMFormatException(e.what());
    }

    ModelProcessorFactoryPtr modelFactory = ModelProcessorFactory::fromName(factoryName, NULL);
    if (!modelFactory)
        throw MMM::Exception::MMMFormatException("ModelProcessorFactory with name " + factoryName + " not known.");

    return modelFactory->createModelProcessor(node);
}

ModelProcessorPtr ModelProcessorFactory::getModelProcessorFromNode(rapidxml::xml_node<>* node)
{
    try
    {
        return getModelProcessorFromNode(simox::xml::RapidXMLWrapperNodePtr(new simox::xml::RapidXMLWrapperNode(node)));
    }
    catch (MMM::Exception::MMMFormatException &e)
    {
        MMM_ERROR << e.what() << std::endl;
        return ModelProcessorPtr();
    }
}

}
