#include "ModelReaderXML.h"

#include <algorithm>
#include <fstream>
#include <filesystem>
#include <SimoxUtility/xml/rapidxml/rapidxml.hpp>
#include <SimoxUtility/algorithm/string.h>
#include <VirtualRobot/XML/RobotIO.h>

#include "MMM/Exceptions.h"
#include "ProcessedModelWrapper.h"
#include "ModelProcessor.h"
#include "LoadModelStrategy.h"
#include "MMM/Motion/XMLTools.h"

namespace MMM
{
    ModelReaderXML::ModelReaderXML()
        = default;

    ProcessedModelWrapperPtr ModelReaderXML::loadMotionModel(std::filesystem::path &modelFilePath, const std::filesystem::path &motionFilePath, ModelProcessorPtr modelProcessor, bool loadVisualization) {
        auto fileName = modelFilePath.stem();
        VirtualRobot::RobotPtr model = nullptr;
        if (LoadModelStrategy::isModelBuffered()) {
            bufferMutex.lock();
            if (LoadModelStrategy::bufferedModels.find(fileName) != LoadModelStrategy::bufferedModels.end()) {
                model = LoadModelStrategy::bufferedModels.at(fileName);
                bufferMutex.unlock();
                if (LoadModelStrategy::isModelBufferedCloned())
                    model = model->cloneScaling();
            }
            else bufferMutex.unlock();
        }
        if (!model) {
            if (!MMM::xml::isValid(modelFilePath)) {
                // check relative filePath
                auto relativePath = std::filesystem::absolute(motionFilePath.parent_path() / modelFilePath);
                if (!MMM::xml::isValid(relativePath)) {
                    if (LoadModelStrategy::LOAD_MODEL_STRATEGY != LoadModelStrategy::Strategy::NONE) {
                        // check additional provided function for determining missing model filepath
                        if (handleModelFilePath) handleModelFilePath(modelFilePath);
                        else throw Exception::MMMFormatException("Could not determine valid filename: " + modelFilePath.generic_string() + ", motion base path:" + motionFilePath.generic_string());
                        if (modelFilePath.empty()) throw Exception::MMMFormatException("No valid motion filename was choosen!");
                    }
                }
                else modelFilePath = relativePath;
            }
            if (LoadModelStrategy::isModelLoaded()) {
                try {
                    model = loadModel(modelFilePath, loadVisualization);
                } catch (VirtualRobot::VirtualRobotException &e) {
                    throw Exception::MMMFormatException(e.what());
                }
                if (LoadModelStrategy::isModelBuffered()) {
                    bufferMutex.lock();
                    LoadModelStrategy::bufferedModels[fileName] = model;
                    bufferMutex.unlock();
                }
            }
        }

        return ProcessedModelWrapperPtr(new ProcessedModelWrapper(model, modelProcessor, modelFilePath, LoadModelStrategy::isModelProcessed()));
    }

    VirtualRobot::RobotPtr ModelReaderXML::loadModel(const std::filesystem::path &xmlFile, bool loadVisualization) {
            return VirtualRobot::RobotIO::loadRobot(xmlFile, loadVisualization ? VirtualRobot::RobotIO::RobotDescription::eFullVisAsCol
                                                                               : VirtualRobot::RobotIO::RobotDescription::eStructureStore);
    }

    VirtualRobot::RobotPtr ModelReaderXML::createModelFromString(const std::string &xmlFile, const std::string &basePath, bool loadVisualization) {
        return VirtualRobot::RobotIO::createRobotFromString(xmlFile, basePath, loadVisualization ? VirtualRobot::RobotIO::RobotDescription::eFullVisAsCol
                                                                           : VirtualRobot::RobotIO::RobotDescription::eStructureStore);
    }
}
