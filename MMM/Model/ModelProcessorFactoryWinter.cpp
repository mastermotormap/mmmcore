#include "ModelProcessorFactoryWinter.h"

#include "ModelProcessorWinter.h"

namespace MMM
{

// register this factory
ModelProcessorFactory::SubClassRegistry ModelProcessorFactoryWinter::registry(ModelProcessorFactoryWinter::getName(), &ModelProcessorFactoryWinter::createInstance);

ModelProcessorFactoryWinter::ModelProcessorFactoryWinter()
: ModelProcessorFactory()
{

}
ModelProcessorFactoryWinter::~ModelProcessorFactoryWinter()
= default;

ModelProcessorPtr ModelProcessorFactoryWinter::createModelProcessor(simox::xml::RapidXMLWrapperNodePtr node)
{
    ModelProcessorPtr p(new ModelProcessorWinter(node));
	return p;
}

std::string ModelProcessorFactoryWinter::getName()
{
	return "Winter";
}

std::shared_ptr<ModelProcessorFactory> ModelProcessorFactoryWinter::createInstance(void*)
{
	std::shared_ptr<ModelProcessorFactory> converterFactory(new ModelProcessorFactoryWinter());
	return converterFactory;
}

}
