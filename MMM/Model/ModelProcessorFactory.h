/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Nikolaus Vahrenkamp
* @copyright  2013 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef _MMM_ModelProcessorFactory_h_
#define _MMM_ModelProcessorFactory_h_

#include "MMM/MMMCore.h"
#include "MMM/AbstractFactoryMethod.h"

#include <string>

#include <SimoxUtility/xml/rapidxml/RapidXMLForwardDecl.h>

namespace MMM
{

class MMM_IMPORT_EXPORT ModelProcessorFactory : public AbstractFactoryMethod<ModelProcessorFactory, void*>
{
public:

	ModelProcessorFactory() { ; }
	virtual ~ModelProcessorFactory() { ; }

    ModelProcessorPtr createModelProcessor(const std::string &configFile);

    virtual ModelProcessorPtr createModelProcessor(simox::xml::RapidXMLWrapperNodePtr node) = 0;

    /*!
    Convenient method to create a model processor via an XML config file.
    The XML tag '<ModelProcessorConfig type='FactoryName'>' is used to identitfy the factory, which is used to create a model processor.
    The model processor is intiialized with the passed config file.
    */
    static ModelProcessorPtr fromConfigFile(const std::string &configFile);

    static ModelProcessorPtr getModelProcessorFromNode(simox::xml::RapidXMLWrapperNodePtr node);

    static ModelProcessorPtr getModelProcessorFromNode(rapidxml::xml_node<char>* node);

protected:
    static simox::xml::RapidXMLWrapperNodePtr getNode(const std::string &configFile);
};

typedef std::shared_ptr<ModelProcessorFactory> ModelProcessorFactoryPtr;


} // namespace MMM

#endif // _MMM_ModelProcessorFactory_h_
