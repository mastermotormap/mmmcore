/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Nikolaus Vahrenkamp
* @copyright  2013 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_ModelProcessor_H_
#define __MMM_ModelProcessor_H_

#include "MMM/MMMCore.h"

#include <string> 
#include <vector>
#include <map>
#include <VirtualRobot/VirtualRobot.h>

namespace MMM
{

/*!
	\brief A processor to adapt a generic model to user specific demands.
*/
class MMM_IMPORT_EXPORT ModelProcessor
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    ModelProcessor(const std::string &name);
	
	/*!
		Convert a model
	*/
    virtual VirtualRobot::RobotPtr convertModel(VirtualRobot::RobotPtr input) = 0;

	/*!
		Get model processor configuration as XML string.
	*/
    virtual std::string toXML(bool indenting = true, int indents = 0);

	virtual std::string getName();

    void appendDataXML(simox::xml::RapidXMLWrapperNodePtr node);

protected:
    virtual void appendProcessorDataXML(simox::xml::RapidXMLWrapperNodePtr /*processorNode*/) {
    }

	std::string name; //! name of modelprocessor
};

}

#endif 
