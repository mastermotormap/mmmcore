#include "ProcessedModelWrapper.h"

#include "ModelReaderXML.h"
#include "ModelProcessor.h"
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/CollisionDetection/CollisionModel.h>
#include <VirtualRobot/RobotFactory.h>
#include "MMM/Motion/XMLTools.h"

namespace MMM
{

ProcessedModelWrapper::ProcessedModelWrapper(VirtualRobot::RobotPtr model, ModelProcessorPtr modelProcessor, const std::filesystem::path &modelFilePath, bool processModel) :
    originalModel(model),
    modelProcessor(modelProcessor),
    modelFilePath(modelFilePath)
{
    if (model && processModel) {
        if (modelProcessor) processedModel = modelProcessor->convertModel(model);
        else processedModel = model; // Original: model->cloneScaling();
    }
}

ProcessedModelWrapper::ProcessedModelWrapper(VirtualRobot::RobotPtr model, VirtualRobot::RobotPtr processedModel, ModelProcessorPtr modelProcessor, const std::filesystem::path &modelFilePath) :
    originalModel(model),
    processedModel(processedModel),
    modelProcessor(modelProcessor),
    modelFilePath(modelFilePath)
{
}

VirtualRobot::RobotPtr ProcessedModelWrapper::getModel(bool processed) {
    if (!originalModel) {
        ModelReaderXMLPtr mr(new ModelReaderXML());
        if (!modelFilePath.empty() && MMM::xml::isValid(modelFilePath)) {
            originalModel = mr->loadModel(modelFilePath);
        }
    }
    if (processed) {
        if (!processedModel) processModel();
        return processedModel;
    }
    return originalModel;
}

ModelProcessorPtr ProcessedModelWrapper::getModelProcessor() {
    return modelProcessor;
}

std::filesystem::path ProcessedModelWrapper::getOriginalModelFilePath() {
    return modelFilePath;
}

std::string ProcessedModelWrapper::getOriginalModelName() {
    if (!originalModel)
        return std::string();
    return originalModel->getType();
}


std::string ProcessedModelWrapper::getOriginalModelFileName() {
    return modelFilePath.stem();
}

ProcessedModelWrapperPtr ProcessedModelWrapper::clone() {
    return ProcessedModelWrapperPtr(new ProcessedModelWrapper(originalModel ? originalModel->clone() : nullptr, modelProcessor, getOriginalModelFilePath(), true));
}

void ProcessedModelWrapper::setModel(VirtualRobot::RobotPtr originalModel) {
    this->originalModel = originalModel;
}

void ProcessedModelWrapper::setProcessedModel(VirtualRobot::RobotPtr processedModel) {
    this->processedModel = processedModel;
}

void ProcessedModelWrapper::setModelProcessor(ModelProcessorPtr modelProcessor) {
    this->modelProcessor = modelProcessor;
}

void ProcessedModelWrapper::processModel() {
    if (!originalModel) return;
    if (getModelProcessor()) processedModel = getModelProcessor()->convertModel(originalModel);
    else processedModel = originalModel; // originalModel->clone();
}

bool ProcessedModelWrapper::updateInertialMatricesFromModels(VirtualRobot::RobotPtr robot)
{
    float scaling = 0.001f; // mm -> m

    for (auto rn : robot->getRobotNodes())
    {
        VirtualRobot::CollisionModelPtr colModel = rn->getCollisionModel();

        Eigen::Matrix3f mZero;
        mZero.setZero();
        if ((rn->getInertiaMatrix() == mZero) && (colModel))
        {
            // get local bbox
            VirtualRobot::BoundingBox bbox = colModel->getBoundingBox(false);
            Eigen::Vector3f halfExtents = (bbox.getMax() - bbox.getMin())*0.5f;
            float lx = 2.0f*(halfExtents.x()) * scaling;
            float ly = 2.0f*(halfExtents.y()) * scaling;
            float lz = 2.0f*(halfExtents.z()) * scaling;
            Eigen::Matrix3f inertia = Eigen::Matrix3f::Zero();
            inertia(0, 0) = rn->getMass() * 1.0f / 12.0f * (ly*ly + lz*lz);
            inertia(1, 1) = rn->getMass() * 1.0f / 12.0f * (lx*lx + lz*lz);
            inertia(2, 2) = rn->getMass() * 1.0f / 12.0f * (lx*lx + ly*ly);
            rn->setInertiaMatrix(inertia);
        }
    }
    return true;
}

ModelPtr ProcessedModelWrapper::createReducedModel(ModelPtr model, const std::vector<std::string> &actuatedJoints) {
    std::vector<std::string> unitableSubnodes = getUnitableSubnodes(model->getRootNode(), actuatedJoints);

    if (unitableSubnodes.empty())
    {
        MMM_INFO << "No robot subnodes can be united." << std::endl;
        return model;
    }
    else
    {
        std::stringstream ss;
        ss << unitableSubnodes[0];

        for (std::vector<std::string>::const_iterator i = unitableSubnodes.begin() + 1; i != unitableSubnodes.end(); ++i)
        {
            ss << ", " << *i;
        }

        MMM_INFO << "Robot nodes that are united: " << ss.str() << std::endl;

        return VirtualRobot::RobotFactory::cloneUniteSubsets(model, "MMM_Reduced_Model", unitableSubnodes);
    }
}

std::vector<std::string> ProcessedModelWrapper::getUnitableSubnodes(VirtualRobot::RobotNodePtr robotNode, const std::vector<std::string> &actuatedJoints)
{
    std::vector<std::string> unitableSubnodes;

    std::vector<VirtualRobot::RobotNodePtr> allDescendants;
    robotNode->collectAllRobotNodes(allDescendants);

    for (std::vector<VirtualRobot::RobotNodePtr>::const_iterator i = allDescendants.begin(); i != allDescendants.end(); ++i)
    {
        // Skip current robot node
        if (*i == robotNode)
            continue;

        if (std::find(actuatedJoints.begin(), actuatedJoints.end(), (*i)->getName()) != actuatedJoints.end())
        {
            /* MMM_INFO << "Cannot join " << robotNode->getName() << ": Contains joint "
                     << *(std::find(actuatedJoints.begin(), actuatedJoints.end(), (*i)->getName())) << "." << std::endl; */

            // Actuated joint in subtree -> recursive descent
            std::vector<VirtualRobot::SceneObjectPtr> children = robotNode->getChildren();
            for (std::vector<VirtualRobot::SceneObjectPtr>::const_iterator j = children.begin(); j != children.end(); ++j)
            {
                VirtualRobot::RobotNodePtr childRobotNode = std::dynamic_pointer_cast<VirtualRobot::RobotNode>(*j);
                if (childRobotNode)
                {
                    std::vector<std::string> s = getUnitableSubnodes(childRobotNode, actuatedJoints);
                    unitableSubnodes.insert(unitableSubnodes.end(), s.begin(), s.end());
                }
                /* else
                    MMM_INFO << "Ignoring " << (*j)->getName() << std::endl; */
            }

            return unitableSubnodes;
        }
    }

    // No actuated joint in subtree
    if (!robotNode->getChildren().empty())
        unitableSubnodes.push_back(robotNode->getName());

    return unitableSubnodes;
}

}
