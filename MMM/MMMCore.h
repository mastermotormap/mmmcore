/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Nikolaus Vahrenkamp
* @copyright  2013 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/
#ifndef _MMM_MMMCore_h_
#define _MMM_MMMCore_h_


#ifdef WIN32
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

// needed to have M_PI etc defined
#if !defined(_USE_MATH_DEFINES)
#define _USE_MATH_DEFINES
#endif

#pragma warning(disable:4996) 

#endif

#include <memory>
#include <iostream>
#include <sstream>

#include "MMMImportExport.h"
#include "Exceptions.h"

#include <SimoxUtility/xml/rapidxml/RapidXMLForwardDecl.h>
#include <VirtualRobot/VirtualRobot.h>

/*!
	\brief The main MMM namespace.
*/
namespace MMM
{
    class MarkerData;
    class ProcessedModelWrapper;
    class ModelProcessor;
    class ModelReaderXML;
    class LegacyMotionConverter;
    class Motion;
    class MotionRecording;
    class PrefixMarkerData;
    class ProcessedModelWrapper;
    class Sensor;
    class SensorFactory;
    class SensorMeasurement;
    class MotionSegment;
    class MotionSegmentation;
    class MotionRecordingSegment;
    class AbstractMotionSegment;
    class MotionAnnotation;

    typedef std::shared_ptr<MarkerData> MarkerDataPtr;
    typedef std::shared_ptr<ProcessedModelWrapper> ProcessedModelWrapperPtr;
    typedef std::shared_ptr<ModelProcessor> ModelProcessorPtr;
    typedef std::shared_ptr<ModelReaderXML> ModelReaderXMLPtr;
    typedef std::shared_ptr<LegacyMotionConverter> LegacyMotionConverterPtr;
    typedef std::shared_ptr<Motion> MotionPtr;
    typedef std::shared_ptr<MotionRecording> MotionRecordingPtr;
    typedef std::weak_ptr<MotionRecording> MotionRecordingWeakPtr;
    typedef std::shared_ptr<PrefixMarkerData> PrefixMarkerDataPtr;
    typedef std::shared_ptr<ProcessedModelWrapper> ProcessedModelWrapperPtr;
    typedef std::shared_ptr<Sensor> SensorPtr;
    typedef std::vector<SensorPtr> SensorList;
    typedef std::shared_ptr<SensorMeasurement> SensorMeasurementPtr;
    typedef std::shared_ptr<MotionSegment> MotionSegmentPtr;
    typedef std::vector<MotionSegmentPtr> MotionSegmentList;
    typedef std::shared_ptr<MotionSegmentation> MotionSegmentationPtr;
    typedef std::weak_ptr<MotionSegmentation> MotionSegmentationWeakPtr;
    typedef std::vector<MotionSegmentationPtr> MotionSegmentationList;
    typedef std::shared_ptr<MotionRecordingSegment> MotionRecordingSegmentPtr;
    typedef std::shared_ptr<AbstractMotionSegment> AbstractMotionSegmentPtr;
    typedef std::weak_ptr<AbstractMotionSegment> AbstractMotionSegmentWeakPtr;
    typedef std::shared_ptr<MotionAnnotation> MotionAnnotationPtr;
    typedef std::vector<MotionAnnotationPtr> MotionAnnotationList;

    typedef VirtualRobot::RobotPtr ModelPtr;

    // LEGACY
    class MarkerInfo;
    class LegacyMotion;
    class MotionFrame;

    typedef std::shared_ptr<MarkerInfo> MarkerInfoPtr;
    typedef std::shared_ptr<LegacyMotion> LegacyMotionPtr;
    typedef std::vector<LegacyMotionPtr> LegacyMotionList;
    typedef std::shared_ptr<MotionFrame> MotionFramePtr;

#define MMM_INFO std::cout <<__FILE__ << ":" << __LINE__ << ": "
#define MMM_WARNING std::cerr <<__FILE__ << ":" << __LINE__ << " -Warning- "
#define MMM_ERROR std::cerr <<__FILE__ << ":" << __LINE__ << " - ERROR - "

} // namespace

#endif // _MMM_MMMCORE_h_
