/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_EXCEPTIONS_H_
#define __MMM_EXCEPTIONS_H_

#include "MMMImportExport.h"
#include <exception>
#include <string>

namespace MMM
{
namespace Exception
{

class MMM_IMPORT_EXPORT MMMException : public std::exception
{
public:
    MMMException(const std::string &msg = "MMMCoreException thrown") : err_msg(msg)  {}

    ~MMMException() throw() {}

    const char *what() const throw() {
        return this->err_msg.c_str();
    }

protected:
    std::string err_msg;
};

class MMM_IMPORT_EXPORT MMMFormatException : public MMMException
{
public:
    MMMFormatException(const std::string &msg = "MMMFormatException thrown") : MMMException(msg)  {}

    ~MMMFormatException() throw() {}
};

class MMM_IMPORT_EXPORT MMMFormatVersionException : public MMMFormatException
{
public:
    MMMFormatVersionException(const std::string &version, const std::string &msg = "MMMVersionException thrown")
        : MMMFormatException(msg), version(version)  {}

    ~MMMFormatVersionException() throw() {}

    std::string getActualVersion() {
        return version;
    }

protected:
    std::string version;
};

class MMM_IMPORT_EXPORT MMMSensorFileException : public MMMFormatException
{
public:
    MMMSensorFileException(const std::string &path, const std::string &msg = "MMMSensorFileException thrown")
        : MMMFormatException(msg), path(path)  {}

    ~MMMSensorFileException() throw() {}

    std::string getFilePath() {
        return path;
    }

protected:
    std::string path;
};

class NotImplementedException : public MMMException
{
public:
    NotImplementedException() : MMMException("MMM function not yet implemented!")  {}

    ~NotImplementedException() throw() {}
};


}
}

#endif // __MMM_EXCEPTIONS_H_
