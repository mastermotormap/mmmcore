/** 
\page mmmplot MMMPlot

This user interface can be used to plot and compare sensor data from different or the same motion.

Plots of varied sensors are created using a specific plugin for each sensor.

\image html mmmplot.png "MMMPlot Graphical User Interface"

\section mmmplot_functionality Functionality overview

Functionality marked with a '*' can only be used in combination with the MMMViewer and not the \ref mmmplot_cmd "standalone version"

\li Opening multiple motions in the mmm2.0 format
\li *Importing motions (e.g. c3d, legacy mmm)
\li Plotting sensor data
\li Renaming title, graphs, ... by double click on the labels (Plots will be renamed when adding another plot)
\li *Opening the motion of a selected graph in the MMMViewer by selecting the motion and rightclick on the graph
\li *Opening all plotted motions in the MMMViewer by rightclick on the graph
\li *Jump to a specific timestep in the MMMViewer by rightclick on the graph or moving the slider after opening the motion(s)
\li Removing plot by selecting and then rightclick on the graph
\li Shift all motion by a delta to align them (either by using the spin box on the left or holding left mouse button while moving a plot)
\li Change the resolution of the plot by dragging or scrolling (selecting an axis beforehand only changes resolution in one direction)
\li Reposition the legend by rightclick on the legend
\li Saving plot as png-image

*/
