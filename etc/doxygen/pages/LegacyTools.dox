/** 
\page legacytools [Legacy] %MMM Tools

Overview of available tools which can be used with \ref legacydataformat.
These tool will be compiled by activating the option <i>MMM_ENABLE_LEGACY_MOTION_TOOLS</i> in the cmake build process of MMMTools.\n

See also \ref legacyconverterscripts for automated scripts to convert legacy motions.

\section cutmmm cutmmm

Takes a legacy mmm dataformat file and cuts each motions into the same time segment by given minimum and maximum frame numbers.


\section diffmmm diffmmm

Compares two motions from different legacy mmm dataformat files.


\section extractmmm extractmmm

Extract a motion from a legacy mmm dataformat file and saves this motion in his own file.


\section joinmotions joinMotions

Concatenates two motions from different legacy mmm dataformat files by attaching second motion to the first motion and saves the result in an own motion file.


\section mergemmm mergemmm

Joins all motions from different legacy mmm dataformat files into one file and expand every motion to the maximum frame number by copying the last motion frames.


\section mmmsegmenter MMMSegmenter

Creates a Gnuplot from a legacy mmm dataformat file showing the segmentation.


\section mmmdynamicscalculator MMMDynamicsCalculator

The MMMDynamicsCalculator takes a legacy mmm dataformat file containing only one motion and calculates the motion's center of mass and angular momentum.\n
These are added as extended Datatype, see \ref legacyextensions.


\section xmlmotioncompleter XMLMotionCompleter

Smoothes model pose/rotation of all motions from a legacy mmm dataformat file and then calculates the joint velocities and accelerations.


\section legacymmmviewer MMMLegacyMotionViewer

The MMMLegacyMotionViewer can be used to visualize \ref models, Legacy Motions, Marker Trajectories and Robots.

The following commandline options are available:

- &ndash;&nbsp;&ndash;&nbsp;motion <legacymotiondata.xml> <br>
The motion data in Legacy %MMM specification. The file is additionally parsed for model files and modelprocessor configurations which are used to specify the model processor in order to setup the model.

\image html MMMLegacyMotionViewer.png "The MMMLegacyMotionViewer window" <!-- TODO Bild austauschen -->


\section legacy_mmmconverter The MMMConverter

The MMMConverter can be used to apply converters which are derived from MMM::Converter in order to convert legacy motions from one model to another. 
Both conversions are supported (human motion -> Legacy %MMM and  Legacy %MMM -> robot).
By default (when no command line options are given) an exemplary conversion of a c3d marekr motion to the Winter %MMM model is performed.

\subsection legacy_mmmconverter_commandline Commandline Arguments

The following commandline options are available:

 - &ndash;&nbsp;&ndash;&nbsp;sourceModel <inModel.xml> <br>
   The input model file that should be used for conversion. (Ignored when Vicon data is converted in Vicon->MMM mode)
   
 - &ndash;&nbsp;&ndash;&nbsp;sourceModelProcessor <processorName> <br>
   Optional tag that specifies the ModelProcessor that converts the input model before applying the converter. (Ignored when vicon data is converted in Vicon->MMM mode)
   
 - &ndash;&nbsp;&ndash;&nbsp;sourceModelProcessorConfigFile <config.xml> <br>
   Optional config file for the input model porcessor. (Ignored when vicon data is converted in Vicon->MMM mode)
   
 - &ndash;&nbsp;&ndash;&nbsp;targetModel <outModel.xml> <br>
   The output model file that should be used for conversion. (Could be an MMM model for Vicon->MMM converters or a robot model for MMM->Robot converters)
   
 - &ndash;&nbsp;&ndash;&nbsp;targetModelProcessor <processorName> <br>
   Optional tag that specifies the ModelProcessor that converts the output model before applying the converter. 
   
 - &ndash;&nbsp;&ndash;&nbsp;targetModelProcessorConfigFile <config.xml> <br>
   Optional config file for the output model porcessor.
   
 - &ndash;&nbsp;&ndash;&nbsp;inputFile <datafile.c3d> <br>
   This tag specifies the input motion file (C3D file for Vicon->MMM or MMM file for MMM->Robot).
   
 - &ndash;&nbsp;&ndash;&nbsp;outputFile <outputfile.xml> <br>
   The output file name. The resulting motion is stored here.
   
 - &ndash;&nbsp;&ndash;&nbsp;libPath <path/to/converter/libraries,/path/to/other/converter/libs/> <br>
   Here, converter libraries are searched. Comma separated list of directories. (not necessary if a default converter should be used)
   
 - &ndash;&nbsp;&ndash;&nbsp;converter <ConverterName> <br>
   Specifies which converter is used, e.g. NloptConverter. The string is compared with all ConverterFactories that are present in the defined library directories.
   
 - &ndash;&nbsp;&ndash;&nbsp;converterConfigFile <ConverterConfig.xml> <br>
   The configuration file of the converter.
   
 - &ndash;&nbsp;&ndash;&nbsp;markerPrefix <String> <br>
   Optional prefix for marker names (for multi-subject recordings, usually not necessary).
   
 - &ndash;&nbsp;&ndash;&nbsp;outputMotionName <String> <br>
   Optional name for the motion in the resulting output file (usually not necessary).

\subsection  mmmconvertergui MMMConverterGUI

The MMMConverterGui offers a graphical user interface for marker-based Converters. These class of converters are usually used for an initial mapping between human motion capture data 
(e.g. vicon marker based) and an MMM reference model (e..g Winter).  
The converter GUI offers the possibility to interact with the converter library (see \ref mmmviconconverter). For configuration options see above (MMMConverter).

The GUI offers the possibility to visualize the model, input and output markers and to inspect the progress of the convertion process. The converter can be intiialized and 
either a stepwise conversion (for debugging purposes) or a complete transformation of the motion can be performed. Furthermore, the results can be inspected, stored to the MMM::Motion format 
and the resulting motion can be saved as a stream of PNG images for further processing.

  \image html MMMConverterGUI_small.png "The MMMConvertrGUI window"
 
\subsection mmmviconconverter The ConverterVicon2MMM Converter

This library provides an exemplary implementaion of an MMM::Converter that maps vicon marker data to a \ref models "reference %MMM Model".
It can be used within the MMMConverter framework. The usage is exemplarily explained below:
\code
	// get a converter factory from name
	std::string converterName("ConverterVicon2MMM");
	MMM::ConverterFactoryPtr converterFactory = MMM::ConverterFactory::fromName(converterName, NULL);
	if (!converterFactory)
	{
		cout << "Could not create converter factory of type " << converterName << endl;
		cout << "Setting up Converter... Failed..." << endl;
		return;
	}
	
	// Create a converter instance
	MMM::ConverterPtr c = converterFactory->createConverter();
	if (!c)
	{
		MMM_ERROR << "Could not build converter..." << endl;
		return;
	}
	
	// We know it is a marker based converter
	converter = boost::dynamic_pointer_cast<MMM::MarkerBasedConverter>(c);
	if (!converter)
	{
		MMM_ERROR << "Could not cast converter to MarkerBasedConverter..." << endl;
		return;
	}

	// load configuration file
	if (!converter->setupFile(converterFile))
	{
		cout << "Error while configuring converter '" << converterName << "' from file " << converterFile << endl;
		cout << "Setting up Converter... Failed..." << endl;
	}

	// get the input-to-output marker mapping
	std::map<std::string, std::string> markerMapping = converter->getMarkerMapping();
\endcode

An exemplary ConverterVicon2%MMM config file may look like this
\code
<?xml version='1.0' encoding='UTF-8'?>
<ConverterConfig type='ConverterVicon2MMM'>
  <Model name='Winter'>

	<MarkerMapping>    
		<Mapping c3d="C7"   mmm="C7_EMPTY"/>  
		<Mapping c3d="L3"   mmm="L3_EMPTY"/>  
		<Mapping c3d="CLAV" mmm="CLAV_EMPTY"/>
		<Mapping c3d="RBAK" mmm="RBAK_EMPTY"/>
		<Mapping c3d="T10"  mmm="T10_EMPTY"/>
		<Mapping c3d="STRN" mmm="STRN_EMPTY"/>
		<Mapping c3d="RBHD" mmm="RBHD_EMPTY"/>
		<Mapping c3d="LBHD" mmm="LBHD_EMPTY"/>
		<Mapping c3d="RASI" mmm="RASI_EMPTY"/>
		<Mapping c3d="LAEL" mmm="LAEL_EMPTY"/>
		<Mapping c3d="LUPA" mmm="LUPA_EMPTY"/>
		<Mapping c3d="LASI" mmm="LASI_EMPTY"/>
		<Mapping c3d="LHIP" mmm="LHIP_EMPTY"/>
		<Mapping c3d="LPSI" mmm="LPSI_EMPTY"/> 
		<Mapping c3d="LKNE" mmm="LKNE_EMPTY"/>
		<Mapping c3d="LSHO" mmm="LSHO_EMPTY"/>
		<Mapping c3d="RPSI" mmm="RPSI_EMPTY"/> 
		<Mapping c3d="LFRA" mmm="LFRA_EMPTY"/>
		<Mapping c3d="LWPS" mmm="LWPS_EMPTY"/>
		<Mapping c3d="LWTS" mmm="LWTS_EMPTY"/>
		<Mapping c3d="LANK" mmm="LANK_EMPTY"/>
		<Mapping c3d="LHEE" mmm="LHEE_EMPTY"/>
		<Mapping c3d="LMT1" mmm="LMT1_EMPTY"/>
		<Mapping c3d="LMT5" mmm="LMT5_EMPTY"/>
		<Mapping c3d="LTOE" mmm="LTOE_EMPTY"/>
		<Mapping c3d="RAEL" mmm="RAEL_EMPTY"/>
		<Mapping c3d="RUPA" mmm="RUPA_EMPTY"/>   
		<Mapping c3d="RSHO" mmm="RSHO_EMPTY"/>
		<Mapping c3d="RHIP" mmm="RHIP_EMPTY"/>
		<Mapping c3d="RKNE" mmm="RKNE_EMPTY"/>           
		<Mapping c3d="RWPS" mmm="RWPS_EMPTY"/>
		<Mapping c3d="RWTS" mmm="RWTS_EMPTY"/>         
		<Mapping c3d="RANK" mmm="RANK_EMPTY"/>
		<Mapping c3d="RHEE" mmm="RHEE_EMPTY"/>
		<Mapping c3d="RMT1" mmm="RMT1_EMPTY"/>
		<Mapping c3d="RMT5" mmm="RMT5_EMPTY"/>
		<Mapping c3d="RTOE" mmm="RTOE_EMPTY"/>                            		
	</MarkerMapping> 	
	
	<JointSet>

		<!-- Torso -->
		<Joint name='BTx_joint'/>
		<Joint name='BTy_joint'/>
		<Joint name='BTz_joint'/>
		
		<!-- Left Foot -->
		<Joint name='LAx_joint'/>
		<Joint name='LAy_joint'/>
		<Joint name='LAz_joint'/>
		
		<!-- Left Elbow -->
		<Joint name='LEx_joint'/>
		<Joint name='LEy_joint'/>
		
		<!-- Left hip -->
		<Joint name='LHx_joint'/>
		<Joint name='LHy_joint'/>
		<Joint name='LHz_joint'/>
		
		<!-- Left Knee -->
		<Joint name='LKx_joint'/>
		
		<!-- Left Shoulder inner -->
		<Joint name='LSCx_joint'/>
		<Joint name='LSCy_joint'/>
		<Joint name='LSCz_joint'/>
		
		<!-- Left Shoulder outer -->
		<Joint name='LSx_joint'/>
		<Joint name='LSy_joint'/>
		<Joint name='LSz_joint'/>
		
		<!-- Left Wrist -->
		<Joint name='LWx_joint'/>
		<Joint name='LWz_joint'/>
		
		<!-- Right Foot -->
		<Joint name='RAx_joint'/>
		<Joint name='RAy_joint'/>
		<Joint name='RAz_joint'/>
		
		<!-- Right Elbow -->
		<Joint name='REx_joint'/>
		<Joint name='REy_joint'/>
		
		<!-- Right Hip -->
		<Joint name='RHx_joint'/>
		<Joint name='RHy_joint'/>
		<Joint name='RHz_joint'/>
		
		<!-- Right Knee -->
		<Joint name='RKx_joint'/>
		
		<!-- Right Shoulder inner -->
		<Joint name='RSCx_joint'/>
		<Joint name='RSCy_joint'/>
		<Joint name='RSCz_joint'/>
		
		<!-- Right Shoulder outer -->		
		<Joint name='RSx_joint'/>
		<Joint name='RSy_joint'/>
		<Joint name='RSz_joint'/>
		
		<!-- Right Wrist -->
		<Joint name='RWx_joint'/>
		<Joint name='RWz_joint'/>
	</JointSet>	
  </Model>
  <IK>
    <InitialIKParams ikSteps='50' checkImprovement='false' minChange='0' stepSize='0.8'/>
    <StepIKParams ikSteps='10' checkImprovement='true' minChange='0.01' stepSize='0.8'/>
  </IK>
</ConverterConfig> 		
\endcode


The converter can be used as follows:
\code

// 1. load an (output) model
MMM::ModelReaderXMLPtr r(new MMM::ModelReaderXML());
MMM::ModelPtr mmmOrigModel = r->loadModel(modelFile);

// 2. process model
MMM::ModelPtr mmmModel = modelProcessor->convertModel(mmmOrigModel);

// 3. load marker based vicon motion
MMM::LegacyMotionReaderC3DPtr c(new MMM::LegacyMotionReaderC3D());
MMM::MarkerMotionPtr markerMotion = c->loadC3D(viconFile);

//  4. setup with 
// empty input model (vicon marker data does not rely on an input model)
// the marker based motion
// and the ouput model
converter->setup(MMM::ModelPtr(), markerMotion, mmmModel);

// 5. intialize converter
MMM::AbstractMotionPtr m = converter->initializeStepwiseConvertion();
MMM::LegacyMotionPtr resultModelMotion = boost::dynamic_pointer_cast<MMM::LegacyMotion>(m);

// 6. convert one step
bool res = converter->convertMotionStep(resultModelMotion);

// 5/6. or convert complete motion
MMM::AbstractMotionPtr m = converter->convertMotion();
MMM::LegacyMotionPtr resultModelMotion = boost::dynamic_pointer_cast<MMM::LegacyMotion>(m);

// 7. store result to file
std::filename("outputmotion.xml");
std::string content = resultModelMotion->toXML();
MMM::XML::saveXML(filename, content);
\endcode

*/
