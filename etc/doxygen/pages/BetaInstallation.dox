/**
\page mmm2-2-installation MMM 2.2 Installation

Work in progress MMM2.2 framework.

The %MMM Libraries officially supports Ubuntu 18.04

This version is currently work in progress and will be merged very soon with the current master branch. The MMM2.2 framework offers among other access to new segmentation/annotation data format within the MMM format as well as tools for manual and automatic segmentation/annotation. Further it also usage of the new project MMMVision which enables to store, visualize and handle additional sensor data of camera-based sensors. If any errors occur, please contact <a href="mailto:andre.meixner@kit.edu">andre.meixner@kit.edu</a>.

If you encounter any problems with the compilation or installation process, please contact <a href="mailto:andre.meixner@kit.edu">andre.meixner@kit.edu</a> or <a href="mailto:franziska.krebs@kit.edu">franziska.krebs@kit.edu</a>. 
In the following, an exemplary installation setup is given that refers to an Ubuntu 18.04 system. All dependencies listed must be adapted for any other distribution.

\section installation-prerequisites Prerequisites (Ubuntu)
First of all be sure git and cmake is installed on your system:

\li Git to fetch the project <a href="http://git-scm.com/">Link</a>  and \li <a href="http://cmake.org">CMake 3.10.2</a> for building the project:

\verbatim
    sudo apt-get install git gitk cmake
\endverbatim

\subsection installation-optional Optional packages

\li GUI Packages for git and CMake:

\verbatim
    sudo apt-get install git-gui cmake-qt-gui
\endverbatim

\li <a href="http://www.stack.nl/~dimitri/doxygen/">Doxygen</a> for generating the documentation:

\verbatim
    sudo apt-get install doxygen doxygen-gui
\endverbatim


\section installation-simox Simox

Simox is required to build MMMCore and MMMTools for model loading, coordinate transformations and collision checking.

The sourcecode of Simox can be fetched by issuing the following commands (make sure to check out the branch mmm_update):

\verbatim
    cd ~
    git clone -b mmm_update https://gitlab.com/simox/simox.git Simox
\endverbatim

This will create a directory named Simox in the home directory of the current user
containing everything which is necessary to get started with MMMCore.

Detailed up-to-date installations steps are descriped <a href="https://gitlab.com/Simox/simox/-/wikis/Installation-Source-Ubuntu">here</a>. In the following you find a copy of the installation instructions which may or may not be up-to-date:

\verbatim
    sudo apt-get install -y curl apt-file
    sudo curl https://packages.humanoids.kit.edu/h2t-key.pub | sudo apt-key add - 
    echo -e "deb http://packages.humanoids.kit.edu/bionic/main bionic main\ndeb http://packages.humanoids.kit.edu/bionic/testing bionic testing" | sudo tee /etc/apt/sources.list.d/armarx.list 
    sudo apt-file update
    sudo apt install -y git g++-8 libboost-all-dev cmake libeigen3-dev libnlopt-dev freeglut3-dev qtbase5-dev libqhull-dev  liburdfdom-dev libassimp-dev libtiff-dev
    sudo apt-get install h2t-libbullet h2t-libsoqt5-dev h2t-libsimage-dev h2t-libcoin80-dev
\endverbatim

After all dependencies are installed the compilation of Simox can be performed by executing the following commands:

\verbatim
    cd ~/Simox
    mkdir build
    cd build
    CXX=g++-8 cmake -DCMAKE_BUILD_TYPE=Release ..
    make
\endverbatim

\section installation-MMMCore MMMCore

The MMMCore Library depends on two libraries:

   - <a href="http://boost.org">Boost (>=1.65)</a> for shared pointers, mutexes, testing, ...
   - <a href="http://eigen.tuxfamily.org/">Eigen 3</a> For algebra

\verbatim
    sudo apt-get install libeigen3-dev libboost-all-dev
\endverbatim


\subsection installation-MMMCore-obtaining Getting MMMCore
The sourcecode of MMMCore can be fetched by issuing the following commands (make sure to check out the branch mmm2.2):

\verbatim
    cd ~
    git clone -b mmm2.2 https://gitlab.com/mastermotormap/mmmcore.git MMMCore
\endverbatim

This will create a directory named MMMCore in the home directory of the current user
containing everything which is necessary to get started with MMMCore.


\subsection installation-MMMCore-compilation Compile MMMCore
After all dependencies are installed the compilation of MMMCore can be performed by
executing the following commands:

\verbatim
    cd ~/MMMCore
    mkdir build
    cd build
    CXX=g++-8 cmake -DCMAKE_BUILD_TYPE=Release ..
    make
\endverbatim


\section installation-MMMTools MMMTools

The MMMTools Library depends on three libraries:

   - \ref installation-MMMCore for data structures
   - \ref installation-simox for model loading, coordinate transformations and collision checking
   - <a href="http://qt-project.org/downloads">qt5</a> for GUI

\verbatim
    sudo apt-get install libqt5-*
\endverbatim

\subsection installing-NLopt-optional Installing NLopt (optional)

If you want to use the NLopt-based motion converter (very likely, if you want to convert motions), you need to install the NLopt development package:

\verbatim
    sudo apt-get install libnlopt-dev
\endverbatim

\subsection installation-MMMTools-obtaining Getting MMMTools
The sourcecode of MMMTools can be fetched by issuing the following commands (make sure to check out the branch mmm2.2):

\verbatim
    cd ~
    git clone -b mmm2.2 https://gitlab.com/mastermotormap/mmmtools.git MMMTools
\endverbatim

This will create a directory named MMMTools in the home directory of the current user
containing everything which is necessary to get started with MMMTools.

\subsection installation-MMMTools-compilation Compiling MMMTools
After all dependencies are installed the compilation of MMMTools can be performed by
executing the following commands:

\verbatim
    cd ~/MMMTools
    mkdir build
    cd build
    CXX=g++-8 cmake -DCMAKE_BUILD_TYPE=Release ..
    make
\endverbatim



\section installation-MMMVision (Optional) MMMVision


Extension of the MMM Framework to store, visualize and handle additional sensor data of camera-based sensors. This framework is very optional and not required.
Make sure you have OpenCV (for RGB videos) and libPCL (for RGBD videos) installed.

\subsection installing-AzureKinectDK-optional Installing Azure Kinect SDK (optional)

If you want to use recorded data from the Azure Kinect from our motion database, you need to install the <a href="https://docs.microsoft.com/en-us/azure/kinect-dk/sensor-sdk-download">Azure Kinect SDK development package</a>:

\subsection installation-MMMVision-obtaining Getting MMMVision
The sourcecode of MMMVision can be fetched by issuing the following commands:

\verbatim
    cd ~
    git clone https://gitlab.com/mastermotormap/mmmvision.git MMMVision
\endverbatim

This will create a directory named MMMVision in the home directory of the current user
containing everything which is necessary to get started with MMMVision.

\subsection installation-MMMVision-compilation Compiling MMMVision
After all dependencies are installed the compilation of MMMVision can be performed by
executing the following commands:

\verbatim
    cd ~/MMMVision
    mkdir build
    cd build
    CXX=g++-8 cmake -DCMAKE_BUILD_TYPE=Release ..
    make
\endverbatim

\subsection MMMViewer

The MMMViewer is the graphical user interface for the MMM framework and part of MMMTools. To use methods provided in MMMVision for sensor I/O, sensor visualisation, video playing and more, the available plugins have to be added to the MMMViewer once. Currently, in the graphical user interface. This process will be simplified in future updates!

Run the MMMViewer 
\verbatim
./mmmtools/build/bin/MMMViewer

Go to Extra -> Plugins.. -> MotionHandler -> Add plugin directory -> Select the directory ../mmmvision/build/lib/motionHandlerPluginLibs -> Open

Go to Extra -> Plugins.. -> Sensor -> Add plugin directory -> Select the directory ../mmmvision/build/lib/sensorVisualisationPluginLibs -> Open

Go to Extra -> Plugins.. -> SensorVisualisation -> Add plugin directory -> Select the directory ../mmmvision/build/lib/sensorPluginLibs -> Open

Restart the MMMViewer
\endverbatim



*/
